package com.springapi.enums;

import org.springframework.core.env.Environment;

/**
 *
 * @author Jyoti_patel
 */
public class MASConstant {
//    public static final String DATABASEURL= "jdbc:mysql://localhost:3306/attendance";
//    public static final String USERNAME= "root";
//    public static final String PASSWORD="tiger@123";

    // public  String DBURL = env.getProperty("spring.datasource.url");
    public static final String DATABASEURL = "jdbc:mysql://35.232.5.251:3306/dheemati_attendance";
    public static final String USERNAME = "root";
    public static final String PASSWORD = "root$123";
    public static final String PROG = "Program";
    public static final String DEPT = "Department";
    public static final int SERVER_PORT = 8085;
//    public static final int SERVER_PORT = 8089;
    public static final int LATE_IN_ALLOW = 10; 
    public static final int EARLY_OUT_ALLOW = 10; 

}
