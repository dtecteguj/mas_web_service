/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.springapi.enums;

import com.springapi.model.Faculty;

/**
 *
 * @author shahb
 */
public class CommonFunction {

    public static String setFormalName(Faculty f) {
        if (f.getfName() != null) {
            if (f.getmName() == null) {
                f.setName(f.getfName() + " " + f.getlName());
            } else {
                if (f.getmName().trim().length() > 0) {
                    f.setName(f.getfName() + " " + f.getmName().substring(0, 1) + " " + f.getlName());
                } else {
                    f.setName(f.getfName() + " " + f.getlName());
                }
            }
        } else {
            String args[] = f.getName().trim().split(" ");
            if (args.length > 3) {
                if (args[2].length() > 0) {
                    args[2] = args[2].substring(0, 1);
                    String Name = "";
                    for (String tmp : args) {
                        Name += tmp;
                    }
                }
            } else if (args.length == 3) {
                if (args[1].length() > 0) {
                    f.setName(args[0] + " " + args[1].substring(0, 1) + " " + args[2]);
                }
            }
        }
        return f.getName();
    }
}
