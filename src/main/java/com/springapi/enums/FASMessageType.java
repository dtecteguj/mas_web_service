/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.springapi.enums;

/**
 *
 * @author Jyoti Patel
 */
public enum FASMessageType {
    // Not Found 
    ENTITY_NOT_FOUND(301, "Entity not found for given details"),
    DUPLICATE_DATA(302, "Multiple records found instead of single value"),
    QR_CODE_INVALID(303, "Invalid QrCode");

    //private final int id;
    private final int code;
    private final String msg;

    FASMessageType(int code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    public int getCode() {
        return this.code;
    }

    public String getMsg() {
        return this.msg;
    }
}
