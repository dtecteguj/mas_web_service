/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.springapi.enums;

/**
 *
 * @author shahb
 */
public enum FacultyAttendanceStatus {
    REGULAR_IN(1),
    LATE_ENTRY(2),
    ABSENTEE(3),
    EARLY_LEAVING(5),
    REGULAR_OUT(6);
    
    private final int id;

    FacultyAttendanceStatus(int id) {
        this.id = id;
    }

    public int getId() {
        return this.id;
    }
}
