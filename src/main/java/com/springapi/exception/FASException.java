/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.springapi.exception;

import com.springapi.dto.FASMessage;
import com.springapi.enums.FASMessageType;

/**
 *
 * @author Jyoti Patel
 */
public class FASException extends RuntimeException {
       
    private final transient FASMessage message;
    
     public FASException(FASMessageType messageType) {
        message = new FASMessage(messageType);
    }

    public FASException(FASMessageType messageType, Object... args) {
        message = new FASMessage(messageType, args);
    }

    public FASMessage getFASMessage() {
        return message;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("[Message Code=");
        sb.append(message.getMessageType().getCode());
        if (message.getArgs() != null && message.getArgs().length > 0) {
            sb.append(", args=");
            for (Object arg : message.getArgs()) {
                if (arg != null)
                    sb.append("{" + arg.toString() + "}");
            }
        }
        sb.append("]");
        return sb.toString();
    }

//    @Override
//    public String toString() {
//        return super.toString(); //To change body of generated methods, choose Tools | Templates.
//    }

    @Override
    public String getMessage() {
        return super.getMessage(); //To change body of generated methods, choose Tools | Templates.
    }

}
