/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.springapi;

import com.twilio.Twilio;
import java.net.URI;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.support.SpringBootServletInitializer;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.http.HttpStatus;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import com.twilio.http.TwilioRestClient;
import com.twilio.rest.api.v2010.account.Call;
import com.twilio.twiml.TwiMLException;
import com.twilio.twiml.VoiceResponse;
import com.twilio.twiml.voice.Record;
import com.twilio.twiml.voice.Say;
import com.twilio.type.PhoneNumber;
import java.net.URISyntaxException;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author shahb
 */
@SpringBootApplication
@EnableJpaAuditing
@EnableScheduling
@EnableSwagger2
@RestController
public class FacultyApplication extends SpringBootServletInitializer {

    public static void main(String[] args) {
        SpringApplication.run(FacultyApplication.class, args);

    }

//    @Bean
//    public Docket productApi() {
//        return new Docket(DocumentationType.SWAGGER_2).select()
//                .apis(RequestHandlerSelectors.basePackage("com.springapi.controller")).build();
//    }
    private final static String ACCOUNT_SID = "AC3872a2ad159d714bc1d99983fa945369";
    private final static String AUTH_ID = "58cdc2f418e4453280f951b4cbc3d52d";

    static {
        Twilio.init(ACCOUNT_SID, AUTH_ID);
    }

    @RequestMapping(value = "/voice-note", method = RequestMethod.POST, produces = MediaType.APPLICATION_XML_VALUE)
    public ResponseEntity<String> getVoiceNote() throws TwiMLException {
        String otp = "12345";
        Say say = new Say.Builder("Your OTP is: " + otp).voice(Say.Voice.WOMAN).build();
        Record record = new Record.Builder().build();
        VoiceResponse response = new VoiceResponse.Builder().say(say).record(record).build();
        return new ResponseEntity<>(response.toXml(), HttpStatus.OK);
    }

    @RequestMapping(value = "/calls", method = RequestMethod.POST)
    public ResponseEntity<Object> makeCall() throws URISyntaxException {
    	String otp = "12345";
        TwilioRestClient client = new TwilioRestClient.Builder(ACCOUNT_SID, AUTH_ID).build();
        PhoneNumber to_number = new PhoneNumber("+917600345578");
        PhoneNumber from_number = new PhoneNumber("+12054420224");
        URI uri = URI.create("http://192.168.1.242:8080/voice-note?otp=12345");
        // Call call = Call.creator(to_number, from_number, uri).create(client);
        Record record = new Record.Builder().build();
        Say say = new Say.Builder("Your OTP is: " + otp).voice(Say.Voice.WOMAN).build();

        VoiceResponse response = new VoiceResponse.Builder().say(say).record(record).build();
        Call call = Call.creator(to_number, from_number, uri).create();
        return new ResponseEntity<Object>("Call has initiated successfully and call SID is:" + call.getSid(), HttpStatus.OK);
    }

}
