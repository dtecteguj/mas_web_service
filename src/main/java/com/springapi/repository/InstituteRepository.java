package com.springapi.repository;

import com.springapi.model.InstituteMaster;
import java.util.List;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Jyoti Patel
 */
@Repository
public interface InstituteRepository extends JpaRepository<InstituteMaster, Long> {

   Optional<InstituteMaster> findById(Long uId);

   List<InstituteMaster> findByInstituteTypeId(Long instituteTypeId);
   
   List<InstituteMaster> findByInstituteTypeIdOrderByName(Long instituteTypeId);

}
