/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.springapi.repository;

import com.springapi.model.TrnDtlLeaveStatus;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author shahb
 */
public interface TrnDtlLeaveStatusRepository extends JpaRepository<TrnDtlLeaveStatus, Long> {

   Optional<TrnDtlLeaveStatus> findById(Long aId);
   
   Optional<TrnDtlLeaveStatus> findByAttdIdAndIsDeletedFalse(Long aId);
    
}
