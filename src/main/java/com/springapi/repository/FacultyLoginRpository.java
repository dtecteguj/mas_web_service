package com.springapi.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.springapi.model.FacultyLogin;

public interface FacultyLoginRpository extends JpaRepository<FacultyLogin, Long>{

	Optional<FacultyLogin> findById(Long id);
}
