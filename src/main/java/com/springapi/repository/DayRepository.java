package com.springapi.repository;

import com.springapi.model.DayMaster;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * @author shahb
 */
@Repository
public interface DayRepository extends JpaRepository<DayMaster, Long> {

    DayMaster findByDay(String day);
    
}
