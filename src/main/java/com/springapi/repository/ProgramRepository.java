package com.springapi.repository;

import com.springapi.model.ProgramMaster;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * @author Jyoti Patel
 */
@Repository
public interface ProgramRepository extends JpaRepository<ProgramMaster, Long> {

    Optional<ProgramMaster> findById(Long uId);

}
