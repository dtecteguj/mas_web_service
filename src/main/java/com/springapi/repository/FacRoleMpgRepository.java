
package com.springapi.repository;

import com.springapi.model.FacRoleMpg;
import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

public interface FacRoleMpgRepository extends JpaRepository<FacRoleMpg, Long> {	

	Optional<FacRoleMpg> findById(Long id);
        List<FacRoleMpg> findByIdFIdAndIsActiveTrue(Long id);
}
