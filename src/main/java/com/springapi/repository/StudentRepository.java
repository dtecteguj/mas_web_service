/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.springapi.repository;

import com.springapi.model.Student;
import java.util.List;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 *
 * @author shahb
 */
@Repository
public interface StudentRepository extends JpaRepository<Student, Long> {

    Optional<Student> findById(Long id);

    public Student findByEmailAndPassword(String email, String password);
    
   Optional<Student> findByEmailOrSEnrollemnt(String email, String enString);

    public List<Student> findAllBySem(Long semId);

}
