package com.springapi.repository;

import com.springapi.model.Faculty;
import java.util.List;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 *
 * @author shahb
 */
@Repository
public interface FacultyRepository extends JpaRepository<Faculty, Long> {

    Optional<Faculty> findById(Long Fid);

    Faculty findByEmail(String facultyEmail);

    public Faculty findByEmailAndPassword(String email, String password);

    List<Faculty> findByInstituteId(Long instituteId);

    List<Faculty> findAllByDepartmentId(Long departmentId);

    List<Faculty> findAllByInstituteIdAndIsDeletedFalse(Long instituteId);
    List<Faculty> findAllByInstituteIdAndRoleNot(Long instituteId,Long role);
    List<Faculty> findAllByInstituteIdAndDepartmentId(Long instituteId,Long departmentId);

}
