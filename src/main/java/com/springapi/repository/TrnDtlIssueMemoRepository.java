/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.springapi.repository;

import com.springapi.model.TrnDtlIssueMemo;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author shahb
 */
public interface TrnDtlIssueMemoRepository extends JpaRepository<TrnDtlIssueMemo, Long> {

   Optional<TrnDtlIssueMemo> findById(Long aId);
   List<TrnDtlIssueMemo> findByLotCreationDate(Date date);
   List<TrnDtlIssueMemo> findByFIdAndIsDeletedFalseOrderByIsSubmittedAsc(Long fId);
   List<TrnDtlIssueMemo> findByInstituteIdAndIsDeletedFalseOrderByIsEndorseAscIsSubmittedDescLotCreationDateDesc(Long iId);
   Optional<TrnDtlIssueMemo> findTopByFIdAndIsDeletedFalseOrderByIdDesc(Long fId);
   boolean existsByFIdAndIsDeletedFalse(Long fId);
   Optional<TrnDtlIssueMemo> findTopByIsDeletedFalseOrderByIdDesc();
   
   
   boolean existsByInstituteIdAndIsDeletedFalseAndIsEndorseFalse(Long iId);
    boolean existsByFIdAndIsDeletedFalseAndIsSubmittedFalse(Long fId);
}
