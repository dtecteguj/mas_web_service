package com.springapi.repository;

import com.springapi.model.TokensManage;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 *
 * @author shahb
 */

@Repository
public interface TokensManageRepository extends JpaRepository<TokensManage, Long> {

    List<TokensManage> findByType(String type);

    public TokensManage findByUId(Long uId);
    
}
