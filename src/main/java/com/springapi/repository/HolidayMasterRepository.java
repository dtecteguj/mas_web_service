/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.springapi.repository;

import com.springapi.model.HolidayMaster;
import java.util.Date;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 *
 * @author shahb
 */
@Repository
public interface HolidayMasterRepository extends JpaRepository<HolidayMaster, Long> {

    Optional<HolidayMaster> findById(Long aId);

    boolean existsByHolidayDateAndIsDeletedFalse(Date holidayDate);

    Optional<HolidayMaster> findByHolidayDateAndIsDeletedFalse (Date holidayDate);
}
