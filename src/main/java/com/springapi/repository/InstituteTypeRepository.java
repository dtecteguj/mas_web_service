package com.springapi.repository;

import com.springapi.model.InstituteMaster;
import com.springapi.model.InstituteTypeMaster;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * @author Jyoti_patel
 */
@Repository
public interface InstituteTypeRepository extends JpaRepository<InstituteTypeMaster, Long> {

    public Optional<InstituteTypeMaster> findById(Long instituteTypeId);
    
}
