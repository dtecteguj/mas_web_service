package com.springapi.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.springapi.model.TrnAttendanceLog;

public interface TrnAttendanceLogRepository  extends JpaRepository<TrnAttendanceLog, Long>{
	Optional<TrnAttendanceLog> findById(Long id);
}
