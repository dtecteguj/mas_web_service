package com.springapi.repository;

import com.springapi.model.TimeTable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * @author Jyoti Patel
 */
@Repository
public interface TimeTableRepository extends JpaRepository<TimeTable, Long> {

   TimeTable findByDayIdAndFacultyIdAndSlotId(Long dayId, Long facultyId, Long slotId);
   
   TimeTable findByDayIdAndSemIdAndSlotId(Long dayId, Long semId, Long slotId);
    
}
