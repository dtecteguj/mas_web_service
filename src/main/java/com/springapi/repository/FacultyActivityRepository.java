
package com.springapi.repository;

import com.springapi.model.FacultyActivity;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 *
 * @author shahb
 */
@Repository
public interface FacultyActivityRepository extends JpaRepository<FacultyActivity, Long> {

    public List<FacultyActivity> findByFacultyIdAndActivityDate(Long facultyId, String date);
    
}
