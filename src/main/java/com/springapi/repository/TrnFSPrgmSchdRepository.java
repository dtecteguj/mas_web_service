/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.springapi.repository;

import com.springapi.model.TrnFSPrgmSchd;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author shahb
 */
@Service
@Transactional
public interface TrnFSPrgmSchdRepository extends JpaRepository<TrnFSPrgmSchd, Long> {

   Optional<TrnFSPrgmSchd> findById(Long tId);
    
}
