/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.springapi.repository;

import com.springapi.model.FacultyAttendance;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 *
 * @author shahb
 */
@Repository
public interface FacultyAttendanceRepository extends JpaRepository<FacultyAttendance, Long> {

    Optional<FacultyAttendance> findById(Long id);

    Optional<FacultyAttendance> findByIdAndAttendanceDateAndFId(Long id, String d, Long facultyId);

    public FacultyAttendance findByAttendanceDateAndFId(String d, Long facultyId);

    public List<FacultyAttendance> findAllByAttendanceDate(String date);

    public List<FacultyAttendance> findAllByAttendanceDateAndInstituteTypeId(String date, Long typeId);

    public List<FacultyAttendance> findAllByAttendanceDateAndStatus(String date, Boolean b);

}
