package com.springapi.repository;

import com.springapi.model.StudentFeedback;
import java.util.Date;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * @author Jyoti Patel
 */
@Repository
public interface StudentFeedbackRepository extends JpaRepository<StudentFeedback, Long> {

    public List<StudentFeedback> findAllByFeedbackDateAndFacultyIdAndSlotId(String date, Long facultyId, Long slotId);
    public List<StudentFeedback> findByStudentIdAndFeedbackDate(Long studentId, String date);
    
}
