package com.springapi.repository;

import com.springapi.model.SlotMaster;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * @author Jyoti_patel
 */
@Repository
public interface SlotRepository extends JpaRepository<SlotMaster, Long> {

     List<SlotMaster> findAllByInstituteId(Long instituteId);
    
}
