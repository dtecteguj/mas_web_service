/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.springapi.repository;

import com.springapi.model.TrnTransferDtls;
import java.util.List;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author shahb
 */
public interface TrnTransferDtlsRepository extends JpaRepository<TrnTransferDtls, Long> {
    Optional<TrnTransferDtls> findById(Long tid);
    List<TrnTransferDtls> findByFId(Long fId);
    Optional<TrnTransferDtls> findByFIdAndIsCancelled(Long fId,Boolean b);
    List<TrnTransferDtls> findBySourceInstId(Long iId);
    List<TrnTransferDtls> findBySourceInstIdAndIsCancelled(Long iId,Boolean b);


}