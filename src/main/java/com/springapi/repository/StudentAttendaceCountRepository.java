package com.springapi.repository;

import com.springapi.model.Semester;
import com.springapi.model.StudentAttendaceCount;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * @author Jyoti_patel
 */
@Repository
public interface StudentAttendaceCountRepository extends JpaRepository<StudentAttendaceCount, Long> {

    public StudentAttendaceCount findBySlotIdAndFacultyIdAndAttendanceDate(Long slotId, Long facultyId, String date);
    
}
