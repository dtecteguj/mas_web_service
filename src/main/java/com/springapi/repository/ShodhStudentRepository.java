/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.springapi.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import com.springapi.model.ShodhStudent;
import java.util.Optional;

/**
 *
 * @author shahb
 */
@Repository
public interface ShodhStudentRepository extends JpaRepository<ShodhStudent, Long> {

    Optional<ShodhStudent> findByStdUniqueId(Long stdUniId);


}
