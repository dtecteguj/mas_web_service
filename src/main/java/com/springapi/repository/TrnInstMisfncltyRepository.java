/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.springapi.repository;

import com.springapi.model.TrnInstMisfnclty;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author shahb
 */
public interface TrnInstMisfncltyRepository extends JpaRepository<TrnInstMisfnclty, Long> {

    Optional<TrnInstMisfnclty> findById(Long aId);

    List<TrnInstMisfnclty> findByFromDateLessThanEqualAndToDateGreaterThanEqualAndIsDeletedFalse(Date fromDate,Date toDate);
    
    List<TrnInstMisfnclty> findByFromDateLessThanEqualAndToDateGreaterThanEqualAndApplicableToIdAndIsDeletedFalse(Date fromDate,Date toDate,Long appTypeId);
}

