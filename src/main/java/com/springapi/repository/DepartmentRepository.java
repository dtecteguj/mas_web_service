package com.springapi.repository;

import com.springapi.model.DepartmentMaster;
import java.util.List;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Jyoti Patel
 */
@Repository
public interface DepartmentRepository extends JpaRepository<DepartmentMaster, Long> {

    Optional<DepartmentMaster> findById(Long uId);

    List<DepartmentMaster> findAllByInstituteId(Long instituteId);

}
