package com.springapi.repository;

import com.springapi.model.ShiftMaster;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * @author Jyoti_patel
 */
@Repository
public interface ShiftRepository extends JpaRepository<ShiftMaster, Long> {

    public ShiftMaster findByInstituteId(Long instituteId);
    public ShiftMaster findById(Long id);
    public List<ShiftMaster> findAllByInstituteId(Long instituteId);
    public List<ShiftMaster> findAllByInstituteIdAndIsDeletedFalse(Long instituteId);
    
}
