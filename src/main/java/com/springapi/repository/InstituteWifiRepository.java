package com.springapi.repository;

import com.springapi.model.InstituteWifiMaster;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * @author Jyoti Patel
 */
@Repository
public interface InstituteWifiRepository extends JpaRepository<InstituteWifiMaster, Long>{

    public Optional<InstituteWifiMaster> findById(Long uId);

    public InstituteWifiMaster findByBssid(String bssid);
    
}
