/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.springapi.repository;

import com.springapi.model.TrnLongleaveDtls;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author shahb
 */
public interface TrnLongleaveDtlsRepository extends JpaRepository<TrnLongleaveDtls, Long> {

    Optional<TrnLongleaveDtls> findById(Long aId);

    List<TrnLongleaveDtls> findByFId(Long fId);

    Optional<TrnLongleaveDtls> findByFromDateLessThanEqualAndToDateGreaterThanEqualAndFIdAndIsDeletedFalse(Date fdate, Date tdate, Long fId);

    Optional<TrnLongleaveDtls> findByFromDateLessThanEqualAndToDateGreaterThanEqualAndIsDeletedFalse(Date fdate, Date tdate);
}
