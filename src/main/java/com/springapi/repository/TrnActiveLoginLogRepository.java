package com.springapi.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.springapi.model.TrnActiveLoginLog;
import java.util.Date;

public interface TrnActiveLoginLogRepository extends JpaRepository<TrnActiveLoginLog, Long> {

	Optional<TrnActiveLoginLog> findById(Long id);
        Optional<TrnActiveLoginLog> findByIdAndFIdAndDeviceId(Long id,Long fId,String deviceId);
         Boolean existsByIdAndFIdAndDeviceIdAndIsActiveTrue(Long id,Long fId,String deviceId);
}
