/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.springapi.repository;

import com.springapi.model.TermMaster;
import java.io.Serializable;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author shahb
 */
public interface TermMasterRepository extends JpaRepository<TermMaster, Long> {
    Optional<TermMaster> findById(Long tId);
    Optional<TermMaster> findByIsDeleted(Boolean b);
    Optional<TermMaster> findByIsActiveTrue();
    
}
