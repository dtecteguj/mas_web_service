package com.springapi.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.springapi.model.TrnbyPriHodLog;


public interface TrnbyPriHodLogRepository extends JpaRepository<TrnbyPriHodLog, Long>{

	Optional<TrnbyPriHodLog> findById(Long id);
}
