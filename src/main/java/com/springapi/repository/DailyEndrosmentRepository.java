package com.springapi.repository;

import com.springapi.model.DailyEndrosment;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * @author Jyoti Patel
 */
@Repository
public interface DailyEndrosmentRepository extends JpaRepository<DailyEndrosment, Long> {

    public DailyEndrosment findByInstituteIdAndDepartmentIdAndEndrosDate(Long instituteId, Long departmentId, String date);

    public DailyEndrosment findByInstituteIdAndEndrosDate(Long instituteId, String date);
    
}
