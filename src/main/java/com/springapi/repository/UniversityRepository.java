package com.springapi.repository;

import com.springapi.model.UniversityMaster;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Jyoti Patel
 */
@Repository
public interface UniversityRepository extends JpaRepository<UniversityMaster, Long> {
    Optional<UniversityMaster> findById(Long uId);
}
