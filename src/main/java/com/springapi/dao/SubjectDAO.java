/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.springapi.dao;

import com.springapi.model.Subject;
import com.springapi.repository.SubjectRepository;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author shahb
 */
@Service
public class SubjectDAO {

    @Autowired
    SubjectRepository subjectRepository;

    /* to Save an Subject */
    public Subject save(Subject subject) {
        return subjectRepository.save(subject);
    }

    /* Search all Subject */
    public List<Subject> findAll() {
        return subjectRepository.findAll();
    }

    /* get an Subject by Id */
    public Subject findOne(Long sid) {
        return subjectRepository.findOne(sid);
    }

    /* Delete an Subject */
    public void delete(Subject subject) {
        subjectRepository.delete(subject);
    }

}
