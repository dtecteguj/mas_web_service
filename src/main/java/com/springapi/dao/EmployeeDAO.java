/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.springapi.dao;

import com.springapi.model.Employee;
import com.springapi.repository.EmployeeRepository;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author shahb
 */
@Service
public class EmployeeDAO {
    
    @Autowired
    EmployeeRepository employeeRepository;
    
    /* to Save an Employee */
    public Employee save(Employee employee)
    {
        return employeeRepository.save(employee);
    }
    
    /* Search all Employee */
    public List<Employee> findAll()
    {
        return employeeRepository.findAll();
    }
    
    /* get an Employee by Id */
    public Employee findOne(Long empid)
    {
        return employeeRepository.findOne(empid);
    }
    
    /* Delete an Employee */
    public void delete(Employee employee)
    {
        employeeRepository.delete(employee);
    }
    
    
}
