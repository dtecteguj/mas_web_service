/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.springapi.dao;

import com.springapi.model.Faculty;
import com.springapi.model.Student;
import com.springapi.repository.StudentRepository;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author shahb
 */
@Service
public class StudentDAO {

    @Autowired
    StudentRepository studentRepository;

    /* to Save an Faculty */
    public Student save(Student student) {
        return studentRepository.save(student);
    }

    /* Search all Faculty */
    public List<Student> findAll() {
        return studentRepository.findAll();
    }

    /* get an Faculty by Id */
    public Student findOne(Long sid) {
        return studentRepository.findOne(sid);
    }

    /* Delete an Faculty */
    public void delete(Student student) {
        studentRepository.delete(student);
    }

}
