/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.springapi.dao;

import com.springapi.model.StudentSubjectFacultyAttendance;
import com.springapi.repository.StudentSubjectFacultyAttendanceRepository;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author shahb
 */
@Service
public class StudentSubjectFacultyAttendanceDAO {

    @Autowired
    StudentSubjectFacultyAttendanceRepository studentSubjectFacultyAttendanceRepository;

    /* to Save an StudentSubjectFacultyAttendance */
    public StudentSubjectFacultyAttendance save(StudentSubjectFacultyAttendance studentSubjectFacultyAttendance) {
        return studentSubjectFacultyAttendanceRepository.save(studentSubjectFacultyAttendance);
    }

    /* Search all StudentSubjectFacultyAttendance */
    public List<StudentSubjectFacultyAttendance> findAll() {
        return studentSubjectFacultyAttendanceRepository.findAll();
    }

    /* get an StudentSubjectFacultyAttendance by Id */
    public StudentSubjectFacultyAttendance findOne(Long ssfaid) {
        return studentSubjectFacultyAttendanceRepository.findOne(ssfaid);
    }

    /* Delete an StudentSubjectFacultyAttendance */
    public void delete(StudentSubjectFacultyAttendance studentSubjectFacultyAttendance) {
        studentSubjectFacultyAttendanceRepository.delete(studentSubjectFacultyAttendance);
    }
}
