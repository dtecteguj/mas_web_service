/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.springapi.dao;

import com.springapi.model.SubjectFaculty;
import com.springapi.repository.SubjectFacultyRepository;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author shahb
 */
@Service
public class SubjectFacultyDAO {

    @Autowired
    SubjectFacultyRepository subjectFacultyRepository;

    /* to Save an SubjectFaculty */
    public SubjectFaculty save(SubjectFaculty subjectFaculty) {
        return subjectFacultyRepository.save(subjectFaculty);
    }

    /* Search all SubjectFaculty */
    public List<SubjectFaculty> findAll() {
        return subjectFacultyRepository.findAll();
    }

    /* get an SubjectFaculty by Id */
    public SubjectFaculty findOne(Long sfid) {
        return subjectFacultyRepository.findOne(sfid);
    }

    /* Delete an SubjectFaculty */
    public void delete(SubjectFaculty subjectFaculty) {
        subjectFacultyRepository.delete(subjectFaculty);
    }
}
