/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.springapi.dao;

import com.springapi.model.Faculty;
import com.springapi.repository.FacultyRepository;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 * @author shahb
 */
public class FacultyDAO {

    @Autowired
    FacultyRepository facultyRepository;

    /* to Save an Faculty */
    public Faculty save(Faculty faculty) {
        return facultyRepository.save(faculty);
    }

    /* Search all Faculty */
    public List<Faculty> findAll() {
        return facultyRepository.findAll();
    }

    /* get an Faculty by Id */
    public Faculty findOne(Long fid) {
        return facultyRepository.findOne(fid);
    }

    /* Delete an Faculty */
    public void delete(Faculty faculty) {
        facultyRepository.delete(faculty);
    }
}
