/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.springapi.dao;

import com.springapi.model.FacultyAttendance;
import com.springapi.repository.FacultyAttendanceRepository;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author shahb
 */
@Service
public class FacultyAttendanceDAO {

    @Autowired
    FacultyAttendanceRepository facultyAttendanceRepository;

    /* to Save an FacultyAttendance */
    public FacultyAttendance save(FacultyAttendance facultyAttendance) {
        return facultyAttendanceRepository.save(facultyAttendance);
    }

    /* Search all FacultyAttendance */
    public List<FacultyAttendance> findAll() {
        return facultyAttendanceRepository.findAll();
    }

    /* get an FacultyAttendance by Id */
    public FacultyAttendance findOne(Long faid) {
        return facultyAttendanceRepository.findOne(faid);
    }

    /* Delete an FacultyAttendance */
    public void delete(FacultyAttendance facultyAttendance) {
        facultyAttendanceRepository.delete(facultyAttendance);
    }
}
