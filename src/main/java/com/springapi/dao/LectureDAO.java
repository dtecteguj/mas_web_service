/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.springapi.dao;

import com.springapi.model.Lecture;
import com.springapi.repository.LectureRepository;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 * @author shahb
 */
public class LectureDAO {

    @Autowired
    LectureRepository lectureRepository;

    /* to Save an Lecture */
    public Lecture save(Lecture lecture) {
        return lectureRepository.save(lecture);
    }

    /* Search all Lecture */
    public List<Lecture> findAll() {
        return lectureRepository.findAll();
    }

    /* get an Lecture by Id */
    public Lecture findOne(Long fid) {
        return lectureRepository.findOne(fid);
    }

    /* Delete an Lecture */
    public void delete(Lecture lecture) {
        lectureRepository.delete(lecture);
    }
}
