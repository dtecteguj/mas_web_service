/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.springapi.dao;

import com.springapi.model.Semester;
import com.springapi.repository.SemesterRepository;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author shahb
 */
@Service
public class SemesterDAO {

    @Autowired
    SemesterRepository semesterRepository;

    /* to Save an Semester */
    public Semester save(Semester semester) {
        return semesterRepository.save(semester);
    }

    /* Search all Semester */
    public List<Semester> findAll() {
        return semesterRepository.findAll();
    }

    /* get an Semester by Id */
    public Semester findOne(Long sid) {
        return semesterRepository.findOne(sid);
    }

    /* Delete an Semester */
    public void delete(Semester semester) {
        semesterRepository.delete(semester);
    }

}
