/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.springapi.model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.Table;
import lombok.Data;

/**
 *
 * @author shahb
 */
@Data
@Entity
@Table(name = "Trn_Dtl_Login_Log")
public class TrnDtlLoginLog  extends BaseEntity implements Serializable{
    
    private Date loginDate;
    private String androidId;
    private Long masId;
    private Long fId;
    private Boolean isActive;
    private Boolean isDeleted;
    private String token;
    private String remark;
    private Byte loginCnt;
    
    
    //Token : 
          
}
