package com.springapi.model;

import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * @author Jyoti Patel
 */
@Entity
@Table(name = "DayMaster")
public class DayMaster extends BaseEntity {
    
    private String day;

    public String getDay() {
        return day;
    }

    public void setDay(String day) {
        this.day = day;
    }
}
