package com.springapi.model;

/**
 *
 * @author shahb
 */
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Transient;

/**
 *
 * @author shahb
 */
@Entity
@Table(name = "Faculty")
public class Faculty extends BaseEntity {

//    @Id
//    @GeneratedValue(strategy = GenerationType.AUTO)
//    private Long id;
    //@NotBlank
    private String name;

    //@NotBlank
    @Column(unique = true)
    private String email;

    //@NotBlank
    private String phone;

    private Long role;

    //@NotBlank
    private String password;

    //@NotBlank
    private String passwordHint;

    private Long instituteId;

    private Long instituteTypeId;

    private Long departmentId;

    private Boolean isDeleted = false;

    private String fName;
    private String mName;
    private String lName;

    private Long designationId;
    private Long classId;
    private String salutationName;
    private Boolean isDeputed = false;
    private Boolean activeFlag = false;

    private Boolean isTeaching = true;

    @Transient
    private Boolean status = false;
    
    @Transient
    private Boolean isLate = false;
    
    @Transient
    private Boolean isEarly = false;

    @Transient
    private String departmentName;

    @Transient
    private Long leaveStatus;
    
    @Transient
    private Long shiftId;

    public Faculty() {

    }

    public Faculty(String fpasswordHint, String femail, String fname, String phone, Long fRole, String password) {
        this.email = femail;
        this.passwordHint = fpasswordHint;
        this.password = password;
        this.phone = phone;
        this.name = fname;
        this.role = fRole;
    }

    public Faculty(Long id, String name, Boolean status) {
        this.id = id;
        this.name = name;
        this.status = status;
    }

    public Faculty(Long id, String name, String departmentName, Boolean status) {
        this.id = id;
        this.name = name;
        this.departmentName = departmentName;
        this.status = status;
    }

    public Faculty(Long id, String name, String departmentName, Boolean status, String fName, String mName, String lName) {
        this.id = id;
        this.name = name;
        this.departmentName = departmentName;
        this.status = status;
        this.fName = fName;
        this.mName = mName;
        this.lName = lName;
    }

    public Faculty(Long id, String name, String departmentName, Boolean status, String fName, String mName, String lName, Long shiftId) {
        this.id = id;
        this.name = name;
        this.departmentName = departmentName;
        this.status = status;
        this.fName = fName;
        this.mName = mName;
        this.lName = lName;
        this.shiftId = shiftId;
    }
    
    public Faculty(Long id, String name, String departmentName, Boolean status, String fName, String mName, String lName, Boolean activeFlag) {
        this.id = id;
        this.name = name;
        this.departmentName = departmentName;
        this.status = status;
        this.fName = fName;
        this.mName = mName;
        this.lName = lName;
        this.activeFlag = activeFlag;
    }
    
    public Faculty(Long id, String name, String departmentName, Boolean status, Long leaveStatus) {
        this.id = id;
        this.name = name;
        this.departmentName = departmentName;
        this.status = status;
        this.leaveStatus = leaveStatus;
    }

    public Faculty(Long id, String name, String departmentName, Boolean status, Long leaveStatus, String fName, String mName, String lName) {
        this.id = id;
        this.name = name;
        this.departmentName = departmentName;
        this.status = status;
        this.leaveStatus = leaveStatus;
        this.fName = fName;
        this.mName = mName;
        this.lName = lName;
    }

    public Faculty(Long id, String name, String departmentName, Boolean status, Long leaveStatus, String fName, String mName, String lName, Boolean isLate, Boolean isEarly) {
        this.id = id;
        this.name = name;
        this.departmentName = departmentName;
        this.status = status;
        this.leaveStatus = leaveStatus;
        this.fName = fName;
        this.mName = mName;
        this.lName = lName;
        this.isEarly = isEarly;
        this.isLate = isLate;
    }
    
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public Long getRole() {
        return role;
    }

    public void setRole(Long role) {
        this.role = role;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPasswordHint() {
        return passwordHint;
    }

    public void setPasswordHint(String passwordHint) {
        this.passwordHint = passwordHint;
    }

    public Long getInstituteId() {
        return instituteId;
    }

    public void setInstituteId(Long instituteId) {
        this.instituteId = instituteId;
    }

    public Long getDepartmentId() {
        return departmentId;
    }

    public void setDepartmentId(Long departmentId) {
        this.departmentId = departmentId;
    }

    public Long getInstituteTypeId() {
        return instituteTypeId;
    }

    public void setInstituteTypeId(Long instituteTypeId) {
        this.instituteTypeId = instituteTypeId;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public String getfName() {
        return fName;
    }

    public void setfName(String fName) {
        this.fName = fName;
    }

    public String getmName() {
        return mName;
    }

    public void setmName(String mName) {
        this.mName = mName;
    }

    public String getlName() {
        return lName;
    }

    public void setlName(String lName) {
        this.lName = lName;
    }

    public Long getDesignationId() {
        return designationId;
    }

    public void setDesignationId(Long designationId) {
        this.designationId = designationId;
    }

    public String getSalutationName() {
        return salutationName;
    }

    public void setSalutationName(String salutationName) {
        this.salutationName = salutationName;
    }

    public Boolean getActiveFlag() {
        return activeFlag;
    }

    public void setActiveFlag(Boolean activeFlag) {
        this.activeFlag = activeFlag;
    }

    public Long getClassId() {
        return classId;
    }

    public void setClassId(Long classId) {
        this.classId = classId;
    }

    public Boolean getIsDeputed() {
        return isDeputed;
    }

    public void setIsDeputed(Boolean isDeputed) {
        this.isDeputed = isDeputed;
    }

    public String getDepartmentName() {
        return departmentName;
    }

    public void setDepartmentName(String departmentName) {
        this.departmentName = departmentName;
    }

    public Long getLeaveStatus() {
        return leaveStatus;
    }

    public void setLeaveStatus(Long leaveStatus) {
        this.leaveStatus = leaveStatus;
    }

    public Boolean getIsDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(Boolean isDeleted) {
        this.isDeleted = isDeleted;
    }

    public Boolean getIsTeaching() {
        return isTeaching;
    }

    public void setIsTeaching(Boolean isTeaching) {
        this.isTeaching = isTeaching;
    }

    public Long getShiftId() {
        return shiftId;
    }

    public void setShiftId(Long shiftId) {
        this.shiftId = shiftId;
    }

    public Boolean getIsLate() {
        return isLate;
    }

    public void setIsLate(Boolean isLate) {
        this.isLate = isLate;
    }

    public Boolean getIsEarly() {
        return isEarly;
    }

    public void setIsEarly(Boolean isEarly) {
        this.isEarly = isEarly;
    }
    
    

}
