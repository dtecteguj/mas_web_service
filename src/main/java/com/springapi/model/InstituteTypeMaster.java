package com.springapi.model;

import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * @author Jyoti_patel
 */

@Entity
@Table(name = "Institute_Type_Master")
public class InstituteTypeMaster extends BaseEntity{
    private String name;
    
    private String description;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
