/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.springapi.model.view;

/**
 *
 * @author shahb
 */
public class FacultyShiftTiming {

    private Long shiftTbleId = 1L;

    private Long shiftRefId = 0L;

    private String actualInTime;

//    @NotBlank
    private String actualOutTime;
    
    
    private Long fId;
    

    public Long getShiftTbleId() {
        return shiftTbleId;
    }

    public void setShiftTbleId(Long shiftTbleId) {
        this.shiftTbleId = shiftTbleId;
    }

    public Long getShiftRefId() {
        return shiftRefId;
    }

    public void setShiftRefId(Long shiftRefId) {
        this.shiftRefId = shiftRefId;
    }

    public String getActualInTime() {
        return actualInTime;
    }

    public void setActualInTime(String actualInTime) {
        this.actualInTime = actualInTime;
    }

    public String getActualOutTime() {
        return actualOutTime;
    }

    public void setActualOutTime(String actualOutTime) {
        this.actualOutTime = actualOutTime;
    }

    public Long getfId() {
        return fId;
    }

    public void setfId(Long fId) {
        this.fId = fId;
    }
    
    
}
