/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.springapi.model.view;

import lombok.Data;

/**
 *
 * @author shahb
 */
public class FacultyMEMOView {

    private Long Institute_Id;
    private String Institute_Name;
    private Long Institute_Type_Id;
    private String Department_Name;
    private String Faculty_Name;
    private String Faculty_Email;
    private String Memo_Date;
    private Long MEMO_Number;

    public Long getInstitute_Id() {
        return Institute_Id;
    }

    public void setInstitute_Id(Long Institute_Id) {
        this.Institute_Id = Institute_Id;
    }

    public String getInstitute_Name() {
        return Institute_Name;
    }

    public void setInstitute_Name(String Institute_Name) {
        this.Institute_Name = Institute_Name;
    }

    public Long getInstitute_Type_Id() {
        return Institute_Type_Id;
    }

    public void setInstitute_Type_Id(Long Institute_Type_Id) {
        this.Institute_Type_Id = Institute_Type_Id;
    }

    public String getDepartment_Name() {
        return Department_Name;
    }

    public void setDepartment_Name(String Department_Name) {
        this.Department_Name = Department_Name;
    }

    public String getFaculty_Name() {
        return Faculty_Name;
    }

    public void setFaculty_Name(String Faculty_Name) {
        this.Faculty_Name = Faculty_Name;
    }

    public String getFaculty_Email() {
        return Faculty_Email;
    }

    public void setFaculty_Email(String Faculty_Email) {
        this.Faculty_Email = Faculty_Email;
    }

    public String getMemo_Date() {
        return Memo_Date;
    }

    public void setMemo_Date(String Memo_Date) {
        this.Memo_Date = Memo_Date;
    }

    public Long getMEMO_Number() {
        return MEMO_Number;
    }

    public void setMEMO_Number(Long MEMO_Number) {
        this.MEMO_Number = MEMO_Number;
    }
    
    

}
