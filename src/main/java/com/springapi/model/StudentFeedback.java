package com.springapi.model;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Transient;

/**
 * @author shahb
 */
@Entity
@Table(name = "StudentFeedback")
public class StudentFeedback extends BaseEntity{
    
    private Long studentId;
    
    private Long rate;
    
    private Boolean isLectureAttend = false;
    
    private Boolean isLectureConducted = false;
    
    private String comment;
    
    private Long facultyId;
    
    private Long semId;
    
    private Long slotId;
    
    private String feedbackDate;
    
    private Long subjectId;
    
    private String subjectNm;
    
    @Transient
    private String facultyName;
    
    @Transient
    private String studentName;
    
    @Transient
    private String semesterName;
    
    @Transient
    private String subjectName;

    public StudentFeedback() {
    }
    
    
    public StudentFeedback(Long studentId, Long rate, Boolean isLectureAttend, Boolean isLectureConducted, 
            String comment, Long facultyId, Long semId, Long slotId, String feedbackDate
            , String facultyName, String studentName, String semesterName
            ,String subjectName
    ) {
        this.studentId = studentId;
        this.rate = rate;
        this.isLectureAttend = isLectureAttend;
        this.isLectureConducted = isLectureConducted;
        this.comment = comment;
        this.facultyId = facultyId;
        this.semId = semId;
        this.slotId = slotId;
        this.feedbackDate = feedbackDate;
        this.facultyName = facultyName;
        this.studentName = studentName;
        this.semesterName = semesterName;
        this.subjectName = subjectName;
    }
    
    public Long getStudentId() {
        return studentId;
    }

    public void setStudentId(Long studentId) {
        this.studentId = studentId;
    }

    public Long getRate() {
        return rate;
    }

    public void setRate(Long rate) {
        this.rate = rate;
    }

    public Boolean getIsLectureAttend() {
        return isLectureAttend;
    }

    public void setIsLectureAttend(Boolean isLectureAttend) {
        this.isLectureAttend = isLectureAttend;
    }

    public Boolean getIsLectureConducted() {
        return isLectureConducted;
    }

    public void setIsLectureConducted(Boolean isLectureConducted) {
        this.isLectureConducted = isLectureConducted;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public Long getFacultyId() {
        return facultyId;
    }

    public void setFacultyId(Long facultyId) {
        this.facultyId = facultyId;
    }

    public Long getSemId() {
        return semId;
    }

    public void setSemId(Long semId) {
        this.semId = semId;
    }

    public Long getSlotId() {
        return slotId;
    }

    public void setSlotId(Long slotId) {
        this.slotId = slotId;
    }

    public String getFeedbackDate() {
        return feedbackDate;
    }

    public void setFeedbackDate(String feedbackDate) {
        this.feedbackDate = feedbackDate;
    }

    public String getFacultyName() {
        return facultyName;
    }

    public void setFacultyName(String facultyName) {
        this.facultyName = facultyName;
    }

    public String getStudentName() {
        return studentName;
    }

    public void setStudentName(String studentName) {
        this.studentName = studentName;
    }

    public String getSemesterName() {
        return semesterName;
    }

    public void setSemesterName(String semesterName) {
        this.semesterName = semesterName;
    }

    public Long getSubjectId() {
        return subjectId;
    }

    public void setSubjectId(Long subjectId) {
        this.subjectId = subjectId;
    }

    public String getSubjectName() {
        return subjectName;
    }

    public void setSubjectName(String subjectName) {
        this.subjectName = subjectName;
    }

    public String getSubjectNm() {
        return subjectNm;
    }

    public void setSubjectNm(String subjectNm) {
        this.subjectNm = subjectNm;
    }
    
    
}
