/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.springapi.model;

import javax.persistence.Entity;
import javax.persistence.Table;

/**
 *
 * @author shahb
 */
@Entity
@Table(name = "Fs_Student_Reg")
public class FsStudentReg extends BaseEntity{
    private Long schdlId;
    private Long StudentId;
    private Long instituteId;
    private Long instituteTypeId;
    private Long crntSemYear;
    private Boolean isDeleted = false;

    public Long getSchdlId() {
        return schdlId;
    }

    public void setSchdlId(Long schdlId) {
        this.schdlId = schdlId;
    }

    public Long getStudentId() {
        return StudentId;
    }

    public void setStudentId(Long StudentId) {
        this.StudentId = StudentId;
    }

    public Long getInstituteId() {
        return instituteId;
    }

    public void setInstituteId(Long instituteId) {
        this.instituteId = instituteId;
    }

    public Long getInstituteTypeId() {
        return instituteTypeId;
    }

    public void setInstituteTypeId(Long instituteTypeId) {
        this.instituteTypeId = instituteTypeId;
    }

    public Long getCrntSemYear() {
        return crntSemYear;
    }

    public void setCrntSemYear(Long crntSemYear) {
        this.crntSemYear = crntSemYear;
    }

    public Boolean getIsDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(Boolean isDeleted) {
        this.isDeleted = isDeleted;
    }
    
    
}
