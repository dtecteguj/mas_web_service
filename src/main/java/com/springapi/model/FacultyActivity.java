package com.springapi.model;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Transient;

/**
 * @author Jyoti Patel
 */

@Entity
@Table(name= "FacultyActivity")
public class FacultyActivity extends BaseEntity{
    
    private Long facultyId;
    
    private String message;
    
    private String activityDate;
    
    private Long dayId;
    
    private Long slotId;
    
    private Long semId;
    
    private Long subjectId;

    @Transient
    private String facultyName;
    
    @Transient
    private String dayDate;
    
    public FacultyActivity() {
    }

    public FacultyActivity(Long facultyId, String message, String facultyName) {
        this.facultyId = facultyId;
        this.message = message;
        this.facultyName= facultyName;
    }
    
    
    public Long getFacultyId() {
        return facultyId;
    }

    public void setFacultyId(Long facultyId) {
        this.facultyId = facultyId;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getActivityDate() {
        return activityDate;
    }

    public void setActivityDate(String activityDate) {
        this.activityDate = activityDate;
    }

    public Long getDayId() {
        return dayId;
    }

    public void setDayId(Long dayId) {
        this.dayId = dayId;
    }

    public Long getSlotId() {
        return slotId;
    }

    public void setSlotId(Long slotId) {
        this.slotId = slotId;
    }

    public Long getSemId() {
        return semId;
    }

    public void setSemId(Long semId) {
        this.semId = semId;
    }

    public Long getSubjectId() {
        return subjectId;
    }

    public void setSubjectId(Long subjectId) {
        this.subjectId = subjectId;
    }

    public String getFacultyName() {
        return facultyName;
    }

    public void setFacultyName(String facultyName) {
        this.facultyName = facultyName;
    }

    public String getDayDate() {
        return dayDate;
    }

    public void setDayDate(String dayDate) {
        this.dayDate = dayDate;
    }

    

    
}
