/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.springapi.model;

import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 *
 * @author shahb
 */
@Entity
@Table(name = "Trn_Dtl_LeaveStatus")
public class TrnDtlLeaveStatus extends BaseEntity {
    private Long fId;
    private Long attdId;
    private Long leaveTypeId;
    private Date Attendance_Date;
    private String leaveDesc;
    private Long year;
    private Boolean isDeleted=false;

    public Long getfId() {
        return fId;
    }

    public void setfId(Long fId) {
        this.fId = fId;
    }

    public Long getAttdId() {
        return attdId;
    }

    public void setAttdId(Long attdId) {
        this.attdId = attdId;
    }

    public Long getLeaveTypeId() {
        return leaveTypeId;
    }

    public void setLeaveTypeId(Long leaveTypeId) {
        this.leaveTypeId = leaveTypeId;
    }

    public Date getAttendance_Date() {
        return Attendance_Date;
    }

    public void setAttendance_Date(Date Attendance_Date) {
        this.Attendance_Date = Attendance_Date;
    }

    public String getLeaveDesc() {
        return leaveDesc;
    }

    public void setLeaveDesc(String leaveDesc) {
        this.leaveDesc = leaveDesc;
    }

    public Long getYear() {
        return year;
    }

    public void setYear(Long year) {
        this.year = year;
    }

    public Boolean getIsDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(Boolean isDeleted) {
        this.isDeleted = isDeleted;
    }
    
    
    
}
