/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.springapi.model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Temporal;

/**
 *
 * @author shahb
 */
@Entity
@Table(name = "Trn_FS_Prgm_Schd")
public class TrnFSPrgmSchd extends BaseEntity implements Serializable {

    private Long instituteId;
    private Long schdlCode;
    private Long trainerId;
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date fromDate;
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date toDate;
    private Long compNumber;
    private Long yearOfPrgm;
    private Boolean isActive = false;
    private Boolean isDeleted = false;
    private Long deletedBy;
    private String reasonForDelete;
    private Long numberOfDays;
    private Long noOfHours;
    private Boolean isActiveSchdl = false;
    private String inTime;
    private String outTime;
    

    public TrnFSPrgmSchd() {
    }

    public Long getInstituteId() {
        return instituteId;
    }

    public void setInstituteId(Long instituteId) {
        this.instituteId = instituteId;
    }

    public Long getTrainerId() {
        return trainerId;
    }

    public void setTrainerId(Long trainerId) {
        this.trainerId = trainerId;
    }

    public Date getFromDate() {
        return fromDate;
    }

    public void setFromDate(Date fromDate) {
        this.fromDate = fromDate;
    }

    public Date getToDate() {
        return toDate;
    }

    public void setToDate(Date toDate) {
        this.toDate = toDate;
    }

    public Long getCompNumber() {
        return compNumber;
    }

    public void setCompNumber(Long compNumber) {
        this.compNumber = compNumber;
    }

    public Long getYearOfPrgm() {
        return yearOfPrgm;
    }

    public void setYearOfPrgm(Long yearOfPrgm) {
        this.yearOfPrgm = yearOfPrgm;
    }

    public Boolean getIsActive() {
        return isActive;
    }

    public void setIsActive(Boolean isActive) {
        this.isActive = isActive;
    }

    public Boolean getIsDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(Boolean isDeleted) {
        this.isDeleted = isDeleted;
    }

    public Long getDeletedBy() {
        return deletedBy;
    }

    public void setDeletedBy(Long deletedBy) {
        this.deletedBy = deletedBy;
    }

    public String getReasonForDelete() {
        return reasonForDelete;
    }

    public void setReasonForDelete(String reasonForDelete) {
        this.reasonForDelete = reasonForDelete;
    }

    public Long getNumberOfDays() {
        return numberOfDays;
    }

    public void setNumberOfDays(Long numberOfDays) {
        this.numberOfDays = numberOfDays;
    }

    public Long getSchdlCode() {
        return schdlCode;
    }

    public void setSchdlCode(Long schdlCode) {
        this.schdlCode = schdlCode;
    }

    public Long getNoOfHours() {
        return noOfHours;
    }

    public void setNoOfHours(Long noOfHours) {
        this.noOfHours = noOfHours;
    }

    public Boolean getIsActiveSchdl() {
        return isActiveSchdl;
    }

    public void setIsActiveSchdl(Boolean isActiveSchdl) {
        this.isActiveSchdl = isActiveSchdl;
    }

    public String getInTime() {
        return inTime;
    }

    public void setInTime(String inTime) {
        this.inTime = inTime;
    }

    public String getOutTime() {
        return outTime;
    }

    public void setOutTime(String outTime) {
        this.outTime = outTime;
    }
    

}
