package com.springapi.model;

import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * @author Jyoti Patel
 */
@Entity
@Table(name = "DailyEndrosment")
public class DailyEndrosment extends BaseEntity {

    private Long departmentId;

    private Long instituteId;

    private Boolean endrosByHod = false;

    private Boolean endrosByPrincipal = false;

    private String endrosDate;

    public Long getDepartmentId() {
        return departmentId;
    }

    public void setDepartmentId(Long departmentId) {
        this.departmentId = departmentId;
    }

    public Long getInstituteId() {
        return instituteId;
    }

    public void setInstituteId(Long instituteId) {
        this.instituteId = instituteId;
    }

    public Boolean getEndrosByHod() {
        return endrosByHod;
    }

    public void setEndrosByHod(Boolean endrosByHod) {
        this.endrosByHod = endrosByHod;
    }

    public Boolean getEndrosByPrincipal() {
        return endrosByPrincipal;
    }

    public void setEndrosByPrincipal(Boolean endrosByPrincipal) {
        this.endrosByPrincipal = endrosByPrincipal;
    }

    public String getEndrosDate() {
        return endrosDate;
    }

    public void setEndrosDate(String endrosDate) {
        this.endrosDate = endrosDate;
    }
}
