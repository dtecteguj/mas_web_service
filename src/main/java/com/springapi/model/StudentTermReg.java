/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.springapi.model;

import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Temporal;

/**
 *
 * @author shahb
 */
@Entity
@Table(name = "Student_Term_Reg")
public class StudentTermReg extends BaseEntity {
    private Long termId;
    private Long studentId;
    private Long instituteId;
    private Long departmentId;
    private Long programId;
    private Long crntSemYear;
    private Long regStatus;
    private Boolean isDeleted = false;
    private String remarks;
    private Boolean isEndorsed = false;
    private Long endorsedBy;
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date endrosedAt;
        
    public Long getTermId() {
        return termId;
    }

    public void setTermId(Long termId) {
        this.termId = termId;
    }

    public Long getStudentId() {
        return studentId;
    }

    public void setStudentId(Long studentId) {
        this.studentId = studentId;
    }

    public Long getInstituteId() {
        return instituteId;
    }

    public void setInstituteId(Long instituteId) {
        this.instituteId = instituteId;
    }

    public Long getProgramId() {
        return programId;
    }

    public void setProgramId(Long programId) {
        this.programId = programId;
    }

    public Long getCrntSemYear() {
        return crntSemYear;
    }

    public void setCrntSemYear(Long crntSemYear) {
        this.crntSemYear = crntSemYear;
    }

    public Long getRegStatus() {
        return regStatus;
    }

    public void setRegStatus(Long regStatus) {
        this.regStatus = regStatus;
    }

    public Boolean getIsDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(Boolean isDeleted) {
        this.isDeleted = isDeleted;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public Long getDepartmentId() {
        return departmentId;
    }

    public void setDepartmentId(Long departmentId) {
        this.departmentId = departmentId;
    }

    public Boolean getIsEndorsed() {
        return isEndorsed;
    }

    public void setIsEndorsed(Boolean isEndorsed) {
        this.isEndorsed = isEndorsed;
    }

    public Long getEndorsedBy() {
        return endorsedBy;
    }

    public void setEndorsedBy(Long endorsedBy) {
        this.endorsedBy = endorsedBy;
    }

    public Date getEndrosedAt() {
        return endrosedAt;
    }

    public void setEndrosedAt(Date endrosedAt) {
        this.endrosedAt = endrosedAt;
    }
       
}
