/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.springapi.model;

import javax.persistence.Entity;
import javax.persistence.Table;
import lombok.Data;

/**
 *
 * @author shahb
 */
@Data
@Entity
@Table(name = "Android_Version")
public class AndroidVersion extends BaseEntity {

    private String versionName;
    private Long versionNumber;
    private String versionDesc;
    private Boolean allowForLive;
    private String divType = "A"; 
    private Boolean isActive = false;
    private String vesionDesc;
    
 

    public String getVersionName() {
        return versionName;
    }

    public void setVersionName(String versionName) {
        this.versionName = versionName;
    }

    public Long getVersionNumber() {
        return versionNumber;
    }

    public void setVersionNumber(Long versionNumber) {
        this.versionNumber = versionNumber;
    }

    public Boolean getIsActive() {
        return isActive;
    }

    public void setIsActive(Boolean isActive) {
        this.isActive = isActive;
    }

    public Boolean getAllowForLive() {
        return allowForLive;
    }

    public void setAllowForLive(Boolean allowForLive) {
        this.allowForLive = allowForLive;
    }

   

    public String getVesionDesc() {
        return vesionDesc;
    }

    public void setVesionDesc(String vesionDesc) {
        this.vesionDesc = vesionDesc;
    }

       public String getVersionDesc() {
        return versionDesc;
    }

    public void setVersionDesc(String versionDesc) {
        this.versionDesc = versionDesc;
    }
    
}
