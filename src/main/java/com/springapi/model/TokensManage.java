package com.springapi.model;

import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * @author shahb
 */

@Entity
@Table(name = "TokensManage")
public class TokensManage extends BaseEntity {
    
    private String token;
    
    private String type;
    
    private Long uId;

    public TokensManage() {
    }
    
    public TokensManage(String token) {
        this.token = token;
//        this.type = type;
//        this.uId = uId;
    }
    

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Long getuId() {
        return uId;
    }

    public void setuId(Long uId) {
        this.uId = uId;
    }
    
}
