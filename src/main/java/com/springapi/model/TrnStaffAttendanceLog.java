/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.springapi.model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.Table;
import lombok.Data;

/**
 *
 * @author shahb
 */
@Data
@Entity
@Table(name = "Trn_Staff_Attendance_Log")
public class TrnStaffAttendanceLog extends BaseEntity implements Serializable{
    
    private Date attendanceDate;
    private String androidId;
    private Long fId;
    private String bssid;
    private String ssid;
    private Double locLate;
    private Double locLong;
    private Double locDistance;
    private Boolean markType;
}
