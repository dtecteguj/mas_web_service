package com.springapi.model;

import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * @author Jyoti_patel
 */
@Entity
@Table(name = "Shift_Master")
public class ShiftMaster  extends BaseEntity{
    
    private String shiftName;
    
    private Long instituteId;
    
    private String inTime;
    
    private String outTime;
    
    private Boolean isDeleted = false;

    public String getShiftName() {
        return shiftName;
    }

    public void setShiftName(String shiftName) {
        this.shiftName = shiftName;
    }

    public Long getInstituteId() {
        return instituteId;
    }

    public void setInstituteId(Long instituteId) {
        this.instituteId = instituteId;
    }

    public String getInTime() {
        return inTime;
    }

    public void setInTime(String inTime) {
        this.inTime = inTime;
    }

    public String getOutTime() {
        return outTime;
    }

    public void setOutTime(String outTime) {
        this.outTime = outTime;
    }

    public Boolean getIsDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(Boolean isDeleted) {
        this.isDeleted = isDeleted;
    }
    
}
