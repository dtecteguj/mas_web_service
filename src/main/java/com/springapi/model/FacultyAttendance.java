package com.springapi.model;

import com.springapi.enums.FacultyAttendanceStatus;
import com.springapi.model.view.FacultyShiftTiming;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Transient;

/**
 *
 * @author shahb
 */
@Entity
@Table(name = "FacultyAttendance")
public class FacultyAttendance extends BaseEntity {

//    @Id
//    @GeneratedValue(strategy = GenerationType.AUTO)
//    private Long id;
//    @NotBlank
    private Long fId;

//    @NotBlank
    private String SSID;

//    @NotBlank
    private String BSSID;

//    @NotBlank
    private String androidID;

//    @NotBlank
    private String macID;

//    @NotBlank
    private String IMEINo;

//    @NotBlank
    private String inTime;

//    @NotBlank
    private String outTime;

    private FacultyAttendanceStatus inStatus;

    private FacultyAttendanceStatus outStatus;

    private String attendanceDate;

    private Long instituteId;

    private Long departmentId;

    private Boolean status = false;

    private String earlyOutReason;

    private String lateComingReason;

    private Long instituteTypeId;

    private Boolean isPrincipalFilled = false;

    private String reasonByPrincipal;

    private Boolean isLate = false;
    private Boolean isEarly = false;
    private Boolean presenceStatus = false;
    private Long leaveStatus=0L;
    
    //    @NotBlank
    private String actualInTime;

//    @NotBlank
    private String actualOutTime;
    
    //    @NotBlank
    private String outSSID;

//    @NotBlank
    private String outBSSID;

//    @NotBlank
    private String outAndroidID;

//    @NotBlank
    private String outMacID;

//    @NotBlank
    private String outIMEINo;

    @Transient
    private String instituteName;

    @Transient
    private String departmentName;

    @Transient
    private String facultyName;

    @Transient
    private Long role;

    private Long schdlTbleId = 0L;

    private Long schdlRefId = 0L;
    
     private Boolean isDeleted = false;
     
     private Long shiftTbleId = 0L;
     
     private Long shiftRefId = 0L;
    

    public FacultyAttendance() {

    }

    public FacultyAttendance(final Long fId, final String BSSID, final String inTime, final String outTime, final Long instituteId, final String instituteName, final String facultyName, final String departmentName, final String attendanceDate, final Boolean status, final String lateComingReason) {
        this.setfId(fId);
        this.setBSSID(BSSID);
        this.setInTime(inTime);
        this.setOutTime(outTime);
        this.setInstituteId(instituteId);
        this.setInstituteName(instituteName);
        this.setFacultyName(facultyName);
        this.setDepartmentName(departmentName);
        this.setAttendanceDate(attendanceDate);
        this.setStatus(status);
        this.setLateComingReason(lateComingReason);
    }

    public FacultyAttendance(final Long fId, final String BSSID, final String inTime, final String outTime, final Long instituteId, final String instituteName, final String facultyName, final String departmentName, final String attendanceDate, final Boolean status, final String lateComingReason, final Boolean isPrincipalFilled, final String reasonByPrincipal, final String earlyOutReason, final Boolean isLate, final Boolean isEarly, final Boolean presenceStatus, final Long leaveStatus) {
        this.setfId(fId);
        this.setBSSID(BSSID);
        this.setInTime(inTime);
        this.setOutTime(outTime);
        this.setInstituteId(instituteId);
        this.setInstituteName(instituteName);
        this.setFacultyName(facultyName);
        this.setDepartmentName(departmentName);
        this.setAttendanceDate(attendanceDate);
        this.setStatus(status);
        this.setLateComingReason(lateComingReason);
        this.setIsPrincipalFilled(isPrincipalFilled);
        this.setReasonByPrincipal(reasonByPrincipal);
        this.setEarlyOutReason(earlyOutReason);
        this.setIsLate(isLate);
        this.setIsEarly(isEarly);
        this.setLeaveStatus(leaveStatus);
        this.setPresenceStatus(presenceStatus);
    }

    public FacultyAttendance(final Long id, final Long fId, final String BSSID, final String inTime, final String outTime, final Long instituteId, final String instituteName, final String facultyName, final String departmentName, final String attendanceDate, final Boolean status, final String lateComingReason, final Boolean isPrincipalFilled, final String reasonByPrincipal, final String earlyOutReason, final Boolean isLate, final Boolean isEarly, final Boolean presenceStatus, final Long leaveStatus) {
        super(id);
        this.setfId(fId);
        this.setBSSID(BSSID);
        this.setInTime(inTime);
        this.setOutTime(outTime);
        this.setInstituteId(instituteId);
        this.setInstituteName(instituteName);
        this.setFacultyName(facultyName);
        this.setDepartmentName(departmentName);
        this.setAttendanceDate(attendanceDate);
        this.setStatus(status);
        this.setLateComingReason(lateComingReason);
        this.setIsPrincipalFilled(isPrincipalFilled);
        this.setReasonByPrincipal(reasonByPrincipal);
        this.setEarlyOutReason(earlyOutReason);
        this.setIsLate(isLate);
        this.setIsEarly(isEarly);
        this.setLeaveStatus(leaveStatus);
        this.setPresenceStatus(presenceStatus);
    }

    public FacultyAttendance(Long id, Long fId, Long instituteId, Boolean status, Long instituteTypeId, Long role) {
        super(id);
        this.fId = fId;
        this.instituteId = instituteId;
        this.status = status;
        this.instituteTypeId = instituteTypeId;
        this.role = role;
    }

    public Long getfId() {
        return fId;
    }

    public void setfId(Long fId) {
        this.fId = fId;
    }

    public String getSSID() {
        return SSID;
    }

    public void setSSID(String SSID) {
        this.SSID = SSID;
    }

    public String getBSSID() {
        return BSSID;
    }

    public void setBSSID(String BSSID) {
        this.BSSID = BSSID;
    }

    public String getAndroidID() {
        return androidID;
    }

    public void setAndroidID(String androidID) {
        this.androidID = androidID;
    }

    public String getMacID() {
        return macID;
    }

    public void setMacID(String macID) {
        this.macID = macID;
    }

    public String getIMEINo() {
        return IMEINo;
    }

    public void setIMEINo(String IMEINo) {
        this.IMEINo = IMEINo;
    }

    public String getInTime() {
        return inTime;
    }

    public void setInTime(String inTime) {
        this.inTime = inTime;
    }

    public String getOutTime() {
        return outTime;
    }

    public void setOutTime(String outTime) {
        this.outTime = outTime;
    }

    public FacultyAttendanceStatus getInStatus() {
        return inStatus;
    }

    public void setInStatus(FacultyAttendanceStatus inStatus) {
        this.inStatus = inStatus;
    }

    public FacultyAttendanceStatus getOutStatus() {
        return outStatus;
    }

    public void setOutStatus(FacultyAttendanceStatus outStatus) {
        this.outStatus = outStatus;
    }

    public String getAttendanceDate() {
        return attendanceDate;
    }

    public void setAttendanceDate(String attendanceDate) {
        this.attendanceDate = attendanceDate;
    }

    public String getInstituteName() {
        return instituteName;
    }

    public void setInstituteName(String instituteName) {
        this.instituteName = instituteName;
    }

    public Long getInstituteId() {
        return instituteId;
    }

    public void setInstituteId(Long instituteId) {
        this.instituteId = instituteId;
    }

    public String getFacultyName() {
        return facultyName;
    }

    public void setFacultyName(String facultyName) {
        this.facultyName = facultyName;
    }

    public Long getDepartmentId() {
        return departmentId;
    }

    public void setDepartmentId(Long departmentId) {
        this.departmentId = departmentId;
    }

    public String getDepartmentName() {
        return departmentName;
    }

    public void setDepartmentName(String departmentName) {
        this.departmentName = departmentName;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public String getEarlyOutReason() {
        return earlyOutReason;
    }

    public void setEarlyOutReason(String earlyOutReason) {
        this.earlyOutReason = earlyOutReason;
    }

    public String getLateComingReason() {
        return lateComingReason;
    }

    public void setLateComingReason(String lateComingReason) {
        this.lateComingReason = lateComingReason;
    }

    public Long getInstituteTypeId() {
        return instituteTypeId;
    }

    public void setInstituteTypeId(Long instituteTypeId) {
        this.instituteTypeId = instituteTypeId;
    }

    public Boolean getIsPrincipalFilled() {
        return isPrincipalFilled;
    }

    public void setIsPrincipalFilled(Boolean isPrincipalFilled) {
        this.isPrincipalFilled = isPrincipalFilled;
    }

    public String getReasonByPrincipal() {
        return reasonByPrincipal;
    }

    public void setReasonByPrincipal(String reasonByPrincipal) {
        this.reasonByPrincipal = reasonByPrincipal;
    }

    public Boolean getIsLate() {
        return isLate;
    }

    public void setIsLate(Boolean isLate) {
        this.isLate = isLate;
    }

    public Boolean getIsEarly() {
        return isEarly;
    }

    public void setIsEarly(Boolean isEarly) {
        this.isEarly = isEarly;
    }

    public Boolean getPresenceStatus() {
        return presenceStatus;
    }

    public void setPresenceStatus(Boolean presenceStatus) {
        this.presenceStatus = presenceStatus;
    }

    public Long getLeaveStatus() {
        return leaveStatus;
    }

    public void setLeaveStatus(Long leaveStatus) {
        this.leaveStatus = leaveStatus;
    }

    public Long getRole() {
        return role;
    }

    public void setRole(Long role) {
        this.role = role;
    }

    public Long getSchdlTbleId() {
        return schdlTbleId;
    }

    public void setSchdlTbleId(Long schdlTbleId) {
        this.schdlTbleId = schdlTbleId;
    }

    public Long getSchdlRefId() {
        return schdlRefId;
    }

    public void setSchdlRefId(Long schdlRefId) {
        this.schdlRefId = schdlRefId;
    }

    public String getOutSSID() {
        return outSSID;
    }

    public void setOutSSID(String outSSID) {
        this.outSSID = outSSID;
    }

    public String getOutBSSID() {
        return outBSSID;
    }

    public void setOutBSSID(String outBSSID) {
        this.outBSSID = outBSSID;
    }

    public String getOutAndroidID() {
        return outAndroidID;
    }

    public void setOutAndroidID(String outAndroidID) {
        this.outAndroidID = outAndroidID;
    }

    public String getOutMacID() {
        return outMacID;
    }

    public void setOutMacID(String outMacID) {
        this.outMacID = outMacID;
    }

    public String getOutIMEINo() {
        return outIMEINo;
    }

    public void setOutIMEINo(String outIMEINo) {
        this.outIMEINo = outIMEINo;
    }

    public Boolean getIsDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(Boolean isDeleted) {
        this.isDeleted = isDeleted;
    }

    public String getActualInTime() {
        return actualInTime;
    }

    public void setActualInTime(String actualInTime) {
        this.actualInTime = actualInTime;
    }

    public String getActualOutTime() {
        return actualOutTime;
    }

    public void setActualOutTime(String actualOutTime) {
        this.actualOutTime = actualOutTime;
    }

    public Long getShiftTbleId() {
        return shiftTbleId;
    }

    public void setShiftTbleId(Long shiftTbleId) {
        this.shiftTbleId = shiftTbleId;
    }

    public Long getShiftRefId() {
        return shiftRefId;
    }

    public void setShiftRefId(Long shiftRefId) {
        this.shiftRefId = shiftRefId;
    }

    public void setShiftTimeData(FacultyShiftTiming facultyShiftTiming)
    {
        setActualInTime(facultyShiftTiming.getActualInTime());
        setActualOutTime(facultyShiftTiming.getActualOutTime());
        setShiftRefId(facultyShiftTiming.getShiftRefId());
        setShiftTbleId(facultyShiftTiming.getShiftTbleId());
    }
}
