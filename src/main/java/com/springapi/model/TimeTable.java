package com.springapi.model;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Transient;

/**
 * @author Jyoti patel
 */
@Entity
@Table(name = "TimeTable")
public class TimeTable extends BaseEntity{
    
    private Long facultyId;
    
    private Long semId;
    
    private Long subjectCode;
    
    private Long dayId;
    
    private Long slotId;
    
    private Long loadtypeId;
    
    private String batch;
    
    @Transient
    private String day;
    
    @Transient
    private String slotStartTime;
    
    @Transient
    private String slotEndTime;
    
    @Transient
    private String loadType;
    
     @Transient
    private String facultyName;
     
      @Transient
    private String subjectName;
      
      @Transient
    private Long subjectId;
    
    public TimeTable() {
    }

    public TimeTable(Long facultyId, Long semId, Long subjectCode, Long dayId, Long slotId, 
            Long loadtypeId, String day, String slotStartTime, String slotEndTime, String loadType,
            String facultyName,String subjectName, Long subjectId, String batch ) {
        this.facultyId = facultyId;
        this.semId = semId;
        this.subjectCode = subjectCode;
        this.dayId = dayId;
        this.slotId = slotId;
        this.loadtypeId = loadtypeId;
        this.day = day;
        this.slotStartTime = slotStartTime;
        this.slotEndTime = slotEndTime;
        this.loadType = loadType;
        this.facultyName = facultyName;
        this.subjectName = subjectName;
        this.subjectId = subjectId;
        this.batch = batch;
    }
    
    public Long getFacultyId() {
        return facultyId;
    }

    public void setFacultyId(Long facultyId) {
        this.facultyId = facultyId;
    }

    public Long getSemId() {
        return semId;
    }

    public void setSemId(Long semId) {
        this.semId = semId;
    }

    public Long getSubjectCode() {
        return subjectCode;
    }

    public void setSubjectCode(Long subjectCode) {
        this.subjectCode = subjectCode;
    }

    public Long getDayId() {
        return dayId;
    }

    public void setDayId(Long dayId) {
        this.dayId = dayId;
    }

    public Long getSlotId() {
        return slotId;
    }

    public void setSlotId(Long slotId) {
        this.slotId = slotId;
    }

    public Long getLoadtypeId() {
        return loadtypeId;
    }

    public void setLoadtypeId(Long loadtypeId) {
        this.loadtypeId = loadtypeId;
    }

    public String getDay() {
        return day;
    }

    public void setDay(String day) {
        this.day = day;
    }

    public String getSlotStartTime() {
        return slotStartTime;
    }

    public void setSlotStartTime(String slotStartTime) {
        this.slotStartTime = slotStartTime;
    }

    public String getSlotEndTime() {
        return slotEndTime;
    }

    public void setSlotEndTime(String slotEndTime) {
        this.slotEndTime = slotEndTime;
    }

    public String getLoadType() {
        return loadType;
    }

    public void setLoadType(String loadType) {
        this.loadType = loadType;
    }

    public String getFacultyName() {
        return facultyName;
    }

    public void setFacultyName(String facultyName) {
        this.facultyName = facultyName;
    }

    public String getSubjectName() {
        return subjectName;
    }

    public void setSubjectName(String subjectName) {
        this.subjectName = subjectName;
    }

    public Long getSubjectId() {
        return subjectId;
    }

    public void setSubjectId(Long subjectId) {
        this.subjectId = subjectId;
    }

    public String getBatch() {
        return batch;
    }

    public void setBatch(String batch) {
        this.batch = batch;
    }
 
}
