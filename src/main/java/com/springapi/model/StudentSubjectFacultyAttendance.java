package com.springapi.model;

import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * @author Jyoti Patel
 */
@Entity
@Table(name = "StudentSubjectFacultyAttendance")
public class StudentSubjectFacultyAttendance extends BaseEntity {

    private Long studentId;
    private Long facultyId;
    private Long semId;
    private Long subjectId;
    private Long dayId;
    private String batch;
    private Boolean status = false;
    private Long slotId;

    public Long getStudentId() {
        return studentId;
    }

    public void setStudentId(Long studentId) {
        this.studentId = studentId;
    }

    public Long getFacultyId() {
        return facultyId;
    }

    public void setFacultyId(Long facultyId) {
        this.facultyId = facultyId;
    }

    public Long getSemId() {
        return semId;
    }

    public void setSemId(Long semId) {
        this.semId = semId;
    }

    public Long getSubjectId() {
        return subjectId;
    }

    public void setSubjectId(Long subjectId) {
        this.subjectId = subjectId;
    }

    public Long getDayId() {
        return dayId;
    }

    public void setDayId(Long dayId) {
        this.dayId = dayId;
    }

    public String getBatch() {
        return batch;
    }

    public void setBatch(String batch) {
        this.batch = batch;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public Long getSlotId() {
        return slotId;
    }

    public void setSlotId(Long slotId) {
        this.slotId = slotId;
    }
    
    
}
