/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.springapi.model;

import javax.persistence.Entity;
import javax.persistence.Table;

/**
 *
 * @author shahb
 */
@Entity
@Table(name = "Schedule_Master")
public class ScheduleMaster extends BaseEntity {
    private String scheduleName;
    private Long scheduleType;
    private String scheduleDate;

    public String getScheduleName() {
        return scheduleName;
    }

    public void setScheduleName(String scheduleName) {
        this.scheduleName = scheduleName;
    }

    public Long getScheduleType() {
        return scheduleType;
    }

    public void setScheduleType(Long scheduleType) {
        this.scheduleType = scheduleType;
    }

    public String getScheduleDate() {
        return scheduleDate;
    }

    public void setScheduleDate(String scheduleDate) {
        this.scheduleDate = scheduleDate;
    }
    
}
