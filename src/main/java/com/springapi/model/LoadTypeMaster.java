package com.springapi.model;

import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * @author Jyoti Patel
 */
@Entity
@Table(name = "LoadTypeMaster")
public class LoadTypeMaster extends BaseEntity {
    
    private String loadType;

    public String getLoadType() {
        return loadType;
    }

    public void setLoadType(String loadType) {
        this.loadType = loadType;
    }
}
