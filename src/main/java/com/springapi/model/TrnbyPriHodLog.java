package com.springapi.model;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import lombok.Data;
@Data
@Entity
@Table(name = "trn_by_priHod_log")
public class TrnbyPriHodLog extends BaseEntity {
	
	
	private Date attendanceDate;
	private Long actlogId;
	private Long masId;
	private Long fId;
	private Long byId;
	private Boolean markType;
	
	
}
