/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.springapi.model;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.Table;
import lombok.Data;

/**
 *
 * @author shahb
 */
@Data
@Entity
@Table(name = "Institute_Loc_Fancing_Info")
public class InstituteLocFancingInfo extends BaseEntity implements Serializable{
    private Long instituteId;
    private Long alogId;
    private Long fId;
    private Double locLat1;
    private Double locLng1;
    
    private Double locLat2;
    private Double locLng2;
    
    private Double locLat3;
    private Double locLng3;
    
    private Double locLat4;
    private Double locLng4;
    
    private Double locLat5;
    private Double locLng5;
    
    
    private Double centerLocLat;
    private Double centerLocLng;
    
    private Double totalArea;
    private Double radius;
    
    private Boolean isActive=true;
    private Boolean isDeleted=false;

    public Long getfId() {
        return fId;
    }

    public void setfId(Long fId) {
        this.fId = fId;
    }
    
    
    
}
