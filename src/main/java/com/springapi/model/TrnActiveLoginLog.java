package com.springapi.model;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Table;
import lombok.Data;

@Entity
@Data
@Table(name ="trn_active_login_log")
public class TrnActiveLoginLog extends BaseEntity{
	
	private Date loginDate;
	private String deviceId;
	private Long masId;
	private Long fId;
	private Boolean isActive = Boolean.TRUE;
	private Boolean isDeleted = Boolean.FALSE;
	private String token;
	
	
	

}
