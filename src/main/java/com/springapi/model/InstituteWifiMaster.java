package com.springapi.model;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Transient;

/**
 *
 * @author Jyoti Patel
 */
@Entity
@Table(name = "Institute_Wifi_Master")
public class InstituteWifiMaster extends BaseEntity {

    private Long instituteId;

    private String bssid;
    
    private String ssid;

    private String mac;

    @Transient
    private InstituteMaster instituteMaster;

   public InstituteWifiMaster() {
    }

    public InstituteWifiMaster(Long id, String bssid, String mac, Long instituteId, String name,
            String link, Long code,
            Long universityId, String uni_name, Long uni_code, String uni_link
    ) {
        setId(id);
        setBssid(bssid);
        setMac(mac);
        setInstituteId(instituteId);

        instituteMaster = new InstituteMaster();
        instituteMaster.setCode(code);
        instituteMaster.setLink(link);
        instituteMaster.setName(name);
        instituteMaster.setUniversityId(universityId);

        UniversityMaster universityMaster = new UniversityMaster();
        universityMaster.setCode(uni_code);
        universityMaster.setLink(uni_link);
        universityMaster.setName(uni_name);
        instituteMaster.setUniversityMaster(universityMaster);
        
        setInstituteMaster(instituteMaster);

    }

    public Long getInstituteId() {
        return instituteId;
    }

    public void setInstituteId(Long instituteId) {
        this.instituteId = instituteId;
    }

    public String getBssid() {
        return bssid;
    }

    public void setBssid(String bssid) {
        this.bssid = bssid;
    }

    public String getMac() {
        return mac;
    }

    public void setMac(String mac) {
        this.mac = mac;
    }

    public InstituteMaster getInstituteMaster() {
        return instituteMaster;
    }

    public void setInstituteMaster(InstituteMaster instituteMaster) {
        this.instituteMaster = instituteMaster;
    }

    public String getSsid() {
        return ssid;
    }

    public void setSsid(String ssid) {
        this.ssid = ssid;
    }
}
