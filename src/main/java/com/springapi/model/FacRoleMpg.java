package com.springapi.model;

import com.springapi.model.BaseEntity;
import javax.persistence.Entity;
import javax.persistence.Table;
import lombok.Data;

@Data
@Entity
@Table(name = "faculty_role_mpg")
public class FacRoleMpg extends BaseEntity{
	 
	    private Long fId;	
	    private Long instituteId; 	   
	    private Long departmentId;
	    private Long role;	
	    private Integer isMaster;	
	    private Boolean isActive = Boolean.FALSE;
	    private Boolean isDeleted = Boolean.FALSE;
		
	    public FacRoleMpg() {
			super();
		}

	/*
	 * public FacRoleMpg(Long fId, Long instituteId, Long departmentId, Long role,
	 * Integer isMaster, Boolean isActive, Boolean isDeleted) { super(); this.fId =
	 * fId; this.instituteId = instituteId; this.departmentId = departmentId;
	 * this.role = role; this.isMaster = isMaster; this.isActive = isActive;
	 * this.isDeleted = isDeleted; }
	 */

		public Long getfId() {
			return fId;
		}

		public void setfId(Long fId) {
			this.fId = fId;
		}

		public Long getInstituteId() {
			return instituteId;
		}

		public void setInstituteId(Long instituteId) {
			this.instituteId = instituteId;
		}

		public Long getDepartmentId() {
			return departmentId;
		}

		public void setDepartmentId(Long departmentId) {
			this.departmentId = departmentId;
		}

		public Long getRole() {
			return role;
		}

		public void setRole(Long role) {
			this.role = role;
		}

		public Integer getIsMaster() {
			return isMaster;
		}

		public void setIsMaster(Integer isMaster) {
			this.isMaster = isMaster;
		}

		public Boolean getIsActive() {
			return isActive;
		}

		public void setIsActive(Boolean isActive) {
			this.isActive = isActive;
		}

		public Boolean getIsDeleted() {
			return isDeleted;
		}

		public void setIsDeleted(Boolean isDeleted) {
			this.isDeleted = isDeleted;
		}
}
