/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.springapi.model;

import javax.persistence.Entity;
import javax.persistence.Table;

/**
 *
 * @author shahb
 */
@Entity
@Table(name = "Faculty_Shift_Mpg")
public class FacultyShiftMpg extends BaseEntity {

    private Long fId;
    private Long shiftId;
    private Long termId;
    private Boolean isActive = true;
    private Boolean isDeleted = false;
    private Long instituteId;
    private Long shiftTypeId = 1L;
    private Long day1 = 0L;
    private Long day2 = 0L;
    private Long day3 = 0L;
    private Long day4 = 0L;
    private Long day5 = 0L;
    private Long day6 = 0L;

    public Long getfId() {
        return fId;
    }

    public void setfId(Long fId) {
        this.fId = fId;
    }

    public Long getShiftId() {
        return shiftId;
    }

    public void setShiftId(Long shiftId) {
        this.shiftId = shiftId;
    }

    public Long getTermId() {
        return termId;
    }

    public void setTermId(Long termId) {
        this.termId = termId;
    }

    public Boolean getIsActive() {
        return isActive;
    }

    public void setIsActive(Boolean isActive) {
        this.isActive = isActive;
    }

    public Boolean getIsDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(Boolean isDeleted) {
        this.isDeleted = isDeleted;
    }

    public Long getInstituteId() {
        return instituteId;
    }

    public void setInstituteId(Long instituteId) {
        this.instituteId = instituteId;
    }

    public Long getShiftTypeId() {
        return shiftTypeId;
    }

    public void setShiftTypeId(Long shiftTypeId) {
        this.shiftTypeId = shiftTypeId;
    }

    public Long getDay1() {
        return day1;
    }

    public void setDay1(Long day1) {
        this.day1 = day1;
    }

    public Long getDay2() {
        return day2;
    }

    public void setDay2(Long day2) {
        this.day2 = day2;
    }

    public Long getDay3() {
        return day3;
    }

    public void setDay3(Long day3) {
        this.day3 = day3;
    }

    public Long getDay4() {
        return day4;
    }

    public void setDay4(Long day4) {
        this.day4 = day4;
    }

    public Long getDay5() {
        return day5;
    }

    public void setDay5(Long day5) {
        this.day5 = day5;
    }

    public Long getDay6() {
        return day6;
    }

    public void setDay6(Long day6) {
        this.day6 = day6;
    }

}
