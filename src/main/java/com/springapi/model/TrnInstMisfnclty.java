/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.springapi.model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Temporal;

/**
 *
 * @author shahb
 */
@Entity
@Table(name = "trn_inst_misfnclty")
public class TrnInstMisfnclty extends BaseEntity implements Serializable {

    private Long instId;

    private Long funcTypeId;

    private Long applicableToId;

    @Temporal(javax.persistence.TemporalType.DATE)
    private Date fromDate;

    @Temporal(javax.persistence.TemporalType.DATE)
    private Date toDate;

    private String docPath;
    private String startTime;

    private String endTime;

    private Long year;

    private String holidayName;

    private String remarks;

    private Boolean isDeleted;

    private Boolean isActive;

    private Long deletedBy;

    private String deletedAt;

    public Long getInstId() {
        return instId;
    }

    public void setInstId(Long instId) {
        this.instId = instId;
    }

    public Long getFuncTypeId() {
        return funcTypeId;
    }

    public void setFuncTypeId(Long funcTypeId) {
        this.funcTypeId = funcTypeId;
    }

    public Long getApplicableToId() {
        return applicableToId;
    }

    public void setApplicableToId(Long applicableToId) {
        this.applicableToId = applicableToId;
    }

    public Date getFromDate() {
        return fromDate;
    }

    public void setFromDate(Date fromDate) {
        this.fromDate = fromDate;
    }

    public Date getToDate() {
        return toDate;
    }

    public void setToDate(Date toDate) {
        this.toDate = toDate;
    }

    public String getDocPath() {
        return docPath;
    }

    public void setDocPath(String docPath) {
        this.docPath = docPath;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public Long getYear() {
        return year;
    }

    public void setYear(Long year) {
        this.year = year;
    }

    public String getHolidayName() {
        return holidayName;
    }

    public void setHolidayName(String holidayName) {
        this.holidayName = holidayName;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public Boolean getIsDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(Boolean isDeleted) {
        this.isDeleted = isDeleted;
    }

    public Boolean getIsActive() {
        return isActive;
    }

    public void setIsActive(Boolean isActive) {
        this.isActive = isActive;
    }

    public Long getDeletedBy() {
        return deletedBy;
    }

    public void setDeletedBy(Long deletedBy) {
        this.deletedBy = deletedBy;
    }

    public String getDeletedAt() {
        return deletedAt;
    }

    public void setDeletedAt(String deletedAt) {
        this.deletedAt = deletedAt;
    }

   

   
}
