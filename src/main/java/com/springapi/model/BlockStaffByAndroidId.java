/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.springapi.model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.Table;
import lombok.Data;

/**
 *
 * @author shahb
 */
@Data
@Entity
@Table(name = "Block_Staff_By_AndroidId")
public class BlockStaffByAndroidId extends BaseEntity implements Serializable{
    
    private Date blockDate;
    private String androidId;
    private Long fId;
    private Boolean isActive;
    private Boolean isDeleted;
    private String remark;
          
}
