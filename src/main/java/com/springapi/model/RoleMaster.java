package com.springapi.model;

import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * @author Jyoti_patel
 */

@Entity
@Table(name = "RoleMaster")
public class RoleMaster extends BaseEntity {
    
    private String role;

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }
}
