/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.springapi.model;

import javax.persistence.Entity;
import javax.persistence.Table;

/**
 *
 * @author shahb
 */

@Entity
@Table(name = "SubjectFaculty")
public class SubjectFaculty extends BaseEntity {

//  @NotBlank
    private Long fId;

//    @NotBlank
    private Long sId;

    public Long getfId() {
        return fId;
    }

    public void setfId(Long fId) {
        this.fId = fId;
    }

    public Long getsId() {
        return sId;
    }

    public void setsId(Long sId) {
        this.sId = sId;
    }

}
