package com.springapi.model;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Transient;

/**
 * @author Jyoti_patel
 */
@Entity
@Table(name = "StudentAttendaceCount")
public class StudentAttendaceCount extends BaseEntity {
    private Long facultyId;
    
    private Long slotId;
    
    private Long semId;
    
    private Long dayId;
    
    private Long totPresent;
    
    private String attendanceDate;
    
    private Long totalCount;
    
    private String subjectName;
    
    private String batchName;
    
    private String lecOrLab;
    
    @Transient
    private String facultyName;
    
    @Transient
    private String slotName;

    public StudentAttendaceCount() {
    }

    public StudentAttendaceCount(Long facultyId, Long slotId, Long semId, Long dayId, Long totPresent, 
            String facultyName, String slotName, String attendanceDate, Long totalCount, String subjectName,
            String batchName, String lecOrLab) {
        this.facultyId = facultyId;
        this.slotId = slotId;
        this.semId = semId;
        this.dayId = dayId;
        this.totPresent = totPresent;
        this.facultyName = facultyName;
        this.slotName = slotName;
        this.attendanceDate = attendanceDate;
        this.totalCount = totalCount;
        this.subjectName = subjectName;
        this.batchName = batchName;
        this.lecOrLab = lecOrLab;
    }
    
    public Long getFacultyId() {
        return facultyId;
    }

    public void setFacultyId(Long facultyId) {
        this.facultyId = facultyId;
    }

    public Long getSlotId() {
        return slotId;
    }

    public void setSlotId(Long slotId) {
        this.slotId = slotId;
    }

    public Long getSemId() {
        return semId;
    }

    public void setSemId(Long semId) {
        this.semId = semId;
    }

    public Long getDayId() {
        return dayId;
    }

    public void setDayId(Long dayId) {
        this.dayId = dayId;
    }

    public Long getTotPresent() {
        return totPresent;
    }

    public void setTotPresent(Long totPresent) {
        this.totPresent = totPresent;
    }

    public String getFacultyName() {
        return facultyName;
    }

    public void setFacultyName(String facultyName) {
        this.facultyName = facultyName;
    }

    public String getSlotName() {
        return slotName;
    }

    public void setSlotName(String slotName) {
        this.slotName = slotName;
    }

    public String getAttendanceDate() {
        return attendanceDate;
    }

    public void setAttendanceDate(String attendanceDate) {
        this.attendanceDate = attendanceDate;
    }

    public Long getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(Long totalCount) {
        this.totalCount = totalCount;
    }

    public String getSubjectName() {
        return subjectName;
    }

    public void setSubjectName(String subjectName) {
        this.subjectName = subjectName;
    }

    public String getBatchName() {
        return batchName;
    }

    public void setBatchName(String batchName) {
        this.batchName = batchName;
    }

    public String getLecOrLab() {
        return lecOrLab;
    }

    public void setLecOrLab(String lecOrLab) {
        this.lecOrLab = lecOrLab;
    }
    
}
