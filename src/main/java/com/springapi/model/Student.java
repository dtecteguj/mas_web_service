package com.springapi.model;

import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Transient;

/**
 * @author shahb
 */
@Entity
@Table(name = "Student")
public class Student extends BaseEntity {

//    @NotBlank
    private String name;

//    @NotBlank
    private String email;

//    @NotBlank
    private String phone;

//    @NotBlank
    private String sEnrollemnt;

//    @NotBlank
    private Long sem;

//    @NotBlank
    private String password;

//    @NotBlank
    private String passwordHint;

    private Long instituteId;

    private Long instituteTypeId;

    private Long departmentId;

    private Boolean isDeleted = false;

    private String fName;
    private String mName;
    private String lName;
    
    private Long termId;
    
    private Long courseId;
    
    private Long divisonId;
    
    private Date dob;
    
    private String address;
    
    private String caste;
    
    private String admitedYear;
    
    private Boolean isActive = false;
    
    private Boolean isPassout = false;
    

    @Transient
    private String semName;

    public Student() {
    }

    public Student(String name, String email, String phone, String sEnrollemnt, Long sem, String password, String passwordHint) {
        this.name = name;
        this.email = email;
        this.phone = phone;
        this.sEnrollemnt = sEnrollemnt;
        this.sem = sem;
        this.password = password;
        this.passwordHint = passwordHint;
    }

    public Student(Long id, String name, String email, String phone, String sEnrollemnt, Long sem,
            String semName) {
        this.id = id;
        this.name = name;
        this.email = email;
        this.phone = phone;
        this.sEnrollemnt = sEnrollemnt;
        this.sem = sem;
        this.semName = semName;
    }

    public String getsEnrollemnt() {
        return sEnrollemnt;
    }

    public void setsEnrollemnt(String sEnrollemnt) {
        this.sEnrollemnt = sEnrollemnt;
    }

    public Long getSem() {
        return sem;
    }

    public void setSem(Long sem) {
        this.sem = sem;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPasswordHint() {
        return passwordHint;
    }

    public void setPasswordHint(String passwordHint) {
        this.passwordHint = passwordHint;
    }

    public String getSemName() {
        return semName;
    }

    public void setSemName(String semName) {
        this.semName = semName;
    }

    public Long getInstituteId() {
        return instituteId;
    }

    public void setInstituteId(Long instituteId) {
        this.instituteId = instituteId;
    }

    public Long getInstituteTypeId() {
        return instituteTypeId;
    }

    public void setInstituteTypeId(Long instituteTypeId) {
        this.instituteTypeId = instituteTypeId;
    }

    public Long getDepartmentId() {
        return departmentId;
    }

    public void setDepartmentId(Long departmentId) {
        this.departmentId = departmentId;
    }

    public Boolean getIsDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(Boolean isDeleted) {
        this.isDeleted = isDeleted;
    }

    

    public String getfName() {
        return fName;
    }

    public void setfName(String fName) {
        this.fName = fName;
    }

    public String getmName() {
        return mName;
    }

    public void setmName(String mName) {
        this.mName = mName;
    }

    public String getlName() {
        return lName;
    }

    public void setlName(String lName) {
        this.lName = lName;
    }

    public Long getTermId() {
        return termId;
    }

    public void setTermId(Long termId) {
        this.termId = termId;
    }

    public Long getCourseId() {
        return courseId;
    }

    public void setCourseId(Long courseId) {
        this.courseId = courseId;
    }

    public Long getDivisonId() {
        return divisonId;
    }

    public void setDivisonId(Long divisonId) {
        this.divisonId = divisonId;
    }

    public Date getDob() {
        return dob;
    }

    public void setDob(Date dob) {
        this.dob = dob;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCaste() {
        return caste;
    }

    public void setCaste(String caste) {
        this.caste = caste;
    }

    public String getAdmitedYear() {
        return admitedYear;
    }

    public void setAdmitedYear(String admitedYear) {
        this.admitedYear = admitedYear;
    }

    public Boolean getIsActive() {
        return isActive;
    }

    public void setIsActive(Boolean isActive) {
        this.isActive = isActive;
    }

    public Boolean getIsPassout() {
        return isPassout;
    }

    public void setIsPassout(Boolean isPassout) {
        this.isPassout = isPassout;
    }
    
    

}
