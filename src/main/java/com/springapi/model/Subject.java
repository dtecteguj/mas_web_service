package com.springapi.model;

import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * @author shahb
 */
@Entity
@Table(name = "Subject")
public class Subject extends BaseEntity {

    private String sName;

    private Long sCode;

    private Long sSem;

    private String sPictureLink;

    public String getsName() {
        return sName;
    }

    public void setsName(String sName) {
        this.sName = sName;
    }

    public Long getsCode() {
        return sCode;
    }

    public void setsCode(Long sCode) {
        this.sCode = sCode;
    }

    public Long getsSem() {
        return sSem;
    }

    public void setsSem(Long sSem) {
        this.sSem = sSem;
    }

    public String getsPictureLink() {
        return sPictureLink;
    }

    public void setsPictureLink(String sPictureLink) {
        this.sPictureLink = sPictureLink;
    }

}
