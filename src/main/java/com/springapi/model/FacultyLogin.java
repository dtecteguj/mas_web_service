package com.springapi.model;

import java.util.Date;
import javax.persistence.Column;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import lombok.Data;

@Data
@Entity
@Table(name = "Faculty_Login")
public class FacultyLogin extends BaseEntity {

    private Date attendanceDate;
    @NotNull
    private String deviceId;
    @NotNull
    private Long fId;
    @NotNull@Column(unique = true)
    private String email;
    @NotNull
    private String password;
    private Boolean isBlock=false;
    private String blockDevId;
    private Boolean isActive=true;
    private Boolean isDeleted=false;
    private Boolean isFirstTime=true;
    private Long resetCount=0L;

}
