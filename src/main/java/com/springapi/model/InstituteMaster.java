package com.springapi.model;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;

/**
 *
 * @author Jyoti Patel
 */
@Entity
@Table(name = "Institute_Master")
public class InstituteMaster extends BaseEntity {

    @NotNull
    private String name;

    private String link;

    private Long universityId;

    private Long code;
    
    private Long instituteTypeId;
    
    private String shortName;
    
    private String address;
    
    private String district;
    
    private Long pincode;
    
    private Boolean isActive = false;
    
    private Boolean isDeleted = false;
    
    private Boolean isLocActive = false;

    public Boolean getIsLocActive() {
        return isLocActive;
    }

    public void setIsLocActive(Boolean isLocActive) {
        this.isLocActive = isLocActive;
    }

    private String email;
    
    private String phone;
    
    @Transient
    private UniversityMaster universityMaster;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public Long getUniversityId() {
        return universityId;
    }

    public void setUniversityId(Long universityId) {
        this.universityId = universityId;
    }

    public Long getCode() {
        return code;
    }

    public void setCode(Long code) {
        this.code = code;
    }

    public UniversityMaster getUniversityMaster() {
        return universityMaster;
    }

    public void setUniversityMaster(UniversityMaster universityMaster) {
        this.universityMaster = universityMaster;
    }

    public Long getInstituteTypeId() {
        return instituteTypeId;
    }

    public void setInstituteTypeId(Long instituteTypeId) {
        this.instituteTypeId = instituteTypeId;
    }
    
    public String getShortName() {
        return shortName;
    }

    public void setShortName(String shortName) {
        this.shortName = shortName;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getDistrict() {
        return district;
    }

    public void setDistrict(String district) {
        this.district = district;
    }

    public Long getPincode() {
        return pincode;
    }

    public void setPincode(Long pincode) {
        this.pincode = pincode;
    }

    public Boolean getIsActive() {
        return isActive;
    }

    public void setIsActive(Boolean isActive) {
        this.isActive = isActive;
    }

    public Boolean getIsDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(Boolean isDeleted) {
        this.isDeleted = isDeleted;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }
    
    
}
