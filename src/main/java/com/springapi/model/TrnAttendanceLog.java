package com.springapi.model;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import lombok.Data;

@Data
@Entity
@Table(name = "trn_attendance_log")
public class TrnAttendanceLog extends BaseEntity{	
	
	private Date attendanceDate;
	private Long actlogId;
	private Long masId;
	private Long fId;
	private String BSSID;
	private String SSID;
	private Double locLat;
	private Double locLong;
	private Boolean attType=false;
	private Boolean markType=false;
        private String deviceId;
        private String deviceTypeId;
        
        
	
	
		
}
