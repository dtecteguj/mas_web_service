/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.springapi.model;

import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Temporal;

/**
 *
 * @author shahb
 */
@Entity
@Table(name = "Trn_Transfer_Dep_Dtls")
public class TrnTransferDtls extends BaseEntity{
    private Long fId;
    private Long sourceInstId;
    private Long destiInstId;
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date relieveDate;
    private String trnsftReason;
    private Long transfrType;
    private Boolean isCancelled= false;
    private Long cancelledBy;
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date cancelledOn;

    public TrnTransferDtls() {
    }

    public TrnTransferDtls(Long fId, Long sourceInstId, Long destiInstId, Date relieveDate, String trnsftReason, Long transfrType, Boolean isCancelled, Long cancelledBy, Date cancelledOn) {
        this.fId = fId;
        this.sourceInstId = sourceInstId;
        this.destiInstId = destiInstId;
        this.relieveDate = relieveDate;
        this.trnsftReason = trnsftReason;
        this.transfrType = transfrType;
        this.isCancelled = isCancelled;
        this.cancelledBy = cancelledBy;
        this.cancelledOn = cancelledOn;
    }

    
    
    public Long getfId() {
        return fId;
    }

    public void setfId(Long fId) {
        this.fId = fId;
    }

    public Long getSourceInstId() {
        return sourceInstId;
    }

    public void setSourceInstId(Long sourceInstId) {
        this.sourceInstId = sourceInstId;
    }

    public Long getDestiInstId() {
        return destiInstId;
    }

    public void setDestiInstId(Long destiInstId) {
        this.destiInstId = destiInstId;
    }

    public Date getRelieveDate() {
        return relieveDate;
    }

    public void setRelieveDate(Date relieveDate) {
        this.relieveDate = relieveDate;
    }

    public String getTrnsftReason() {
        return trnsftReason;
    }

    public void setTrnsftReason(String trnsftReason) {
        this.trnsftReason = trnsftReason;
    }

    public Long getTransfrType() {
        return transfrType;
    }

    public void setTransfrType(Long transfrType) {
        this.transfrType = transfrType;
    }

    public Boolean getIsCancelled() {
        return isCancelled;
    }

    public void setIsCancelled(Boolean isCancelled) {
        this.isCancelled = isCancelled;
    }

    public Long getCancelledBy() {
        return cancelledBy;
    }

    public void setCancelledBy(Long cancelledBy) {
        this.cancelledBy = cancelledBy;
    }

    public Date getCancelledOn() {
        return cancelledOn;
    }

    public void setCancelledOn(Date cancelledOn) {
        this.cancelledOn = cancelledOn;
    }
    
    
}
