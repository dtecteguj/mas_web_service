/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.springapi.model;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Transient;

/**
 *
 * @author shahb
 */
@Entity
@Table(name = "Program_Mst")
public class ProgramMst extends BaseEntity {

    private String code;
    private String progLevel;
    private String name;
    private String studyType;
    private Long acdmctType;
    private Long duration;
    private Long deptType;
    private String progDesc;
    @Transient
    private String shiftName;
    @Transient
    private Long shiftCount;
    @Transient
    private Long intake;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getLevel() {
        return progLevel;
    }

    public void setLevel(String progLevel) {
        this.progLevel = progLevel;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getStudyType() {
        return studyType;
    }

    public void setStudyType(String studyType) {
        this.studyType = studyType;
    }

    public Long getAcdmctType() {
        return acdmctType;
    }

    public void setAcdmctType(Long acdmctType) {
        this.acdmctType = acdmctType;
    }

    public Long getDuration() {
        return duration;
    }

    public void setDuration(Long duration) {
        this.duration = duration;
    }

    public Long getDeptType() {
        return deptType;
    }

    public void setDeptType(Long deptType) {
        this.deptType = deptType;
    }

    public String getProgDesc() {
        return progDesc;
    }

    public void setProgDesc(String progDesc) {
        this.progDesc = progDesc;
    }

    public String getProgLevel() {
        return progLevel;
    }

    public void setProgLevel(String progLevel) {
        this.progLevel = progLevel;
    }

    public String getShiftName() {
        return shiftName;
    }

    public void setShiftName(String shiftName) {
        this.shiftName = shiftName;
    }

    public Long getShiftCount() {
        return shiftCount;
    }

    public void setShiftCount(Long shiftCount) {
        this.shiftCount = shiftCount;
    }

    public Long getIntake() {
        return intake;
    }

    public void setIntake(Long intake) {
        this.intake = intake;
    }

    public ProgramMst(){}
    
    @Override
    public String toString() {
        return "ProgramMst{" + "code=" + code + ", progLevel=" + progLevel + ", name=" + name + ", studyType=" + studyType + ", acdmctType=" + acdmctType + ", duration=" + duration + ", deptType=" + deptType + ", progDesc=" + progDesc + ", shiftName=" + shiftName + ", shiftCount=" + shiftCount + ", intake=" + intake + '}';
    }

    public ProgramMst(Long id, String code, String progLevel, String name, String studyType, Long acdmctType, Long duration, Long deptType, String progDesc, String shiftName, Long shiftCount, Long intake) {
        super(id);
        this.code = code;
        this.progLevel = progLevel;
        this.name = name;
        this.studyType = studyType;
        this.acdmctType = acdmctType;
        this.duration = duration;
        this.deptType = deptType;
        this.progDesc = progDesc;
        this.shiftName = shiftName;
        this.shiftCount = shiftCount;
        this.intake = intake;
    }

}
