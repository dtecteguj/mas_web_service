/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.springapi.model;

import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 *
 * @author shahb
 */
@Entity
@Table(name = "Trn_Dlt_Deputation")
public class TrnDltDeputation extends BaseEntity {
   private Long fId;
   private Boolean typeDeputation = false;
    private Date depStartDate;
    private Date depEndDate;
    private Long parentInstId;
    private Long deputedInstId;
    private String deputationPlace;
    private Boolean isCancelled = false;
    private Long cancelledBy;
    private Boolean day1 = false;
    private Boolean day2 = false;
    private Boolean day3 = false;
    private Boolean day4 = false;
    private Boolean day5 = false;
    private Boolean day6 = false;
    private Boolean day7 = false;

    public Long getfId() {
        return fId;
    }

    public void setfId(Long fId) {
        this.fId = fId;
    }

    public Boolean getTypeDeputation() {
        return typeDeputation;
    }

    public void setTypeDeputation(Boolean typeDeputation) {
        this.typeDeputation = typeDeputation;
    }

    public Date getDepStartDate() {
        return depStartDate;
    }

    public void setDepStartDate(Date depStartDate) {
        this.depStartDate = depStartDate;
    }

    public Date getDepEndDate() {
        return depEndDate;
    }

    public void setDepEndDate(Date depEndDate) {
        this.depEndDate = depEndDate;
    }

    public Long getParentInstId() {
        return parentInstId;
    }

    public void setParentInstId(Long parentInstId) {
        this.parentInstId = parentInstId;
    }

    public Long getDeputedInstId() {
        return deputedInstId;
    }

    public void setDeputedInstId(Long deputedInstId) {
        this.deputedInstId = deputedInstId;
    }

    public String getDeputationPlace() {
        return deputationPlace;
    }

    public void setDeputationPlace(String deputationPlace) {
        this.deputationPlace = deputationPlace;
    }

    public Boolean getIsCancelled() {
        return isCancelled;
    }

    public void setIsCancelled(Boolean isCancelled) {
        this.isCancelled = isCancelled;
    }

    public Long getCancelledBy() {
        return cancelledBy;
    }

    public void setCancelledBy(Long cancelledBy) {
        this.cancelledBy = cancelledBy;
    }

    public Boolean getDay1() {
        return day1;
    }

    public void setDay1(Boolean day1) {
        this.day1 = day1;
    }

    public Boolean getDay2() {
        return day2;
    }

    public void setDay2(Boolean day2) {
        this.day2 = day2;
    }

    public Boolean getDay3() {
        return day3;
    }

    public void setDay3(Boolean day3) {
        this.day3 = day3;
    }

    public Boolean getDay4() {
        return day4;
    }

    public void setDay4(Boolean day4) {
        this.day4 = day4;
    }

    public Boolean getDay5() {
        return day5;
    }

    public void setDay5(Boolean day5) {
        this.day5 = day5;
    }

    public Boolean getDay6() {
        return day6;
    }

    public void setDay6(Boolean day6) {
        this.day6 = day6;
    }

    public Boolean getDay7() {
        return day7;
    }

    public void setDay7(Boolean day7) {
        this.day7 = day7;
    }
    
    
}
