/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.springapi.model;

import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Temporal;

/**
 *
 * @author shahb
 */
@Entity
@Table(name = "Trn_Dtl_Issue_Memo")
public class TrnDtlIssueMemo extends BaseEntity {

    private Long typeId;
    private Long fId;
    private Long instituteId;
    private Long memoCount;
    private Long memoLotNo;
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date lotCreationDate;
    private Long issueYear;
    private Boolean isDeleted=false;
    private Long trnId;
    private Boolean isSubmitted = false;
    private String memoReason ;
    private Boolean isEndorse = false ;
    private Long endorsedBy;
    private String remark;

    public Long getTypeId() {
        return typeId;
    }

    public void setTypeId(Long typeId) {
        this.typeId = typeId;
    }

    public Long getfId() {
        return fId;
    }

    public void setfId(Long fId) {
        this.fId = fId;
    }

    public Long getMemoCount() {
        return memoCount;
    }

    public void setMemoCount(Long memoCount) {
        this.memoCount = memoCount;
    }

    public Long getMemoLotNo() {
        return memoLotNo;
    }

    public void setMemoLotNo(Long memoLotNo) {
        this.memoLotNo = memoLotNo;
    }

    public Date getLotCreationDate() {
        return lotCreationDate;
    }

    public void setLotCreationDate(Date lotCreationDate) {
        this.lotCreationDate = lotCreationDate;
    }

    public Long getIssueYear() {
        return issueYear;
    }

    public void setIssueYear(Long issueYear) {
        this.issueYear = issueYear;
    }

    public Boolean getIsDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(Boolean isDeleted) {
        this.isDeleted = isDeleted;
    }

    public Long getTrnId() {
        return trnId;
    }

    public void setTrnId(Long trnId) {
        this.trnId = trnId;
    }

    public Boolean getIsSubmitted() {
        return isSubmitted;
    }

    public void setIsSubmitted(Boolean isSubmitted) {
        this.isSubmitted = isSubmitted;
    }

    public String getMemoReason() {
        return memoReason;
    }

    public void setMemoReason(String memoReason) {
        this.memoReason = memoReason;
    }

    public Boolean getIsEndorse() {
        return isEndorse;
    }

    public void setIsEndorse(Boolean isEndorse) {
        this.isEndorse = isEndorse;
    }

    public Long getEndorsedBy() {
        return endorsedBy;
    }

    public void setEndorsedBy(Long endorsedBy) {
        this.endorsedBy = endorsedBy;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public Long getInstituteId() {
        return instituteId;
    }

    public void setInstituteId(Long instituteId) {
        this.instituteId = instituteId;
    }

}
