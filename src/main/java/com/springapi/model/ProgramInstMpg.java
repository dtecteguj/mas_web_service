/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.springapi.model;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Transient;

/**
 *
 * @author shahb
 */
@Entity
@Table(name = "Program_Inst_Mpg")
public class ProgramInstMpg extends BaseEntity {
    private Long prgmId;
    private Long instituteId;
    private Long instituteTypeId;
    private Long instituteDepartmentId;
    private String description;
    private Boolean isDeleted = false;
    private String deleteReason;
    private String shiftName;
    private Long shiftCount;
    private Long intake;
    
    @Transient
    private String departmentName;

    public Long getPrgmId() {
        return prgmId;
    }

    public void setPrgmId(Long prgmId) {
        this.prgmId = prgmId;
    }

    public Long getInstituteId() {
        return instituteId;
    }

    public void setInstituteId(Long instituteId) {
        this.instituteId = instituteId;
    }

    public Long getInstituteTypeId() {
        return instituteTypeId;
    }

    public void setInstituteTypeId(Long instituteTypeId) {
        this.instituteTypeId = instituteTypeId;
    }

    public Long getInstituteDepartmentId() {
        return instituteDepartmentId;
    }

    public void setInstituteDepartmentId(Long instituteDepartmentId) {
        this.instituteDepartmentId = instituteDepartmentId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Boolean getIsDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(Boolean isDeleted) {
        this.isDeleted = isDeleted;
    }

    public String getDeleteReason() {
        return deleteReason;
    }

    public void setDeleteReason(String deleteReason) {
        this.deleteReason = deleteReason;
    }

    public String getShiftName() {
        return shiftName;
    }

    public void setShiftName(String shiftName) {
        this.shiftName = shiftName;
    }

    public Long getShiftCount() {
        return shiftCount;
    }

    public void setShiftCount(Long shiftCount) {
        this.shiftCount = shiftCount;
    }

    public Long getIntake() {
        return intake;
    }

    public void setIntake(Long intake) {
        this.intake = intake;
    }

    @Override
    public String toString() {
        return "ProgramInstMpg{" + "prgmId=" + prgmId + ", instituteId=" + instituteId + ", instituteTypeId=" + instituteTypeId + ", instituteDepartmentId=" + instituteDepartmentId + ", description=" + description + ", isDeleted=" + isDeleted + ", deleteReason=" + deleteReason + ", shiftName=" + shiftName + ", shiftCount=" + shiftCount + ", intake=" + intake + ", departmentName=" + departmentName + '}';
    }

    public ProgramInstMpg(Long id,Long prgmId, Long instituteId, Long instituteTypeId, Long instituteDepartmentId, String description, String shiftName, Long shiftCount, Long intake, String departmentName) {
        super(id);
        this.prgmId = prgmId;
        this.instituteId = instituteId;
        this.instituteTypeId = instituteTypeId;
        this.instituteDepartmentId = instituteDepartmentId;
        this.description = description;
        this.shiftName = shiftName;
        this.shiftCount = shiftCount;
        this.intake = intake;
        this.departmentName = departmentName;
    }

    public String getDepartmentName() {
        return departmentName;
    }

    public void setDepartmentName(String departmentName) {
        this.departmentName = departmentName;
    }
    
    
    
    
}
