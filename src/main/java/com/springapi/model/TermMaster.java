/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.springapi.model;

import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Temporal;

/**
 *
 * @author shahb
 */
@Entity
@Table(name="Term_Master")
public class TermMaster extends BaseEntity{
    
    private String name;
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date termStartDate;
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date termEndDate;
    private Long termYear;
    private Boolean isDeleted = false;
    private Boolean termType = false;
    private Boolean isActive = true;

    public Date getTermStartDate() {
        return termStartDate;
    }

    public void setTermStartDate(Date termStartDate) {
        this.termStartDate = termStartDate;
    }

    public Date getTermEndDate() {
        return termEndDate;
    }

    public void setTermEndDate(Date termEndDate) {
        this.termEndDate = termEndDate;
    }

    public Long getTermYear() {
        return termYear;
    }

    public void setTermYear(Long termYear) {
        this.termYear = termYear;
    }

    public Boolean getIsDeleted() {
        return isDeleted;
    }

        public void setIsDeleted(Boolean isDeleted) {
            this.isDeleted = isDeleted;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public Boolean getTermType() {
            return termType;
        }

        public void setTermType(Boolean termType) {
            this.termType = termType;
        }

        public Boolean getIsActive() {
            return isActive;
        }

        public void setIsActive(Boolean isActive) {
            this.isActive = isActive;
        }


}
