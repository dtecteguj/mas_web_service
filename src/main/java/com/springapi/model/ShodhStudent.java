/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.springapi.model;

import javax.persistence.Entity;
import javax.persistence.Table;
import lombok.Data;

/**
 *
 * @author shahb
 */
@Data
@Entity
@Table(name = "Shodh_Student")
public class ShodhStudent extends BaseEntity{

    private Integer stdUniqueId;
    private Integer fId;
    private String stdSalutation;
    private String stdFirstName;
    private String stdSecondName;
    private String stdLastName;
    private String stdGender;
    private String stdCategory;
    private String stdEmailId;
    private String stdMobileNo;
    private Integer universityId;
    private Integer stdEnrollYear;
    private Boolean stdIsDropped = false;
    private Boolean isActive = true;
    private Boolean isDeleted = false;
    
}
