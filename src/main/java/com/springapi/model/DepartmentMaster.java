/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.springapi.model;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

/**
 *
 * @author Jyoti Patel
 */
@Entity
@Table(name = "Department_Master")
public class DepartmentMaster extends BaseEntity{
    @NotNull
    private String name;

    private String description;

    private Long code;
    
    private Long instituteId;
    
    private Boolean isDeleted = false;
   

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Long getCode() {
        return code;
    }

    public void setCode(Long code) {
        this.code = code;
    }

    public Long getInstituteId() {
        return instituteId;
    }

    public void setInstituteId(Long instituteId) {
        this.instituteId = instituteId;
    }

    public DepartmentMaster(String name, String description, Long code, Long instituteId) {
        this.name = name;
        this.description = description;
        this.code = code;
        this.instituteId = instituteId;
    }

    public Boolean getIsDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(Boolean isDeleted) {
        this.isDeleted = isDeleted;
    }
    
    
    
     public DepartmentMaster(){}
}
