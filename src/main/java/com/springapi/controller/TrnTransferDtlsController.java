/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.springapi.controller;

import com.springapi.model.TrnTransferDtls;
import com.springapi.service.TrnTransferDtlsService;
import java.util.List;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author shahb
 */
@RestController
@RequestMapping("/api")
@CrossOrigin(origins = "*")
public class TrnTransferDtlsController {
    
    @Autowired
    private TrnTransferDtlsService trnTransferDtlsService;
     /* to Save an Faculty*/
    @PostMapping("/trnTransferDtls")
    public TrnTransferDtls createTrnTransferDtls(@Valid @RequestBody TrnTransferDtls trnTransferDtls) {
        return trnTransferDtlsService.create(trnTransferDtls);
    }

    /* get all Faculty */
    @GetMapping("/trnTransferDtls/list")
    public List<TrnTransferDtls> getAllTrnTransferDtls() {
        return trnTransferDtlsService.list();
    }

    @GetMapping("/trnTransferDtls/{id}")
    public TrnTransferDtls readTrnTransferDtls(@PathVariable(value = "id") Long fId) {
        return trnTransferDtlsService.read(fId);
    }

    /* Update an Faculty by FacultyId */
    @PutMapping("/trnTransferDtls")
    public TrnTransferDtls updateTrnTransferDtls(@Valid @RequestBody TrnTransferDtls trnTransferDtls) {
        return trnTransferDtlsService.update(trnTransferDtls);
    }

    /* Delete an Faculty */
    @DeleteMapping("/trnTransferDtls/{id}")
    public String deleteTrnTransferDtls(@PathVariable(value = "id") Long fId) {
        trnTransferDtlsService.delete(fId);
        return "";
    }
    @GetMapping("/trnTransferDtls/active/faculty/{id}")
    public TrnTransferDtls readFacultyTransActiveHist(@PathVariable(value = "id") Long fId) {
        return trnTransferDtlsService.facultyTransActiveHist(fId);
    }
    @GetMapping("/trnTransferDtls/faculty/{id}")
    public List<TrnTransferDtls> readFacultyTransHist(@PathVariable(value = "id") Long fId) {
        return trnTransferDtlsService.facultyTransHist(fId);
    }
    @GetMapping("/trnTransferDtls/institute/{id}")
    public List<TrnTransferDtls> readInstFacTransHist(@PathVariable(value = "id") Long fId) {
        return trnTransferDtlsService.instFacTransHist(fId);
    }
    @GetMapping("/trnTransferDtls/active/institute/{id}")
    public List<TrnTransferDtls> readaInstFacTransActiveHist(@PathVariable(value = "id") Long fId) {
        return trnTransferDtlsService.instFacTransActiveHist(fId);
    }
}
