package com.springapi.controller;

import com.springapi.dto.request.StudentRegistrationRequest;
import com.springapi.dto.request.StudentRequest;
import com.springapi.dto.response.RegistrationResponse;
import com.springapi.model.Student;
import com.springapi.service.StudentService;
import java.util.List;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author shahb
 */
@RestController
@RequestMapping("/api")
public class StudentController {
   
    @Autowired
    private StudentService studentService;

    /* to Save an Student*/
    @PostMapping("/student")
    public Student create(@Valid @RequestBody Student student) {
        return studentService.create(student);
    }

    /* get all Student */
//    @GetMapping("/student/list")
//    public List<Student> getAllStudents() {
//        return studentService.list();
//    }

    @GetMapping("/student/{id}")
    public Student read(@PathVariable(value = "id") Long fId) {
        return studentService.read(fId);
    }

    /* Update an Student by StudentId */
    @PutMapping("/student")
    public Student update(@Valid @RequestBody Student studentDetails) {
        return studentService.update(studentDetails);
    }

    /* Delete an Student */
    @DeleteMapping("/student/{id}")
    public String delete(@PathVariable(value = "id") Long fId) {
        studentService.delete(fId);
        return "";
    }
    
    /* get all Student */
    @PostMapping("/student/list")
    public List<Student> getStudentList(@Valid @RequestBody StudentRequest studentRequest) {
        return studentService.list(studentRequest);
    }
    
     @PostMapping("/student/registration")
    public RegistrationResponse create(@Valid @RequestBody StudentRegistrationRequest studentRegistrationRequest) {
         //System.out.println(studentRegistrationRequest);
        return studentService.studentRegist(studentRegistrationRequest);
    }
}
