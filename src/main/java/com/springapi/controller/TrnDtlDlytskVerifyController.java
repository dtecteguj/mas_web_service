/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.springapi.controller;

import com.springapi.model.TrnDtlDlytskVerify;
import com.springapi.service.TrnDtlDlytskVerifyService;
import java.util.List;
import javax.validation.Valid;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author shahb
 */
@RestController
@RequestMapping("/api")
@CrossOrigin(origins = "*")
public class TrnDtlDlytskVerifyController {
      static Logger logger = Logger.getLogger(ProgramMasterController.class);
     
     @Autowired
     private TrnDtlDlytskVerifyService trnDtlDlytskVerifyService;
     
     /* to Save an ProgramMaster*/
    @PostMapping("/TrnDtlDlytskVerify")
    public TrnDtlDlytskVerify create(@Valid @RequestBody TrnDtlDlytskVerify trnDtlDlytskVerify) {
        logger.debug("create");
        return trnDtlDlytskVerifyService.create(trnDtlDlytskVerify);
    }

    /* get all ProgramMaster */
    @GetMapping("/TrnDtlDlytskVerify/list")
    public List<TrnDtlDlytskVerify> getAllTrnDtlDlytskVerify() {
        logger.debug("list");
        return trnDtlDlytskVerifyService.list();
    }

    @GetMapping("/TrnDtlDlytskVerify/{id}")
    public TrnDtlDlytskVerify read(@PathVariable(value = "id") Long uId) {
        logger.debug("read");
        return trnDtlDlytskVerifyService.read(uId);
    }

    /* Update an ProgramMaster by ProgramMasterId */
    @PutMapping("/TrnDtlDlytskVerify")
    public TrnDtlDlytskVerify update(@Valid @RequestBody TrnDtlDlytskVerify trnDtlDlytskVerify) {
        logger.debug("update");
        return trnDtlDlytskVerifyService.update(trnDtlDlytskVerify);
    }

    /* Delete an ProgramMaster */
    @DeleteMapping("/TrnDtlDlytskVerify/{id}")
    public String delete(@PathVariable(value = "id") Long fId) {
        logger.debug("delete");
        trnDtlDlytskVerifyService.delete(fId);
        return "";
    }
}
