/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.springapi.controller;

import com.springapi.model.Subject;
import com.springapi.service.SubjectService;
import java.util.List;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author shahb
 */
@RestController
@RequestMapping("/gmca")
public class SubjectController {

//  
    @Autowired
    private SubjectService subjectService;

    /* to Save an Subject*/
    @PostMapping("/subject")
    public Subject create(@Valid @RequestBody Subject subject) {
        return subjectService.create(subject);
    }

    /* get all Subject */
    @GetMapping("/subject/list")
    public List<Subject> getAllSubjects() {
        return subjectService.list();
    }

    @GetMapping("/subject/{id}")
    public Subject read(@PathVariable(value = "id") Long sId) {
        return subjectService.read(sId);
    }

    /* Update an Subject by SubjectId */
    @PutMapping("/subject")
    public Subject update(@Valid @RequestBody Subject subjectDetails) {
        return subjectService.update(subjectDetails);
    }

    /* Delete an Subject */
    @DeleteMapping("/subject/{id}")
    public String delete(@PathVariable(value = "id") Long sId) {
        subjectService.delete(sId);
        return "";
    }

}
