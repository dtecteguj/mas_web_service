/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.springapi.controller;

import static com.springapi.controller.LeaveTypeMasterController.logger;
import com.springapi.dto.response.NoticeResponse;
import com.springapi.model.LeaveTypeMaster;
import com.springapi.model.TrnDtlIssueMemo;
import com.springapi.service.LeaveTypeMasterService;
import com.springapi.service.TrnDtlIssueMemoService;
import java.util.List;
import javax.validation.Valid;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author shahb
 */
@RestController
@RequestMapping("/api")
@CrossOrigin(origins = "*")
public class TrnDtlIssueMemoController {
    static Logger logger = Logger.getLogger(TrnDtlIssueMemoController.class);
      @Autowired
    private TrnDtlIssueMemoService  trnDtlIssueMemoService;
    
    @PostMapping("/trnDtlIssueMemo")
    public TrnDtlIssueMemo create(@Valid @RequestBody TrnDtlIssueMemo trnDtlIssueMemo)
    {
        logger.debug("create");
        return trnDtlIssueMemoService.create(trnDtlIssueMemo);
    }
    @GetMapping("/trnDtlIssueMemo/list")
    public List<TrnDtlIssueMemo> getAllFacultyShiftMpg()
    {
        logger.debug("list");
        return trnDtlIssueMemoService.list();
    }
    
    @GetMapping("/trnDtlIssueMemo/{id}")
    public TrnDtlIssueMemo read(@PathVariable(value = "id") Long lId) {
        logger.debug("read");
        return trnDtlIssueMemoService.read(lId);
    }
    
   /* Update an LeaveType Master */
    @PostMapping("/trnDtlIssueMemo/updatebyFaculty")
    public TrnDtlIssueMemo update(@Valid @RequestBody TrnDtlIssueMemo trnDtlIssueMemo) {
        logger.debug("update");
        return trnDtlIssueMemoService.update(trnDtlIssueMemo);
    }

    /* Delete an LeaveType Master */
    @DeleteMapping("/trnDtlIssueMemo/{id}")
    public String delete(@PathVariable(value = "id") Long lId) {
        logger.debug("delete");
        trnDtlIssueMemoService.delete(lId);
        return "";
    }
    @GetMapping("/trnDtlIssueMemo/faculty/{id}")
    public List<NoticeResponse> readbyfid(@PathVariable(value = "id") Long fId) {
        logger.debug("read");
        return trnDtlIssueMemoService.readbyfid(fId);
    }
    @GetMapping("/trnDtlIssueMemo/institute/{id}")
    public List<NoticeResponse> readbyinstid(@PathVariable(value = "id") Long iId) {
        logger.debug("read");
        return trnDtlIssueMemoService.readbyinstid(iId);
    }
}
