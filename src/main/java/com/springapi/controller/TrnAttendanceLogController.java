package com.springapi.controller;

import com.springapi.model.TrnAttendanceLog;
import com.springapi.service.TrnAttendanceLogService;
import java.util.List;

import javax.validation.Valid;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping("/api")
@CrossOrigin(origins = "*")
public class TrnAttendanceLogController {
	
	static Logger logger = Logger.getLogger(TrnAttendanceLogController.class);

	@Autowired
	private TrnAttendanceLogService TrnAttendanceLogService1;
	
	@PostMapping("/TrnAttendanceLog")
	public TrnAttendanceLog create(@Valid @RequestBody TrnAttendanceLog fm) {
		logger.debug("create");
		return TrnAttendanceLogService1.create(fm);
	}

	
	@GetMapping("/TrnAttendanceLog/list")
	public List<TrnAttendanceLog> getAllfacultyRoleMpg() {
		logger.debug("list");
		return TrnAttendanceLogService1.list();
	}

	@GetMapping("/TrnAttendanceLog/{id}")
	public TrnAttendanceLog read(@PathVariable(value = "id") Long id) {
		logger.debug("read");
		return TrnAttendanceLogService1.read(id);
	}

	@PutMapping("/TrnAttendanceLog")
	public TrnAttendanceLog update(@Valid @RequestBody TrnAttendanceLog tal) {
		logger.debug("update");
		return TrnAttendanceLogService1.update(tal);
	}

	@DeleteMapping("/TrnAttendanceLog/{id}")
	public String delete(@PathVariable(value = "id") Long id) {
		logger.debug("delete");
		TrnAttendanceLogService1.delete(id);
		return "";
	}

}
