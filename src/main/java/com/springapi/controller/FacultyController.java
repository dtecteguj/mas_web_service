package com.springapi.controller;

import com.springapi.dto.response.FacultyData;
import com.springapi.dto.response.MessageResponse;
import com.springapi.model.Faculty;
import com.springapi.model.InstituteMaster;
import com.springapi.model.QRCodeData;
import com.springapi.service.FacultyService;
import com.twilio.rest.chat.v1.service.User;
import java.util.List;
import javax.servlet.http.HttpServletResponse;
import com.opencsv.CSVWriter;
import com.opencsv.bean.ColumnPositionMappingStrategy;
import com.opencsv.bean.StatefulBeanToCsv;
import com.opencsv.bean.StatefulBeanToCsvBuilder;
import com.springapi.dto.request.FacultyLStatusRequest;
import com.springapi.dto.response.GeneralListResponse;
import com.springapi.dto.response.InstituteWiseFacultyWithDepartment;
import com.springapi.service.SendEmailService;
import com.springapi.service.impl.FacultyServiceImpl;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author shahb
 */
@RestController
@RequestMapping("/api")
@CrossOrigin(origins = "*")
public class FacultyController {

    @Autowired
    private FacultyService facultyService;
    
     @Autowired
    private SendEmailService sendEmailService;

    /* to Save an Faculty*/
    @PostMapping("/faculty")
    public Faculty create(@Valid @RequestBody Faculty faculty) {
        return facultyService.create(faculty);
    }

    
     /* to Save an Faculty*/
    @PostMapping("/faculty/status/today")
    public GeneralListResponse facultyStatusToday(@Valid @RequestBody FacultyLStatusRequest facultyLStatusRequest) {
        return facultyService.facultyStatusToday(facultyLStatusRequest);
    }

    
    /* get all Faculty */
    @GetMapping("/list")
    public List<Faculty> getAllFacultys() {
        return facultyService.list();
    }

    @GetMapping("/faculty/{id}")
    public Faculty read(@PathVariable(value = "id") Long fId) {
        return facultyService.read(fId);
    }

    /* Update an Faculty by FacultyId */
    @PutMapping("/faculty")
    public Faculty update(@Valid @RequestBody Faculty facultyDetails) {
        return facultyService.update(facultyDetails);
    }

    /* Delete an Faculty */
    @DeleteMapping("/faculty/{id}")
    public Faculty delete(@PathVariable(value = "id") Long fId) {
        return facultyService.delete(fId);

    }

    /* Generate QR code for faculty attendance daily */
    @GetMapping("/generate/QRcode")
    public QRCodeData generateQRCode() {
        return facultyService.generateQRCode();
    }

    // Api for faculty list with present count of today
    @GetMapping("/faculty/list/{instituteId}")
    public List<Faculty> getFacultyByInstitute(@PathVariable(value = "instituteId") Long instituteId) {
        return facultyService.getFacultyByInstitute(instituteId);
    }
    
    @GetMapping("/faculty/list/{instituteId}/{fId}")
    public List<Faculty> getFacultyByHoD(@PathVariable(value = "instituteId") Long instituteId,@PathVariable(value = "fId") Long fId) {
        return facultyService.getFacultyByHoD(instituteId,fId);
    }


    @GetMapping("/faculty/list/present/{instituteId}")
    public List<Faculty> getFacultyPresentByInstitute(@PathVariable(value = "instituteId") Long instituteId){
        return facultyService.getFacultyPresentByInstitute(instituteId);
    }
    
    @GetMapping("/faculty/list/present/{instituteId}/{fId}")
    public List<Faculty> getFacultyPresentByHoD(@PathVariable(value = "instituteId") Long instituteId,@PathVariable(value = "fId") Long fId){
        return facultyService.getFacultyPresentByHoD(instituteId,fId);
    }
    
    
     @GetMapping("/faculty/list/status/{instituteId}")
    public List<Faculty> getFacultyStatusByInstitute(@PathVariable(value = "instituteId") Long instituteId){
        return facultyService.getFacultyStatusByInstitute(instituteId);
    }
    
    @GetMapping("/faculty/list/status/{instituteId}/{fId}")
    public List<Faculty> getFacultyStatusByHoD(@PathVariable(value = "instituteId") Long instituteId,@PathVariable(value = "fId") Long fId){
        return facultyService.getFacultyStatusByHoD(instituteId,fId);
    }

    //Api of list of all faculty list
    @GetMapping("/faculty/list/by/institute/{instituteId}")
    public List<Faculty> getFacultyListByInstitute(@PathVariable(value = "instituteId") Long instituteId) {
        return facultyService.getFacultyListByInstitute(instituteId);
    }

    
    
    //Api of list of all faculty list
    @GetMapping("/faculty/dropdown/by/institute/{instituteId}")
    public List<InstituteWiseFacultyWithDepartment> getFacultyAndDepartmentListByInstituteId(@PathVariable(value = "instituteId") Long instituteId) {
        return facultyService.getFacultyDeptListByInstitute(instituteId);
    }

    
    @GetMapping("/faculty/list/by/{departmentId}")
    public List<Faculty> getFacultyByDepartment(@PathVariable(value = "departmentId") Long departmentId) {
        return facultyService.getFacultyByDepartmentId(departmentId);
    }

    @GetMapping("/faculty/data/{id}")
    public FacultyData readdata(@PathVariable(value = "id") Long fId) {
        return facultyService.readAllData(fId);
    }

    @PostMapping("/faculty/profile/edit")
    public Faculty editdata(@Valid @RequestBody Faculty facultyDetails) {
        return facultyService.editdata(facultyDetails);
    }

    @GetMapping("/faculty/list/by/institute/{instituteId}/{departmentId}")
    public List<Faculty> getFacultyListByInstituteDepartment(@PathVariable(value = "instituteId") Long instituteId, @PathVariable(value = "departmentId") Long departmentId) {
        return facultyService.getFacultyListByInstituteDept(instituteId, departmentId);
    }

    @GetMapping("/export-faculty")
    public void exportCSV(HttpServletResponse response) throws Exception {

        //set file name and content type
        try {
            System.out.println("1 inside try !!");
            sendEmailService.sendEmail("shahbhavya5800@gmail.com");
        } catch (Exception ex) {
            Logger.getLogger(FacultyServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
        }

        System.out.println(" Please check your mail !!!");

    }
}
