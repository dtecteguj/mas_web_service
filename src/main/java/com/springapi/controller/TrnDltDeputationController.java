/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.springapi.controller;

import com.springapi.model.TrnDltDeputation;
import com.springapi.service.TrnDltDeputationService;
import java.util.List;
import javax.validation.Valid;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author shahb
 */
@RestController
@RequestMapping("/api")
@CrossOrigin(origins = "*")
public class TrnDltDeputationController {

    static Logger logger = Logger.getLogger(TrnDltDeputationController.class);
    
    @Autowired
    private TrnDltDeputationService trnDltDeputationService;

    @PostMapping("/trnDltDeputation")
    public TrnDltDeputation create(@Valid @RequestBody TrnDltDeputation trnDltDeputation) {
        logger.debug("create");
        return trnDltDeputationService.create(trnDltDeputation);
    }

    @GetMapping("/trnDltDeputation/list")
    public List<TrnDltDeputation> getAllFacultyShiftMpg() {
        logger.debug("list");
        return trnDltDeputationService.list();
    }

    @GetMapping("/trnDltDeputation/{id}")
    public TrnDltDeputation read(@PathVariable(value = "id") Long lId) {
        logger.debug("read");
        return trnDltDeputationService.read(lId);
    }

    /* Update an LeaveType Master */
    @PutMapping("/trnDltDeputation")
    public TrnDltDeputation update(@Valid @RequestBody TrnDltDeputation trnDltDeputation) {
        logger.debug("update");
        return trnDltDeputationService.update(trnDltDeputation);
    }

    /* Delete an LeaveType Master */
    @DeleteMapping("/trnDltDeputation/{id}")
    public String delete(@PathVariable(value = "id") Long lId) {
        logger.debug("delete");
        trnDltDeputationService.delete(lId);
        return "";
    }

    @GetMapping("/trnDltDeputation/faculty/{id}")
    public List<TrnDltDeputation> readbyfid(@PathVariable(value = "id") Long fId) {
        logger.debug("read");
        return trnDltDeputationService.readbyfid(fId);
    }

}
