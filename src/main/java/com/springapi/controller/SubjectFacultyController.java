/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.springapi.controller;

import com.springapi.model.SubjectFaculty;
import com.springapi.service.SubjectFacultyService;
import java.util.List;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author shahb
 */
@RestController
@RequestMapping("/gmca")
public class SubjectFacultyController {

    @Autowired
    private SubjectFacultyService subjectFacultyService;

    /* to Save an SubjectFaculty*/
    @PostMapping("/subjectFaculty")
    public SubjectFaculty create(@Valid @RequestBody SubjectFaculty subjectFaculty) {
        return subjectFacultyService.create(subjectFaculty);
    }

    /* get all SubjectFaculty */
    @GetMapping("/subjectFaculty/list")
    public List<SubjectFaculty> getAllSubjectFacultys() {
        return subjectFacultyService.list();
    }

    @GetMapping("/subjectFaculty/{id}")
    public SubjectFaculty read(@PathVariable(value = "id") Long fId) {
        return subjectFacultyService.read(fId);
    }

    /* Update an SubjectFaculty by SubjectFacultyId */
    @PutMapping("/subjectFaculty")
    public SubjectFaculty update(@Valid @RequestBody SubjectFaculty subjectFacultyDetails) {
        return subjectFacultyService.update(subjectFacultyDetails);
    }

    /* Delete an SubjectFaculty */
    @DeleteMapping("/subjectFaculty/{id}")
    public String delete(@PathVariable(value = "id") Long fId) {
        subjectFacultyService.delete(fId);
        return "";
    }
}
