package com.springapi.controller;

import com.springapi.model.TrnbyPriHodLog;
import com.springapi.service.TrnbyPriHodLogService;
import java.util.List;

import javax.validation.Valid;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping("/api")
@CrossOrigin(origins = "*")
public class TrnbyPriHodLogController {

	static Logger logger = Logger.getLogger(TrnbyPriHodLogController.class);

	@Autowired
	private TrnbyPriHodLogService TrnbyPriHodLogService1;


	@PostMapping("/TrnbyPriHodLog")
	public TrnbyPriHodLog create(@Valid @RequestBody TrnbyPriHodLog fm) {
		logger.debug("create");
		return TrnbyPriHodLogService1.create(fm);
	}


	@GetMapping("/TrnbyPriHodLog/list")
	public List<TrnbyPriHodLog> getAllfacultyRoleMpg() {
		logger.debug("list");
		return TrnbyPriHodLogService1.list();
	}

	@GetMapping("/TrnbyPriHodLog/{id}")
	public TrnbyPriHodLog read(@PathVariable(value = "id") Long id) {
		logger.debug("read");
		return TrnbyPriHodLogService1.read(id);
	}

	@PutMapping("/TrnbyPriHodLog")
	public TrnbyPriHodLog update(@Valid @RequestBody TrnbyPriHodLog tpl) {
		logger.debug("update");
		return TrnbyPriHodLogService1.update(tpl);
	}

	@DeleteMapping("/TrnbyPriHodLog/{id}")
	public String delete(@PathVariable(value = "id") Long id) {
		logger.debug("delete");
		TrnbyPriHodLogService1.delete(id);
		return "";
	}
}
