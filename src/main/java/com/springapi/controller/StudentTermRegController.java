/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.springapi.controller;

import com.springapi.model.StudentTermReg;
import com.springapi.service.StudentTermRegService;
import java.util.List;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author shahb
 */
@RestController
@RequestMapping("/api")
@CrossOrigin(origins = "*")
public class StudentTermRegController {

    @Autowired
    private StudentTermRegService studentTermRegService;

    /* to Save an Faculty*/
    @PostMapping("/studentTermReg")
    public StudentTermReg create(@Valid @RequestBody StudentTermReg studentTermReg) {
        return studentTermRegService.create(studentTermReg);
    }

    /* get all Faculty */
    @GetMapping("/studentTermReg/list")
    public List<StudentTermReg> getAllStudentTermReg() {
        return studentTermRegService.list();
    }

    @GetMapping("/studentTermReg/{id}")
    public StudentTermReg read(@PathVariable(value = "id") Long fId) {
        return studentTermRegService.read(fId);
    }

    /* Update an Faculty by FacultyId */
    @PutMapping("/studentTermReg")
    public StudentTermReg update(@Valid @RequestBody StudentTermReg studentTermReg) {
        return studentTermRegService.update(studentTermReg);
    }

    /* Delete an Faculty */
    @DeleteMapping("/studentTermReg/{id}")
    public String delete(@PathVariable(value = "id") Long fId) {
        studentTermRegService.delete(fId);
        return "";
    }
}
