/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.springapi.controller;

import com.springapi.dao.EmployeeDAO;
import com.springapi.model.Employee;
import java.util.List;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author shahb
 */
@RestController
@RequestMapping("/company")
public class EmployeeController {

    @Autowired
    EmployeeDAO employeeDAO;

    /* to Save an Employee*/
    @PostMapping("/employees")
    public Employee createEmployee(@Valid @RequestBody Employee employee) {
        return employeeDAO.save(employee);
    }

    /* get all Employee */
    @GetMapping("/employees")
    public List<Employee> getAllEmployees() {
        return employeeDAO.findAll();
    }

    /* get Employee by Partiqular Id */
    @GetMapping("/employees/{id}")
    public ResponseEntity<Employee> getEmployeeById(@PathVariable(value = "id") Long empid) {
        Employee employee = employeeDAO.findOne(empid);
        if (employee == null) {
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok().body(employee);
    }

    /* Update an Employee by EmpId */
    @PutMapping("/employees/{id}")
    public ResponseEntity<Employee> updateEmployee(@PathVariable(value = "id") Long empid, @Valid @RequestBody Employee employeeDetails) {
        Employee employee = employeeDAO.findOne(empid);
        if (employee == null) {
            return ResponseEntity.notFound().build();
        }
        employee.setName(employeeDetails.getName());
        employee.setDesignation(employeeDetails.getDesignation());
        employee.setExpertise(employeeDetails.getExpertise());

        Employee updateEmployee = employeeDAO.save(employee);
        return ResponseEntity.ok().body(updateEmployee);

    }

    /* Delete an Employee */
    @DeleteMapping("/employees/{id}")
    public ResponseEntity<Employee> deleteEmployee(@PathVariable(value = "id") Long empid) {
        Employee employee = employeeDAO.findOne(empid);
        if (employee == null) {
            return ResponseEntity.notFound().build();
        }
        employeeDAO.delete(employee);
        return ResponseEntity.ok().build();
    }

}
