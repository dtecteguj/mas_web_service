package com.springapi.controller;

import com.springapi.model.TrnActiveLoginLog;
import com.springapi.service.TrnActiveLoginLogService;
import java.util.List;

import javax.validation.Valid;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;



@RestController
@RequestMapping("/api")
@CrossOrigin(origins = "*")
public class TrnActiveLoginLogController {
	static Logger logger = Logger.getLogger(FacRoleMpgController.class);
    
    @Autowired
    private TrnActiveLoginLogService TrnActiveLoginLogService1;
    
   
   @PostMapping("/TrnActiveLoginLog")
   public TrnActiveLoginLog create(@Valid @RequestBody TrnActiveLoginLog fm) {
       logger.debug("create");
       return TrnActiveLoginLogService1.create(fm);
   }
  
   @GetMapping("/TrnActiveLoginLog/list")
   public List<TrnActiveLoginLog> getAllfacultyRoleMpg() {
       logger.debug("list");
       return TrnActiveLoginLogService1.list();
   }

   @GetMapping("/TrnActiveLoginLog/{id}")
   public TrnActiveLoginLog read(@PathVariable(value = "id") Long id) {
       logger.debug("read");
       return TrnActiveLoginLogService1.read(id);
   }

  
   @PutMapping("/TrnActiveLoginLog")
   public TrnActiveLoginLog update(@Valid @RequestBody TrnActiveLoginLog fm) {
       logger.debug("update");
       return TrnActiveLoginLogService1.update(fm);
   }

   @DeleteMapping("/TrnActiveLoginLog/{id}")
   public String delete(@PathVariable(value = "id") Long id) {
       logger.debug("delete");
       TrnActiveLoginLogService1.delete(id);
       return "";
   }    

}
