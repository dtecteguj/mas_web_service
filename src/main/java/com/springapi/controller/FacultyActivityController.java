package com.springapi.controller;

import com.springapi.dto.request.FacultyActivityRequest;
import com.springapi.dto.response.FacultyActivityRecordResponse;
import com.springapi.dto.response.FacultyActivityResponse;
import com.springapi.model.FacultyActivity;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.springapi.service.FacultyActivityService;
import java.util.List;
import org.springframework.web.bind.annotation.CrossOrigin;

/**
 *
 * @author Jyoti
 */
@RestController
@RequestMapping("/api")
@CrossOrigin(origins = "*")
public class FacultyActivityController {

    @Autowired
    private FacultyActivityService facultyActivityService;

    @PostMapping("/faculty/activity")
    public FacultyActivity create(@Valid @RequestBody FacultyActivityRequest facultyActivityRequest) {
        return facultyActivityService.create(facultyActivityRequest);
    }

    @PostMapping("/faculty/lecture/info")
    public FacultyActivityResponse getFacultyInfo(@Valid @RequestBody FacultyActivityRequest facultyActivityRequest) {
        return facultyActivityService.getFacultyInfo(facultyActivityRequest);
    }

    @PostMapping("/faculty/activity/update")
    public FacultyActivity updateFacultyInfo(@Valid @RequestBody FacultyActivity facultyActivity) {
        return facultyActivityService.updateFacultyInfo(facultyActivity);
    }

    //List<FacultyActivityRecordResponse>
    @PostMapping("/faculty/activity/record")
    public List<FacultyActivityRecordResponse> facultyActivityRecord(@Valid @RequestBody FacultyActivity facultyActivity) {
        System.out.println("faculty activity record !!");
        return facultyActivityService.facultyActivityRecord(facultyActivity);
        //return null;
    }

    @PostMapping("/faculty/activity/list")
    public List<FacultyActivity> getFacultyActivities(@Valid @RequestBody FacultyActivity facultyActivity) {
        return facultyActivityService.getFacultyActivities(facultyActivity);
    }
    
   
}
