/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.springapi.controller;

import static com.springapi.controller.HolidayMasterController.logger;
import com.springapi.model.HolidayMaster;
import com.springapi.model.LeaveTypeMaster;
import com.springapi.service.HolidayMasterService;
import com.springapi.service.LeaveTypeMasterService;
import java.util.List;
import javax.validation.Valid;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author shahb
 */

@RestController
@RequestMapping("/api")
@CrossOrigin(origins = "*")
public class LeaveTypeMasterController {
    static Logger logger = Logger.getLogger(LeaveTypeMasterController.class);
    
     @Autowired
    private LeaveTypeMasterService  leaveTypeMasterService;
    
    @PostMapping("/leaveTypeMaster")
    public LeaveTypeMaster create(@Valid @RequestBody LeaveTypeMaster leaveTypeMaster)
    {
        logger.debug("create");
        return leaveTypeMasterService.create(leaveTypeMaster);
    }
    @GetMapping("/leaveTypeMaster/list")
    public List<LeaveTypeMaster> getAllFacultyShiftMpg()
    {
        logger.debug("list");
        return leaveTypeMasterService.list();
    }
    
    @GetMapping("/leaveTypeMaster/{id}")
    public LeaveTypeMaster read(@PathVariable(value = "id") Long lId) {
        logger.debug("read");
        return leaveTypeMasterService.read(lId);
    }
    
   /* Update an LeaveType Master */
    @PutMapping("/leaveTypeMaster")
    public LeaveTypeMaster update(@Valid @RequestBody LeaveTypeMaster leaveTypeMaster) {
        logger.debug("update");
        return leaveTypeMasterService.update(leaveTypeMaster);
    }

    /* Delete an LeaveType Master */
    @DeleteMapping("/leaveTypeMaster/{id}")
    public String delete(@PathVariable(value = "id") Long lId) {
        logger.debug("delete");
        leaveTypeMasterService.delete(lId);
        return "";
    }
    
}
