/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.springapi.controller;

import com.springapi.model.FsStudentReg;
import com.springapi.service.FsStudentRegService;
import java.util.List;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author shahb
 */
@RestController
@RequestMapping("/api")
@CrossOrigin(origins = "*")
public class FsStudentRegController {
    
    @Autowired
    private FsStudentRegService fsStudentRegService;
    
      /* to Save an Faculty*/
    @PostMapping("/fsStudentReg")
    public FsStudentReg create(@Valid @RequestBody FsStudentReg fsStudentReg) {
        return fsStudentRegService.create(fsStudentReg);
    }

    /* get all Faculty */
    @GetMapping("/fsStudentReg/list")
    public List<FsStudentReg> getAllFacultys() {
        return fsStudentRegService.list();
    }

    @GetMapping("/fsStudentReg/{id}")
    public FsStudentReg read(@PathVariable(value = "id") Long fId) {
        return fsStudentRegService.read(fId);
    }

    /* Update an Faculty by FacultyId */
    @PutMapping("/fsStudentReg")
    public FsStudentReg update(@Valid @RequestBody FsStudentReg editAttndcLog) {
        return fsStudentRegService.update(editAttndcLog);
    }

    /* Delete an Faculty */
    @DeleteMapping("/fsStudentReg/{id}")
    public String delete(@PathVariable(value = "id") Long fId) {
        fsStudentRegService.delete(fId);
        return "";
    }
    
}
