package com.springapi.controller;

import com.springapi.dto.request.AttendanceByPrincipalRequest;
import com.springapi.dto.request.FacAttendVerifyRequest;
import com.springapi.dto.request.FacultyAttendanceListRequest;
import com.springapi.dto.request.FacultyQRVerificationRequest;
import com.springapi.dto.request.LateAttendanceReasonRequest;
import com.springapi.dto.response.BaseResponse;
import com.springapi.enums.MASConstant;
import com.springapi.model.FacultyAttendance;
import com.springapi.service.FacultyAttendanceService;
import java.lang.management.ManagementFactory;
import java.util.Set;
import javax.management.MBeanServer;
import javax.management.ObjectName;
import javax.management.Query;

import javax.validation.Valid;
import org.json.JSONException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author shahb
 */
@RestController
@RequestMapping("/api")
@CrossOrigin(origins = "*")
public class FacultyAttendanceController {

    @Autowired
    private FacultyAttendanceService facultyAttendanceService;

    /* to Save an FacultyAttendance*/
    @PostMapping("/facultyAttendance")
    public FacultyAttendance create(@Valid @RequestBody FacultyAttendance facultyAttendance) {
        return facultyAttendanceService.create(facultyAttendance);
    }

    /* get all FacultyAttendance */
//    @GetMapping("/facultyAttendance/list")
//    public List<FacultyAttendance> getAllFacultyAttendances() {
//        return facultyAttendanceService.list();
//    }
    @GetMapping("/facultyAttendance/{id}")
    public FacultyAttendance read(@PathVariable(value = "id") Long fId) {
        getIpAddressAndPort();
        return facultyAttendanceService.read(fId);
    }
    
    

    /* Update an FacultyAttendance by FacultyAttendanceId */
    @PutMapping("/facultyAttendance")
    public FacultyAttendance update(@Valid @RequestBody FacultyAttendance facultyAttendanceDetails) {
        return facultyAttendanceService.update(facultyAttendanceDetails);
    }

    /* Delete an FacultyAttendance */
    @DeleteMapping("/facultyAttendance/{id}")
    public String delete(@PathVariable(value = "id") Long fId) {
        facultyAttendanceService.delete(fId);
        return "";
    }

    /* Verify QR code for faculty attendance daily */
    @PostMapping("/verify/QRcode")
    public BaseResponse verifyQRCode(@Valid @RequestBody FacultyQRVerificationRequest facultyQRVerificationRequest) {
        return facultyAttendanceService.verifyQRCode(facultyQRVerificationRequest);
    }

    @PostMapping("/verify/attendance")
    public BaseResponse verifyAttendance(@Valid @RequestBody FacAttendVerifyRequest facAttendVerifyRequest) {
        return facultyAttendanceService.verifyAndAdd(facAttendVerifyRequest);
    }

    @PostMapping("/verify/in/attendance")
    public BaseResponse verifyInAttendance(@Valid @RequestBody FacAttendVerifyRequest facAttendVerifyRequest) {
        return facultyAttendanceService.verifyInAttendance(facAttendVerifyRequest);
    }

    @PostMapping("/verify/out/attendance")
    public BaseResponse verifyOutAttendance(@Valid @RequestBody FacAttendVerifyRequest facAttendVerifyRequest) {
        return facultyAttendanceService.verifyOutAttendance(facAttendVerifyRequest);
    }

    @PostMapping("/check/in/attendance")
    public BaseResponse checkInAttendance(@Valid @RequestBody FacAttendVerifyRequest facAttendVerifyRequest) {
        return facultyAttendanceService.checkInAttendance(facAttendVerifyRequest);
    }

    @PostMapping("/check/out/attendance")
    public BaseResponse checkOutAttendance(@Valid @RequestBody FacAttendVerifyRequest facAttendVerifyRequest) {
        return facultyAttendanceService.checkOutAttendance(facAttendVerifyRequest);
    }

    @PostMapping("/facultyAttendance/list")
    public Page<FacultyAttendance> attendanceList(@Valid @RequestBody FacultyAttendanceListRequest facultyAttendanceListRequest) {
        return facultyAttendanceService.facultyAttendanceList(facultyAttendanceListRequest);
    }

    @PostMapping("/facultyAttendance/late/reason")
    public FacultyAttendance lateAttendanceReason(@Valid @RequestBody LateAttendanceReasonRequest lateAttendanceReasonRequest) {
        return facultyAttendanceService.lateAttendanceReason(lateAttendanceReasonRequest);
    }

    @PostMapping("/facultyAttendance/by/institute")
    public String facultyAttendanceByInstituteId(@Valid @RequestBody AttendanceByPrincipalRequest attendanceByPrincipalRequest) {
        return facultyAttendanceService.facultyAttendanceByInstituteId(attendanceByPrincipalRequest);
    }

    @GetMapping("/facultyAttendance/today/by/{fid}")
    public FacultyAttendance readByFacultyId(@PathVariable(value = "fid") Long fId) {
        return facultyAttendanceService.readByFacultyId(fId);
    }

    @Scheduled(cron = "0 55 10 * * *")
    public void displayMsg() throws JSONException {
         System.out.println("Scheduler Calling...");
        if (getIpAddressAndPort() == MASConstant.SERVER_PORT) {
          //  System.err.println("Hello, Bhavya Whats Happen with u?");
            facultyAttendanceService.dailySchedule();
        }
    }

//    @Scheduled(cron = "0 10 17 * * *")
//    public void fsMsg() throws JSONException {
//        if (getIpAddressAndPort() == 8081) {
//            //  System.err.println("Hello, Bhavya you are to Good!");
//        }
//    }

//    @Scheduled(cron = "0 0 13 * * *")
    public void holidayStatus() throws JSONException {
       
        if (getIpAddressAndPort() == MASConstant.SERVER_PORT) {
            facultyAttendanceService.dailyHolidaySchedule();
        }

    }

//    @Scheduled(cron = "0 10 19 * * MON-SAT")
    public void displayMemoMsg() throws JSONException {

        if (getIpAddressAndPort() == MASConstant.SERVER_PORT) {
            System.out.println("Scheduler Attendance");
            facultyAttendanceService.dailyMemoSchedule();
        }
    }

//   @Scheduled(cron = "0 15 19 * * MON-SAT")
    public void mailMEMO() throws JSONException {
         System.out.println("Callinb...");
        if (getIpAddressAndPort() == MASConstant.SERVER_PORT) {
            System.out.println("Scheduler Attendance");
            facultyAttendanceService.memoList();
        }
    }
    
    public int getIpAddressAndPort() {
        int pPort = 0;
        try {
            MBeanServer beanServer = ManagementFactory.getPlatformMBeanServer();

            Set<ObjectName> objectNames = beanServer.queryNames(new ObjectName("*:type=Connector,*"),
                    Query.match(Query.attr("protocol"), Query.value("HTTP/1.1")));

            String port = objectNames.iterator().next().getKeyProperty("port");

            pPort = Integer.parseInt(port);
        } catch (Exception e) {
            pPort = 0;
        }
        return pPort;
    }
}
