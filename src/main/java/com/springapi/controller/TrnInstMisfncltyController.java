/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.springapi.controller;

import com.springapi.model.TrnInstMisfnclty;
import com.springapi.service.TrnInstMisfncltyService;
import java.util.List;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author shahb
 */
@RestController
@RequestMapping("/api")
@CrossOrigin(origins = "*")
public class TrnInstMisfncltyController {

    @Autowired
    private TrnInstMisfncltyService trnInstMisfncltyService;

    /* to Save an Faculty*/
    @PostMapping("/trnInstMisfnclty")
    public TrnInstMisfnclty create(@Valid @RequestBody TrnInstMisfnclty trnInstMisfnclty) {
        return trnInstMisfncltyService.create(trnInstMisfnclty);
    }

    /* get all Faculty */
    @GetMapping("/trnInstMisfnclty/list")
    public List<TrnInstMisfnclty> getAllFacultys() {
        return trnInstMisfncltyService.list();
    }

    @GetMapping("/trnInstMisfnclty/{id}")
    public TrnInstMisfnclty read(@PathVariable(value = "id") Long fId) {
        return trnInstMisfncltyService.read(fId);
    }

    /* Update an Faculty by FacultyId */
    @PutMapping("/trnInstMisfnclty")
    public TrnInstMisfnclty update(@Valid @RequestBody TrnInstMisfnclty trnInstMisfnclty) {
        return trnInstMisfncltyService.update(trnInstMisfnclty);
    }

    /* Delete an Faculty */
    @DeleteMapping("/trnInstMisfnclty/{id}")
    public String delete(@PathVariable(value = "id") Long fId) {
        trnInstMisfncltyService.delete(fId);
        return "";
    }
}
