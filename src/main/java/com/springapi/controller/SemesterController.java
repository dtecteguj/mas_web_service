/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.springapi.controller;

import com.springapi.model.Semester;
import com.springapi.service.SemesterService;
import java.util.List;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author shahb
 */
@RestController
@RequestMapping("/gmca")
public class SemesterController {

    @Autowired
    private SemesterService semesterService;

    /* to Save an Semester*/
    @PostMapping("/semester")
    public Semester create(@Valid @RequestBody Semester semester) {
        return semesterService.create(semester);
    }

    /* get all Semester */
    @GetMapping("/semester/list")
    public List<Semester> getAllSemesters() {
        return semesterService.list();
    }

    @GetMapping("/semester/{id}")
    public Semester read(@PathVariable(value = "id") Long sId) {
        return semesterService.read(sId);
    }

    /* Update an Semester by SemesterId */
    @PutMapping("/semester")
    public Semester update(@Valid @RequestBody Semester semesterDetails) {
        return semesterService.update(semesterDetails);
    }

    /* Delete an Semester */
    @DeleteMapping("/semester/{id}")
    public String delete(@PathVariable(value = "id") Long sId) {
        semesterService.delete(sId);
        return "";
    }

}
