package com.springapi.controller;

import com.springapi.model.ProgramMaster;
import com.springapi.service.ProgramService;
import java.util.List;
import javax.validation.Valid;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author Jyoti Patel
 */
@RestController
@RequestMapping("/api")
@CrossOrigin(origins = "*")
public class ProgramMasterController {
    static Logger logger = Logger.getLogger(ProgramMasterController.class);
     
     @Autowired
     private ProgramService programService;
     
     /* to Save an ProgramMaster*/
    @PostMapping("/program")
    public ProgramMaster create(@Valid @RequestBody ProgramMaster program) {
        logger.debug("create");
        return programService.create(program);
    }

    /* get all ProgramMaster */
    @GetMapping("/program/list")
    public List<ProgramMaster> getAllProgramMasters() {
        logger.debug("list");
        return programService.list();
    }

    @GetMapping("/program/{id}")
    public ProgramMaster read(@PathVariable(value = "id") Long uId) {
        logger.debug("read");
        return programService.read(uId);
    }

    /* Update an ProgramMaster by ProgramMasterId */
    @PutMapping("/program")
    public ProgramMaster update(@Valid @RequestBody ProgramMaster programDetails) {
        logger.debug("update");
        return programService.update(programDetails);
    }

    /* Delete an ProgramMaster */
    @DeleteMapping("/program/{id}")
    public String delete(@PathVariable(value = "id") Long fId) {
        logger.debug("delete");
        programService.delete(fId);
        return "";
    }
    
    
    
}
