/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.springapi.controller;

import com.springapi.model.EditAttndcLog;
import com.springapi.service.EditAttndcLogService;
import java.util.List;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author shahb
 */
@RestController
@RequestMapping("/api")
@CrossOrigin(origins = "*")
public class EditAttndcLogController {
    
    @Autowired
    private EditAttndcLogService editAttndcLogService;
    
      /* to Save an Faculty*/
    @PostMapping("/editAttndcLog")
    public EditAttndcLog create(@Valid @RequestBody EditAttndcLog editAttndcLog) {
        return editAttndcLogService.create(editAttndcLog);
    }

    /* get all Faculty */
    @GetMapping("/editAttndcLog/list")
    public List<EditAttndcLog> getAllFacultys() {
        return editAttndcLogService.list();
    }

    @GetMapping("/editAttndcLog/{id}")
    public EditAttndcLog read(@PathVariable(value = "id") Long fId) {
        return editAttndcLogService.read(fId);
    }

    /* Update an Faculty by FacultyId */
    @PutMapping("/editAttndcLog")
    public EditAttndcLog update(@Valid @RequestBody EditAttndcLog editAttndcLog) {
        return editAttndcLogService.update(editAttndcLog);
    }

    /* Delete an Faculty */
    @DeleteMapping("/editAttndcLog/{id}")
    public String delete(@PathVariable(value = "id") Long fId) {
        editAttndcLogService.delete(fId);
        return "";
    }
    
    
}
