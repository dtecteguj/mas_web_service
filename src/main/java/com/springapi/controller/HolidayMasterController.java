/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.springapi.controller;

import com.springapi.model.FacultyShiftMpg;
import com.springapi.model.HolidayMaster;
import com.springapi.service.FacultyShiftMpgService;
import com.springapi.service.HolidayMasterService;
import java.util.List;
import javax.validation.Valid;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author shahb
 */
@RestController
@RequestMapping("/api")
@CrossOrigin(origins = "*")
public class HolidayMasterController {
    static Logger logger = Logger.getLogger(HolidayMasterController.class);
    
     @Autowired
    private HolidayMasterService  holidayMasterService;
    
    @PostMapping("/holidayMasterService")
    public HolidayMaster create(@Valid @RequestBody HolidayMaster holidayMaster)
    {
        logger.debug("create");
        return holidayMasterService.create(holidayMaster);
    }
    @GetMapping("/holidayMasterService/list")
    public List<HolidayMaster> getAllFacultyShiftMpg()
    {
        logger.debug("list");
        return holidayMasterService.list();
    }
    
    @GetMapping("/holidayMasterService/{id}")
    public HolidayMaster read(@PathVariable(value = "id") Long hId) {
        logger.debug("read");
        return holidayMasterService.read(hId);
    }
    
   /* Update an Holiday Master */
    @PutMapping("/holidayMasterService")
    public HolidayMaster update(@Valid @RequestBody HolidayMaster holidayMaster) {
        logger.debug("update");
        return holidayMasterService.update(holidayMaster);
    }

    /* Delete an Holiday Master */
    @DeleteMapping("/holidayMasterService/{id}")
    public String delete(@PathVariable(value = "id") Long hId) {
        logger.debug("delete");
        holidayMasterService.delete(hId);
        return "";
    }
    
}
