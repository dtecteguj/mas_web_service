/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.springapi.controller;

import static com.springapi.controller.DashboardController.logger;
import com.springapi.dto.request.CMDashRequest;
import com.springapi.dto.request.CMDashSHODHRequest;
import com.springapi.dto.response.CMDashResponse;
import com.springapi.dto.response.SHODHStudentAttendanceRespNew;
import com.springapi.dto.response.SHODHStudentAttendanceResponse;
import com.springapi.model.ShodhStudent;
import com.springapi.service.ShodhStudentService;
import java.util.List;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author shahb
 */
@RestController
@RequestMapping("/api")
@CrossOrigin(origins = "*")
public class ShodhStudentController {
        
    @Autowired
    private ShodhStudentService shodhStudentService;
    
     /* to Save an Faculty*/
    @PostMapping("/ShodhStudent")
    public ShodhStudent create(@Valid @RequestBody ShodhStudent shodhStudent) {
        return shodhStudentService.create(shodhStudent);
    }

    /* get all Faculty */
    @GetMapping("/ShodhStudent/list")
    public List<ShodhStudent> getAllFacultys() {
        return shodhStudentService.list();
    }

    @GetMapping("/ShodhStudent/{id}")
    public ShodhStudent read(@PathVariable(value = "id") Long fId) {
        return shodhStudentService.read(fId);
    }   
    
    /* Update an Faculty by FacultyId */
    @PutMapping("/ShodhStudent")
    public ShodhStudent update(@Valid @RequestBody ShodhStudent shodhStudent) {
        return shodhStudentService.update(shodhStudent);
    }

    /* Delete an Faculty */
    @DeleteMapping("/ShodhStudent/{id}")
    public String delete(@PathVariable(value = "id") Long fId) {
        shodhStudentService.delete(fId);
        return "";
    }
    
    @PostMapping("/dashboard/nic/attendance/SHODH/")
    public SHODHStudentAttendanceRespNew getDateWiseAttnForCMDash(@Valid @RequestBody CMDashSHODHRequest cMDashSHODHRequest) {
        logger.debug("getInstituteWiseAttnForDirector");
        return shodhStudentService.countAttendanceForCMDash(cMDashSHODHRequest);
    }
}
