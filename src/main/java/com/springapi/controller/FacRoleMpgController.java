package com.springapi.controller;


import com.springapi.model.FacRoleMpg;
import com.springapi.service.FacRoleMpgService;
import java.util.List;

import javax.validation.Valid;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;



@RestController
@RequestMapping("/api")
@CrossOrigin(origins = "*")
public class FacRoleMpgController {
	static Logger logger = Logger.getLogger(FacRoleMpgController.class);
    
    @Autowired
    private FacRoleMpgService FacRoleMpgService1;
    
   
   @PostMapping("/FacRoleMpg")
   public FacRoleMpg create(@Valid @RequestBody FacRoleMpg fm) {
       logger.debug("create");
       return FacRoleMpgService1.create(fm);
   }

  
   @GetMapping("/FacRoleMpg/list")
   public List<FacRoleMpg> getAllfacultyRoleMpg() {
       logger.debug("list");
       return FacRoleMpgService1.list();
   }

   @GetMapping("/FacRoleMpg/{id}")
   public FacRoleMpg read(@PathVariable(value = "id") Long id) {
       logger.debug("read");
       return FacRoleMpgService1.read(id);
   }

  
   @PutMapping("/FacRoleMpg")
   public FacRoleMpg update(@Valid @RequestBody FacRoleMpg fm) {
       logger.debug("update");
       return FacRoleMpgService1.update(fm);
   }

  
   @DeleteMapping("/FacRoleMpg/{id}")
   public String delete(@PathVariable(value = "id") Long id) {
       logger.debug("delete");
       FacRoleMpgService1.delete(id);
       return "";
   }    
}
