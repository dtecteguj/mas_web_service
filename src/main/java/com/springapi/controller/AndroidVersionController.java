/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.springapi.controller;

import com.springapi.dto.request.VrfVrsRequest;
import com.springapi.dto.response.AndroidVersionStatus;
import com.springapi.dto.response.VrfVrsResponse;
import com.springapi.model.AndroidVersion;
import com.springapi.service.AndroidVersionService;
import java.util.List;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author shahb
 */
@RestController
@RequestMapping("/api")
@CrossOrigin(origins = "*")
public class AndroidVersionController {
    
    
    @Autowired
    private AndroidVersionService androidVersionService;
    
     /* to Save an Faculty*/
    @PostMapping("/androidVersion")
    public AndroidVersion create(@Valid @RequestBody AndroidVersion androidVersion) {
        return androidVersionService.create(androidVersion);
    }

     @PostMapping("/androidVersion/VerifyVersion")
    public VrfVrsResponse checkVrfVrs(VrfVrsRequest vrfVrsRequest) {
        return androidVersionService.verifyVersion(vrfVrsRequest);
    }
    
    /* get all Faculty */
    @GetMapping("/androidVersion/list")
    public List<AndroidVersion> getAllFacultys() {
        return androidVersionService.list();
    }

    @GetMapping("/androidVersion/{id}")
    public AndroidVersionStatus read(@PathVariable(value = "id") Long fId) {
        return androidVersionService.read(fId);
    }

    
     @GetMapping("/androidVersion/{type}/{id}")
    public AndroidVersionStatus readByTypeAndId(@PathVariable(value = "id") Long fId,@PathVariable(value = "type") Character type) {
        return androidVersionService.read(fId);
    }
    
    /* Update an Faculty by FacultyId */
    @PutMapping("/androidVersion")
    public AndroidVersion update(@Valid @RequestBody AndroidVersion androidVersion) {
        return androidVersionService.update(androidVersion);
    }

    /* Delete an Faculty */
    @DeleteMapping("/androidVersion/{id}")
    public String delete(@PathVariable(value = "id") Long fId) {
        androidVersionService.delete(fId);
        return "";
    }
    
}
