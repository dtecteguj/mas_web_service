/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.springapi.controller;

import com.springapi.model.InstituteLocFancingInfo;
import com.springapi.model.TrnStaffAttendanceLog;
import com.springapi.service.TrnStaffAttendanceLogService;
import java.util.List;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author shahb
 */
@RestController
@RequestMapping("/api")
@CrossOrigin(origins = "*")
public class TrnStaffAttendanceLogController {

    @Autowired
    private TrnStaffAttendanceLogService trnStaffAttendanceLogService;

    
      /* to Save an Faculty*/
    @PostMapping("/trnStaffAttendanceLog")
    public TrnStaffAttendanceLog create(@Valid @RequestBody TrnStaffAttendanceLog trnStaffAttendanceLog) {
        return trnStaffAttendanceLogService.create(trnStaffAttendanceLog);
    }

    /* get all Faculty */
    @GetMapping("/trnStaffAttendanceLog/list")
    public List<TrnStaffAttendanceLog> getAllFacultys() {
        return trnStaffAttendanceLogService.list();
    }

    @GetMapping("/trnStaffAttendanceLog/{id}")
    public TrnStaffAttendanceLog read(@PathVariable(value = "id") Long fId) {
        return trnStaffAttendanceLogService.read(fId);
    }

    /* Update an Faculty by FacultyId */
    @PutMapping("/trnStaffAttendanceLog")
    public TrnStaffAttendanceLog update(@Valid @RequestBody TrnStaffAttendanceLog trnStaffAttendanceLog) {
        return trnStaffAttendanceLogService.update(trnStaffAttendanceLog);
    }

    /* Delete an Faculty */
    @DeleteMapping("/trnStaffAttendanceLog/{id}")
    public String delete(@PathVariable(value = "id") Long fId) {
        trnStaffAttendanceLogService.delete(fId);
        return "";
    }
    
}
