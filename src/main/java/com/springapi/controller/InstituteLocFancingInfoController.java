/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.springapi.controller;

import com.springapi.model.BlockStaffByAndroidId;
import com.springapi.model.InstituteLocFancingInfo;
import com.springapi.service.InstituteLocFancingInfoService;
import java.util.List;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author shahb
 */
@RestController
@RequestMapping("/api")
@CrossOrigin(origins = "*")
public class InstituteLocFancingInfoController {

    @Autowired
    private InstituteLocFancingInfoService instituteLocFancingInfoService;
    
     /* to Save an Institute Area*/
    @PostMapping("/instituteLocFancingInfoService")
    public InstituteLocFancingInfo create(@Valid @RequestBody InstituteLocFancingInfo instituteLocFancingInfo) {
        return instituteLocFancingInfoService.create(instituteLocFancingInfo);
    }

    /* get all Faculty */
    @GetMapping("/instituteLocFancingInfoService/list")
    public List<InstituteLocFancingInfo> getAllFacultys() {
        return instituteLocFancingInfoService.list();
    }

    @GetMapping("/instituteLocFancingInfoService/{id}")
    public InstituteLocFancingInfo read(@PathVariable(value = "id") Long fId) {
        return instituteLocFancingInfoService.read(fId);
    }

    /* Update an Faculty by FacultyId */
    @PutMapping("/instituteLocFancingInfoService")
    public InstituteLocFancingInfo update(@Valid @RequestBody InstituteLocFancingInfo instituteLocFancingInfo) {
        return instituteLocFancingInfoService.update(instituteLocFancingInfo);
    }

    /* Delete an Faculty */
    @DeleteMapping("/instituteLocFancingInfoService/{id}")
    public String delete(@PathVariable(value = "id") Long fId) {
        instituteLocFancingInfoService.delete(fId);
        return "";
    }
    
//     /* Get Institue Location Info based on Institute ID */
//    @GetMapping("/instituteLocFancingInfoService")
//    public List<InstituteLocFancingInfo> findByInstituteId(Long fId) {
//        return instituteLocFancingInfoService.findByInstituteId(fId);
//    }
    
//    @PostMapping("/institute/activity/insert")
//    public InstituteLocFancingInfo insertLocInfo(@Valid @RequestBody InstituteLocFancingInfo instituteLocFancingInfo) {
//      return  instituteLocFancingInfoService.insertFancingInfo(instituteLocFancingInfo);
//    }

}
