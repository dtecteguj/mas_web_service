/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.springapi.controller;

import static com.springapi.controller.TrnDtlIssueMemoController.logger;
import com.springapi.dto.response.LeaveDetails;
import com.springapi.model.TrnDtlLeaveStatus;
import com.springapi.service.TrnDtlIssueMemoService;
import com.springapi.service.TrnDtlLeaveStatusService;
import java.util.List;
import javax.validation.Valid;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author shahb
 */
@RestController
@RequestMapping("/api")
@CrossOrigin(origins = "*")
public class TrnDtlLeaveStatusController {

    static Logger logger = Logger.getLogger(TrnDtlLeaveStatusController.class);
    @Autowired
    private TrnDtlLeaveStatusService trnDtlLeaveStatusService;

    @PostMapping("/trnDtlLeaveStatus")
    public TrnDtlLeaveStatus create(@Valid @RequestBody TrnDtlLeaveStatus trnDtlLeaveStatus) {
        logger.debug("create");
        return trnDtlLeaveStatusService.create(trnDtlLeaveStatus);
    }

    @GetMapping("/trnDtlLeaveStatus/list")
    public List<TrnDtlLeaveStatus> getAllFacultyShiftMpg() {
        logger.debug("list");
        return trnDtlLeaveStatusService.list();
    }

    @GetMapping("/trnDtlLeaveStatus/{id}")
    public TrnDtlLeaveStatus read(@PathVariable(value = "id") Long lId) {
        logger.debug("read");
        return trnDtlLeaveStatusService.read(lId);
    }

    /* Update an LeaveType Master */
    @PutMapping("/trnDtlLeaveStatus")
    public TrnDtlLeaveStatus update(@Valid @RequestBody TrnDtlLeaveStatus trnDtlLeaveStatus) {
        logger.debug("update");
        return trnDtlLeaveStatusService.update(trnDtlLeaveStatus);
    }

    /* Delete an LeaveType Master */
    @DeleteMapping("/trnDtlLeaveStatus/{id}")
    public String delete(@PathVariable(value = "id") Long lId) {
        logger.debug("delete");
        trnDtlLeaveStatusService.delete(lId);
        return "";
    }

    @GetMapping("/trnDtlLeaveStatus/Day/{faid}")
    public LeaveDetails readData(@PathVariable(value = "faid") Long lId) {
        logger.debug("read");
        return trnDtlLeaveStatusService.readData(lId);

    }
    @GetMapping("/trnDtlLeaveStatus/today/{fid}")
    public LeaveDetails readtoData(@PathVariable(value = "fid") Long lId) {
        logger.debug("read");
        return trnDtlLeaveStatusService.readTodayData(lId);

    }
    
}
