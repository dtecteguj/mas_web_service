package com.springapi.controller;

import com.springapi.dto.request.ChangePasswordRequest;
import com.springapi.dto.request.FacultyLoginRequest;
import com.springapi.dto.request.ForgetPasswordRequest;
import com.springapi.dto.request.ResetPasswordRequest;
import com.springapi.dto.response.ChangePasswordResponse;
import com.springapi.dto.response.LoginResponse;
import com.springapi.service.FacultyService;
import com.springapi.service.StudentService;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author shahb
 */
@RestController
@RequestMapping("/api")
@CrossOrigin(origins = "*", maxAge = 3600)
public class AuthenticationController {
    
    @Autowired
    private FacultyService facultyService;
    
    @Autowired
    private StudentService studentService;
    
//    @PostMapping("/faculty/register")
//    public Faculty registerFaculty(@Valid @RequestBody FacultyRegistrationRequest facultyRegistrationRequest) {
//        return facultyService.register(facultyRegistrationRequest);
//    }
    
    // Faculty Login Api
    @PostMapping("/faculty/login")
    public LoginResponse loginFaculty(@Valid @RequestBody FacultyLoginRequest facultyLoginRequest) {
        return facultyService.login(facultyLoginRequest);
    }
    
    // Student Login Api
     @PostMapping("/student/login")
    public LoginResponse loginStudent(@Valid @RequestBody FacultyLoginRequest facultyLoginRequest) {
        return studentService.login(facultyLoginRequest);
        //return null;
    }
    
    // Faculty Login Api For angular
    @PostMapping("/login")
    public LoginResponse login(@Valid @RequestBody FacultyLoginRequest facultyLoginRequest) {
        return facultyService.loginUser(facultyLoginRequest);
    }
    
    @PostMapping("/change/password")
    public ChangePasswordResponse changePassword(@Valid @RequestBody ChangePasswordRequest changePasswordRequest) {
        return facultyService.changePassword(changePasswordRequest);
    }
    
    @PostMapping("/forget/password")
    public String forgetPassword(@Valid @RequestBody ForgetPasswordRequest forgetPasswordRequest) {
        return facultyService.forgetPassword(forgetPasswordRequest);
    }
    
    @PostMapping("/resetPassword")
    public ChangePasswordResponse resetPassword(@Valid @RequestBody ResetPasswordRequest resetPasswordRequest) {
        return facultyService.resetPassword(resetPasswordRequest);
    }
    
    @GetMapping("/faculty/today/status/{fid}/{vCode}")
    public LoginResponse status(@PathVariable(value = "fid") Long fId,@PathVariable(value = "vCode") Long vCode) {
        return facultyService.userStatus(fId,vCode);
    }
//    @GetMapping("/faculty/login/{email}/{password}/{bssid}/{ssid}")
//    public LoginResponse login(@PathVariable String email,
//			@PathVariable String password, @PathVariable String bssid,
//			@PathVariable String ssid) {
//        return facultyService.login(email, password, bssid, ssid);
//    }
//
//
//    @GetMapping("/student/login/{email}/{password}/{bssid}/{ssid}")
//    public LoginResponse loginStudent(@PathVariable String email,
//			@PathVariable String password, @PathVariable String bssid,
//			@PathVariable String ssid) {
//        return studentService.loginStudent(email, password, bssid, ssid);
//    }

}
