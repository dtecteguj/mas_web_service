package com.springapi.controller;

import com.springapi.dto.request.CMDashRequest;
import com.springapi.dto.request.DashboardRequest;
import com.springapi.dto.response.CMDashDailyTaskResponse;
import com.springapi.dto.response.CMDashResponse;
import com.springapi.dto.response.DashboardResponse;
import com.springapi.dto.response.TotalFacultyRegisterResponse;
import com.springapi.service.DashboardService;
import java.util.List;
import javax.validation.Valid;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Jyoti Patel
 */
@RestController
@RequestMapping("/api")
@CrossOrigin(origins = "*", maxAge = 3600)
public class DashboardController {

    static Logger logger = Logger.getLogger(DashboardController.class);

    @Autowired
    private DashboardService dashboardService;

    /* principal Dashboard chart*/
//   @GetMapping("/dasboard/principle/status")
//    public List<DashboardResponse> getAllPresentInstituteWise() {
//        logger.debug("getAllPresentInstituteWise");
//        return dashboardService.getPrincipalCount();
//    }
    @PostMapping("/dasboard/principle/status")
    public List<DashboardResponse> getAllPresentInstituteWise(@Valid @RequestBody DashboardRequest dashboardRequest) {
        logger.debug("getAllPresentInstituteWise");
        return dashboardService.getPrincipalCount(dashboardRequest);
    }

    /* Faculty Dashboard chart*/
    @PostMapping("/dasboard/faculty/status")
    public List<DashboardResponse> getTotalMonthlyFacultyAttendance() {
        logger.debug("getTotalMonthlyFacultyAttendance");
        return dashboardService.getFacultyCount();
    }

    /* HOD Dashboard chart*/
    @PostMapping("/dasboard/hod/status")
    public List<DashboardResponse> getFacultyAttendanceDepartmentWise(@Valid @RequestBody DashboardRequest dashboardRequest) {
        logger.debug("getFacultyAttendanceDepartmentWise");
        return dashboardService.getHodCount(dashboardRequest);

    }

    /* CM Dashboard chart*/
//    @GetMapping("/dasboard/cm/status")
//    public List<DashboardResponse> getFacultyAttendanceOverAll() {
//        logger.debug("getFacultyAttendanceOverAll");
//        return dashboardService.getCMCount();
//    }
    @PostMapping("/dasboard/cm/status")
    public List<DashboardResponse> getFacultyAttendanceOverAll(@Valid @RequestBody DashboardRequest dashboardRequest) {
        logger.debug("getFacultyAttendanceOverAll");
        List<DashboardResponse> dashboardList = dashboardService.getCMCount(dashboardRequest);
        return dashboardList;
    }

//     Director dashboard count service Start
    /* Total faculty count */
    @GetMapping("/total/faculty/register/count")
    public List<TotalFacultyRegisterResponse> getTotalFacultyCount() {
        logger.debug("getTotalFacultyCount");
        return dashboardService.getTotalFacultyCount();
    }

    @GetMapping("/total/faculty/present/count")
    public List<TotalFacultyRegisterResponse> getTotalFacultyPresentCount() {
        logger.debug("getTotalFacultyPresentCount");
        return dashboardService.getTotalFacultyPresentCount();
    }

    @GetMapping("/total/faculty/absent/count")
    public List<TotalFacultyRegisterResponse> getTotalFacultyAbsentCount() {
        logger.debug("getTotalFacultyAbsentCount");
        return dashboardService.getTotalFacultyAbsentCount();
    }

    @PostMapping("/dasboard/director/status")
    public List<DashboardResponse> getFacultyAttendanceForDirector(@Valid @RequestBody DashboardRequest dashboardRequest) {
        logger.debug("getFacultyAttendanceForDirector");
        return dashboardService.getDirectorCount(dashboardRequest);
    }
    
    @PostMapping("/dasboard/director/attendance")
    public List<DashboardResponse> getInstituteWiseAttnForDirector(@Valid @RequestBody DashboardRequest dashboardRequest) {
        logger.debug("getInstituteWiseAttnForDirector");
        return dashboardService.countAttendanceForDirector(dashboardRequest);
    }
    @PostMapping("/dashboard/cmdash/attendance")
    public List<CMDashResponse> getDateWiseAttnForCMDash(@Valid @RequestBody CMDashRequest cMDashRequest) {
        logger.debug("getInstituteWiseAttnForDirector");
        return dashboardService.countAttendanceForCMDash(cMDashRequest);
    }
    
     @PostMapping("/dashboard/cmdash/facultyDailyTask")
    public List<CMDashDailyTaskResponse> getDateWiseFacultyDailtyTaskForCMDash(@Valid @RequestBody CMDashRequest cMDashRequest) {
        logger.debug("getInstituteWiseAttnForDirector");
        return dashboardService.facultyDailyTaskForCMDash(cMDashRequest);
    }
//     Director dashboard count service End
}
