package com.springapi.controller;

import com.springapi.model.InstituteTypeMaster;
import com.springapi.service.InstituteTypeService;
import java.util.List;
import javax.validation.Valid;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author Jyoti_patel
 */
@RestController
@RequestMapping("/api")
@CrossOrigin(origins = "*")
public class InstituteTypeController {
     static Logger logger = Logger.getLogger(InstituteTypeController.class);
    @Autowired
    private InstituteTypeService instituteTypeService;
    
    @PostMapping("/institute/type")
    public InstituteTypeMaster create(@Valid @RequestBody InstituteTypeMaster institute) {
        logger.debug("create");
        return instituteTypeService.create(institute);
    }

    /* get all InstituteTypeMaster */
    @GetMapping("/institute/type/list")
    public List<InstituteTypeMaster> getAllInstituteTypeMasters() {
        logger.debug("list");
        return instituteTypeService.list();
    }

    @GetMapping("/institute/type/{id}")
    public InstituteTypeMaster read(@PathVariable(value = "id") Long instituteTypeId) {
        logger.debug("read");
        return instituteTypeService.read(instituteTypeId);
    }

    /* Delete an InstituteTypeMaster */
    @DeleteMapping("/institute/type/{id}")
    public String delete(@PathVariable(value = "id") Long instituteTypeId) {
        logger.debug("delete");
        instituteTypeService.delete(instituteTypeId);
        return "";
    }

}
