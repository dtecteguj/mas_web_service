package com.springapi.controller;

import com.springapi.service.FcmDailyPushNotificationService;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.json.JSONObject;
import org.springframework.scheduling.annotation.Scheduled;

/**
 * @author shahb
 */
@RestController
@RequestMapping("/api")
public class FcmDailyPushNotificationController {

    static Logger logger = Logger.getLogger(FcmDailyPushNotificationController.class);

    @Autowired
    private FcmDailyPushNotificationService fcmDailyPushNotificationService;

    public List<String> getAllActiveTokenView() {
        logger.debug("getAllActiveTokenView");
        return fcmDailyPushNotificationService.getTokensForFaculty();
    }

    public List<String> getAllStudentView() {
        logger.debug("getAllStudentView");
        return fcmDailyPushNotificationService.getTokensForStudent();
    }

    public List<String> getAllAttendanceTokenView() {
        logger.debug("getAllAttendanceTokenView");
        return fcmDailyPushNotificationService.getAllAttendanceTokenView();
    }

    // List of Faculty who are active for timetable
    @GetMapping("/send")
//    @Scheduled(cron = "0 30 10,11,15,16 * * MON-SAT")
    // @Scheduled(cron = "0 0/3 * * * MON-SAT")
    public ResponseEntity<String> send() throws JSONException {
        List<String> tokensManageList = getAllActiveTokenView();

        //Create Json For push notification
        Long slotId = fcmDailyPushNotificationService.getSlotId();
        HttpEntity<String> request = createJsonData(tokensManageList, "F", slotId);
        // HttpEntity<String> requestForInActive = createJsonData(tokensManageListForInActive,"F");

        CompletableFuture<String> pushNotification = null;
        if (tokensManageList.size() > 0) {
            pushNotification = fcmDailyPushNotificationService.send(request);
            CompletableFuture.allOf(pushNotification).join();
        }

        try {
            String firebaseResponse = pushNotification.get();
            return new ResponseEntity<>(firebaseResponse, HttpStatus.OK);
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
        return new ResponseEntity<>("Push Notification ERROR!", HttpStatus.BAD_REQUEST);
    }

    @GetMapping("/sendSeconfShift")
//    @Scheduled(cron = "0 0 13,14 * * MON-SAT")
    public ResponseEntity<String> sendSecondShift() throws JSONException {

        List<String> tokensManageList = getAllActiveTokenView();
        //  List<String> tokensManageListForInActive = getAllInActiveTokenView();

        //Create Json For push notification
        Long slotId = fcmDailyPushNotificationService.getSlotId();
        HttpEntity<String> request = createJsonData(tokensManageList, "F", slotId);
        //HttpEntity<String> requestForInActive = createJsonData(tokensManageListForInActive,"F");

        CompletableFuture<String> pushNotification = null;
        if (tokensManageList.size() > 0) {
            pushNotification = fcmDailyPushNotificationService.send(request);
            CompletableFuture.allOf(pushNotification).join();
        }

        try {
            String firebaseResponse = pushNotification.get();
            return new ResponseEntity<>(firebaseResponse, HttpStatus.OK);
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }

        return new ResponseEntity<>("Push Notification ERROR!", HttpStatus.BAD_REQUEST);
    }

    @GetMapping("/attendance")
//    @Scheduled(cron = "0 25 11,12,16,17 * * MON-SAT")
    //  @Scheduled(cron = "0 0/2 * * * MON-SAT")
    public ResponseEntity<String> attendanceSend() throws JSONException {
        List<String> tokensManageList = getAllAttendanceTokenView();
        //Create Json For push notification
        Long slotId = fcmDailyPushNotificationService.getStudAndFacAttnVerificationSlotId();
        HttpEntity<String> request = createJsonData(tokensManageList, "V", slotId);
        CompletableFuture<String> pushNotification = null;
        if (tokensManageList.size() > 0) {
            pushNotification = fcmDailyPushNotificationService.send(request);
            CompletableFuture.allOf(pushNotification).join();
        }
        try {
            String firebaseResponse = pushNotification.get();
            return new ResponseEntity<>(firebaseResponse, HttpStatus.OK);
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
        return new ResponseEntity<>("Push Notification ERROR!", HttpStatus.BAD_REQUEST);
    }

    @GetMapping("/student/popup")
//    @Scheduled(cron = "0 15 11,12,16,17 * * MON-SAT")
    //@Scheduled(cron = "0 0/3 * * * MON-SAT")
    public ResponseEntity<String> studentSend() throws JSONException {
        List<String> tokensManageList = getAllStudentView();

        //Create Json For push notification
        Long slotId = fcmDailyPushNotificationService.getStudAndFacAttnVerificationSlotId();
        HttpEntity<String> request = createJsonData(tokensManageList, "S", slotId);

        CompletableFuture<String> pushNotification = null;
        if (tokensManageList.size() > 0) {
            pushNotification = fcmDailyPushNotificationService.send(request);
            CompletableFuture.allOf(pushNotification).join();
        }

        try {
            String firebaseResponse = pushNotification.get();
            return new ResponseEntity<>(firebaseResponse, HttpStatus.OK);
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
        return new ResponseEntity<>("Push Notification ERROR!", HttpStatus.BAD_REQUEST);
    }

    //Common method
    private HttpEntity<String> createJsonData(List<String> tokensManageList, String facultySlotType, Long slotId) {
        JSONObject body = new JSONObject();
        body.put("priority", "high");

        JSONObject notification = new JSONObject();
        notification.put("title", "Mobile Attendance System (Faculty)");
        notification.put("body", "Lecture Notification");
        notification.put("click_action", "PopupFacultyActivity");

        Long day = fcmDailyPushNotificationService.getTodayDay();

        JSONObject data = new JSONObject();
        data.put("User", facultySlotType);
        data.put("DayId", day);
        data.put("SlotId", slotId);

        body.put("notification", notification);

        JSONArray jSONArray = new JSONArray();
        for (String token : tokensManageList) {
            jSONArray.put(token);
        }

        body.put("registration_ids", jSONArray);
        body.put("data", data);
        System.out.println("body ::" + body);
        System.out.println("body ::" + body.toString());
        HttpEntity<String> request = new HttpEntity<>(body.toString());

        return request;
    }
    
    /* @Scheduled */ 
    // For Memo and 

}
