/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.springapi.controller;

import com.springapi.model.AndroidVersion;
import com.springapi.model.ShiftMaster;
import com.springapi.service.AndroidVersionService;
import com.springapi.service.ShiftMasterService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author shahb
 */
@RestController
@RequestMapping("/api")
@CrossOrigin(origins = "*")
public class ShiftMasterController {

    @Autowired
    private ShiftMasterService shiftMasterService;

    @GetMapping("/shift/institute/{id}")
    public List<ShiftMaster> read(@PathVariable(value = "id") Long iId) {
        return shiftMasterService.readbyinst(iId);
    }
}
