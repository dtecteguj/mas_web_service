/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.springapi.controller;

import static com.springapi.controller.HolidayMasterController.logger;
import com.springapi.model.HolidayMaster;
import com.springapi.model.InstituteDepartmentMpg;
import com.springapi.service.HolidayMasterService;
import com.springapi.service.InstituteDepartmentMpgService;
import java.util.List;
import javax.validation.Valid;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author shahb
 */

@RestController
@RequestMapping("/api")
@CrossOrigin(origins = "*")
public class InstituteDepartmentMpgController {
    static Logger logger = Logger.getLogger(InstituteDepartmentMpgController.class);
    
    @Autowired
    private InstituteDepartmentMpgService  instituteDepartmentMpgService;
    
    @PostMapping("/instituteDepartmentMpg")
    public InstituteDepartmentMpg create(@Valid @RequestBody InstituteDepartmentMpg instituteDepartmentMpg)
    {
        logger.debug("create");
        return instituteDepartmentMpgService.create(instituteDepartmentMpg);
    }
    @GetMapping("/instituteDepartmentMpg/list")
    public List<InstituteDepartmentMpg> getAllFacultyShiftMpg()
    {
        logger.debug("list");
        return instituteDepartmentMpgService.list();
    }
    
    @GetMapping("/instituteDepartmentMpg/{id}")
    public InstituteDepartmentMpg read(@PathVariable(value = "id") Long iId) {
        logger.debug("read");
        return instituteDepartmentMpgService.read(iId);
    }
    
   /* Update an InstituteDepartmentMpgService Master */
    @PutMapping("/instituteDepartmentMpg")
    public InstituteDepartmentMpg update(@Valid @RequestBody InstituteDepartmentMpg instituteDepartmentMpg) {
        logger.debug("update");
        return instituteDepartmentMpgService.update(instituteDepartmentMpg);
    }

    /* Delete an InstituteDepartmentMpgService Master */
    @DeleteMapping("/instituteDepartmentMpg/{id}")
    public String delete(@PathVariable(value = "id") Long hId) {
        logger.debug("delete");
        instituteDepartmentMpgService.delete(hId);
        return "";
    }
}
