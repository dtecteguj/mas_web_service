/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.springapi.controller;

import static com.springapi.controller.InstituteMasterController.logger;
import com.springapi.model.InstituteMaster;
import com.springapi.model.TermMaster;
import com.springapi.service.TermMasterService;
import java.util.List;
import javax.validation.Valid;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author shahb
 */
@RestController
@RequestMapping("/api")
@CrossOrigin(origins = "*")
public class TermMasterController {
    static Logger logger = Logger.getLogger(TermMasterController.class);
    
    @Autowired
    private TermMasterService termMasterService;
    
    @PostMapping("/termMaster")
    public TermMaster create(@Valid @RequestBody TermMaster termMaster)
    {
        logger.debug("create");
        return termMasterService.create(termMaster);
    }
    
    @GetMapping("/termMaster/list")
    public List<TermMaster> getAllTermMaster()
    {
        logger.debug("list");
        return termMasterService.list();
    }
    
    @GetMapping("/termMaster/{id}")
    public TermMaster read(@PathVariable(value = "id") Long tId) {
        logger.debug("read");
        return termMasterService.read(tId);
    }
    
   /* Update an Term Master */
    @PutMapping("/termMaster")
    public TermMaster update(@Valid @RequestBody TermMaster termMaster) {
        logger.debug("update");
        return termMasterService.update(termMaster);
    }

    /* Delete an Term Master */
    @DeleteMapping("/termMaster/{id}")
    public String delete(@PathVariable(value = "id") Long tId) {
        logger.debug("delete");
        termMasterService.delete(tId);
        return "";
    }
}
