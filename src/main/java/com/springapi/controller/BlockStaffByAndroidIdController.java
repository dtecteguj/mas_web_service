/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.springapi.controller;

import com.springapi.model.BlockStaffByAndroidId;
import com.springapi.model.TrnDtlLoginLog;
import com.springapi.service.BlockStaffByAndroidIdService;
import java.util.List;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author shahb
 */
@RestController
@RequestMapping("/api")
@CrossOrigin(origins = "*")
public class BlockStaffByAndroidIdController {
    
    @Autowired
    private BlockStaffByAndroidIdService blockStaffByAndroidIdService;

    
    /* to Save an Faculty*/
    @PostMapping("/blockStaffByAndroidId")
    public BlockStaffByAndroidId create(@Valid @RequestBody BlockStaffByAndroidId blockStaffByAndroidId) {
        return blockStaffByAndroidIdService.create(blockStaffByAndroidId);
    }

    /* get all Faculty */
    @GetMapping("/blockStaffByAndroidId/list")
    public List<BlockStaffByAndroidId> getAllFacultys() {
        return blockStaffByAndroidIdService.list();
    }

    @GetMapping("/blockStaffByAndroidId/{id}")
    public BlockStaffByAndroidId read(@PathVariable(value = "id") Long fId) {
        return blockStaffByAndroidIdService.read(fId);
    }

    /* Update an Faculty by FacultyId */
    @PutMapping("/blockStaffByAndroidId")
    public BlockStaffByAndroidId update(@Valid @RequestBody BlockStaffByAndroidId blockStaffByAndroidId) {
        return blockStaffByAndroidIdService.update(blockStaffByAndroidId);
    }

    /* Delete an Faculty */
    @DeleteMapping("/blockStaffByAndroidId/{id}")
    public String delete(@PathVariable(value = "id") Long fId) {
        blockStaffByAndroidIdService.delete(fId);
        return "";
    }
}
