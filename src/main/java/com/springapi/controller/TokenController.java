package com.springapi.controller;

import com.springapi.model.Faculty;
import com.springapi.model.TokensManage;
import com.springapi.service.FacultyService;
import com.springapi.service.TokenService;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author Jyoti_patel
 */

@RestController
@RequestMapping("/api")
@CrossOrigin(origins = "*")
public class TokenController {
    
    @Autowired
    private TokenService tokenService;

    /* to create or update token*/
    @PostMapping("/token/create")
    public TokensManage createOrUpdate(@Valid @RequestBody TokensManage tokenManange) {
        return tokenService.createOrUpdate(tokenManange);
    }
}
