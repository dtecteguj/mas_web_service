/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.springapi.controller;

import com.springapi.model.Lecture;
import com.springapi.service.LectureService;
import java.util.List;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author shahb
 */
@RestController
@RequestMapping("/gmca")
public class LectureController {

    @Autowired
    private LectureService lectureService;

    /* to Save an Lecture*/
    @PostMapping("/lecture")
    public Lecture create(@Valid @RequestBody Lecture lecture) {
        return lectureService.create(lecture);
    }

    /* get all Lecture */
    @GetMapping("/lecture/list")
    public List<Lecture> getAllLectures() {
        return lectureService.list();
    }

    @GetMapping("/lecture/{id}")
    public Lecture read(@PathVariable(value = "id") Long fId) {
        return lectureService.read(fId);
    }

    /* Update an Lecture by LectureId */
    @PutMapping("/lecture")
    public Lecture update(@Valid @RequestBody Lecture lectureDetails) {
        return lectureService.update(lectureDetails);
    }

    /* Delete an Lecture */
    @DeleteMapping("/lecture/{id}")
    public String delete(@PathVariable(value = "id") Long fId) {
        lectureService.delete(fId);
        return "";
    }

}
