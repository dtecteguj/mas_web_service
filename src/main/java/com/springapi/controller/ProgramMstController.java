/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.springapi.controller;

import com.springapi.model.ProgramMst;
import com.springapi.service.ProgramMstService;
import java.util.List;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author shahb
 */
@RestController
@RequestMapping("/api")
@CrossOrigin(origins = "*")
public class ProgramMstController {
    @Autowired
    private ProgramMstService programMstService;

    /* to Save an Faculty*/
    @PostMapping("/programMst")
    public ProgramMst create(@Valid @RequestBody ProgramMst programMst) {
        return programMstService.create(programMst);
    }

    /* get all Faculty */
    @GetMapping("/programMst/list")
    public List<ProgramMst> getAllProgramMst() {
        return programMstService.list();
    }

    @GetMapping("/programMst/{id}")
    public ProgramMst read(@PathVariable(value = "id") Long fId) {
        return programMstService.read(fId);
    }

    /* Update an Faculty by FacultyId */
    @PutMapping("/programMst")
    public ProgramMst update(@Valid @RequestBody ProgramMst programMst) {
        return programMstService.update(programMst);
    }

    /* Delete an Faculty */
    @DeleteMapping("/programMst/{id}")
    public String delete(@PathVariable(value = "id") Long fId) {
        programMstService.delete(fId);
        return "";
    }
}
