package com.springapi.controller;

import com.springapi.dto.request.SlotRequest;
import com.springapi.model.Semester;
import com.springapi.model.SlotMaster;
import com.springapi.service.SlotService;
import java.util.List;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author Jyoti_patel
 */
@RestController
@RequestMapping("/api")
@CrossOrigin(origins = "*")
public class SlotController {
    
    @Autowired
    private SlotService slotService;
    
//    @GetMapping("/slot/list/by/{instituteId}")
//    public List<SlotMaster> getSlotByInstitute(@PathVariable(value = "instituteId") Long instituteId) {
//        return slotService.getSlotByInstitute(instituteId);
//    }
    
    @PostMapping("/slot/list")
    public List<SlotMaster> getSlotByInstitute(@Valid @RequestBody SlotRequest slotRequest) {
        return slotService.getSlotByInstitute(slotRequest);
    }
}
