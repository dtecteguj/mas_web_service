package com.springapi.controller;

import com.springapi.model.InstituteMaster;
import com.springapi.service.InstituteService;
import java.util.HashMap;
import java.util.List;
import javax.validation.Valid;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author Jyoti Patel
 */
@RestController
@RequestMapping("/api")
@CrossOrigin(origins = "*")
public class InstituteMasterController {
    static Logger logger = Logger.getLogger(InstituteMasterController.class);
     
     @Autowired
     private InstituteService instituteService;
     
     /* to Save an InstituteMaster*/
    @PostMapping("/institute")
    public InstituteMaster create(@Valid @RequestBody InstituteMaster institute) {
        logger.debug("create");
        return instituteService.create(institute);
    }

    /* get all InstituteMaster */
    @GetMapping("/institute/list")
    public List<InstituteMaster> getAllInstituteMasters() {
        logger.debug("list");
        return instituteService.list();
    }
    
    /* get all InstituteMaster by institute type*/
    @GetMapping("/institute/list/by/type/{instituteTypeId}")
    public List<InstituteMaster> getAllInstituteByType(@PathVariable(value = "instituteTypeId") Long instituteTypeId) {
        logger.debug("getAllInstituteByType");
        return instituteService.getAllInstituteByType(instituteTypeId);
    }

    @GetMapping("/institute/{id}")
    public InstituteMaster read(@PathVariable(value = "id") Long uId) {
        logger.debug("read");
        return instituteService.read(uId);
    }

    /* Update an InstituteMaster by InstituteMasterId */
    @PutMapping("/institute")
    public InstituteMaster update(@Valid @RequestBody InstituteMaster instituteDetails) {
        logger.debug("update");
        return instituteService.update(instituteDetails);
    }

    /* Delete an InstituteMaster */
    @DeleteMapping("/institute/{id}")
    public String delete(@PathVariable(value = "id") Long fId) {
        logger.debug("delete");
        instituteService.delete(fId);
        return "";
    }

     @GetMapping("/institute/progDep/{id}")
    public HashMap<String,Object> readDepartmentAndProgarm(@PathVariable(value = "id") Long uId) {
        logger.debug("read");
        return instituteService.readDepartmentAndProgarm(uId);
    }
}
