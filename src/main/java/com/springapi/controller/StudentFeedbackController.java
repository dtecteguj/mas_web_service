package com.springapi.controller;

import com.springapi.dto.request.StudentTimeTableRequest;
import com.springapi.dto.response.StudentFeedbackAttendanceResponse;
import com.springapi.dto.response.StudentTimeTableResponse;
import com.springapi.model.StudentFeedback;
import com.springapi.service.StudentFeedbackService;
import java.util.List;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author shahb
// */
@RestController
@RequestMapping("/api")
@CrossOrigin(origins = "*")
public class StudentFeedbackController {
    
    @Autowired
    private StudentFeedbackService studentFeedbackService;
    
    @PostMapping("/student/feedback")
    public StudentFeedback create(@Valid @RequestBody StudentFeedback studentFeedbackRequest) {
        return studentFeedbackService.create(studentFeedbackRequest);
        //return null;
    }
    
    @PostMapping("/student/timetable/info")
    public StudentTimeTableResponse studentTimeTableInfo(@Valid @RequestBody StudentTimeTableRequest studentFeedbackRequest) {
        return studentFeedbackService.studentTimeTableInfo(studentFeedbackRequest);
    }
    
    @PostMapping("/student/attendance/list")
    public StudentFeedbackAttendanceResponse studentAttendanceList(@Valid @RequestBody StudentTimeTableRequest studentFeedbackRequest) {
        return studentFeedbackService.studentAttendanceList(studentFeedbackRequest);
    }
    
    @PostMapping("/student/feedback/list")
    public Page<StudentFeedback> studentFeedbackList(@Valid @RequestBody StudentTimeTableRequest studentFeedbackRequest) {
        return studentFeedbackService.studentFeedbackList(studentFeedbackRequest);
    }
     @PostMapping("/student/feedback/daily/list")
    public List<StudentFeedback> studentDailyFeedbackList(@Valid @RequestBody StudentFeedback studentFeedback) {
        return studentFeedbackService.studentFeedbackDailyList(studentFeedback);
       //   return null;
    }
}
