/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.springapi.controller;

import com.springapi.dto.request.LoginLogRequest;
import com.springapi.dto.response.AndroidVersionStatus;
import com.springapi.model.AndroidVersion;
import com.springapi.model.TrnDtlLoginLog;
import com.springapi.service.AndroidVersionService;
import com.springapi.service.TrnDtlLoginLogService;
import java.util.List;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author shahb
 */
@RestController
@RequestMapping("/api")
@CrossOrigin(origins = "*")
public class TrnDtlLoginLogController {

    @Autowired
    private TrnDtlLoginLogService trnDtlLoginLogService;

    /* to Save an Faculty*/
    @PostMapping("/trnDtlLoginLogService")
    public TrnDtlLoginLog create(@Valid @RequestBody TrnDtlLoginLog trnDtlLoginLog) {
        return trnDtlLoginLogService.create(trnDtlLoginLog);
    }

    /* get all Faculty */
    @GetMapping("/trnDtlLoginLogService/list")
    public List<TrnDtlLoginLog> getAllFacultys() {
        return trnDtlLoginLogService.list();
    }

    @GetMapping("/trnDtlLoginLogService/{id}")
    public TrnDtlLoginLog read(@PathVariable(value = "id") Long fId) {
        return trnDtlLoginLogService.read(fId);
    }

    /* Update an Faculty by FacultyId */
    @PutMapping("/trnDtlLoginLogService")
    public TrnDtlLoginLog update(@Valid @RequestBody TrnDtlLoginLog trnDtlLoginLog) {
        return trnDtlLoginLogService.update(trnDtlLoginLog);
    }

    /* Delete an Faculty */
    @DeleteMapping("/trnDtlLoginLogService/{id}")
    public String delete(@PathVariable(value = "id") Long fId) {
        trnDtlLoginLogService.delete(fId);
        return "";
    }

    @PostMapping("/trnDtlLoginLogService/find/by/androidId")
    public TrnDtlLoginLog findByAndroidId(@Valid @RequestBody LoginLogRequest loginLogRequest) {
        return trnDtlLoginLogService.findByAndroidId(loginLogRequest);
    }
}
