/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.springapi.controller;

import com.springapi.model.TrnLongleaveDtls;
import com.springapi.service.TrnLongleaveDtlsService;
import java.util.List;
import javax.validation.Valid;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author shahb
 */
@RestController
@RequestMapping("/api")
@CrossOrigin(origins = "*")
public class TrnLongleaveDtlsController {

    static Logger logger = Logger.getLogger(TrnDtlIssueMemoController.class);
    @Autowired
    private TrnLongleaveDtlsService trnLongleaveDtlsService;

    @PostMapping("/trnLongleaveDtls")
    public TrnLongleaveDtls create(@Valid @RequestBody TrnLongleaveDtls trnLongleaveDtls) {
        logger.debug("create");
        return trnLongleaveDtlsService.create(trnLongleaveDtls);
    }

    @GetMapping("/trnLongleaveDtls/list")
    public List<TrnLongleaveDtls> getAllFacultyShiftMpg() {
        logger.debug("list");
        return trnLongleaveDtlsService.list();
    }

    @GetMapping("/trnLongleaveDtls/{id}")
    public TrnLongleaveDtls read(@PathVariable(value = "id") Long lId) {
        logger.debug("read");
        return trnLongleaveDtlsService.read(lId);
    }

    /* Update an LeaveType Master */
    @PutMapping("/trnLongleaveDtls")
    public TrnLongleaveDtls update(@Valid @RequestBody TrnLongleaveDtls trnLongleaveDtls) {
        logger.debug("update");
        return trnLongleaveDtlsService.update(trnLongleaveDtls);
    }

    /* Delete an LeaveType Master */
    @DeleteMapping("/trnLongleaveDtls/{id}")
    public String delete(@PathVariable(value = "id") Long lId) {
        logger.debug("delete");
        trnLongleaveDtlsService.delete(lId);
        return "";
    }

    @GetMapping("/trnLongleaveDtls/faculty/{id}")
    public List<TrnLongleaveDtls> readbyfid(@PathVariable(value = "id") Long fId) {
        logger.debug("read");
        return trnLongleaveDtlsService.readbyfid(fId);
    }

}
