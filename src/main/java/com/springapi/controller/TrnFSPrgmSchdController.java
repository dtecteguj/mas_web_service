/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.springapi.controller;

import static com.springapi.controller.TrnDtlLeaveStatusController.logger;
import com.springapi.model.TrnFSPrgmSchd;
import com.springapi.service.TrnFSPrgmSchdService;
import java.util.List;
import javax.validation.Valid;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author shahb
 */
@RestController
@RequestMapping("/api")
@CrossOrigin(origins = "*")
public class TrnFSPrgmSchdController {
    static Logger logger = Logger.getLogger(TrnFSPrgmSchdController.class);
    @Autowired
    private TrnFSPrgmSchdService trnFSPrgmSchdService;
    
    @PostMapping("/trnFSPrgmSchd")
    public TrnFSPrgmSchd create(@Valid @RequestBody TrnFSPrgmSchd trnFSPrgmSchd) {
        logger.debug("create");
        return trnFSPrgmSchdService.create(trnFSPrgmSchd);
    }

    @GetMapping("/trnFSPrgmSchd/list")
    public List<TrnFSPrgmSchd> getAllTrnFSPrgmSchd() {
        logger.debug("list");
        return trnFSPrgmSchdService.list();
    }

    @GetMapping("/trnFSPrgmSchd/{id}")
    public TrnFSPrgmSchd read(@PathVariable(value = "id") Long lId) {
        logger.debug("read");
        return trnFSPrgmSchdService.read(lId);
    }

    /* Update an LeaveType Master */
    @PutMapping("/trnFSPrgmSchd")
    public TrnFSPrgmSchd update(@Valid @RequestBody TrnFSPrgmSchd trnFSPrgmSchd) {
        logger.debug("update");
        return trnFSPrgmSchdService.update(trnFSPrgmSchd);
    }

    /* Delete an LeaveType Master */
    @DeleteMapping("/trnFSPrgmSchd/{id}")
    public String delete(@PathVariable(value = "id") Long lId) {
        logger.debug("delete");
        trnFSPrgmSchdService.delete(lId);
        return "";
    }
    
}
