package com.springapi.controller;

import com.springapi.model.InstituteWifiMaster;
import com.springapi.service.InstituteWifiService;
import java.util.List;
import javax.validation.Valid;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author Jyoti Patel
 */
@RestController
@RequestMapping
public class InstituteWifiMasterController {

    static Logger logger = Logger.getLogger(InstituteWifiMasterController.class);

    @Autowired
    private InstituteWifiService instituteWifiService;

    /* to Save an InstituteWifiMaster*/
    @PostMapping("/instituteWifi")
    public InstituteWifiMaster create(@Valid @RequestBody InstituteWifiMaster instituteWifi) {
        logger.debug("create");
        return instituteWifiService.create(instituteWifi);
    }

    /* get all InstituteWifiMaster */
    @GetMapping("/instituteWifi/list")
    public List<InstituteWifiMaster> getAllInstituteWifiMasters() {
        logger.debug("list");
        return instituteWifiService.list();
    }

    @GetMapping("/instituteWifi/{id}")
    public InstituteWifiMaster read(@PathVariable(value = "id") Long uId) {
        logger.debug("read");
        return instituteWifiService.read(uId);
    }

    /* Update an InstituteWifiMaster by InstituteWifiMasterId */
    @PutMapping("/instituteWifi")
    public InstituteWifiMaster update(@Valid @RequestBody InstituteWifiMaster instituteWifiDetails) {
        logger.debug("update");
        return instituteWifiService.update(instituteWifiDetails);
    }

    /* Delete an InstituteWifiMaster */
    @DeleteMapping("/instituteWifi/{id}")
    public String delete(@PathVariable(value = "id") Long fId) {
        logger.debug("delete");
        instituteWifiService.delete(fId);
        return "";
    }

}
