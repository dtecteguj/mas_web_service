package com.springapi.controller;

import com.springapi.model.DepartmentMaster;
import com.springapi.service.DepartmentService;
import java.util.List;
import javax.validation.Valid;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author USER
 */
@RestController
@RequestMapping("/api")
@CrossOrigin(origins = "*")
public class DepartmentMasterController {

    static Logger logger = Logger.getLogger(DepartmentMasterController.class);

    @Autowired
    private DepartmentService departmentService;

    /* to Save an DepartmentMaster*/
    @PostMapping("/department")
    public DepartmentMaster create(@Valid @RequestBody DepartmentMaster department) {
        logger.debug("create");
        return departmentService.create(department);
    }

    /* get all DepartmentMaster */
    @GetMapping("/department/list")
    public List<DepartmentMaster> getAllDepartmentMasters() {
        logger.debug("list");
        return departmentService.list();
    }

    @GetMapping("/department/{id}")
    public DepartmentMaster read(@PathVariable(value = "id") Long dId) {
        logger.debug("read");
        return departmentService.read(dId);
    }

    /* Update an DepartmentMaster by DepartmentMasterId */
    @PutMapping("/department")
    public DepartmentMaster update(@Valid @RequestBody DepartmentMaster departmentDetails) {
        logger.debug("update");
        return departmentService.update(departmentDetails);
    }

    /* Delete an DepartmentMaster */
    @DeleteMapping("/department/{id}")
    public String delete(@PathVariable(value = "id") Long fId) {
        logger.debug("delete");
        departmentService.delete(fId);
        return "";
    }
    
    @GetMapping("/department/by/institute/{instituteId}")
    public List<DepartmentMaster> readDepartmentByInstituteId(@PathVariable(value = "instituteId") Long instituteId) {
        logger.debug("readDepartmentByInstituteId");
        return departmentService.readDepartmentByInstituteId(instituteId);
    }

}
