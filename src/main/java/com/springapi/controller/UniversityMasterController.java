package com.springapi.controller;

import com.springapi.model.UniversityMaster;
import com.springapi.service.UniversityService;
import java.util.List;
import javax.validation.Valid;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Jyoti Patel
 */
@RestController
@RequestMapping("/api")
@CrossOrigin(origins = "*")
public class UniversityMasterController {
     static Logger logger = Logger.getLogger(UniversityMasterController.class);
     
     @Autowired
     private UniversityService universityService;
     
     /* to Save an UniversityMaster*/
    @PostMapping("/university")
    public UniversityMaster create(@Valid @RequestBody UniversityMaster university) {
        logger.debug("create");
        return universityService.create(university);
    }

    /* get all UniversityMaster */
    @GetMapping("/university/list")
    public List<UniversityMaster> getAllUniversityMasters() {
        logger.debug("list");
        return universityService.list();
    }

    @GetMapping("/university/{id}")
    public UniversityMaster read(@PathVariable(value = "id") Long uId) {
        logger.debug("read");
        return universityService.read(uId);
    }

    /* Update an UniversityMaster by UniversityMasterId */
    @PutMapping("/university")
    public UniversityMaster update(@Valid @RequestBody UniversityMaster universityDetails) {
        logger.debug("update");
        return universityService.update(universityDetails);
    }

    /* Delete an UniversityMaster */
    @DeleteMapping("/university/{id}")
    public String delete(@PathVariable(value = "id") Long fId) {
        logger.debug("delete");
        universityService.delete(fId);
        return "";
    }

}
