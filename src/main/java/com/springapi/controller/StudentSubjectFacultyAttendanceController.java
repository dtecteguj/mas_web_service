package com.springapi.controller;

import com.springapi.dto.request.FacStudAttenRequest;
import com.springapi.dto.request.StudentAttendanceDetailRequest;
import com.springapi.dto.request.StudentAttendanceRequest;
import com.springapi.dto.request.StudentAttendanceTotalRequest;
import com.springapi.dto.response.StudentAttendanceCountResponse;
import com.springapi.model.StudentAttendaceCount;
import com.springapi.model.StudentSubjectFacultyAttendance;
import com.springapi.service.StudentSubjectFacultyAttendanceService;
import java.util.List;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author Jyoti 
 */
@RestController
@RequestMapping("/api")
@CrossOrigin(origins = "*")
public class StudentSubjectFacultyAttendanceController {

    @Autowired
    private StudentSubjectFacultyAttendanceService studentSubjectFacultyAttendanceService;

    /* to Save an StudentSubjectFacultyAttendance*/
    @PostMapping("/studentSubjectFacultyAttendance")
    public StudentSubjectFacultyAttendance create(@Valid @RequestBody StudentSubjectFacultyAttendance studentSubjectFacultyAttendance) {
        return studentSubjectFacultyAttendanceService.create(studentSubjectFacultyAttendance);
    }

    /* get all StudentSubjectFacultyAttendance */
    @GetMapping("/studentSubjectFacultyAttendance/list")
    public List<StudentSubjectFacultyAttendance> getAllStudentSubjectFacultyAttendances() {
        return studentSubjectFacultyAttendanceService.list();
    }

    @GetMapping("/studentSubjectFacultyAttendance/{id}")
    public StudentSubjectFacultyAttendance read(@PathVariable(value = "id") Long fId) {
        return studentSubjectFacultyAttendanceService.read(fId);
    }

    /* Delete an StudentSubjectFacultyAttendance */
    @DeleteMapping("/studentSubjectFacultyAttendance/{id}")
    public String delete(@PathVariable(value = "id") Long fId) {
        studentSubjectFacultyAttendanceService.delete(fId);
        return "";
    }
    
     /* default Faculty Entry in table*/
    @PostMapping("/faculty/sem/lecture")
    public List<StudentSubjectFacultyAttendance> defaultAttendance (@Valid @RequestBody FacStudAttenRequest facStudAttenRequest) {
        return studentSubjectFacultyAttendanceService.defaultAttendance(facStudAttenRequest);
    }
    
     /* default Faculty Entry in table*/
    @PostMapping("/student/attendance/daily")
    public String studentAttendance (@Valid @RequestBody StudentAttendanceRequest studentAttendanceRequest) {
        return studentSubjectFacultyAttendanceService.studentAttendance(studentAttendanceRequest);
    }
    
    @PostMapping("/student/attendance/count/daily")
    public  List<StudentAttendanceCountResponse> studentAttendanceCount (@Valid @RequestBody StudentAttendanceRequest studentAttendanceRequest) {
        return studentSubjectFacultyAttendanceService.studentAttendanceCount(studentAttendanceRequest);
    }
    
    
    @PostMapping("/student/attendance/total")
    public  String studentAttendanceCount (@Valid @RequestBody StudentAttendanceTotalRequest studentAttendanceTotalRequest) {
        return studentSubjectFacultyAttendanceService.studentAttendanceTotalCount(studentAttendanceTotalRequest);
    }
    
    @GetMapping("/student/attendance/total/count")
    public  List<StudentAttendaceCount> studentAttendanceCount () {
        return studentSubjectFacultyAttendanceService.studentAttendanceCountList();
    }
    
    @PostMapping("/student/attendance/detail")
    public  StudentAttendaceCount getStudentAtendance (@Valid @RequestBody StudentAttendanceDetailRequest studentAttendanceDetailRequest) {
        return studentSubjectFacultyAttendanceService.getStudentAtendance(studentAttendanceDetailRequest);
    }

}
