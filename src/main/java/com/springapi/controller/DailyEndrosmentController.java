package com.springapi.controller;

import com.springapi.dto.request.DailyEndrosRequest;
import com.springapi.dto.request.DailyEndrosStatusRequest;
import com.springapi.model.DailyEndrosment;
import com.springapi.service.DailyEndrosmentService;
import javax.validation.Valid;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author Jyoti Patel
 */
@RestController
@RequestMapping("/api")
@CrossOrigin(origins = "*")
public class DailyEndrosmentController {

    static Logger logger = Logger.getLogger(DailyEndrosmentController.class);

    @Autowired
    private DailyEndrosmentService dailyEndrosmentService;

    @PostMapping("/daily/endros/hod")
    public DailyEndrosment endrosByHod(@Valid @RequestBody DailyEndrosRequest dailyEndrosRequest) {
        logger.debug("endrosByHod");
        return dailyEndrosmentService.endrosByHod(dailyEndrosRequest);
    }

    @PostMapping("/daily/endros/principal")
    public DailyEndrosment endrosByprincipal(@Valid @RequestBody DailyEndrosRequest dailyEndrosRequest) {
        logger.debug("endrosByprincipal");
        return dailyEndrosmentService.endrosByprincipal(dailyEndrosRequest);
    }

    @PostMapping("/daily/endros/status/hod")
    public Boolean getEndrosStatusForHod(@Valid @RequestBody DailyEndrosStatusRequest dailyEndrosStatusRequest) {
        logger.debug("getEndrosStatusForHod");
        return dailyEndrosmentService.endrosStatusForHod(dailyEndrosStatusRequest);
    }

    @PostMapping("/daily/endros/status/principal")
    public Boolean getEndrosStatusForPrincipal(@Valid @RequestBody DailyEndrosStatusRequest dailyEndrosStatusRequest) {
        logger.debug("getEndrosStatusForPrincipal");
        return dailyEndrosmentService.endrosStatusForPrincipal(dailyEndrosStatusRequest);
    }
}
