package com.springapi.controller;

import com.springapi.dto.request.TimeTableRequest;
import com.springapi.model.TimeTable;
import com.springapi.service.FacultyAttendanceService;
import com.springapi.service.TimeTableService;
import java.util.List;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Jyoti Patel
 */
@RestController
@RequestMapping("/api")
@CrossOrigin(origins = "*")
public class TimeTableController {
    
     @Autowired
    private TimeTableService timeTableService;

    @PostMapping("/timetable/list")
    public List<TimeTable> timeTableList(@Valid @RequestBody TimeTableRequest timeTableRequest) {
       return timeTableService.timeTableList(timeTableRequest);
    }
}
