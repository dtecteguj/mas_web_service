/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.springapi.controller;

import static com.springapi.controller.DepartmentMasterController.logger;
import com.springapi.model.DesignationMaster;
import com.springapi.service.DesignationMasterService;
import java.util.List;
import javax.validation.Valid;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author shahb
 */

@RestController
@RequestMapping("/api")
@CrossOrigin(origins = "*")
public class DesignationMasterController {
     static Logger logger = Logger.getLogger(DesignationMasterController.class);

    @Autowired
    private DesignationMasterService designationMasterService;
 /* to Save an DepartmentMaster*/
    @PostMapping("/designation")
    public DesignationMaster create(@Valid @RequestBody DesignationMaster designationMaster) {
        logger.debug("create");
        return designationMasterService.create(designationMaster);
    }

    /* get all DepartmentMaster */
    @GetMapping("/designation/list")
    public List<DesignationMaster> getAllDesignationMasters() {
        logger.debug("list");
        return designationMasterService.list();
    }

    @GetMapping("/designation/{id}")
    public DesignationMaster read(@PathVariable(value = "id") Long dId) {
        logger.debug("read");
        return designationMasterService.read(dId);
    }

    /* Update an DepartmentMaster by DepartmentMasterId */
    @PutMapping("/designation")
    public DesignationMaster update(@Valid @RequestBody DesignationMaster designationMaster) {
        logger.debug("update");
        return designationMasterService.update(designationMaster);
    }

    /* Delete an DepartmentMaster */
    @DeleteMapping("/designation/{id}")
    public String delete(@PathVariable(value = "id") Long dId) {
        logger.debug("delete");
        designationMasterService.delete(dId);
        return "";
    }
    
}
