/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.springapi.controller;


import com.springapi.model.TestUsers;
import java.util.List;
import javax.validation.Valid;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author shahb
 */


@RestController
@RequestMapping("/api")
@CrossOrigin(origins ="*")
public class TestUsersController {
  //  static Logger logger = new Logger.getLogger(TestUsersController.class);
    
    @PostMapping("/TestUser")
    public TestUsers create(@Valid @RequestBody TestUsers testUsers)
    {
        return testUsers;
    }
    
    @GetMapping("/TestUser/list")
    public List<TestUsers> getAllTestUser()
    {
        return null;
    }
    
    @GetMapping("/TestUser/{id}")
    public TestUsers read (@PathVariable(value="id") Long uId)
    {
        return null;
    }
    
    @PutMapping("/TestUser")
    public TestUsers update(@Valid @RequestBody TestUsers testUsers)
    {
        return null;
    }
    
    
    @DeleteMapping("/TestUser/{id}")
    public String delete(@PathVariable(value="id") Long uId)
    {
        return "";
    }
   
    
}
