package com.springapi.controller;

import com.springapi.dto.request.BlockByFacultyIdRequest;
import com.springapi.dto.response.BlockUserByDeviceResponse;
import com.springapi.model.FacultyLogin;
import java.util.List;

import javax.validation.Valid;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.springapi.service.FacultyLoginService;

@RestController
@RequestMapping("/api")
@CrossOrigin(origins = "*")
public class FacultyLoginController {

    static Logger logger = Logger.getLogger(FacultyLoginController.class);

    @Autowired
    private FacultyLoginService facultyLoginService;

    @PostMapping("/facultyLogin")
    public FacultyLogin create(@Valid @RequestBody FacultyLogin baf) {
        logger.debug("create");
        return facultyLoginService.create(baf);
    }

    @PostMapping("/facultyLogin")
    public BlockUserByDeviceResponse blockUserById(BlockByFacultyIdRequest blockByFacultyIdRequest) {
        logger.debug("create");
        return facultyLoginService.blockUserById(blockByFacultyIdRequest);
    }

    @GetMapping("/facultyLogin/list")
    public List<FacultyLogin> getAllfacultyRoleMpg() {
        logger.debug("list");
        return facultyLoginService.list();
    }

    @GetMapping("/facultyLogin/{id}")
    public FacultyLogin read(@PathVariable(value = "id") Long id) {
        logger.debug("read");
        return facultyLoginService.read(id);
    }

    @PutMapping("/facultyLogin")
    public FacultyLogin update(@Valid @RequestBody FacultyLogin tpl) {
        logger.debug("update");
        return facultyLoginService.update(tpl);
    }

    @DeleteMapping("/facultyLogin/{id}")
    public String delete(@PathVariable(value = "id") Long id) {
        logger.debug("delete");
        facultyLoginService.delete(id);
        return "";
    }

}
