/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.springapi.controller;

import com.springapi.model.ProgramInstMpg;
import com.springapi.model.ProgramMst;
import com.springapi.service.ProgramInstMpgService;
import java.util.List;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author shahb
 */
@RestController
@RequestMapping("/api")
@CrossOrigin(origins = "*")
public class ProgramInstMpgController {

    @Autowired
    private ProgramInstMpgService programInstMpgService;

    /* to Save an Faculty*/
    @PostMapping("/programInstMpg")
    public ProgramInstMpg create(@Valid @RequestBody ProgramInstMpg programInstMpg) {
        return programInstMpgService.create(programInstMpg);
    }

    /* get all Faculty */
    @GetMapping("/programInstMpg/list")
    public List<ProgramInstMpg> getAllProgramMst() {
        return programInstMpgService.list();
    }

    @GetMapping("/programInstMpg/{id}")
    public ProgramInstMpg read(@PathVariable(value = "id") Long fId) {
        return programInstMpgService.read(fId);
    }

    /* Update an Faculty by FacultyId */
    @PutMapping("/programInstMpg")
    public ProgramInstMpg update(@Valid @RequestBody ProgramInstMpg programInstMpg) {
        return programInstMpgService.update(programInstMpg);
    }

    /* Delete an Faculty */
    @DeleteMapping("/programInstMpg/{id}")
    public String delete(@PathVariable(value = "id") Long fId) {
        programInstMpgService.delete(fId);
        return "";
    }

    @GetMapping("/instituteProgMpg/{id}")
    public List<ProgramMst> readByinstitute(@PathVariable(value = "id") Long iId) {
        return programInstMpgService.readProgramByInstituteId(iId);
    }
    @GetMapping("/instituteProgMpg/{instId}/{progId}")
    public List<ProgramInstMpg> readByinstitute(@PathVariable(value = "instId") Long iId,@PathVariable(value = "progId") Long pId) {
        return programInstMpgService.readProgramByProgramIdAndInstituteId(iId,pId);
    }
}
