/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.springapi.service;

import com.springapi.model.TestUsers;
import java.util.List;

/**
 *
 * @author shahb
 */
public interface TestUserService {
    public TestUsers create(TestUsers testUsers);
    
    public List<TestUsers> list();
    
    public TestUsers read(Long uId);
    
    public TestUsers update(TestUsers testUsers);
    
    public String delete(Long uId);
    
    
}
