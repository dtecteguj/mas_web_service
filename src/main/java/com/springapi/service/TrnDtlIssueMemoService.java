/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.springapi.service;

import com.springapi.dto.response.NoticeResponse;
import java.util.List;
import com.springapi.model.TrnDtlIssueMemo;

/**
 *
 * @author shahb
 */
public interface TrnDtlIssueMemoService {

    TrnDtlIssueMemo create(TrnDtlIssueMemo trnDtlIssueMemo);

    List<TrnDtlIssueMemo> list();

    TrnDtlIssueMemo read(Long iId);

    TrnDtlIssueMemo update(TrnDtlIssueMemo trnDtlIssueMemo);

    void delete(Long iId);

    List<NoticeResponse> readbyinstid(Long iId);

    List<NoticeResponse> readbyfid(Long iId);
}
