/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.springapi.service;

import com.springapi.model.Subject;
import java.util.List;

/**
 *
 * @author shahb
 */
public interface SubjectService {

    Subject create(Subject subject);

    List<Subject> list();

    Subject read(Long sId);

    Subject update(Subject subjectDetails);

    void delete(Long sId);

    Subject readByCode(Long subjectCode);

}
