/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.springapi.service;

import com.springapi.model.ProgramMst;
import java.util.List;

/**
 *
 * @author shahb
 */
public interface ProgramMstService {
    ProgramMst create(ProgramMst programMst);

    List<ProgramMst> list();

    ProgramMst read(Long pId);

    ProgramMst update(ProgramMst programMst);

    void delete(Long pId);
}
