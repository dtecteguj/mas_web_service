/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.springapi.service;

import com.springapi.dto.request.CMDashRequest;
import com.springapi.dto.request.DashboardRequest;
import com.springapi.dto.response.CMDashDailyTaskResponse;
import com.springapi.dto.response.CMDashResponse;
import com.springapi.dto.response.DashboardResponse;
import com.springapi.dto.response.TotalFacultyRegisterResponse;
import java.util.List;

/**
 *
 * @author shahb
 */
public interface DashboardService {

    List<DashboardResponse> getFacultyCount();

    List<DashboardResponse> getHodCount(DashboardRequest dashboardRequest);

//    List<DashboardResponse> getCMCount();
    List<DashboardResponse> getCMCount(DashboardRequest dashboardRequest);

//    List<DashboardResponse> getPrincipalCount();
    List<DashboardResponse> getPrincipalCount(DashboardRequest dashboardRequest);

    List<TotalFacultyRegisterResponse> getTotalFacultyCount();

    List<TotalFacultyRegisterResponse> getTotalFacultyPresentCount();

    List<TotalFacultyRegisterResponse> getTotalFacultyAbsentCount();

    List<DashboardResponse> getDirectorCount(DashboardRequest dashboardRequest);

    List<DashboardResponse> countAttendanceForDirector(DashboardRequest dashboardRequest);

    List<CMDashResponse> countAttendanceForCMDash(CMDashRequest cMDashRequest);

    List<CMDashDailyTaskResponse> facultyDailyTaskForCMDash(CMDashRequest cMDashRequest);
}
