package com.springapi.service.impl;

import com.springapi.enums.FASMessageType;
import com.springapi.exception.FASException;
import com.springapi.model.InstituteTypeMaster;
import com.springapi.repository.InstituteTypeRepository;
import com.springapi.service.InstituteTypeService;
import java.util.List;
import java.util.Optional;
import javax.persistence.EntityManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author Jyoti_patel
 */
@Service
@Transactional
public class InstituteTypeServiceImpl implements InstituteTypeService {

    static Logger logger = Logger.getLogger(InstituteTypeServiceImpl.class);

    @Autowired
    private InstituteTypeRepository instituteTypeRepository;

    @Autowired
    private EntityManager entityManager;

    @Override
    public InstituteTypeMaster create(InstituteTypeMaster instituteType) {
        logger.debug("create");
        return instituteTypeRepository.save(instituteType);
    }

    @Override
    public List<InstituteTypeMaster> list() {
        logger.debug("list");
        return instituteTypeRepository.findAll();
    }

    @Override
    public InstituteTypeMaster read(Long instituteTypeId) {
        logger.debug("read");
        Optional<InstituteTypeMaster> optional = instituteTypeRepository.findById(instituteTypeId);
        if (!optional.isPresent()) {
            throw new FASException(FASMessageType.ENTITY_NOT_FOUND);
        }
        return optional.get();
    }

    @Override
    public void delete(Long instituteTypeId) {
        logger.debug("delete");
        InstituteTypeMaster instituteType = read(instituteTypeId);
        instituteTypeRepository.delete(instituteType);
    }

}
