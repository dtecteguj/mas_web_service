package com.springapi.service.impl;

import com.springapi.dto.request.FacultyLoginRequest;
import com.springapi.dto.request.StudentRegistrationRequest;
import com.springapi.dto.request.StudentRequest;
import com.springapi.dto.response.LoginResponse;
import com.springapi.dto.response.RegistrationResponse;
import com.springapi.enums.FASMessageType;
import com.springapi.exception.FASException;
import com.springapi.model.InstituteMaster;
import com.springapi.model.InstituteWifiMaster;
import com.springapi.model.Semester;
import com.springapi.model.Student;
import com.springapi.model.StudentTermReg;
import com.springapi.repository.InstituteRepository;
import com.springapi.repository.InstituteWifiRepository;
import com.springapi.repository.StudentRepository;
import com.springapi.repository.StudentTermRegRepository;
import com.springapi.service.StudentService;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.logging.Level;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Order;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author shahb
 */
@Service
@Transactional
public class StudentServiceImpl implements StudentService {

    static Logger logger = Logger.getLogger(FacultyAttendanceServiceImpl.class);

    @Autowired
    private StudentRepository studentRepository;

    @Autowired
    private StudentTermRegRepository studentTermRegRepository;

    @Autowired
    private InstituteWifiRepository instituteWifiRepository;

    @Autowired
    private InstituteRepository instituteRepository;

    @Autowired
    private EntityManager entityManager;

    @Override
    public Student create(Student student) {
        return studentRepository.save(student);
    }

//    @Override
//    public List<Student> list() {
//        return studentRepository.findAll();
//    }
    @Override
    public Student read(Long sId) {
        Optional<Student> optional = studentRepository.findById(sId);
        if (!optional.isPresent()) {
            throw new FASException(FASMessageType.ENTITY_NOT_FOUND);
        }
        return optional.get();
    }

    @Override
    public Student update(Student studentDetails) {
        Student student = read(studentDetails.getId());

        student.setName(studentDetails.getName());
        student.setEmail(studentDetails.getEmail());
        student.setsEnrollemnt(studentDetails.getsEnrollemnt());
        student.setPassword(studentDetails.getPassword());
        student.setPasswordHint(studentDetails.getPasswordHint());
        student.setPhone(studentDetails.getPhone());
        student.setSem(studentDetails.getSem());

        return studentRepository.save(student);
    }

    @Override
    public void delete(Long sId) {
        Student student = read(sId);
        studentRepository.delete(student);
    }

    @Override
    public LoginResponse login(FacultyLoginRequest facultyLoginRequest) {
        LoginResponse loginResponse = new LoginResponse();

        Student student = studentRepository.findByEmailAndPassword(facultyLoginRequest.getEmail(), facultyLoginRequest.getPassword());
        if (student != null) {
            if (!student.getIsActive()) {
                loginResponse.setMessage("your Accont not yet Endorse by Institute Admin CELL");
                loginResponse.setUserType("Student");
                loginResponse.setId(-1L);
                return loginResponse;
            }
            loginResponse.setMessage("Successfully logged in");
            loginResponse.setUserType("Student");
            loginResponse.setId(student.getId());
            loginResponse.setInstituteId(student.getInstituteId());
            Optional<InstituteMaster> instituteMaster = instituteRepository.findById(student.getInstituteId());
            if (instituteMaster.isPresent()) {

                loginResponse.setInstituteName(instituteMaster.get().getName());
            }
            loginResponse.setDepartmentId(student.getDepartmentId());
            loginResponse.setStudentSemId(student.getSem());
            loginResponse.setStudentName(student.getName());
            return loginResponse;
        }
        loginResponse.setMessage("Invalid Email / Password");
        loginResponse.setUserType("Student");
        loginResponse.setId(-1L);
        return loginResponse;
    }

    @Override
    public LoginResponse loginStudent(String email, String password, String bssid, String ssid) {
        LoginResponse loginResponse = new LoginResponse();

        Student student = studentRepository.findByEmailAndPassword(email, password);
        if (student != null) {
            loginResponse.setMessage("Successfully logged in");
            loginResponse.setId(student.getId());
            return loginResponse;
        }
        loginResponse.setMessage("Invalid Email / Password");
        loginResponse.setId(-1L);
        return loginResponse;
    }

    @Override
    public List<Student> list(StudentRequest studentRequest) {
        logger.debug("studentlist");

        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery criteriaQuery = criteriaBuilder.createQuery(Student.class);

        prepareListQuery(criteriaQuery, criteriaBuilder, false, studentRequest);

        TypedQuery typedQuery = entityManager.createQuery(criteriaQuery);

        List results = typedQuery.getResultList();
        return results;
    }

    private void prepareListQuery(CriteriaQuery criteriaQuery, CriteriaBuilder criteriaBuilder,
            boolean countQuery, StudentRequest listRequest) {
        logger.debug("prepareListQuery");
        Root<Student> studentRoot = criteriaQuery.from(Student.class);
        Root<Semester> semRoot = criteriaQuery.from(Semester.class);
        if (countQuery) {
            //criteriaQuery.select(criteriaBuilder.count(instituteWifiRoot));
        } else {
            criteriaQuery.multiselect(
                    studentRoot.get("id"), studentRoot.get("name"), studentRoot.get("email"),
                    studentRoot.get("phone"), studentRoot.get("sEnrollemnt"),
                    studentRoot.get("sem"), semRoot.get("Sname")
            );

            List<Order> orders = new ArrayList<>();
            if (!orders.isEmpty()) {
                criteriaQuery.orderBy(orders);
            }
        }
        ArrayList<Predicate> predicateList = new ArrayList<>();

        predicateList.add(criteriaBuilder.equal(studentRoot.get("sem"), semRoot.get("id")));
        if (listRequest.getSemId() != null) {
            predicateList.add(criteriaBuilder.equal(studentRoot.get("sem"), listRequest.getSemId()));
        }

        Predicate[] predicates = new Predicate[predicateList.size()];
        predicateList.toArray(predicates);
        criteriaQuery.where(predicates);

    }

    @Override
    public List<Student> findAllStudentBySem(Long semId) {
        List<Student> studentList = studentRepository.findAllBySem(semId);
        if (studentList != null) {
            return studentList;
        }
        return null;
    }

    @Override
    public RegistrationResponse studentRegist(StudentRegistrationRequest studentRegistrationRequest) {
        System.out.println(studentRegistrationRequest);
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        Student student = new Student();
        RegistrationResponse registrationResponse = new RegistrationResponse();
        student.setsEnrollemnt(studentRegistrationRequest.getEnrollment());
        student.setEmail(studentRegistrationRequest.getEmail());

        Optional<Student> instituteMaster = studentRepository.findByEmailOrSEnrollemnt(student.getEmail(), student.getsEnrollemnt());
        if (instituteMaster.isPresent()) {

            registrationResponse.setStatus(Boolean.TRUE);
            registrationResponse.setStatusResponse("Student Enrollment Number Or Email Already Exists!");

            return registrationResponse;
        }
        student.setName(studentRegistrationRequest.getName());
        student.setfName(studentRegistrationRequest.getfName());
        student.setlName(studentRegistrationRequest.getlName());
        student.setmName(studentRegistrationRequest.getmName());
        student.setInstituteId(studentRegistrationRequest.getInstituteId());
        student.setInstituteTypeId(studentRegistrationRequest.getInstituteTypeId());
        student.setDepartmentId(studentRegistrationRequest.getDepartmentId());
        student.setDivisonId(studentRegistrationRequest.getDivId());

        try {
            student.setDob(new SimpleDateFormat("dd/MM/yyyy").parse(studentRegistrationRequest.getDob()));
        } catch (ParseException ex) {
            java.util.logging.Logger.getLogger(StudentServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        student.setAdmitedYear(studentRegistrationRequest.getAdmityearName());
        student.setSem(studentRegistrationRequest.getSemId());
        student.setIsActive(Boolean.FALSE);
        student.setIsDeleted(Boolean.FALSE);
        student.setIsPassout(Boolean.FALSE);
        student.setCourseId(studentRegistrationRequest.getProgramId());
        student.setTermId(1L);
        student.setPassword(studentRegistrationRequest.getPassword());
        student.setPhone(studentRegistrationRequest.getPhone());
        studentRepository.save(student);
        StudentTermReg studentTermReg = new StudentTermReg();
        studentTermReg.setCrntSemYear(student.getSem());
        studentTermReg.setInstituteId(student.getInstituteId());
        studentTermReg.setIsDeleted(Boolean.FALSE);
        studentTermReg.setProgramId(studentRegistrationRequest.getProgramId());
        studentTermReg.setTermId(1L);
        studentTermReg.setStudentId(student.getId());
        studentTermReg.setRegStatus(0L);
        studentTermReg.setCrntSemYear(studentRegistrationRequest.getSemId());
        studentTermReg.setDepartmentId(studentRegistrationRequest.getDepartmentId());
        studentTermReg.setIsEndorsed(Boolean.FALSE);

        studentTermReg.setRegStatus(1L);

        studentTermRegRepository.save(studentTermReg);
        registrationResponse.setStatus(Boolean.TRUE);
        registrationResponse.setStatusResponse("Student Register Successfully!");

        return registrationResponse;

    }

}
