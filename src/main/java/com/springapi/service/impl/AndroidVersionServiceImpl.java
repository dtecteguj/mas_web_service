/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.springapi.service.impl;

import com.springapi.dto.request.VrfVrsRequest;
import com.springapi.dto.response.AndroidVersionStatus;
import com.springapi.dto.response.VrfVrsResponse;
import com.springapi.enums.FASMessageType;
import com.springapi.exception.FASException;
import com.springapi.model.AndroidVersion;
import com.springapi.model.FacultyAttendance;
import com.springapi.repository.AndroidVersionRepository;
import com.springapi.repository.FacultyAttendanceRepository;
import com.springapi.repository.TrnActiveLoginLogRepository;
import com.springapi.repository.TrnDtlIssueMemoRepository;
import com.springapi.service.AndroidVersionService;
import com.springapi.util.CustDateFormat;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import javax.persistence.EntityManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author shahb
 */
@Service
@Transactional
public class AndroidVersionServiceImpl implements AndroidVersionService {

    static Logger logger = Logger.getLogger(InstituteTypeServiceImpl.class);

    @Autowired
    private AndroidVersionRepository androidVersionRepository;

    @Autowired
    private EntityManager entityManager;

    @Autowired
    private TrnDtlIssueMemoRepository trnDtlIssueMemoRepository;

    @Autowired
    private TrnActiveLoginLogRepository trnActiveLoginLogRepository;

    @Autowired
    private FacultyAttendanceRepository facultyAttendanceRepository;

    @Override
    public AndroidVersion create(AndroidVersion androidVersion) {
        logger.debug("create");
        return androidVersionRepository.save(androidVersion);
    }

    @Override
    public List<AndroidVersion> list() {
        logger.debug("list");
        return androidVersionRepository.findAll();
    }

    @Override
    public AndroidVersionStatus read(Long aId) {
        logger.debug("read");
        Optional<AndroidVersion> optional = androidVersionRepository.findByVersionNumber(aId);
        AndroidVersionStatus androidVersionStatus = new AndroidVersionStatus();
        if (!optional.isPresent()) {
            androidVersionStatus.setIsActive(Boolean.FALSE);
        } else {
            androidVersionStatus.setIsActive(optional.get().getIsActive());
        }

        return androidVersionStatus;
    }

    @Override
    public AndroidVersion update(AndroidVersion androidVersion) {
        logger.debug("update");
        Optional<AndroidVersion> optional = androidVersionRepository.findByVersionNumber(androidVersion.getVersionNumber());
        if (!optional.isPresent()) {
            throw new FASException(FASMessageType.ENTITY_NOT_FOUND);
        }
        optional.get().setVersionName(androidVersion.getVersionName());
        optional.get().setVersionNumber(androidVersion.getVersionNumber());
        optional.get().setVersionDesc(androidVersion.getVersionDesc());
        return androidVersionRepository.save(optional.get());
    }

    @Override
    public void delete(Long aId) {
        logger.debug("delete");
        Optional<AndroidVersion> optional = androidVersionRepository.findByVersionNumber(aId);
        if (!optional.isPresent()) {
            throw new FASException(FASMessageType.ENTITY_NOT_FOUND);
        }

        androidVersionRepository.delete(optional.get());
    }

    @Override
    public AndroidVersionStatus read(Long aId, String type) {
        logger.debug("read");
        Optional<AndroidVersion> optional = androidVersionRepository.findByVersionNumberAndDivTypeAndIsActiveTrue(aId, type);
        AndroidVersionStatus androidVersionStatus = new AndroidVersionStatus();
        if (!optional.isPresent()) {
            androidVersionStatus.setIsActive(Boolean.FALSE);
        } else {
            androidVersionStatus.setIsActive(optional.get().getIsActive());
        }

        return androidVersionStatus;
    }

    @Override
    public VrfVrsResponse verifyVersion(VrfVrsRequest vrfVrsRequest) {
        // TODO Auto-generated method stub
        VrfVrsResponse vrfVrsResponse = new VrfVrsResponse();
        Optional<AndroidVersion> optional = androidVersionRepository.findByVersionNumberAndDivTypeAndIsActiveTrue(vrfVrsRequest.getVersionId(), vrfVrsRequest.getVersionType());
        // AndroidVersionStatus androidVersionStatus=new AndroidVersionStatus();
        if (!optional.isPresent() || Objects.equals(optional.get().getIsActive(), Boolean.FALSE)) {
            vrfVrsResponse.setErrorStatus(101L, "Application Running on older Version");
            return vrfVrsResponse;
        } else {
            vrfVrsResponse.setMasId(optional.get().getId());
            if (!vrfVrsRequest.getIsLogin()) {
                return vrfVrsResponse;
            }

            if (!trnActiveLoginLogRepository.existsByIdAndFIdAndDeviceIdAndIsActiveTrue(vrfVrsRequest.getActLogId(), vrfVrsRequest.getFId(), vrfVrsRequest.getDeviceId())) {
                return vrfVrsResponse;
            }

            FacultyAttendance facultyAttendance = facultyAttendanceRepository.findByAttendanceDateAndFId(vrfVrsResponse.setTodayDateWithReturn(CustDateFormat.todaySQLString()), vrfVrsRequest.getFId());

            if (facultyAttendance == null) {
                return vrfVrsResponse;
            }

            vrfVrsResponse.setIsLogin(Boolean.TRUE);
            vrfVrsResponse.setInTime(facultyAttendance.getInTime());
            vrfVrsResponse.setOutTime(facultyAttendance.getOutTime());
            vrfVrsResponse.setIsEarly(facultyAttendance.getIsEarly());
            vrfVrsResponse.setIsLate(facultyAttendance.getIsLate());
            vrfVrsResponse.setIsStatus(facultyAttendance.getStatus());
            vrfVrsResponse.setPresentStatus(facultyAttendance.getPresenceStatus());
            vrfVrsResponse.setLeaveStatus(facultyAttendance.getLeaveStatus());
            vrfVrsResponse.setIsMemo(trnDtlIssueMemoRepository.existsByFIdAndIsDeletedFalseAndIsSubmittedFalse(vrfVrsRequest.getFId()));

            // androidVersionStatus.setIsActive(optional.get().getIsActive());
        }

        return vrfVrsResponse;

    }

}
