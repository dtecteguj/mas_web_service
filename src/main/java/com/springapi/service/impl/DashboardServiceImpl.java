package com.springapi.service.impl;

import com.springapi.dto.request.CMDashRequest;
import com.springapi.dto.request.DashboardRequest;
import com.springapi.dto.response.CMDashDailyTaskResponse;
import com.springapi.dto.response.CMDashResponse;
import com.springapi.dto.response.DashboardResponse;
import com.springapi.dto.response.TotalFacultyRegisterResponse;
import com.springapi.enums.MASConstant;
import com.springapi.model.DepartmentMaster;
import com.springapi.model.Faculty;
import com.springapi.model.FacultyAttendance;
import com.springapi.model.InstituteMaster;
import com.springapi.model.InstituteTypeMaster;
import com.springapi.model.UniversityMaster;
import com.springapi.service.DashboardService;
import com.springapi.util.DefaultValueSet;
import static com.springapi.util.DefaultValueSet.fillNullObjects;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat.Field;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Order;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author shahb
 */
@Service
@Transactional
public class DashboardServiceImpl implements DashboardService {

    static Logger logger = Logger.getLogger(DashboardServiceImpl.class);

    @Autowired
    private EntityManager entityManager;

    @Autowired
    private Environment env;

    @Override
    public List<DashboardResponse> getPrincipalCount(DashboardRequest dashboardRequest) {
        logger.debug("getPrincipalCount");

        // Code for for loop started 
        List<DashboardResponse> dashboardResponseList = new ArrayList<DashboardResponse>();
        try {
            Class.forName("com.mysql.jdbc.Driver");
            String path = env.getProperty("spring.datasource.url");
            String userName = env.getProperty("spring.datasource.username");
            String password = env.getProperty("spring.datasource.password");

            Connection c = DriverManager.getConnection(path, userName, password);
            // gets a new connection
            Statement s = c.createStatement();
            LocalDate startD = LocalDate.now();

            String pattern = "yyyy-MM-dd";
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);

            String startDate = simpleDateFormat.format(new Date());

            String sql = "select fa.attendance_date as dates"
                    + ",count(case when fa.status = 0  then 1 end) as absent_count"
                    + ",count(case when fa.status = 1 then 1 end) as present_count"
                    + ",dm.name as dname   "
                    + "  from faculty_attendance as fa , department_master as dm where dm.id = fa.department_id and"
                    + " fa.attendance_date = '" + startDate + "'"
                    //                                + " And fa.attendance_date<= '" + edate + " ' " + startDate + " "
                    + " And fa.institute_id=" + dashboardRequest.getInstituteId()
                    //                    + " And fa.institute_id= im.id"
                    + " group by fa.attendance_date,dm.name;";
            ResultSet rs = s.executeQuery(sql);
            while (rs.next()) {
                String data = rs.getString("absent_count");
                String data1 = rs.getString("present_count");
                System.out.println("data of absent ::" + data);
                System.out.println("data of present ::" + data1);
                DashboardResponse dashboardResponse = new DashboardResponse();
                dashboardResponse.setCount(rs.getLong("present_count"));
                dashboardResponse.setAbsentCount(rs.getLong("absent_count"));
                dashboardResponse.setDate(rs.getString("dates"));
                dashboardResponse.setInstituteName(rs.getString("dname"));
                dashboardResponseList.add(dashboardResponse);
            }

        } catch (ClassNotFoundException | SQLException e) {
            e.printStackTrace();
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return dashboardResponseList;
        //End !!

//        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
//        CriteriaQuery criteriaQuery = criteriaBuilder.createQuery(DashboardResponse.class);
//
//        try {
//       
//           prepareListQuery(criteriaQuery, criteriaBuilder, dashboardRequest, false);
//        } catch (ParseException ex) {
//            java.util.logging.Logger.getLogger(DashboardServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
//        }
//
//        TypedQuery typedQuery = entityManager.createQuery(criteriaQuery);
//
//        List results = typedQuery.getResultList();
//        System.out.println("result::" + results);
//        return results;
    }

    private void prepareListQuery(CriteriaQuery criteriaQuery, CriteriaBuilder criteriaBuilder, DashboardRequest listRequest, boolean countQuery) throws ParseException {

        Root<FacultyAttendance> facAttenRoot = criteriaQuery.from(FacultyAttendance.class);
        Root<DepartmentMaster> departmentRoot = criteriaQuery.from(DepartmentMaster.class);

        if (countQuery) {
            criteriaQuery.select(criteriaBuilder.count(facAttenRoot));
        } else {
//            criteriaQuery.multiselect(
//                    facAttenRoot.get("status"), facAttenRoot.get("attendanceDate"), criteriaBuilder.count(facAttenRoot)
//            ).groupBy(facAttenRoot.get("attendanceDate"), facAttenRoot.get("status"));
//            
            criteriaQuery.multiselect(
                    criteriaBuilder.count(facAttenRoot), facAttenRoot.get("status"), departmentRoot.get("name")
            ).groupBy(facAttenRoot.get("attendanceDate"), facAttenRoot.get("status"), facAttenRoot.get("departmentId"));

            List<Order> orders = new ArrayList<>();

            if (!orders.isEmpty()) {
                //criteriaQuery.orderBy(orders);
                criteriaQuery.orderBy(criteriaBuilder.desc(facAttenRoot.get("id")));
            }
        }

        String pattern = "yyyy-MM-dd";
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
        String date = simpleDateFormat.format(new Date());
//        Date startDate = simpleDateFormat.parse("2018-12-25");
//        Date endDate = simpleDateFormat.parse("2018-12-31");
//
//        Date startMonthlyDate = simpleDateFormat.parse("2018-12-01");

        ArrayList<Predicate> predicateList = new ArrayList<>();
//        if (listRequest.isIsLastSevenDay() == true) {
//            predicateList.add(criteriaBuilder.greaterThanOrEqualTo(facAttenRoot.<Date>get("attendanceDate"), startDate));
//            predicateList.add(criteriaBuilder.lessThanOrEqualTo(facAttenRoot.<Date>get("attendanceDate"), endDate));
//        }
//
//        if (listRequest.isIsMonthly() == true) {
//            predicateList.add(criteriaBuilder.greaterThanOrEqualTo(facAttenRoot.<Date>get("attendanceDate"), startMonthlyDate));
//            predicateList.add(criteriaBuilder.lessThanOrEqualTo(facAttenRoot.<Date>get("attendanceDate"), endDate));
//        }

        predicateList.add(criteriaBuilder.equal(facAttenRoot.get("departmentId"), departmentRoot.get("id")));
        predicateList.add(criteriaBuilder.equal(facAttenRoot.get("attendanceDate"), date));
        if (listRequest.getInstituteId() != null) {
            predicateList.add(criteriaBuilder.equal(facAttenRoot.get("instituteId"), listRequest.getInstituteId()));
        }
        // predicateList.add(criteriaBuilder.equal(instituteRoot.get("universityId"),universityRoot.get("id")));

        Predicate[] predicates = new Predicate[predicateList.size()];
        predicateList.toArray(predicates);
        criteriaQuery.where(predicates);
    }

    @Override
    public List<DashboardResponse> getFacultyCount() {
        logger.debug("getFacultyCount");
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery criteriaQuery = criteriaBuilder.createQuery(DashboardResponse.class);

        // Prepare list query
        prepareListFacultyQuery(criteriaQuery, criteriaBuilder, false);

        TypedQuery typedQuery = entityManager.createQuery(criteriaQuery);
        List results = typedQuery.getResultList();
        System.out.println("result::" + results);
        return results;
    }

    private void prepareListFacultyQuery(CriteriaQuery criteriaQuery, CriteriaBuilder criteriaBuilder, boolean countQuery) {

        Root<FacultyAttendance> facAttenRoot = criteriaQuery.from(FacultyAttendance.class);
        Root<UniversityMaster> universityRoot = criteriaQuery.from(UniversityMaster.class);
        Root<InstituteMaster> instituteRoot = criteriaQuery.from(InstituteMaster.class);
        Root<DepartmentMaster> departmentRoot = criteriaQuery.from(DepartmentMaster.class);

        if (countQuery) {
            criteriaQuery.select(criteriaBuilder.count(facAttenRoot));
        } else {
            criteriaQuery.multiselect(
                    criteriaBuilder.count(facAttenRoot), facAttenRoot.get("status"),
                    criteriaBuilder.selectCase()
                            .when(criteriaBuilder.equal(facAttenRoot.get("status"), 1), "Present")
                            .when(criteriaBuilder.equal(facAttenRoot.get("status"), 0), "Absent")
            ).groupBy(facAttenRoot.get("status"));

            List<Order> orders = new ArrayList<>();

            if (!orders.isEmpty()) {
                criteriaQuery.orderBy(orders);
            }
        }

        ArrayList<Predicate> predicateList = new ArrayList<>();
        predicateList.add(criteriaBuilder.equal(facAttenRoot.get("departmentId"), departmentRoot.get("id")));
        predicateList.add(criteriaBuilder.equal(facAttenRoot.get("instituteId"), 1));
        // predicateList.add(criteriaBuilder.equal(instituteRoot.get("universityId"),universityRoot.get("id")));

        Predicate[] predicates = new Predicate[predicateList.size()];
        predicateList.toArray(predicates);
        criteriaQuery.where(predicates);
    }

    @Override
    public List<DashboardResponse> getHodCount(DashboardRequest dashboardRequest) {
        logger.debug("getHodCount");
//        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
//        CriteriaQuery criteriaQuery = criteriaBuilder.createQuery(DashboardResponse.class);
//
//        try {
//            // Prepare list query
//            prepareListHodQuery(criteriaQuery, criteriaBuilder, dashboardRequest, false);
//        } catch (ParseException ex) {
//            java.util.logging.Logger.getLogger(DashboardServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
//        }
//
//        TypedQuery typedQuery = entityManager.createQuery(criteriaQuery);
//        if (!dashboardRequest.isIsMonthly()) {
//            // typedQuery.setMaxResults(7);
//        }
//        List results = typedQuery.getResultList();
//        System.out.println("result::" + results);
//        return results;

        List<DashboardResponse> dashboardResponseList = new ArrayList<DashboardResponse>();

        try {
            Class.forName("com.mysql.jdbc.Driver");
            // loads driver
            String path = env.getProperty("spring.datasource.url");
            String userName = env.getProperty("spring.datasource.username");
            String password = env.getProperty("spring.datasource.password");

            Connection c = DriverManager.getConnection(path, userName, password);
            Statement s = c.createStatement();
//            DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd");
            LocalDate startD = LocalDate.now();

            String pattern = "yyyy-MM-dd";
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);

            Date startDate = simpleDateFormat.parse(startD.withDayOfMonth(1).toString());
            Date endDate = simpleDateFormat.parse(startD.withDayOfMonth(startD.lengthOfMonth()).toString());
            System.out.println("start date :" + startDate + "  end date ::" + endDate);
            String sdate = simpleDateFormat.format(startDate);
            String edate = simpleDateFormat.format(endDate);
            System.out.println("start dte ::" + sdate + " end date ::" + edate);
            String sql = "select fa.attendance_date as date"
                    + ",count(case when status = 0  then 1 end) as absent_count"
                    + ",count(case when status = 1 then 1 end) as present_count"
                    + "  from faculty_attendance as fa"
                    + " where fa.attendance_date>= '" + sdate + " ' "
                    + " And fa.attendance_date<= '" + edate + " ' "
                    + " And fa.department_id=" + dashboardRequest.getDepartmentId()
                    //                    + " And fa.institute_id= im.id"
                    + " group by fa.attendance_date;";
            ResultSet rs = s.executeQuery(sql);

            while (rs.next()) {
                String data = rs.getString("absent_count");
                String data1 = rs.getString("present_count");
                System.out.println("data of absent ::" + data);
                System.out.println("data of present ::" + data1);
                DashboardResponse dashboardResponse = new DashboardResponse();
                dashboardResponse.setCount(rs.getLong("present_count"));
                dashboardResponse.setAbsentCount(rs.getLong("absent_count"));
                dashboardResponse.setDate(rs.getString("date"));
//                dashboardResponse.setInstituteName(rs.getString("instituteName"));
                dashboardResponseList.add(dashboardResponse);
            }

        } catch (ClassNotFoundException | SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return dashboardResponseList;

    }

    private void prepareListHodQuery(CriteriaQuery criteriaQuery, CriteriaBuilder criteriaBuilder, DashboardRequest listRequest, boolean countQuery) throws ParseException {

        Root<FacultyAttendance> facAttenRoot = criteriaQuery.from(FacultyAttendance.class);

        if (countQuery) {
            criteriaQuery.select(criteriaBuilder.count(facAttenRoot));
        } else {
//            criteriaQuery.multiselect(
//                    criteriaBuilder.count(facAttenRoot), facAttenRoot.get("status")
//            ).groupBy(facAttenRoot.get("status"));
            criteriaQuery.multiselect(
                    facAttenRoot.get("status"), facAttenRoot.get("attendanceDate"), criteriaBuilder.count(facAttenRoot)
            ).groupBy(facAttenRoot.get("attendanceDate"), facAttenRoot.get("status"));

            List<Order> orders = new ArrayList<>();

            if (!orders.isEmpty()) {
//                criteriaQuery.orderBy(orders);
                criteriaQuery.orderBy(criteriaBuilder.desc(facAttenRoot.get("id")));
            }
        }

        LocalDate startD = LocalDate.now();

        String pattern = "yyyy-MM-dd";
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
        String date = simpleDateFormat.format(new Date());
//        Date startDate = simpleDateFormat.parse("2019-01-01");
//        Date endDate = simpleDateFormat.parse("2019-01-31");
        Date startDate = simpleDateFormat.parse(startD.withDayOfMonth(1).toString());
        Date endDate = simpleDateFormat.parse(startD.withDayOfMonth(startD.lengthOfMonth()).toString());
        System.out.println("start  date ::" + startDate + "end date ::" + endDate);
//        Date startMonthlyDate = simpleDateFormat.parse("2019-01-01");
        ArrayList<Predicate> predicateList = new ArrayList<>();
//        if (listRequest.isIsLastSevenDay() == true) {
//            predicateList.add(criteriaBuilder.greaterThanOrEqualTo(facAttenRoot.<Date>get("attendanceDate"), startDate));
//            predicateList.add(criteriaBuilder.lessThanOrEqualTo(facAttenRoot.<Date>get("attendanceDate"), endDate));
//        }
//
//        if (listRequest.isIsMonthly() == true) {
        predicateList.add(criteriaBuilder.greaterThanOrEqualTo(facAttenRoot.<Date>get("attendanceDate"), startDate));
        predicateList.add(criteriaBuilder.lessThanOrEqualTo(facAttenRoot.<Date>get("attendanceDate"), endDate));
//        }
        if (listRequest.getInstituteId() != null) {
            predicateList.add(criteriaBuilder.equal(facAttenRoot.get("instituteId"), listRequest.getInstituteId()));
        }
        predicateList.add(criteriaBuilder.equal(facAttenRoot.get("departmentId"), listRequest.getDepartmentId()));
//        predicateList.add(criteriaBuilder.equal(facAttenRoot.get("attendanceDate"), date));

        Predicate[] predicates = new Predicate[predicateList.size()];
        predicateList.toArray(predicates);
        criteriaQuery.where(predicates);
    }

    @Override
    public List<DashboardResponse> getCMCount(DashboardRequest dashboardRequest) {
        logger.debug("getCMCount");
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery criteriaQuery = criteriaBuilder.createQuery(DashboardResponse.class);

        try {
            // Prepare list query
            prepareListDirectorQuery(criteriaQuery, criteriaBuilder, dashboardRequest, false);
        } catch (ParseException ex) {
            java.util.logging.Logger.getLogger(DashboardServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
        }

        TypedQuery typedQuery = entityManager.createQuery(criteriaQuery);
        List results = typedQuery.getResultList();
        System.out.println("result::" + results);
        return results;
    }

    private void prepareListDirectorQuery(CriteriaQuery criteriaQuery, CriteriaBuilder criteriaBuilder, DashboardRequest listRequest, boolean countQuery) throws ParseException {

        Root<FacultyAttendance> facAttenRoot = criteriaQuery.from(FacultyAttendance.class);
        Root<InstituteMaster> instituteRoot = criteriaQuery.from(InstituteMaster.class);
//        Root<InstituteTypeMaster> instituteTypeMaster = criteriaQuery.from(InstituteTypeMaster.class);

        if (countQuery) {
            criteriaQuery.select(criteriaBuilder.count(facAttenRoot));
        } else {
            criteriaQuery.multiselect(
                    criteriaBuilder.count(facAttenRoot), facAttenRoot.get("status"), instituteRoot.get("name")
            ).groupBy(facAttenRoot.get("attendanceDate"), facAttenRoot.get("status"), facAttenRoot.get("instituteId"));

            List<Order> orders = new ArrayList<>();

            if (!orders.isEmpty()) {
                //criteriaQuery.orderBy(orders);
                criteriaQuery.orderBy(criteriaBuilder.desc(facAttenRoot.get("id")));
            }
        }

        String pattern = "yyyy-MM-dd";
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
        String date = simpleDateFormat.format(new Date());

        System.out.println("date ::" + date);

        ArrayList<Predicate> predicateList = new ArrayList<>();
        predicateList.add(criteriaBuilder.equal(facAttenRoot.get("attendanceDate"), date));
        predicateList.add(criteriaBuilder.equal(facAttenRoot.get("instituteId"), instituteRoot.get("id")));
        predicateList.add(criteriaBuilder.equal(facAttenRoot.get("instituteTypeId"), listRequest.getInstituteTypeId()));
        Predicate[] predicates = new Predicate[predicateList.size()];
        predicateList.toArray(predicates);
        criteriaQuery.where(predicates);
    }

    @Override
    public List<TotalFacultyRegisterResponse> getTotalFacultyCount() {
        logger.debug("getTotalFacultyCount");
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery criteriaQuery = criteriaBuilder.createQuery(TotalFacultyRegisterResponse.class);
        // Prepare list query
        prepareListForFacultyRegCountQuery(criteriaQuery, criteriaBuilder, false);
        TypedQuery typedQuery = entityManager.createQuery(criteriaQuery);
        List results = typedQuery.getResultList();
        System.out.println("result::" + results);
        return results;
    }

    private void prepareListForFacultyRegCountQuery(CriteriaQuery criteriaQuery, CriteriaBuilder criteriaBuilder, boolean countQuery) {
        Root<Faculty> facultyRoot = criteriaQuery.from(Faculty.class);
        Root<InstituteTypeMaster> instituteTypeRoot = criteriaQuery.from(InstituteTypeMaster.class);

        if (countQuery) {
            criteriaQuery.select(criteriaBuilder.count(facultyRoot));
        } else {
            criteriaQuery.multiselect(
                    criteriaBuilder.count(facultyRoot), instituteTypeRoot.get("name")
            ).groupBy(facultyRoot.get("instituteTypeId"));

            List<Order> orders = new ArrayList<>();
            if (!orders.isEmpty()) {
                criteriaQuery.orderBy(criteriaBuilder.desc(facultyRoot.get("id")));
            }
        }

        ArrayList<Predicate> predicateList = new ArrayList<>();
        predicateList.add(criteriaBuilder.equal(facultyRoot.get("instituteTypeId"), instituteTypeRoot.get("id")));

        Predicate[] predicates = new Predicate[predicateList.size()];
        predicateList.toArray(predicates);
        criteriaQuery.where(predicates);
    }

    @Override
    public List<TotalFacultyRegisterResponse> getTotalFacultyPresentCount() {
        logger.debug("getTotalFacultyPresentCount");
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery criteriaQuery = criteriaBuilder.createQuery(TotalFacultyRegisterResponse.class);
        // Prepare list query
        prepareListForFacultyPresentCountQuery(criteriaQuery, criteriaBuilder, false);
        TypedQuery typedQuery = entityManager.createQuery(criteriaQuery);
        List results = typedQuery.getResultList();
        System.out.println("result::" + results);
        return results;
    }

    private void prepareListForFacultyPresentCountQuery(CriteriaQuery criteriaQuery, CriteriaBuilder criteriaBuilder, boolean countQuery) {
        Root<FacultyAttendance> facultyAttendanceRoot = criteriaQuery.from(FacultyAttendance.class);
        Root<InstituteTypeMaster> instituteTypeRoot = criteriaQuery.from(InstituteTypeMaster.class);

        if (countQuery) {
            criteriaQuery.select(criteriaBuilder.count(facultyAttendanceRoot));
        } else {
            criteriaQuery.multiselect(
                    criteriaBuilder.count(facultyAttendanceRoot), instituteTypeRoot.get("name")
            ).groupBy(facultyAttendanceRoot.get("instituteTypeId"));

            List<Order> orders = new ArrayList<>();
            if (!orders.isEmpty()) {
                criteriaQuery.orderBy(criteriaBuilder.desc(facultyAttendanceRoot.get("id")));
            }
        }

        String pattern = "yyyy-MM-dd";
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
        String date = simpleDateFormat.format(new Date());

        ArrayList<Predicate> predicateList = new ArrayList<>();
        predicateList.add(criteriaBuilder.equal(facultyAttendanceRoot.get("instituteTypeId"), instituteTypeRoot.get("id")));
        predicateList.add(criteriaBuilder.equal(facultyAttendanceRoot.get("attendanceDate"), date));
        predicateList.add(criteriaBuilder.equal(facultyAttendanceRoot.get("status"), 1));

        Predicate[] predicates = new Predicate[predicateList.size()];
        predicateList.toArray(predicates);
        criteriaQuery.where(predicates);
    }

    @Override
    public List<TotalFacultyRegisterResponse> getTotalFacultyAbsentCount() {
        logger.debug("getTotalFacultyPresentCount");
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery criteriaQuery = criteriaBuilder.createQuery(TotalFacultyRegisterResponse.class);
        // Prepare list query
        prepareListForFacultyAbsentCountQuery(criteriaQuery, criteriaBuilder, false);
        TypedQuery typedQuery = entityManager.createQuery(criteriaQuery);
        List results = typedQuery.getResultList();
        System.out.println("result::" + results);
        return results;
    }

    private void prepareListForFacultyAbsentCountQuery(CriteriaQuery criteriaQuery, CriteriaBuilder criteriaBuilder, boolean countQuery) {
        Root<FacultyAttendance> facultyAttendanceRoot = criteriaQuery.from(FacultyAttendance.class);
        Root<InstituteTypeMaster> instituteTypeRoot = criteriaQuery.from(InstituteTypeMaster.class);

        if (countQuery) {
            criteriaQuery.select(criteriaBuilder.count(facultyAttendanceRoot));
        } else {
            criteriaQuery.multiselect(
                    criteriaBuilder.count(facultyAttendanceRoot), instituteTypeRoot.get("name")
            ).groupBy(facultyAttendanceRoot.get("instituteTypeId"));

            List<Order> orders = new ArrayList<>();
            if (!orders.isEmpty()) {
                criteriaQuery.orderBy(criteriaBuilder.desc(facultyAttendanceRoot.get("id")));
            }
        }

        String pattern = "yyyy-MM-dd";
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
        String date = simpleDateFormat.format(new Date());

        ArrayList<Predicate> predicateList = new ArrayList<>();
        predicateList.add(criteriaBuilder.equal(facultyAttendanceRoot.get("instituteTypeId"), instituteTypeRoot.get("id")));
        predicateList.add(criteriaBuilder.equal(facultyAttendanceRoot.get("attendanceDate"), date));
        predicateList.add(criteriaBuilder.equal(facultyAttendanceRoot.get("status"), 0));

        Predicate[] predicates = new Predicate[predicateList.size()];
        predicateList.toArray(predicates);
        criteriaQuery.where(predicates);
    }

    @Override
    public List<DashboardResponse> getDirectorCount(DashboardRequest dashboardRequest) {
        logger.debug("getDirectorCount");
//        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
//        CriteriaQuery criteriaQuery = criteriaBuilder.createQuery(DashboardResponse.class);
//
//        try {
//            prepareListForDirectorQuery(criteriaQuery, criteriaBuilder, dashboardRequest, false);
//        } catch (ParseException ex) {
//            java.util.logging.Logger.getLogger(DashboardServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
//        }
//
//        TypedQuery typedQuery = entityManager.createQuery(criteriaQuery);
//        List results = typedQuery.getResultList();
//        System.out.println("result::" + results);
//        return results;

        List<DashboardResponse> dashboardResponseList = new ArrayList<DashboardResponse>();

        try {
            Class.forName("com.mysql.jdbc.Driver");
            // loads driver
//            Connection c = DriverManager.getConnection("jdbc:mysql://localhost:3306/dheemati_attendance", "dheemati", "Chetan_2_11_68"); // gets a new connection
            String path = env.getProperty("spring.datasource.url");
            String userName = env.getProperty("spring.datasource.username");
            String password = env.getProperty("spring.datasource.password");

            Connection c = DriverManager.getConnection(path, userName, password);
            Statement s = c.createStatement();

            DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd");
            LocalDateTime now = LocalDateTime.now();

            String sql = "select fa.attendance_date as date,im.name as instituteName"
                    + ",count(case when status = 0  then 1 end) as absent_count"
                    + ",count(case when status = 1 then 1 end) as present_count"
                    + "  from faculty_attendance as fa, institute_type_master as im"
                    + " where fa.attendance_date='" + dtf.format(now) + "' "
                    //                    + " And fa.institute_type_id=" + dashboardRequest.getInstituteTypeId() 
                    + " And fa.institute_type_id= im.id"
                    + " group by fa.attendance_date,fa.institute_type_id;";
            ResultSet rs = s.executeQuery(sql);

            while (rs.next()) {
                String data = rs.getString("absent_count");
                String data1 = rs.getString("present_count");
                System.out.println("data of absent ::" + data);
                System.out.println("data of present ::" + data1);
                DashboardResponse dashboardResponse = new DashboardResponse();
                dashboardResponse.setCount(rs.getLong("present_count"));
                dashboardResponse.setAbsentCount(rs.getLong("absent_count"));
                dashboardResponse.setDate(rs.getString("date"));
                dashboardResponse.setInstituteName(rs.getString("instituteName"));
                dashboardResponseList.add(dashboardResponse);
            }

        } catch (ClassNotFoundException | SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return dashboardResponseList;

    }

    private void prepareListForDirectorQuery(CriteriaQuery criteriaQuery, CriteriaBuilder criteriaBuilder, DashboardRequest listRequest, boolean countQuery) throws ParseException {

        Root<FacultyAttendance> facAttenRoot = criteriaQuery.from(FacultyAttendance.class);
        Root<InstituteTypeMaster> instituteTypeMaster = criteriaQuery.from(InstituteTypeMaster.class);

        if (countQuery) {
            criteriaQuery.select(criteriaBuilder.count(facAttenRoot));
        } else {
            criteriaQuery.multiselect(
                    instituteTypeMaster.get("name"), criteriaBuilder.count(facAttenRoot), facAttenRoot.get("status")
            ).groupBy(facAttenRoot.get("attendanceDate"), facAttenRoot.get("status"), facAttenRoot.get("instituteTypeId"));

            List<Order> orders = new ArrayList<>();

            if (!orders.isEmpty()) {
                //criteriaQuery.orderBy(orders);
                criteriaQuery.orderBy(criteriaBuilder.desc(facAttenRoot.get("id")));
            }
        }

        String pattern = "yyyy-MM-dd";
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
        String date = simpleDateFormat.format(new Date());

        System.out.println("date ::" + date);

        ArrayList<Predicate> predicateList = new ArrayList<>();
        predicateList.add(criteriaBuilder.equal(facAttenRoot.get("attendanceDate"), date));
        predicateList.add(criteriaBuilder.equal(facAttenRoot.get("instituteTypeId"), instituteTypeMaster.get("id")));
        Predicate[] predicates = new Predicate[predicateList.size()];
        predicateList.toArray(predicates);
        criteriaQuery.where(predicates);
    }

    @Override
    public List<DashboardResponse> countAttendanceForDirector(DashboardRequest dashboardRequest) {
        List<DashboardResponse> dashboardResponseList = new ArrayList<>();

        try {
            Class.forName("com.mysql.jdbc.Driver");
            // loads driver
//            Connection c = DriverManager.getConnection("jdbc:mysql://localhost:3306/dheemati_attendance", "dheemati", "Chetan_2_11_68"); // gets a new connection
            String path = env.getProperty("spring.datasource.url");
            String userName = env.getProperty("spring.datasource.username");
            String password = env.getProperty("spring.datasource.password");

            Connection c = DriverManager.getConnection(path, userName, password);

            Statement s = c.createStatement();

            DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd");
            LocalDateTime now = LocalDateTime.now();

            String sql = "select fa.attendance_date as date,im.name as instituteName"
                    + ",count(case when status = 0  then 1 end) as absent_count"
                    + ",count(case when status = 1 then 1 end) as present_count"
                    + "  from faculty_attendance as fa, institute_master as im"
                    + " where fa.attendance_date='" + dtf.format(now) + "' "
                    + " And fa.institute_type_id=" + dashboardRequest.getInstituteTypeId()
                    + " And fa.institute_id= im.id"
                    + " group by fa.attendance_date,fa.institute_id;";
            ResultSet rs = s.executeQuery(sql);

            while (rs.next()) {
                String data = rs.getString("absent_count");
                String data1 = rs.getString("present_count");
                System.out.println("data of absent ::" + data);
                System.out.println("data of present ::" + data1);
                DashboardResponse dashboardResponse = new DashboardResponse();
                dashboardResponse.setCount(rs.getLong("present_count"));
                dashboardResponse.setAbsentCount(rs.getLong("absent_count"));
                dashboardResponse.setDate(rs.getString("date"));
                dashboardResponse.setInstituteName(rs.getString("instituteName"));
                dashboardResponseList.add(dashboardResponse);
            }

        } catch (ClassNotFoundException | SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return dashboardResponseList;
    }

    @Override
    public List<CMDashResponse> countAttendanceForCMDash(CMDashRequest cMDashRequest) {
        logger.debug("CMDashAttendanceCount");
        if (cMDashRequest.getKey().equalsIgnoreCase("177b8a0026e666670110412e802e5675")) {
            // Code for for loop started 
            List<CMDashResponse> cMDashResponses = new ArrayList<>();
            try {
                Class.forName("com.mysql.jdbc.Driver");
                String path = env.getProperty("spring.datasource.url");
                String userName = env.getProperty("spring.datasource.username");
                String password = env.getProperty("spring.datasource.password");
                System.out.println("Path : " + path);
                Connection c = DriverManager.getConnection(path, userName, password);
                // gets a new connection
                Statement s = c.createStatement();
                //     LocalDate startD = LocalDate.now();
                SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
                Date Gendate = format.parse(cMDashRequest.getStartDate());

                String pattern = "yyyy-MM-dd";
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);

                String startDate = simpleDateFormat.format(Gendate);
                System.out.println("Date :: " + startDate);
                String sql = "SELECT * FROM dheemati_attendance.cmdash_fac_v where date = '" + startDate + "' order by instituteId;";

                ResultSet rs = s.executeQuery(sql);
                while (rs.next()) {
//                    String data = rs.getString("absent_count");
//                    String data1 = rs.getString("present_count");
//                    System.out.println("data of absent ::" + data);
//                    System.out.println("data of present ::" + data1);
                    CMDashResponse cMDashResponse = new CMDashResponse();

                    cMDashResponse.setDates(cMDashRequest.getStartDate());

                    cMDashResponse.setInstituteName(rs.getString("instituteName"));
                    cMDashResponse.setInstId(rs.getLong("instituteId"));
                    cMDashResponse.setDistId(rs.getLong("DCode"));
                    cMDashResponse.setDistName(rs.getString("DistName"));
                    cMDashResponse.setDepartmentName(rs.getString("departmentName"));
                    cMDashResponse.setDistId(rs.getLong("DCode"));

                    cMDashResponse.setInstTypeId(rs.getLong("instituteTypeId"));
                    cMDashResponse.setInstituteTypeName(rs.getString("instituteTpeName"));
                    cMDashResponse.setUniversityId(rs.getLong("universityId"));
                    cMDashResponse.setUniversityName(rs.getString("universityName"));

                    cMDashResponse.setFacInTime(rs.getString("facInTime"));
                    cMDashResponse.setFacOutTimre(rs.getString("facOutTime"));
                    cMDashResponse.setActInTime(rs.getString("actInTime"));

                    cMDashResponse.setActOutTime(rs.getString("actOutTime"));

                    cMDashResponse.setFacultyName(rs.getString("facultyName"));
                    cMDashResponse.setUniversityId(rs.getLong("universityId"));
                    cMDashResponse.setUniversityName(rs.getString("universityName"));

                    cMDashResponse.setPresent(rs.getBoolean("present"));
                    cMDashResponse.setLeave(rs.getBoolean("lev"));
                    cMDashResponse.setEarly(rs.getBoolean("early"));
                    cMDashResponse.setLate(rs.getBoolean("late"));
                    cMDashResponse.setOnDuty(rs.getBoolean("onDuty"));
                    cMDashResponse.setAbsent(rs.getBoolean("absent"));
                    cMDashResponse.setHalfCL(rs.getBoolean("halfCL"));
                    fillNullObjects(cMDashResponse);
                    if(cMDashResponse.getActInTime().equalsIgnoreCase("#"))
                    {
                        cMDashResponse.setActInTime(null);
                         cMDashResponse.setActOutTime(null);
                    }
                  
                    cMDashResponse.setAbsLast30(0L);
                    cMDashResponses.add(cMDashResponse);
                }

            } catch (ClassNotFoundException | SQLException e) {
                e.printStackTrace();
                e.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();
            }

            return cMDashResponses;
        } else {
            return null;

        }
    }

    @Override
    public List<CMDashDailyTaskResponse> facultyDailyTaskForCMDash(CMDashRequest cMDashRequest) {
        logger.debug("CMDashDailyTask");
        if (cMDashRequest.getKey().equalsIgnoreCase("177b8a0026e666670110412e802e5675")) {
            // Code for for loop started 
            List<CMDashDailyTaskResponse> cMDashDailyTaskResponses = new ArrayList<>();
            try {
                Class.forName("com.mysql.jdbc.Driver");
                String path = env.getProperty("spring.datasource.url");
                String userName = env.getProperty("spring.datasource.username");
                String password = env.getProperty("spring.datasource.password");
                System.out.println("Path : " + path);
                Connection c = DriverManager.getConnection(path, userName, password);
                // gets a new connection
                Statement s = c.createStatement();
                //     LocalDate startD = LocalDate.now();
                SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
                Date Gendate = format.parse(cMDashRequest.getStartDate());

                String pattern = "yyyy-MM-dd";
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);

                String startDate = simpleDateFormat.format(Gendate);
                System.out.println("Date :: " + startDate);
                String sql = "SELECT * FROM dheemati_attendance.cmdash_daily_task where Date = '" + startDate + "' order by instituteId;";

                ResultSet rs = s.executeQuery(sql);
                while (rs.next()) {
//                    String data = rs.getString("absent_count");
//                    String data1 = rs.getString("present_count");
//                    System.out.println("data of absent ::" + data);
//                    System.out.println("data of present ::" + data1);
                    CMDashDailyTaskResponse cMDashResponse = new CMDashDailyTaskResponse();

                    cMDashResponse.setDates(cMDashRequest.getStartDate());

                    cMDashResponse.setInstituteName(rs.getString("instituteName"));
                    cMDashResponse.setInstId(rs.getLong("instituteId"));
                    cMDashResponse.setDistId(rs.getLong("DCode"));
                    cMDashResponse.setDistName(rs.getString("DistName"));
                    cMDashResponse.setDepartmentName(rs.getString("departmentName"));
                    cMDashResponse.setDistId(rs.getLong("DCode"));

                    cMDashResponse.setInstTypeId(rs.getLong("instituteTypeId"));
                    cMDashResponse.setInstituteTypeName(rs.getString("instituteTpeName"));
                    cMDashResponse.setUniversityId(rs.getLong("universityId"));
                    cMDashResponse.setUniversityName(rs.getString("universityName"));

                    cMDashResponse.setFacultyName(rs.getString("facultyName"));

                    cMDashResponse.setUniversityId(rs.getLong("universityId"));
                    cMDashResponse.setUniversityName(rs.getString("universityName"));
                    cMDashResponse.setPresentStudent(rs.getLong("Present"));
                    cMDashResponse.setTotalStudent(rs.getLong("Total"));
                    String div[] = rs.getString("Subject").split("-");
                    cMDashResponse.setDivison(div[div.length - 1]);
                    cMDashResponse.setSlotNumber(rs.getLong("SlotNumber"));
                    cMDashResponse.setSemester(rs.getLong("SEM"));
                    fillNullObjects(cMDashResponse);
                    
                    cMDashDailyTaskResponses.add(cMDashResponse);
                }

            } catch (ClassNotFoundException | SQLException e) {
                e.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();
            }

            return cMDashDailyTaskResponses;
        } else {
            return null;

        }
        //To change body of generated methods, choose Tools | Templates.
    }

   

}
