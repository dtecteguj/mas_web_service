package com.springapi.service.impl;

import com.springapi.model.DayMaster;
import com.springapi.model.TimeTable;
import com.springapi.model.TokensManage;
import com.springapi.repository.DayRepository;
import com.springapi.repository.TokensManageRepository;
import com.springapi.service.FcmDailyPushNotificationService;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Order;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.persistence.criteria.Subquery;
import org.apache.log4j.Logger;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.RestTemplate;

/**
 * @author shahb
 */
@Service
@Transactional
public class FcmDailyPushNotificationServiceImpl implements FcmDailyPushNotificationService {

    private static final String FIREBASE_SERVER_KEY = "AIzaSyCTa8SYsHhiamhdBdF2erjvuqNtvloDUR4";
    private static final String FIREBASE_API_URL = "https://fcm.googleapis.com/fcm/send";

    static Logger logger = Logger.getLogger(FcmDailyPushNotificationServiceImpl.class);

    @Autowired
    private EntityManager entityManager;

    @Autowired
    private TokensManageRepository tokensManageRepository;

    @Autowired
    private DayRepository dayRepository;

    @Override
    public List<String> getTokensForNotification() {
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery criteriaQuery = criteriaBuilder.createQuery(String.class);

        prepareListQuery(criteriaQuery, criteriaBuilder, false);

        TypedQuery typedQuery = entityManager.createQuery(criteriaQuery);
        List results = typedQuery.getResultList();
        return results;
    }

    private void prepareListQuery(CriteriaQuery criteriaQuery, CriteriaBuilder criteriaBuilder, boolean countQuery) {

        Root<TokensManage> tokensManageRoot = criteriaQuery.from(TokensManage.class);
        // Root<TimeTable> timeTableRoot = criteriaQuery.from(TimeTable.class);

        if (countQuery) {
            criteriaQuery.select(criteriaBuilder.count(tokensManageRoot));
        } else {
            criteriaQuery.multiselect(
                    tokensManageRoot.get("token")
            );

            List<Order> orders = new ArrayList<>();
            if (!orders.isEmpty()) {
                criteriaQuery.orderBy(orders);
            }
        }

//        Subquery<TimeTable> subquery = criteriaQuery.subquery(TimeTable.class);
//        Root<TimeTable> subRoot = subquery.from(TimeTable.class);
//        subquery.select(subRoot);
//
//        ArrayList<Predicate> subPredicateList = new ArrayList<>();
//        subPredicateList.add(criteriaBuilder.equal(subRoot.get("dayId"), 5));
//        subPredicateList.add(criteriaBuilder.equal(subRoot.get("slotId"), 4));
//       
//        Predicate[] p = new Predicate[subPredicateList.size()];
//        subPredicateList.toArray(p);
//        subquery.where(p);
        ArrayList<Predicate> predicateList = new ArrayList<>();
        predicateList.add(criteriaBuilder.equal(tokensManageRoot.get("type"), "F"));

        Predicate[] predicates = new Predicate[predicateList.size()];
        predicateList.toArray(predicates);
        criteriaQuery.where(predicates);
    }

    @Override
    public List<String> getAllAttendanceTokenView() {
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery criteriaQuery = criteriaBuilder.createQuery(String.class);

        prepareListQueryForAttendance(criteriaQuery, criteriaBuilder, false);

        TypedQuery typedQuery = entityManager.createQuery(criteriaQuery);
        List results = typedQuery.getResultList();
        System.out.println("result::" + results);
        return results;
    }

    private void prepareListQueryForAttendance(CriteriaQuery criteriaQuery, CriteriaBuilder criteriaBuilder, boolean countQuery) {

        Root<TokensManage> tokensManageRoot = criteriaQuery.from(TokensManage.class);
        Root<TimeTable> timeTableRoot = criteriaQuery.from(TimeTable.class);

        if (countQuery) {
            criteriaQuery.select(criteriaBuilder.count(tokensManageRoot));
        } else {
            criteriaQuery.multiselect(
                    tokensManageRoot.get("token")
            );

            List<Order> orders = new ArrayList<>();
            if (!orders.isEmpty()) {
                criteriaQuery.orderBy(orders);
            }
        }
//        String day = getTodayDay();
//        System.out.println("day is ::" + day);
//        DayMaster dayMaster = dayRepository.findByDay(day);
        Long slotId = getSlotId();

        ArrayList<Predicate> predicateList = new ArrayList<>();
        predicateList.add(criteriaBuilder.equal(tokensManageRoot.get("uId"), timeTableRoot.get("facultyId")));
//        predicateList.add(criteriaBuilder.equal(timeTableRoot.get("dayId"), dayMaster.getId()));
        predicateList.add(criteriaBuilder.equal(timeTableRoot.get("slotId"), slotId));
        predicateList.add(criteriaBuilder.equal(tokensManageRoot.get("type"), "F"));

        Predicate[] predicates = new Predicate[predicateList.size()];
        predicateList.toArray(predicates);
        criteriaQuery.where(predicates);
    }

    @Override
    public List<String> getTokensForFaculty() {
        List<TokensManage> tokensManageList = tokensManageRepository.findByType("F");
        List<String> facultyTokens = new ArrayList<>();;
        if (tokensManageList.size() > 0) {
            System.out.println("Inside faculty List !!");
            for (TokensManage tokensManage : tokensManageList) {
                facultyTokens.add(tokensManage.getToken());
            }
            return facultyTokens;
        }
        return facultyTokens;
    }

    @Override
    public List<String> getTokensForStudent() {
        List<TokensManage> tokensManageList = tokensManageRepository.findByType("S");
        List<String> studentTokens = new ArrayList<>();
        if (tokensManageList.size() > 0) {
            System.out.println("Inside student List !!");
            for (TokensManage tokensManage : tokensManageList) {
                System.out.println("Inside for loop ");
                studentTokens.add(tokensManage.getToken());
            }
            return studentTokens;
        }
        return studentTokens;
    }

    @Async
    @Override
    public CompletableFuture<String> send(HttpEntity<String> entity) {

        RestTemplate restTemplate = new RestTemplate();
        JSONObject jsonObject = null;

        ArrayList<ClientHttpRequestInterceptor> interceptors = new ArrayList<>();
        interceptors.add(new HeaderRequestInterceptor("Authorization", "key=" + FIREBASE_SERVER_KEY));
        interceptors.add(new HeaderRequestInterceptor("Content-Type", "application/json"));
        restTemplate.setInterceptors(interceptors);

        try {
            // RestTemplate restTemplate = new RestTemplate();
            ResponseEntity<String> responseEntity = restTemplate.exchange(
                    FIREBASE_API_URL, HttpMethod.POST, entity,
                    String.class);
            System.out.println("Response::" + responseEntity);

            if (responseEntity.getStatusCode() == HttpStatus.OK) {
                System.out.println("Inside status ok");
                try {
                    jsonObject = new JSONObject(responseEntity.getBody());
                } catch (JSONException e) {
                    throw new RuntimeException("JSONException occurred");
                }
            }
        } catch (final HttpClientErrorException httpClientErrorException) {
            System.out.println("Catch 1!!");
        } catch (HttpServerErrorException httpServerErrorException) {
            System.out.println("Catch 2!!");
        } catch (Exception exception) {
            System.out.println("Catch 3!!");
        }

        System.out.println("Before push !!");
        //   String firebaseResponse = restTemplate.postForObject(FIREBASE_API_URL, entity, String.class);

        //System.out.println("After push ::" + firebaseResponse);
        String firebaseResponse = jsonObject.toString();
        return CompletableFuture.completedFuture(firebaseResponse);
    }

    @Override
    public Long getTodayDay() {
        Calendar calendar = Calendar.getInstance();
        int dayOfWeek = calendar.get(Calendar.DAY_OF_WEEK);
        System.out.println("day of the week ::" + dayOfWeek);
        Long day = null;
        switch (dayOfWeek) {
            case 1:
                day = 0L;
                break;
            case 2:
                day = 1L;
                break;
            case 3:
                day = 2L;
                break;
            case 4:
                day = 3L;
                break;
            case 5:
                day = 4L;
                break;
            case 6:
                day = 5L;
                break;
            case 7:
                day = 6L;
                break;
            default:
                System.out.println("Invalid Entry");
        }
        return day;
    }
    
    @Override
    public Long getSlotId() {
        String currentTime = new SimpleDateFormat("HH").format(new Date());
        System.out.println("currentTime ::" + currentTime);
        Long slotId = null;
        //String timeToCompare = "15:30";
        // boolean x = currentTime.equals(timeToCompare);

        switch (currentTime) {
            case "10":
                slotId = 1L;
                break;
            case "11":
                slotId = 2L;
                break;
            case "13":
                slotId = 4L;
                break;
            case "14":
                slotId = 5L;
                break;
            case "15":
                slotId = 7L;
                break;
            case "16":
                slotId = 8L;
                break;
            default:
                System.out.println("Invalid Entry");
        }

        return slotId;
    }
    
    @Override
     public Long getStudAndFacAttnVerificationSlotId() {
        String currentTime = new SimpleDateFormat("HH").format(new Date());
        System.out.println("currentTime ::" + currentTime);
        Long slotId = null;
      
        switch (currentTime) {
            case "11":
                slotId = 1L;
                break;
            case "12":
                slotId = 2L;
                break;
            case "13":
                slotId = 4L;
                break;
            case "14":
                slotId = 5L;
                break;
            case "16":
                slotId = 7L;
                break;
            case "17":
                slotId = 8L;
                break;
            default:
                System.out.println("Invalid Entry");
        }

        return slotId;
    }
}
