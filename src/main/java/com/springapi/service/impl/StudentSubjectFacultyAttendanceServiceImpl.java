package com.springapi.service.impl;

import com.springapi.dto.request.FacStudAttenRequest;
import com.springapi.dto.request.StudentAttendanceDetailRequest;
import com.springapi.dto.request.StudentAttendanceRequest;
import com.springapi.dto.request.StudentAttendanceTotalRequest;
import com.springapi.dto.request.StudentList;
import com.springapi.dto.response.StudentAttendanceCountResponse;
import com.springapi.model.Faculty;
import com.springapi.model.SlotMaster;
import com.springapi.model.Student;
import com.springapi.model.StudentAttendaceCount;
import com.springapi.model.StudentSubjectFacultyAttendance;
import com.springapi.model.TimeTable;
import com.springapi.repository.StudentAttendaceCountRepository;
import com.springapi.repository.StudentSubjectFacultyAttendanceRepository;
import com.springapi.repository.TimeTableRepository;
import com.springapi.service.FcmDailyPushNotificationService;
import com.springapi.service.StudentService;
import com.springapi.service.StudentSubjectFacultyAttendanceService;
import static com.springapi.service.impl.DashboardServiceImpl.logger;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.logging.Level;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Order;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author shahb
 */
@Service
@Transactional
public class StudentSubjectFacultyAttendanceServiceImpl implements StudentSubjectFacultyAttendanceService {

    @Autowired
    private StudentSubjectFacultyAttendanceRepository studentSubjectFacultyAttendanceRepository;

    @Autowired
    private StudentService studentService;

    @Autowired
    private FcmDailyPushNotificationService fcmDailyPushNotificationService;

    @Autowired
    private TimeTableRepository timeTableRepository;

    @Autowired
    private EntityManager entityManager;

    @Autowired
    private StudentAttendaceCountRepository studentAttendaceCountRepository;

    @Override
    public StudentSubjectFacultyAttendance create(StudentSubjectFacultyAttendance studentSubjectFacultyAttendance) {
        return studentSubjectFacultyAttendanceRepository.save(studentSubjectFacultyAttendance);
    }

    @Override
    public List<StudentSubjectFacultyAttendance> list() {
        return studentSubjectFacultyAttendanceRepository.findAll();
    }

    @Override
    public StudentSubjectFacultyAttendance read(Long lId) {
        Optional<StudentSubjectFacultyAttendance> optional = studentSubjectFacultyAttendanceRepository.findById(lId);
        if (!optional.isPresent()) {
            // throw Exception;
        }
        return optional.get();
    }

    @Override
    public void delete(Long lId) {
        StudentSubjectFacultyAttendance studentSubjectFacultyAttendance = read(lId);
        studentSubjectFacultyAttendanceRepository.delete(studentSubjectFacultyAttendance);
    }

    @Override
    public List<StudentSubjectFacultyAttendance> defaultAttendance(FacStudAttenRequest facStudAttenRequest) {
        List<Student> studentList = studentService.findAllStudentBySem(facStudAttenRequest.getSemId());
        List<StudentSubjectFacultyAttendance> studentSubjectFacultyAttendances = new ArrayList<StudentSubjectFacultyAttendance>();
        if (studentList != null) {

            for (Student student : studentList) {
                StudentSubjectFacultyAttendance studentSubjectFacultyAttendance = new StudentSubjectFacultyAttendance();
                studentSubjectFacultyAttendance.setCreatedAt(new Date());
                studentSubjectFacultyAttendance.setFacultyId(facStudAttenRequest.getfId());
                studentSubjectFacultyAttendance.setSubjectId(facStudAttenRequest.getSubjectId());
                studentSubjectFacultyAttendance.setDayId(facStudAttenRequest.getDayId());
                studentSubjectFacultyAttendance.setSemId(facStudAttenRequest.getSemId());
                studentSubjectFacultyAttendance.setStudentId(student.getId());
                studentSubjectFacultyAttendance.setStatus(Boolean.FALSE);

                studentSubjectFacultyAttendances.add(studentSubjectFacultyAttendance);
                studentSubjectFacultyAttendanceRepository.save(studentSubjectFacultyAttendance);
            }
        }
        return studentSubjectFacultyAttendances;
    }

//    @Override
//    public String studentAttendance(List<StudentAttendanceRequest> studentAttendanceRequestList){
//        for (StudentAttendanceRequest studentAttendanceRequest : studentAttendanceRequestList) {
//            StudentSubjectFacultyAttendance studentSubjectFacultyAttendance = studentSubjectFacultyAttendanceRepository.findByStudentIdAndFacultyIdAndSemIdAndSubjectId(
//            studentAttendanceRequest.getStudentId(), studentAttendanceRequest.getFacultyId(), studentAttendanceRequest.getSemId(), studentAttendanceRequest.getSubjectId());
//        
//            if(studentSubjectFacultyAttendance != null){
//                studentSubjectFacultyAttendance.setStatus(Boolean.TRUE);
//                studentSubjectFacultyAttendanceRepository.save(studentSubjectFacultyAttendance);
//            }
//        }
//        return "updated sucessfully";
//    }
    @Override
    public String studentAttendance(StudentAttendanceRequest studentAttendanceRequest) {
        Long dayId = fcmDailyPushNotificationService.getTodayDay();
        TimeTable timeTable = timeTableRepository.findByDayIdAndFacultyIdAndSlotId(dayId,
                studentAttendanceRequest.getFacultyId(), studentAttendanceRequest.getSlotId());

        List<StudentList> studentInfoList = studentAttendanceRequest.getStudentInfo();
        for (StudentList studentList : studentInfoList) {
            StudentSubjectFacultyAttendance studentSubjectFacultyAttendance
                    = new StudentSubjectFacultyAttendance();

            studentSubjectFacultyAttendance.setCreatedAt(new Date());
            studentSubjectFacultyAttendance.setDayId(dayId);
            studentSubjectFacultyAttendance.setFacultyId(studentAttendanceRequest.getFacultyId());
            studentSubjectFacultyAttendance.setSemId(timeTable.getSemId() != null ? timeTable.getSemId() : null);
            studentSubjectFacultyAttendance.setStatus(studentList.getAttendance());
            studentSubjectFacultyAttendance.setStudentId(studentList.getStudentId());
            studentSubjectFacultyAttendance.setSubjectId(timeTable.getSubjectId() != null ? timeTable.getSubjectId() : null);
            studentSubjectFacultyAttendance.setSlotId(studentAttendanceRequest.getSlotId());

            studentSubjectFacultyAttendanceRepository.save(studentSubjectFacultyAttendance);
        }

        return "success";
    }

    @Override
    public List<StudentAttendanceCountResponse> studentAttendanceCount(StudentAttendanceRequest studentAttendanceRequest) {
        logger.debug("studentAttendanceCount");
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery criteriaQuery = criteriaBuilder.createQuery(StudentAttendanceCountResponse.class);

        try {
            // Prepare list query
            prepareListQuery(criteriaQuery, criteriaBuilder, studentAttendanceRequest, false);
        } catch (ParseException ex) {
            java.util.logging.Logger.getLogger(StudentSubjectFacultyAttendanceServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
        }

        TypedQuery typedQuery = entityManager.createQuery(criteriaQuery);

        List results = typedQuery.getResultList();
        System.out.println("result::" + results);
        return results;

    }

    private void prepareListQuery(CriteriaQuery criteriaQuery, CriteriaBuilder criteriaBuilder, StudentAttendanceRequest listRequest, boolean countQuery) throws ParseException {

        Root<StudentSubjectFacultyAttendance> studenRoot = criteriaQuery.from(StudentSubjectFacultyAttendance.class);
        //  Root<DepartmentMaster> departmentRoot = criteriaQuery.from(DepartmentMaster.class);
        Date yesterday = new Date(System.currentTimeMillis() - 1000L * 60L * 60L * 24L);
        System.out.println("yesterday ::" + yesterday);
        if (countQuery) {
            criteriaQuery.select(criteriaBuilder.count(studenRoot));
        } else {

            criteriaQuery.multiselect(
                    studenRoot.get("facultyId"), studenRoot.get("semId"), criteriaBuilder.count(studenRoot)
            ).groupBy(studenRoot.get("facultyId"), studenRoot.get("semId"));

            List<Order> orders = new ArrayList<>();

            if (!orders.isEmpty()) {
                //criteriaQuery.orderBy(orders);
                criteriaQuery.orderBy(criteriaBuilder.desc(studenRoot.get("id")));
            }
        }

        ArrayList<Predicate> predicateList = new ArrayList<>();

        Predicate[] predicates = new Predicate[predicateList.size()];
        predicateList.toArray(predicates);
        criteriaQuery.where(predicates);
    }

    @Override
    public String studentAttendanceTotalCount(StudentAttendanceTotalRequest studentAttendanceTotalRequest) {
        String pattern = "yyyy-MM-dd";
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
        String date = simpleDateFormat.format(new Date());

        StudentAttendaceCount studentAttendanceCount = new StudentAttendaceCount();
        studentAttendanceCount.setCreatedAt(new Date());
        studentAttendanceCount.setDayId(studentAttendanceTotalRequest.getDayId());
        studentAttendanceCount.setFacultyId(studentAttendanceTotalRequest.getFacultyId());
        studentAttendanceCount.setSemId(studentAttendanceTotalRequest.getSemId());
        studentAttendanceCount.setSlotId(studentAttendanceTotalRequest.getSlotId());
        studentAttendanceCount.setTotPresent(studentAttendanceTotalRequest.getTotPresent());
        studentAttendanceCount.setTotalCount(studentAttendanceTotalRequest.getTotCount());
        studentAttendanceCount.setSubjectName(studentAttendanceTotalRequest.getSubjectName());
        studentAttendanceCount.setBatchName(studentAttendanceTotalRequest.getBatchName());
        studentAttendanceCount.setLecOrLab(studentAttendanceTotalRequest.getLecOrLab());
        studentAttendanceCount.setAttendanceDate(date);

        studentAttendaceCountRepository.save(studentAttendanceCount);
        return "Submitted Succesfully";
    }

    @Override
    public List<StudentAttendaceCount> studentAttendanceCountList() {
        logger.debug("studentAttendanceCountList");
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery criteriaQuery = criteriaBuilder.createQuery(StudentAttendaceCount.class);

        try {
            // Prepare list query
            prepareListQuery(criteriaQuery, criteriaBuilder, false);
        } catch (ParseException ex) {
            java.util.logging.Logger.getLogger(DashboardServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
        }

        TypedQuery typedQuery = entityManager.createQuery(criteriaQuery);

        List results = typedQuery.getResultList();
        System.out.println("result::" + results);
        return results;

    }

    private void prepareListQuery(CriteriaQuery criteriaQuery, CriteriaBuilder criteriaBuilder, boolean countQuery) throws ParseException {
        Root<StudentAttendaceCount> studentAttendanceRoot = criteriaQuery.from(StudentAttendaceCount.class);
        Root<Faculty> facultyRoot = criteriaQuery.from(Faculty.class);
        Root<SlotMaster> slotRoot = criteriaQuery.from(SlotMaster.class);

        if (countQuery) {
            criteriaQuery.select(criteriaBuilder.count(studentAttendanceRoot));
        } else {

            criteriaQuery.multiselect(
                    studentAttendanceRoot.get("facultyId"), studentAttendanceRoot.get("slotId"),
                    studentAttendanceRoot.get("semId"), studentAttendanceRoot.get("dayId"),
                    studentAttendanceRoot.get("totPresent"), facultyRoot.get("name"), slotRoot.get("startTime"),
                    studentAttendanceRoot.get("attendanceDate"), studentAttendanceRoot.get("totalCount"),
                     studentAttendanceRoot.get("subjectName"), studentAttendanceRoot.get("batchName"),
                    studentAttendanceRoot.get("lecOrLab"));

            List<Order> orders = new ArrayList<>();

            if (!orders.isEmpty()) {
                criteriaQuery.orderBy(criteriaBuilder.desc(studentAttendanceRoot.get("id")));
            }
        }

        String pattern = "yyyy-MM-dd";
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
        String date = simpleDateFormat.format(new Date());

        ArrayList<Predicate> predicateList = new ArrayList<>();
        predicateList.add(criteriaBuilder.equal(studentAttendanceRoot.get("facultyId"), facultyRoot.get("id")));
        predicateList.add(criteriaBuilder.equal(studentAttendanceRoot.get("slotId"), slotRoot.get("id")));
        predicateList.add(criteriaBuilder.equal(studentAttendanceRoot.get("attendanceDate"), date));
//        predicateList.add(criteriaBuilder.lessThanOrEqualTo(studentAttendanceRoot.<Date>get("attendanceDate"), endDate));

        Predicate[] predicates = new Predicate[predicateList.size()];
        predicateList.toArray(predicates);
        criteriaQuery.where(predicates);

    }

    @Override
    public StudentAttendaceCount getStudentAtendance(StudentAttendanceDetailRequest studentAttendanceDetailRequest) {
//        String pattern = "yyyy-MM-dd";
//        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
//        String date = simpleDateFormat.format(new Date());

        StudentAttendaceCount studentAttendaceCount = studentAttendaceCountRepository.findBySlotIdAndFacultyIdAndAttendanceDate(
                studentAttendanceDetailRequest.getSlotId(), studentAttendanceDetailRequest.getFacultyId(), studentAttendanceDetailRequest.getDayDate());

        if (studentAttendaceCount == null) {
            return null;
        }
        return studentAttendaceCount;
    }
}
