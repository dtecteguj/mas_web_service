/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.springapi.service.impl;

import com.springapi.enums.FASMessageType;
import com.springapi.exception.FASException;
import com.springapi.model.DepartmentMaster;
import com.springapi.model.ProgramInstMpg;
import com.springapi.model.ProgramMst;
import com.springapi.repository.ProgramInstMpgRepository;
import com.springapi.service.ProgramInstMpgService;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author shahb
 */
@Service
@Transactional
public class ProgramInstMpgServiceImpl implements ProgramInstMpgService {

    static Logger logger = Logger.getLogger(InstituteTypeServiceImpl.class);

    @Autowired
    private ProgramInstMpgRepository programInstMpgRepository;

    @Autowired
    private EntityManager entityManager;

    @Override
    public ProgramInstMpg create(ProgramInstMpg programInstMpg) {
        logger.debug("create");
        return programInstMpgRepository.save(programInstMpg);
    }

    @Override
    public List<ProgramInstMpg> list() {
        logger.debug("list");
        return programInstMpgRepository.findAll();
    }

    @Override
    public ProgramInstMpg read(Long pId) {
        logger.debug("read");
        Optional<ProgramInstMpg> optional = programInstMpgRepository.findById(pId);
        if (!optional.isPresent()) {
            throw new FASException(FASMessageType.ENTITY_NOT_FOUND);
        }
        return optional.get();
    }

    @Override
    public ProgramInstMpg update(ProgramInstMpg programInstMpg) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void delete(Long pId) {
        logger.debug("delete");
        Optional<ProgramInstMpg> optional = programInstMpgRepository.findById(pId);
        if (!optional.isPresent()) {
            throw new FASException(FASMessageType.ENTITY_NOT_FOUND);
        }

        programInstMpgRepository.delete(optional.get());
    }

    @Override
    public List<ProgramMst> readProgramByInstituteId(Long instituteId) {
        logger.debug("readProgramByInstituteId");
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery criteriaQuery = criteriaBuilder.createQuery(ProgramMst.class);
        prepareListQuery(criteriaQuery, criteriaBuilder, instituteId);
        TypedQuery typedQuery = entityManager.createQuery(criteriaQuery);
        List<ProgramMst> programMsts = typedQuery.getResultList();
        //  List<DepartmentMaster> departmentList = departmentRepository.findAllByInstituteId(instituteId);
        return programMsts;
    }

    private void prepareListQuery(CriteriaQuery criteriaQuery, CriteriaBuilder criteriaBuilder, Long instituteId) {
        Root<ProgramInstMpg> pRoot = criteriaQuery.from(ProgramInstMpg.class);
        Root<ProgramMst> pRoot1 = criteriaQuery.from(ProgramMst.class);
        criteriaQuery.multiselect(pRoot1.get("id"),pRoot1.get("code"), pRoot1.get("progLevel"),pRoot1.get("name"),pRoot1.get("studyType"),pRoot1.get("acdmctType"),pRoot1.get("duration"),pRoot1.get("deptType"),pRoot1.get("progDesc"),pRoot.get("shiftName"),pRoot.get("shiftCount"),pRoot.get("intake") );
        ArrayList<Predicate> predicateList = new ArrayList<>();
        predicateList.add(criteriaBuilder.equal(pRoot.get("prgmId"), pRoot1.get("id")));
        if (instituteId != null) {
            predicateList.add(criteriaBuilder.equal(pRoot.get("instituteId"), instituteId));
        }
        Predicate[] predicates = new Predicate[predicateList.size()];
        predicateList.toArray(predicates);
        criteriaQuery.where(predicates);

    }
    
    
    
    @Override
    public List<ProgramInstMpg> readProgramByProgramIdAndInstituteId(Long instituteId,Long programId) {
        logger.debug("readProgramByInstituteId");
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery criteriaQuery = criteriaBuilder.createQuery(ProgramInstMpg.class);
        prepareListQuerys(criteriaQuery, criteriaBuilder, instituteId,programId);
        TypedQuery typedQuery = entityManager.createQuery(criteriaQuery);
        List<ProgramInstMpg> programMsts = typedQuery.getResultList();

        //  List<DepartmentMaster> departmentList = departmentRepository.findAllByInstituteId(instituteId);
        return programMsts;
    }

    private void prepareListQuerys(CriteriaQuery criteriaQuery, CriteriaBuilder criteriaBuilder, Long instituteId,Long programId) {
        Root<ProgramInstMpg> pRoot = criteriaQuery.from(ProgramInstMpg.class);
        Root<DepartmentMaster> pRoot1 = criteriaQuery.from(DepartmentMaster.class);
        criteriaQuery.multiselect(pRoot.get("id"),pRoot.get("prgmId"), pRoot.get("instituteId"),pRoot.get("instituteTypeId"),pRoot.get("instituteDepartmentId"),pRoot.get("description"),pRoot.get("shiftName"),pRoot.get("shiftCount"),pRoot.get("intake"),pRoot1.get("name") );
        ArrayList<Predicate> predicateList = new ArrayList<>();
        predicateList.add(criteriaBuilder.equal(pRoot.get("instituteDepartmentId"), pRoot1.get("id")));
        if (instituteId != null) {
            predicateList.add(criteriaBuilder.equal(pRoot.get("instituteId"), instituteId));
        }
        if (programId != null) {
            predicateList.add(criteriaBuilder.equal(pRoot.get("prgmId"), programId));
        }
        Predicate[] predicates = new Predicate[predicateList.size()];
        predicateList.toArray(predicates);
        criteriaQuery.where(predicates);

    }
    
}
