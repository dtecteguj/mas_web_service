/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.springapi.service.impl;

import com.springapi.enums.FASMessageType;
import com.springapi.exception.FASException;
import com.springapi.model.TrnTransferDtls;
import com.springapi.repository.TrnTransferDtlsRepository;
import com.springapi.service.TrnTransferDtlsService;
import java.util.List;
import java.util.Optional;
import javax.persistence.EntityManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author shahb
 */
@Service
@Transactional
public class TrnTransferDtlsServiceImpl implements TrnTransferDtlsService {

    static Logger logger = Logger.getLogger(TrnTransferDtlsServiceImpl.class);

    @Autowired
    private TrnTransferDtlsRepository trnTransferDtlsRepository;

    @Autowired
    private EntityManager entityManager;

    @Override
    public TrnTransferDtls create(TrnTransferDtls trnTransferDtls) {
        logger.debug("create");
        return trnTransferDtlsRepository.save(trnTransferDtls);
    }

    @Override
    public List<TrnTransferDtls> list() {
        logger.debug("list");
        return trnTransferDtlsRepository.findAll();
    }

    @Override
    public TrnTransferDtls read(Long aId) {
        logger.debug("read");
        Optional<TrnTransferDtls> optional = trnTransferDtlsRepository.findById(aId);
        if (!optional.isPresent()) {
            throw new FASException(FASMessageType.ENTITY_NOT_FOUND);
        }
        return optional.get();
    }

    @Override
    public TrnTransferDtls update(TrnTransferDtls trnTransferDtls) {
        TrnTransferDtls transferDtls = read(trnTransferDtls.getId());

        transferDtls.setfId(trnTransferDtls.getfId());
        transferDtls.setSourceInstId(trnTransferDtls.getSourceInstId());
        transferDtls.setDestiInstId(trnTransferDtls.getDestiInstId());
        transferDtls.setTransfrType(trnTransferDtls.getTransfrType());
        transferDtls.setTrnsftReason(trnTransferDtls.getTrnsftReason());
        transferDtls.setRelieveDate(trnTransferDtls.getRelieveDate());
        transferDtls.setIsCancelled(trnTransferDtls.getIsCancelled());
        transferDtls.setCancelledBy(trnTransferDtls.getCancelledBy());
        transferDtls.setCancelledOn(trnTransferDtls.getCancelledOn());
        return trnTransferDtlsRepository.save(transferDtls);
    }

    @Override
    public void delete(Long aId) {
        logger.debug("delete");
        logger.debug("delete");
        TrnTransferDtls trnTransferDtls = read(aId);
        trnTransferDtlsRepository.delete(trnTransferDtls);
    }

    @Override
    public List<TrnTransferDtls> facultyTransHist(Long fId) {
        return trnTransferDtlsRepository.findByFId(fId);
    }

    @Override
    public TrnTransferDtls facultyTransActiveHist(Long fId) {
        Optional<TrnTransferDtls> optional = trnTransferDtlsRepository.findByFIdAndIsCancelled(fId, Boolean.FALSE);
        if (!optional.isPresent()) {
            throw new FASException(FASMessageType.ENTITY_NOT_FOUND);
        }
        return optional.get();
    }

    @Override
    public List<TrnTransferDtls> instFacTransHist(Long iId) {
        return trnTransferDtlsRepository.findBySourceInstId(iId);
    }

    @Override
    public List<TrnTransferDtls> instFacTransActiveHist(Long iId) {
       return trnTransferDtlsRepository.findBySourceInstIdAndIsCancelled(iId, Boolean.FALSE);
    }

}
