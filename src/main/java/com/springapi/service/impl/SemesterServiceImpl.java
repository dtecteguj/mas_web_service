package com.springapi.service.impl;

import com.springapi.enums.FASMessageType;
import com.springapi.exception.FASException;
import com.springapi.model.Semester;
import com.springapi.repository.SemesterRepository;
import com.springapi.service.SemesterService;
import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author shahb
 */
@Service
@Transactional
public class SemesterServiceImpl implements SemesterService {

    @Autowired
    private SemesterRepository semesterRepository;

    @Override
    public Semester create(Semester semester) {
        return semesterRepository.save(semester);
    }

    @Override
    public List<Semester> list() {
        return semesterRepository.findAll();
    }

    @Override
    public Semester read(Long sId) {
        Optional<Semester> optional = semesterRepository.findById(sId);
        if (!optional.isPresent()) {
             throw new FASException(FASMessageType.ENTITY_NOT_FOUND);
        }
        return optional.get();
    }

    @Override
    public Semester update(Semester semesterDetails) {
        Semester semester = read(semesterDetails.getId());

        semester.setName(semesterDetails.getName());

        return semesterRepository.save(semester);
    }

    @Override
    public void delete(Long sId) {
        Semester semester = read(sId);
        semesterRepository.delete(semester);
    }
}
