package com.springapi.service.impl;

import com.springapi.dto.request.BlockByFacultyIdRequest;
import com.springapi.dto.response.BlockUserByDeviceResponse;
import java.util.List;
import java.util.Optional;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.springapi.model.FacultyLogin;
import com.springapi.repository.FacultyLoginRpository;
import com.springapi.service.FacultyLoginService;

@Service
@Transactional
public class FacultyLoginServiceImpl implements FacultyLoginService {

    static Logger logger = Logger.getLogger(TrnActiveLoginLogServiceImpl.class);

    @Autowired
    private FacultyLoginRpository BlockAndroidIdsForFacultyRpository1;

    @Override
    public FacultyLogin create(FacultyLogin tpl) {
        logger.debug("create");
        return BlockAndroidIdsForFacultyRpository1.save(tpl);
    }

    @Override
    public List<FacultyLogin> list() {
        logger.debug("list");
        return BlockAndroidIdsForFacultyRpository1.findAll();
    }

    @Override
    public FacultyLogin read(Long id) {
        logger.debug("read");
        Optional<FacultyLogin> optional = BlockAndroidIdsForFacultyRpository1.findById(id);
        if (!optional.isPresent()) {
            optional = null;
        }
        return optional.get();
    }

    @Override
    public FacultyLogin update(FacultyLogin tpl) {
        logger.debug("update");
        FacultyLogin tpl1 = read(tpl.getId());
        return BlockAndroidIdsForFacultyRpository1.save(tpl1);
    }

    @Override
    public void delete(Long id) {
        logger.debug("delete");
        FacultyLogin tpl = read(id);
        BlockAndroidIdsForFacultyRpository1.delete(tpl);
    }

    @Override
    public BlockUserByDeviceResponse blockUserById(BlockByFacultyIdRequest blockByFacultyIdRequest) {
        // throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        FacultyLogin facultyLogin = read(blockByFacultyIdRequest.getFId());
        BlockUserByDeviceResponse blockUserByDeviceResponse = new BlockUserByDeviceResponse();
        if (facultyLogin != null) {
            facultyLogin.setIsBlock(blockByFacultyIdRequest.getIsBlock());
            facultyLogin.setBlockDevId(blockByFacultyIdRequest.getDeviceId());

        } else {
            blockUserByDeviceResponse.setErrorStatus(301L, "User Invalid");
            blockUserByDeviceResponse.setIsBlock(Boolean.FALSE);
        }
        return blockUserByDeviceResponse;
    }
}
