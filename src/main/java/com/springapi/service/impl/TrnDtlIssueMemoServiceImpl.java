/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.springapi.service.impl;

import com.springapi.dto.response.FacultyData;
import com.springapi.dto.response.NoticeResponse;
import com.springapi.enums.FASMessageType;
import com.springapi.exception.FASException;
import com.springapi.model.TrnDtlIssueMemo;
import com.springapi.repository.TrnDtlIssueMemoRepository;
import com.springapi.service.TrnDtlIssueMemoService;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import javax.persistence.EntityManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author shahb
 */
@Service
@Transactional
public class TrnDtlIssueMemoServiceImpl implements TrnDtlIssueMemoService {

    static Logger logger = Logger.getLogger(TermMasterServiceImpl.class);

    @Autowired
    private TrnDtlIssueMemoRepository trnDtlIssueMemoRepository;

    @Autowired
    private FacultyServiceImpl facultyServiceImpl;

    @Autowired
    private EntityManager entityManager;

    @Override
    public TrnDtlIssueMemo create(TrnDtlIssueMemo trnDtlIssueMemo) {
        logger.debug("create");
        return trnDtlIssueMemoRepository.save(trnDtlIssueMemo);
    }

    @Override
    public List<TrnDtlIssueMemo> list() {
        logger.debug("list");
        return trnDtlIssueMemoRepository.findAll();
    }

    @Override
    public TrnDtlIssueMemo read(Long tId) {
        logger.debug("read");
        Optional<TrnDtlIssueMemo> optional = trnDtlIssueMemoRepository.findById(tId);
        if (!optional.isPresent()) {
            throw new FASException(FASMessageType.ENTITY_NOT_FOUND);
        }
        return optional.get();
    }

    @Override
    public TrnDtlIssueMemo update(TrnDtlIssueMemo trnDtlIssueMemo) {
        logger.debug("update");
        TrnDtlIssueMemo term = read(trnDtlIssueMemo.getId());
        if (trnDtlIssueMemo.getfId() == null) {
            System.out.println("Faculty APP Use!");
            term.setIsSubmitted(trnDtlIssueMemo.getIsSubmitted());
            term.setMemoReason(trnDtlIssueMemo.getMemoReason());

            term.setCreatedBy(term.getfId());
            term.setCreatedAt(new Date());
        } else {
            System.out.println("Principal APP Use!");
            term.setUpdateBy(trnDtlIssueMemo.getfId());
            term.setUpdatedAt(new Date());
            term.setIsEndorse(trnDtlIssueMemo.getIsEndorse());
            term.setRemark(trnDtlIssueMemo.getRemark());
        }

        return trnDtlIssueMemoRepository.save(term);
    }

    @Override
    public void delete(Long iId) {
        logger.debug("delete");
        TrnDtlIssueMemo trnDtlIssueMemo = read(iId);
        trnDtlIssueMemoRepository.delete(trnDtlIssueMemo);
    }

    @Override
    public List<NoticeResponse> readbyfid(Long fId) {
        List<TrnDtlIssueMemo> trnDtlIssueMemos = trnDtlIssueMemoRepository.findByFIdAndIsDeletedFalseOrderByIsSubmittedAsc(fId);

        List<NoticeResponse> noticeResponses = new ArrayList<>();
        for (TrnDtlIssueMemo memo : trnDtlIssueMemos) {
            NoticeResponse noticeResponse = new NoticeResponse();
            noticeResponse.setfId(memo.getfId());
            noticeResponse.setMemoStatus("Pending");
            noticeResponse.setTypeId(memo.getTypeId());
            noticeResponse.setMemoCount(memo.getMemoCount());
            noticeResponse.setMemoLotNo(memo.getMemoLotNo());
            noticeResponse.setIsSubmitted(memo.getIsSubmitted());
            noticeResponse.setId(memo.getId());
            noticeResponse.setMemoReason(memo.getMemoReason());
            String pattern = "yyyy-MM-dd";
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);

            String startDate = simpleDateFormat.format(memo.getLotCreationDate());
            noticeResponse.setLotCreationDate(startDate);
            noticeResponses.add(noticeResponse);
        }
        return noticeResponses;
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<NoticeResponse> readbyinstid(Long iId) {
        List<TrnDtlIssueMemo> trnDtlIssueMemos = trnDtlIssueMemoRepository.findByInstituteIdAndIsDeletedFalseOrderByIsEndorseAscIsSubmittedDescLotCreationDateDesc(iId);
        List<NoticeResponse> noticeResponses = new ArrayList<>();
        for (TrnDtlIssueMemo memo : trnDtlIssueMemos) {
            NoticeResponse noticeResponse = new NoticeResponse();
            noticeResponse.setfId(memo.getfId());
//             FacultyData readAllData = facultyServiceImpl.readAllDatafoNotice(memo.getfId());
//            try{
//                
//                 if(readAllData == null)
//                 {
//                     continue;
//                 }
//                     
//            }catch(Exception e)
//            {
//                continue;
//            }
            
            FacultyData readAllData = facultyServiceImpl.readAllDatafoNotice(memo.getfId());
            
           
            if (readAllData.getFName() != null) {
                if (readAllData.getMName() == null) {
                    noticeResponse.setFacultyName(readAllData.getFName() + " " + readAllData.getLName());
                } else {
                    if (readAllData.getMName().length() > 0) {
                        noticeResponse.setFacultyName(readAllData.getFName().charAt(0) + " " + readAllData.getMName().charAt(0) + " " + readAllData.getLName());
                    }
                }
            } else {
                String[] arr1 = readAllData.getName().split(" ");
                String facName = "";
                for (int i = 0; i < arr1.length - 1; i++) {
                    facName += arr1[i].charAt(0) + ".";
                }
                facName += arr1[arr1.length - 1];
                noticeResponse.setFacultyName(facName);
            }
            //noticeResponse.setFacultyName(readAllData.getName());
            noticeResponse.setDepartmentId(readAllData.getDepartmentId());
            String[] arr = readAllData.getDepartmentName().split(" ");
            // print all the initials
            String deptName = "";
            for (int i = 1; i < arr.length - 1; i++) {
                deptName += arr[i].charAt(0) + ".";
            }
            if (arr.length == 1) {
               
                if(arr[0].length()>4)
                     deptName += arr[0].substring(0, 5);
                else if(arr[0].length()>3)
                     deptName += arr[0].substring(0, 4);
                 else if(arr[0].length()>2)
                     deptName += arr[0].substring(0, 3);
                 else 
                     deptName += arr[0].substring(0, 2);
                    
            } else {
                deptName += arr[arr.length - 1].charAt(0);
            }
            noticeResponse.setDepartmentName(deptName);
            noticeResponse.setMemoStatus("Pending");
            noticeResponse.setTypeId(memo.getTypeId());
            noticeResponse.setMemoCount(memo.getMemoCount());
            noticeResponse.setMemoLotNo(memo.getMemoLotNo());
            noticeResponse.setIsSubmitted(memo.getIsSubmitted());
            noticeResponse.setId(memo.getId());
            noticeResponse.setMemoReason(memo.getMemoReason());
            noticeResponse.setIsEndorse(memo.getIsEndorse());
            noticeResponse.setRemark(memo.getRemark());
            String pattern = "yyyy-MM-dd";
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
            String startDate = simpleDateFormat.format(memo.getLotCreationDate());
            noticeResponse.setLotCreationDate(startDate);
            noticeResponses.add(noticeResponse);
        }

//        Collections.sort(noticeResponses, new Comparator<NoticeResponse>() {
//            @Override
//            public int compare(final NoticeResponse object1, final NoticeResponse object2) {
//                return object1.getDepartmentName().compareTo(object2.getDepartmentName());
//            }
//        });
        return noticeResponses;
        // throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
