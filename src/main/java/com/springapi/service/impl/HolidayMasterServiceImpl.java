/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.springapi.service.impl;

import com.springapi.enums.FASMessageType;
import com.springapi.exception.FASException;
import com.springapi.model.HolidayMaster;
import com.springapi.repository.HolidayMasterRepository;
import com.springapi.service.HolidayMasterService;
import static com.springapi.service.impl.FacultyShiftMpgServiceImpl.logger;
import java.util.List;
import java.util.Optional;
import javax.persistence.EntityManager;
import org.jboss.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author shahb
 */
@Service
@Transactional
public class HolidayMasterServiceImpl implements HolidayMasterService {
     static Logger logger=Logger.getLogger(HolidayMasterServiceImpl.class);
    
    @Autowired
    private HolidayMasterRepository holidayMasterRepository;
    
    @Autowired
    private EntityManager entityManager;
    
    @Override
    public HolidayMaster create(HolidayMaster holidayMaster) {
        logger.debug("create");
        return holidayMasterRepository.save(holidayMaster);
    }

    @Override
    public List<HolidayMaster> list() {
        logger.debug("list");
        return holidayMasterRepository.findAll();
    }

    @Override
    public HolidayMaster read(Long hId) {
         logger.debug("read");
        Optional<HolidayMaster> optional = holidayMasterRepository.findById(hId);
        if (!optional.isPresent()) {
            throw new FASException(FASMessageType.ENTITY_NOT_FOUND);
        }
        return optional.get();
       }

    @Override
    public HolidayMaster update(HolidayMaster holidayMaster) {
       logger.debug("update");
        HolidayMaster holidayMaster1 = read(holidayMaster.getId());

        holidayMaster1.setName(holidayMaster.getName());
        holidayMaster1.setHolidayDate(holidayMaster.getHolidayDate());
        holidayMaster1.setYear(holidayMaster.getYear());
        return holidayMasterRepository.save(holidayMaster1);   
    }

    @Override
    public void delete(Long hId) {
        logger.debug("delete");
        HolidayMaster holidayMaster = read(hId);
        holidayMasterRepository.delete(holidayMaster);
    }
    

}
