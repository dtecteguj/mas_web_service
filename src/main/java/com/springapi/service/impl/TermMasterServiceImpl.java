/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.springapi.service.impl;

import com.springapi.enums.FASMessageType;
import com.springapi.exception.FASException;
import com.springapi.model.InstituteMaster;
import com.springapi.model.TermMaster;
import com.springapi.repository.AndroidVersionRepository;
import com.springapi.repository.TermMasterRepository;
import com.springapi.service.TermMasterService;
import static com.springapi.service.impl.AndroidVersionServiceImpl.logger;
import static com.springapi.service.impl.InstituteServiceImpl.logger;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import javax.persistence.EntityManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author shahb
 */

@Service
@Transactional
public class TermMasterServiceImpl implements TermMasterService {

    static Logger logger = Logger.getLogger(TermMasterServiceImpl.class);

    @Autowired
    private TermMasterRepository termMasterRepository;

    @Autowired
    private EntityManager entityManager;

    @Override
    public TermMaster create(TermMaster termMaster) {
        logger.debug("create");
        return termMasterRepository.save(termMaster);
    }

    @Override
    public List<TermMaster> list() {
        logger.debug("list");
        return termMasterRepository.findAll();
    }

    @Override
    public TermMaster read(Long tId) {
        logger.debug("read");
        Optional<TermMaster> optional = termMasterRepository.findById(tId);
        if (!optional.isPresent()) {
            throw new FASException(FASMessageType.ENTITY_NOT_FOUND);
        }
        return optional.get();
    }

    @Override
    public TermMaster update(TermMaster termMaster) {
       logger.debug("update");
        TermMaster term = read(termMaster.getId());

        term.setTermStartDate(termMaster.getTermStartDate());
        term.setTermEndDate(termMaster.getTermStartDate());
        term.setTermYear(termMaster.getTermYear());
        return termMasterRepository.save(term);   
    }

    @Override
    public void delete(Long tId) {
        logger.debug("delete");
        TermMaster termMaster = read(tId);
        termMasterRepository.delete(termMaster);
    }

}
