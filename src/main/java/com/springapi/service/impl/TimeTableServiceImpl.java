package com.springapi.service.impl;

import com.springapi.dto.request.FacultyAttendanceListRequest;
import com.springapi.dto.request.TimeTableRequest;
import com.springapi.model.DayMaster;
import com.springapi.model.DepartmentMaster;
import com.springapi.model.Faculty;
import com.springapi.model.FacultyAttendance;
import com.springapi.model.InstituteMaster;
import com.springapi.model.LoadTypeMaster;
import com.springapi.model.SlotMaster;
import com.springapi.model.Subject;
import com.springapi.model.TimeTable;
import com.springapi.repository.TimeTableRepository;
import com.springapi.service.TimeTableService;
import static com.springapi.service.impl.FacultyAttendanceServiceImpl.logger;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Order;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import org.apache.log4j.Logger;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author Jyoti Patel
 */
@Service
@Transactional
public class TimeTableServiceImpl implements TimeTableService{
    
    static Logger logger = Logger.getLogger(FacultyAttendanceServiceImpl.class);
    
    @Autowired
    private TimeTableRepository timeTableRepository;
    
    @Autowired
    private EntityManager entityManager;

    @Override
    public List<TimeTable> timeTableList(TimeTableRequest timeTableRequest) {
        logger.debug("timeTableList");

        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery criteriaQuery = criteriaBuilder.createQuery(TimeTable.class);

        prepareListQuery(criteriaQuery, criteriaBuilder, false, timeTableRequest);

        TypedQuery typedQuery = entityManager.createQuery(criteriaQuery);

        List results = typedQuery.getResultList();
        return results;
    }
    
    private void prepareListQuery(CriteriaQuery criteriaQuery, CriteriaBuilder criteriaBuilder, boolean countQuery,
            TimeTableRequest listRequest) {
        logger.debug("prepareListQuery");
        Root<TimeTable> timeTableRoot = criteriaQuery.from(TimeTable.class);
        Root<SlotMaster> slotRoot = criteriaQuery.from(SlotMaster.class);
        Root<DayMaster> dayRoot = criteriaQuery.from(DayMaster.class);
        Root<LoadTypeMaster> loadTypeRoot = criteriaQuery.from(LoadTypeMaster.class);
        Root<Faculty> facultyRoot = criteriaQuery.from(Faculty.class);
        Root<Subject> subjectRoot = criteriaQuery.from(Subject.class);
        
        if (countQuery) {
            //criteriaQuery.select(criteriaBuilder.count(instituteWifiRoot));
        } else {
            criteriaQuery.multiselect(
                    timeTableRoot.get("facultyId"), timeTableRoot.get("semId"), timeTableRoot.get("subjectCode"),
                    timeTableRoot.get("dayId"), timeTableRoot.get("slotId"), timeTableRoot.get("loadtypeId"),
                    dayRoot.get("day"), slotRoot.get("startTime"), slotRoot.get("endTime"),
                    loadTypeRoot.get("loadType"), facultyRoot.get("name"), subjectRoot.get("sName"),
                    subjectRoot.get("id"), timeTableRoot.get("batch")
            );

            List<Order> orders = new ArrayList<>();

            if (!orders.isEmpty()) {
                criteriaQuery.orderBy(orders);
            }
        }
        ArrayList<Predicate> predicateList = new ArrayList<>();
        predicateList.add(criteriaBuilder.equal(timeTableRoot.get("dayId"), dayRoot.get("id")));
        predicateList.add(criteriaBuilder.equal(timeTableRoot.get("slotId"), slotRoot.get("id")));
        predicateList.add(criteriaBuilder.equal(timeTableRoot.get("loadtypeId"), loadTypeRoot.get("id")));
        predicateList.add(criteriaBuilder.equal(timeTableRoot.get("facultyId"), facultyRoot.get("id")));
        predicateList.add(criteriaBuilder.equal(timeTableRoot.get("subjectCode"), subjectRoot.get("sCode")));
        
        if (listRequest.getFacultyId()!= null) {
            predicateList.add(criteriaBuilder.equal(timeTableRoot.get("facultyId"), listRequest.getFacultyId()));
        }

        if (listRequest.getDayId()!= null) {
            predicateList.add(criteriaBuilder.equal(timeTableRoot.get("dayId"), listRequest.getDayId()));
        }
        
        if (listRequest.getSemId()!= null) {
            predicateList.add(criteriaBuilder.equal(timeTableRoot.get("semId"), listRequest.getSemId()));
        }
        
        if (listRequest.getBatch()!= null) {
            predicateList.add(criteriaBuilder.or(criteriaBuilder.equal(timeTableRoot.get("batch"), listRequest.getBatch()), criteriaBuilder.isNull(timeTableRoot.get("batch"))));
           }

        Predicate[] predicates = new Predicate[predicateList.size()];
        predicateList.toArray(predicates);
        criteriaQuery.where(predicates);
    }

    
}
