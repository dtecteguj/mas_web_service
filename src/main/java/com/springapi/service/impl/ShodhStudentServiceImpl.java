/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.springapi.service.impl;

import com.springapi.dto.request.CMDashSHODHRequest;
import com.springapi.dto.response.SHODHStudentAttendanceRespNew;
import com.springapi.dto.response.SHODHStudentAttendanceResponse;
import com.springapi.model.ShodhStudent;
import com.springapi.repository.ShodhStudentRepository;
import com.springapi.service.ShodhStudentService;
import com.springapi.util.CustDateFormat;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import javax.persistence.EntityManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author shahb
 */
@Service
@Transactional
public class ShodhStudentServiceImpl implements ShodhStudentService {

    @Autowired
    ShodhStudentRepository shodhStudentRepository;

    static Logger logger = Logger.getLogger(DashboardServiceImpl.class);

    @Autowired
    private EntityManager entityManager;

    @Autowired
    private Environment env;

    @Override
    public ShodhStudent create(ShodhStudent shodhStudent) {
        return shodhStudentRepository.save(shodhStudent);
    }

    @Override
    public List<ShodhStudent> list() {
        return shodhStudentRepository.findAll();
    }

    @Override
    public ShodhStudent read(Long aId) {

        Optional<ShodhStudent> shodhStudent = shodhStudentRepository.findByStdUniqueId(aId);
        if (shodhStudent.isPresent()) {
            return shodhStudent.get();
        } else {
            return null;
        }
    }

    @Override
    public ShodhStudent update(ShodhStudent shodhStudent) {
        return shodhStudentRepository.save(shodhStudent);
    }

    @Override
    public void delete(Long aId) {
        shodhStudentRepository.delete(aId);
    }

    @Override
    public SHODHStudentAttendanceRespNew countAttendanceForCMDash(CMDashSHODHRequest cMDashSHODHRequest) {
        SHODHStudentAttendanceRespNew sHODHStudentAttendanceRespNew = new SHODHStudentAttendanceRespNew();
        logger.debug("CMDashAttendanceCount");
        if (cMDashSHODHRequest.getKey().equalsIgnoreCase("177b8a0026e666670110412e802e5675")) {
            // Code for for-loop started 

            List<SHODHStudentAttendanceResponse> cMDashResponses = new ArrayList<>();
            try {
                Class.forName("com.mysql.jdbc.Driver");
                String path = env.getProperty("spring.datasource.url");
                String userName = env.getProperty("spring.datasource.username");
                String password = env.getProperty("spring.datasource.password");
                System.out.println("Path : " + path);
                Connection c = DriverManager.getConnection(path, userName, password);
                // gets a new connection
                Statement s = c.createStatement();
                //     LocalDate startD = LocalDate.now();
                SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
                Date FromGendate;
                try {
                    FromGendate = format.parse(cMDashSHODHRequest.getFromDate());
                } catch (ParseException ex) {
                    sHODHStudentAttendanceRespNew.setErrorNo(2);
                    sHODHStudentAttendanceRespNew.setIsError(Boolean.TRUE);
                    sHODHStudentAttendanceRespNew.setMessage("Invalid From-Date format. Correct format is dd/MM/yyyy ");
                    return sHODHStudentAttendanceRespNew;
                }
                Date ToGendate;
                try {
                    ToGendate = format.parse(cMDashSHODHRequest.getToDate());
                } catch (ParseException ex) {
                    sHODHStudentAttendanceRespNew.setErrorNo(3);
                    sHODHStudentAttendanceRespNew.setIsError(Boolean.TRUE);
                    sHODHStudentAttendanceRespNew.setMessage("Invalid To-Date format. Correct format is dd/MM/yyyy ");
                    return sHODHStudentAttendanceRespNew;
                }
                if (FromGendate.compareTo(ToGendate) > 0) {
                    sHODHStudentAttendanceRespNew.setErrorNo(4);
                    sHODHStudentAttendanceRespNew.setIsError(Boolean.TRUE);
                    sHODHStudentAttendanceRespNew.setMessage("Invalid From-Date and To-Date Range");
                    return sHODHStudentAttendanceRespNew;
                }
                String pattern = "yyyy-MM-dd";
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);

                String fromDate = simpleDateFormat.format(FromGendate);
                String toDate = simpleDateFormat.format(ToGendate);
                System.out.println("Date :: " + fromDate + "-" + toDate);
                String sql = "SELECT * FROM dheemati_attendance.cmdash_fac_v where `Date` between '" + fromDate + "' and '" + toDate + "' order by instituteId limit 10;";

                if (cMDashSHODHRequest.getStuUniqueId() != 0) {
                    sql = "SELECT * FROM dheemati_attendance.cmdash_fac_v where date between '" + fromDate + "' and '" + toDate + "' and f_id = '" + cMDashSHODHRequest.getStuUniqueId() + "';";
                }
                ResultSet rs;
                try {
                    rs = s.executeQuery(sql);
                } catch (SQLException e) {
                    sHODHStudentAttendanceRespNew.setErrorNo(5);
                    sHODHStudentAttendanceRespNew.setIsError(Boolean.TRUE);
                    sHODHStudentAttendanceRespNew.setMessage("Server issue");
                    return sHODHStudentAttendanceRespNew;

                }
                while (rs.next()) {
//                    String data = rs.getString("absent_count");
//                    String data1 = rs.getString("present_count");
//                    System.out.println("data of absent ::" + data);

                    SHODHStudentAttendanceResponse cMDashResponse = new SHODHStudentAttendanceResponse();
                    String sqlDate = rs.getString("Date").substring(0, 10);
                    String attDate = CustDateFormat.dateToINDString(CustDateFormat.SQLTypeStringToDate(sqlDate));
                    // System.out.println("data ::" + attDate);
                    cMDashResponse.setAttDate(attDate);
                    cMDashResponse.setStuUniqueId(rs.getInt("f_id"));
                    cMDashResponse.setInTime(rs.getString("facInTime"));
                    cMDashResponse.setOutTime(rs.getString("facOutTime"));
                    cMDashResponse.setActInTime(rs.getString("actInTime"));
                    cMDashResponse.setActOutTime(rs.getString("actOutTime"));
                    cMDashResponse.setIsPrecence(rs.getBoolean("present"));
                    cMDashResponse.setOnDuty(rs.getBoolean("onDuty"));
                    // fillNullObjects(cMDashResponse);
                    if (cMDashResponse.getActInTime().equalsIgnoreCase("#")) {
                        cMDashResponse.setActInTime(null);
                        cMDashResponse.setActOutTime(null);
                    }

                    cMDashResponses.add(cMDashResponse);
                }

            } catch (ClassNotFoundException | SQLException | ParseException e) {
                sHODHStudentAttendanceRespNew.setErrorNo(6);
                sHODHStudentAttendanceRespNew.setIsError(Boolean.TRUE);
                sHODHStudentAttendanceRespNew.setMessage("Server Connection issue");
                return sHODHStudentAttendanceRespNew;
            }
            sHODHStudentAttendanceRespNew.setAttendance(cMDashResponses);
            return sHODHStudentAttendanceRespNew;

        } else {

            sHODHStudentAttendanceRespNew.setErrorNo(1);
            sHODHStudentAttendanceRespNew.setIsError(Boolean.TRUE);
            sHODHStudentAttendanceRespNew.setMessage("Invalid Token-Key");
            return sHODHStudentAttendanceRespNew;

        }
    }

}
