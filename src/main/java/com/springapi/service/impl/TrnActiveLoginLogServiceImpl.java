package com.springapi.service.impl;
import java.util.List;
import java.util.Optional;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.springapi.model.TrnActiveLoginLog;
import com.springapi.repository.TrnActiveLoginLogRepository;
import com.springapi.service.TrnActiveLoginLogService;
import java.util.Date;

@Service
@Transactional
public class TrnActiveLoginLogServiceImpl implements TrnActiveLoginLogService {
	
	static Logger logger = Logger.getLogger(TrnActiveLoginLogServiceImpl.class);
	
	@Autowired
	private TrnActiveLoginLogRepository trnActiveLoginLogRepository;

	@Override
	public TrnActiveLoginLog create(TrnActiveLoginLog tall) {
		logger.debug("create");
		return trnActiveLoginLogRepository.save(tall);
	}

	@Override
	public List<TrnActiveLoginLog> list() {
		logger.debug("list");
		return trnActiveLoginLogRepository.findAll();
	}

	@Override
	public TrnActiveLoginLog read(Long id) {
		logger.debug("read");
		Optional<TrnActiveLoginLog> optional = trnActiveLoginLogRepository.findById(id);
		if (!optional.isPresent()) {
			optional = null;
		}
		return optional.get();
	}

	@Override
	public TrnActiveLoginLog update(TrnActiveLoginLog tall) {
		logger.debug("update");
		TrnActiveLoginLog tall1 = read(tall.getId());
		return trnActiveLoginLogRepository.save(tall1);
	}

	@Override
	public void delete(Long id) {
		logger.debug("delete");
		TrnActiveLoginLog tall = read(id);
		trnActiveLoginLogRepository.delete(tall);
	}

    @Override
    public TrnActiveLoginLog readForVerification(Long id, Long fId, String deviceId) {
        logger.debug("readForVerification");
		Optional<TrnActiveLoginLog> optional = trnActiveLoginLogRepository.findByIdAndFIdAndDeviceId(id,fId,deviceId);
		if (!optional.isPresent()) {
			//throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
                        return null;
		}
		return optional.get();
    }
}
