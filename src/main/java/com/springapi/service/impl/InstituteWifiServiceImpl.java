package com.springapi.service.impl;

import com.springapi.enums.FASMessageType;
import com.springapi.exception.FASException;
import com.springapi.model.InstituteMaster;
import com.springapi.model.InstituteWifiMaster;
import com.springapi.model.UniversityMaster;
import com.springapi.repository.InstituteWifiRepository;
import com.springapi.service.InstituteWifiService;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Order;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.data.domain.PageRequest;
//import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
/**
 * @author Jyoti Patel
 */
@Service
@Transactional
public class InstituteWifiServiceImpl implements InstituteWifiService {

    static Logger logger = Logger.getLogger(InstituteWifiServiceImpl.class);

    @Autowired
    private InstituteWifiRepository instituteWifiRepository;
    
    @Autowired
    private EntityManager entityManager;

    @Override
    public InstituteWifiMaster create(InstituteWifiMaster instituteWifi) {
        logger.debug("create");
        return instituteWifiRepository.save(instituteWifi);
    }

    @Override
    public List<InstituteWifiMaster> list() {
        logger.debug("list");
        
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery criteriaQuery = criteriaBuilder.createQuery(InstituteWifiMaster.class);

        // Prepare list query
        //prepareListQuery(criteriaQuery, criteriaBuilder, listRequest, false);
        prepareListQuery(criteriaQuery, criteriaBuilder, false);

        // Prepare Pagination
       // Pageable pageable = PageRequest.of(listRequest.getPage(), listRequest.getSize());
        TypedQuery typedQuery = entityManager.createQuery(criteriaQuery);
//        typedQuery.setFirstResult((int) pageable.getOffset());
//        typedQuery.setMaxResults(pageable.getPageSize());

        List results = typedQuery.getResultList();
        System.out.println("result::"+results);
        //return new PageImpl(results, pageable, getListCount(listRequest));
        return results;
       // return instituteWifiRepository.findAll();
    }
    private void prepareListQuery(CriteriaQuery criteriaQuery, CriteriaBuilder criteriaBuilder, boolean countQuery) {

        Root<InstituteWifiMaster> instituteWifiRoot = criteriaQuery.from(InstituteWifiMaster.class);
        Root<UniversityMaster> universityRoot = criteriaQuery.from(UniversityMaster.class);
        Root<InstituteMaster> instituteRoot = criteriaQuery.from(InstituteMaster.class);

        if (countQuery) {
            criteriaQuery.select(criteriaBuilder.count(instituteWifiRoot));
        } else {
            criteriaQuery.multiselect(
                    instituteWifiRoot.get("id"), instituteWifiRoot.get("bssid"), instituteWifiRoot.get("mac"),
                    instituteWifiRoot.get("instituteId"), instituteRoot.get("name"), instituteRoot.get("link"),
                    instituteRoot.get("code")
                    ,instituteRoot.get("universityId"), universityRoot.get("name"), universityRoot.get("code"), 
               universityRoot.get("link") 
                    );

            List<Order> orders = new ArrayList<>();
//            if (!CollectionUtils.isEmpty(listRequest.getSort())) {
//                for (Map<String, ListSortOrder> properties : listRequest.getSort()) {
//                    properties.forEach((property, sortOrder) -> {
//                        orders.add(ListSortOrder.DESC.toString().equalsIgnoreCase(sortOrder.toString())
//                                ? criteriaBuilder.desc(cityRoot.get(property))
//                                : criteriaBuilder.asc(cityRoot.get(property)));
//
//                    });
//                }
//            }

            if (!orders.isEmpty())
                criteriaQuery.orderBy(orders);
        }

        ArrayList<Predicate> predicateList = new ArrayList<>();
        predicateList.add(criteriaBuilder.equal(instituteWifiRoot.get("instituteId"), instituteRoot.get("id")));
        predicateList.add(criteriaBuilder.equal(instituteRoot.get("universityId"),universityRoot.get("id")));

        Predicate[] predicates = new Predicate[predicateList.size()];
        predicateList.toArray(predicates);
        criteriaQuery.where(predicates);
    }

//    private long getListCount(CityListRequest listRequest) {
//        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
//        CriteriaQuery<Long> criteriaQuery = criteriaBuilder.createQuery(Long.class);
//
//        // Prepare list query
//        prepareListQuery(criteriaQuery, criteriaBuilder, listRequest, true);
//
//        return entityManager.createQuery(criteriaQuery).getSingleResult();
//    }

    @Override
    public InstituteWifiMaster read(Long uId) {
        logger.debug("read");
        Optional<InstituteWifiMaster> optional = instituteWifiRepository.findById(uId);
        if (!optional.isPresent()) {
            throw new FASException(FASMessageType.ENTITY_NOT_FOUND);
        }
        return optional.get();
    }

    @Override
    public InstituteWifiMaster update(InstituteWifiMaster instituteWifiDetails) {
        logger.debug("update");
        InstituteWifiMaster instituteWifi = read(instituteWifiDetails.getId());

        instituteWifi.setBssid(instituteWifiDetails.getBssid());
        instituteWifi.setMac(instituteWifiDetails.getMac());
        instituteWifi.setInstituteId(instituteWifiDetails.getInstituteId());
        instituteWifi.setUpdatedAt(new Date());

        return instituteWifiRepository.save(instituteWifi);
    }

    @Override
    public void delete(Long uId) {
        logger.debug("delete");
        InstituteWifiMaster instituteWifi = read(uId);
        instituteWifiRepository.delete(instituteWifi);
    }
}
