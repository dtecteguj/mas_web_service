package com.springapi.service.impl;

import com.springapi.dto.StaftDepartment;
import com.springapi.model.FacRoleMpg;
import com.springapi.repository.FacRoleMpgRepository;
import com.springapi.service.DepartmentService;
import java.util.List;
import java.util.Optional;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.springapi.service.FacRoleMpgService;
import java.util.ArrayList;

@Service
@Transactional
public class FacRoleMpgServiceImpl implements FacRoleMpgService {

    static Logger logger = Logger.getLogger(FacRoleMpgServiceImpl.class);
@Autowired
    private DepartmentService departmentService;

    @Autowired
    private FacRoleMpgRepository FacRoleMpgRepository1;

    @Override
    public FacRoleMpg create(FacRoleMpg fm) {
        logger.debug("create");
        return FacRoleMpgRepository1.save(fm);
    }

    @Override
    public List<FacRoleMpg> list() {
        logger.debug("list");
        return FacRoleMpgRepository1.findAll();
    }

    @Override
    public FacRoleMpg read(Long id) {
        logger.debug("read");
        Optional<FacRoleMpg> optional = FacRoleMpgRepository1.findById(id);
        if (!optional.isPresent()) {
            optional = null;
        }
        return optional.get();
    }

    @Override
    public FacRoleMpg update(FacRoleMpg fm) {
        logger.debug("update");
        FacRoleMpg program = read(fm.getId());
        return FacRoleMpgRepository1.save(program);
    }

    @Override
    public void delete(Long id) {
        logger.debug("delete");
        FacRoleMpg program = read(id);
        FacRoleMpgRepository1.delete(program);
    }

    @Override
    public List<StaftDepartment> activeRolelistByFId(Long fId) {

        List<StaftDepartment> staftDepartments = new ArrayList<>();
        for (FacRoleMpg facRoleMpg : FacRoleMpgRepository1.findByIdFIdAndIsActiveTrue(fId)) {
                StaftDepartment sd = new StaftDepartment();
                sd.setDepartmentId(facRoleMpg.getDepartmentId());
                sd.setRole(facRoleMpg.getRole());
                sd.setIsMaster(facRoleMpg.getIsMaster());
                sd.setDepartmentName(departmentService.namebyId(sd.getDepartmentId()));
                staftDepartments.add(sd);
                
        }
        return staftDepartments;
    }
}
