package com.springapi.service.impl;

import com.springapi.dto.request.DailyEndrosRequest;
import com.springapi.dto.request.DailyEndrosStatusRequest;
import com.springapi.model.DailyEndrosment;
import com.springapi.repository.DailyEndrosmentRepository;
import com.springapi.service.DailyEndrosmentService;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import javax.persistence.EntityManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author Jyoti Patel
 */
@Service
@Transactional
public class DailyEndrosmentServiceImpl implements DailyEndrosmentService {

    static Logger logger = Logger.getLogger(FacultyAttendanceServiceImpl.class);

    @Autowired
    private EntityManager entityManager;

    @Autowired
    private DailyEndrosmentRepository dailyEndrosmentRepository;

    @Override
    public DailyEndrosment endrosByHod(DailyEndrosRequest dailyEndrosRequest) {
        String pattern = "yyyy-MM-dd";
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
        String date = simpleDateFormat.format(new Date());

        DailyEndrosment dailyEndrosment
                = dailyEndrosmentRepository.findByInstituteIdAndDepartmentIdAndEndrosDate(dailyEndrosRequest.getInstituteId(), dailyEndrosRequest.getDepartmentId(), date);

        if (dailyEndrosment != null) {
            dailyEndrosment.setEndrosByHod(dailyEndrosRequest.getEndrosStatus());
            return dailyEndrosment;
        }
        dailyEndrosment = new DailyEndrosment();
        dailyEndrosment.setCreatedAt(new Date());
        dailyEndrosment.setDepartmentId(dailyEndrosRequest.getDepartmentId());
        dailyEndrosment.setInstituteId(dailyEndrosRequest.getInstituteId());
        dailyEndrosment.setEndrosByHod(dailyEndrosRequest.getEndrosStatus());
        dailyEndrosment.setEndrosDate(date);
        return dailyEndrosmentRepository.save(dailyEndrosment);
    }

    @Override
    public DailyEndrosment endrosByprincipal(DailyEndrosRequest dailyEndrosRequest) {
        String pattern = "yyyy-MM-dd";
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
        String date = simpleDateFormat.format(new Date());

        DailyEndrosment dailyEndrosment = null;
        dailyEndrosment = dailyEndrosmentRepository.findByInstituteIdAndEndrosDate(dailyEndrosRequest.getInstituteId(), date);

        if (dailyEndrosment != null) {
            dailyEndrosment.setEndrosByPrincipal(dailyEndrosRequest.getEndrosStatus());
            return dailyEndrosment;
        }
        dailyEndrosment = new DailyEndrosment();
        dailyEndrosment.setCreatedAt(new Date());
        dailyEndrosment.setDepartmentId(dailyEndrosRequest.getDepartmentId());
        dailyEndrosment.setInstituteId(dailyEndrosRequest.getInstituteId());
        dailyEndrosment.setEndrosByPrincipal(dailyEndrosRequest.getEndrosStatus());
        dailyEndrosment.setEndrosDate(date);
        return dailyEndrosmentRepository.save(dailyEndrosment);
//        if (dailyEndrosmentList != null) {
//            for (DailyEndrosment dailyEndrosment : dailyEndrosmentList) {
//                if (dailyEndrosment.getDepartmentId().equals(dailyEndrosRequest.getDepartmentId())) {
//                    dailyEndrosment.setEndrosByPrincipal(dailyEndrosRequest.getEndrosStatus());
//                    return dailyEndrosmentRepository.save(dailyEndrosment);
//                }
//            }
//        } else {
//            DailyEndrosment dailyEndrosment = new DailyEndrosment();
//            dailyEndrosment.setCreatedAt(new Date());
//            dailyEndrosment.setDepartmentId(dailyEndrosRequest.getDepartmentId());
//            dailyEndrosment.setInstituteId(dailyEndrosRequest.getInstituteId());
//            dailyEndrosment.setEndrosByPrincipal(dailyEndrosRequest.getEndrosStatus());
//            return dailyEndrosmentRepository.save(dailyEndrosment);
//        }
//        return null;
    }

    @Override
    public Boolean endrosStatusForHod(DailyEndrosStatusRequest dailyEndrosStatusRequest) {
        String pattern = "yyyy-MM-dd";
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
        String date = simpleDateFormat.format(new Date());

        DailyEndrosment dailyEndrosment
                = dailyEndrosmentRepository.findByInstituteIdAndDepartmentIdAndEndrosDate(dailyEndrosStatusRequest.getInstituteId(), dailyEndrosStatusRequest.getDepartmentId(), date);

        if (dailyEndrosment != null) {
            return dailyEndrosment.getEndrosByHod();
        }
        return false;
    }

    @Override
    public Boolean endrosStatusForPrincipal(DailyEndrosStatusRequest dailyEndrosStatusRequest) {
        String pattern = "yyyy-MM-dd";
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
        String date = simpleDateFormat.format(new Date());

        DailyEndrosment dailyEndrosment = null;
        dailyEndrosment = dailyEndrosmentRepository.findByInstituteIdAndEndrosDate(dailyEndrosStatusRequest.getInstituteId(), date);

        if (dailyEndrosment != null) {
            return dailyEndrosment.getEndrosByPrincipal();
        }
        return false;
    }
}
