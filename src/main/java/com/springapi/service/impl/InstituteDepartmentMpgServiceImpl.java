/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.springapi.service.impl;

import com.springapi.enums.FASMessageType;
import com.springapi.exception.FASException;
import com.springapi.model.InstituteDepartmentMpg;
import com.springapi.repository.HolidayMasterRepository;
import com.springapi.repository.InstituteDepartmentMpgRepository;
import com.springapi.service.InstituteDepartmentMpgService;
import static com.springapi.service.impl.HolidayMasterServiceImpl.logger;
import java.util.List;
import java.util.Optional;
import javax.persistence.EntityManager;
import org.jboss.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author shahb
 */

@Service
@Transactional
public class InstituteDepartmentMpgServiceImpl implements InstituteDepartmentMpgService {
     static Logger logger=Logger.getLogger(InstituteDepartmentMpgServiceImpl.class);
    
    @Autowired
    private InstituteDepartmentMpgRepository instituteDepartmentMpgRepository;
    
    @Autowired
    private EntityManager entityManager;
    
    @Override
    public InstituteDepartmentMpg create(InstituteDepartmentMpg instituteDepartmentMpg) {
        logger.debug("create");
        return instituteDepartmentMpgRepository.save(instituteDepartmentMpg);
    }

    @Override
    public List<InstituteDepartmentMpg> list() {
        logger.debug("list");
        return instituteDepartmentMpgRepository.findAll();
    }

    @Override
    public InstituteDepartmentMpg read(Long iId) {
         logger.debug("read");
        Optional<InstituteDepartmentMpg> optional = instituteDepartmentMpgRepository.findById(iId);
        if (!optional.isPresent()) {
            throw new FASException(FASMessageType.ENTITY_NOT_FOUND);
        }
        return optional.get();
       }

    @Override
    public InstituteDepartmentMpg update(InstituteDepartmentMpg instituteDepartmentMpg) {
       logger.debug("update");
        InstituteDepartmentMpg instituteDepartmentMpg1 = read(instituteDepartmentMpg.getId());

        instituteDepartmentMpg1.setDepartmentId(instituteDepartmentMpg.getDepartmentId());
       instituteDepartmentMpg1.setInstituteId(instituteDepartmentMpg.getInstituteId());
        return instituteDepartmentMpgRepository.save(instituteDepartmentMpg1);   
    }

    @Override
    public void delete(Long iId) {
        logger.debug("delete");
        InstituteDepartmentMpg instituteDepartmentMpg = read(iId);
        instituteDepartmentMpgRepository.delete(instituteDepartmentMpg);
    }
}
