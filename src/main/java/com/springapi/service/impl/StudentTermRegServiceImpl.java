/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.springapi.service.impl;

import com.springapi.enums.FASMessageType;
import com.springapi.exception.FASException;
import com.springapi.model.StudentTermReg;
import com.springapi.repository.StudentTermRegRepository;
import com.springapi.service.StudentTermRegService;
import java.util.List;
import java.util.Optional;
import javax.persistence.EntityManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author shahb
 */

@Service
@Transactional
public class StudentTermRegServiceImpl implements StudentTermRegService{
    static Logger logger = Logger.getLogger(InstituteTypeServiceImpl.class);

    @Autowired
    private StudentTermRegRepository studentTermRegRepository;

    @Autowired
    private EntityManager entityManager;

    @Override
    public StudentTermReg create(StudentTermReg studentTermReg) {
        logger.debug("create");
        return studentTermRegRepository.save(studentTermReg);
    }

    @Override
    public List<StudentTermReg> list() {
         logger.debug("list");
        return studentTermRegRepository.findAll();
    }

    @Override
    public StudentTermReg read(Long sId) {
         logger.debug("read");
        Optional<StudentTermReg> optional = studentTermRegRepository.findById(sId);
        if (!optional.isPresent()) {
            throw new FASException(FASMessageType.ENTITY_NOT_FOUND);
        }
        return optional.get();
    }

    @Override
    public StudentTermReg update(StudentTermReg studentTermReg) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void delete(Long sId) {
        logger.debug("delete");
        Optional<StudentTermReg> optional = studentTermRegRepository.findById(sId);
        if (!optional.isPresent()) {
            throw new FASException(FASMessageType.ENTITY_NOT_FOUND);
        }
        studentTermRegRepository.delete(optional.get());
    }

  
}
