/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.springapi.service.impl;

import com.springapi.dto.response.AndroidVersionStatus;
import com.springapi.enums.FASMessageType;
import com.springapi.exception.FASException;
import com.springapi.model.AndroidVersion;
import com.springapi.model.EditAttndcLog;
import com.springapi.repository.EditAttndcLogRepository;
import com.springapi.service.EditAttndcLogService;
import static com.springapi.service.impl.AndroidVersionServiceImpl.logger;
import java.util.List;
import java.util.Optional;
import javax.persistence.EntityManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author shahb
 */
@Service
@Transactional
public class EditAttndcLogServiceImpl implements EditAttndcLogService {

    @Autowired
    static Logger logger = Logger.getLogger(InstituteTypeServiceImpl.class);

    @Autowired
    private EditAttndcLogRepository editAttndcLogRepository;

    @Autowired
    private EntityManager entityManager;

    @Override
    public EditAttndcLog create(EditAttndcLog editAttndcLog) {
        logger.debug("create");
        return editAttndcLogRepository.save(editAttndcLog);
    }

    @Override
    public List<EditAttndcLog> list() {
        logger.debug("list");
        return editAttndcLogRepository.findAll();
    }

    @Override
    public EditAttndcLog read(Long aId) {
        logger.debug("read");
        Optional<EditAttndcLog> optional = editAttndcLogRepository.findById(aId);
        AndroidVersionStatus androidVersionStatus = new AndroidVersionStatus();
        if (!optional.isPresent()) {
            throw new FASException(FASMessageType.ENTITY_NOT_FOUND);
        }

        return optional.get();
    }

    @Override
    public EditAttndcLog update(EditAttndcLog editAttndcLog) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void delete(Long aId) {
        logger.debug("delete");
        Optional<EditAttndcLog> optional = editAttndcLogRepository.findById(aId);
        if (!optional.isPresent()) {
            throw new FASException(FASMessageType.ENTITY_NOT_FOUND);
        }

        editAttndcLogRepository.delete(optional.get());
    }

}
