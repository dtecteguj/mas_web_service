package com.springapi.service.impl;

import com.springapi.enums.FASMessageType;
import com.springapi.exception.FASException;
import com.springapi.model.ProgramMaster;
import com.springapi.repository.ProgramRepository;
import com.springapi.service.ProgramService;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author Jyoti Patel
 */
@Service
@Transactional
public class ProgramServiceImpl implements ProgramService {

    static Logger logger = Logger.getLogger(ProgramServiceImpl.class);

    @Autowired
    private ProgramRepository programRepository;

    @Override
    public ProgramMaster create(ProgramMaster program) {
        logger.debug("create");
        return programRepository.save(program);
    }

    @Override
    public List<ProgramMaster> list() {
        logger.debug("list");
        return programRepository.findAll();
    }

    @Override
    public ProgramMaster read(Long uId) {
        logger.debug("read");
        Optional<ProgramMaster> optional = programRepository.findById(uId);
        if (!optional.isPresent()) {
            throw new FASException(FASMessageType.ENTITY_NOT_FOUND);
        }
        return optional.get();
    }

    @Override
    public ProgramMaster update(ProgramMaster programDetails) {
        logger.debug("update");
        ProgramMaster program = read(programDetails.getId());

        program.setCode(programDetails.getCode());
        program.setName(programDetails.getName());
        program.setDescription(programDetails.getDescription());
        program.setUpdatedAt(new Date());

        return programRepository.save(program);
    }

    @Override
    public void delete(Long uId) {
        logger.debug("delete");
        ProgramMaster program = read(uId);
        programRepository.delete(program);
    }
}
