/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.springapi.service.impl;

import com.springapi.enums.FASMessageType;
import com.springapi.exception.FASException;
import com.springapi.model.Faculty;
import com.springapi.model.TrnLongleaveDtls;
import com.springapi.repository.TrnLongleaveDtlsRepository;
import com.springapi.service.TrnLongleaveDtlsService;
import java.util.List;
import java.util.Optional;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author shahb
 */
@Service
@Transactional
public class TrnLongleaveDtlsServiceImpl implements TrnLongleaveDtlsService {

    static Logger logger = Logger.getLogger(TrnLongleaveDtlsServiceImpl.class);

    @Autowired
    private TrnLongleaveDtlsRepository trnLongleaveDtlsServiceRepository;

    @Autowired
    private EntityManager entityManager;

    @Override
    public TrnLongleaveDtls create(TrnLongleaveDtls trnLongleaveDtls) {
        logger.debug("create");
        return trnLongleaveDtlsServiceRepository.save(trnLongleaveDtls);
    }

    @Override
    public List<TrnLongleaveDtls> list() {
        logger.debug("list");
        return trnLongleaveDtlsServiceRepository.findAll();
    }

    @Override
    public TrnLongleaveDtls read(Long iId) {
        logger.debug("read");
        Optional<TrnLongleaveDtls> optional = trnLongleaveDtlsServiceRepository.findById(iId);
        if (!optional.isPresent()) {
            throw new FASException(FASMessageType.ENTITY_NOT_FOUND);
        }
        return optional.get();
    }

    @Override
    public TrnLongleaveDtls update(TrnLongleaveDtls trnLongleaveDtls) {
         logger.debug("update");
        TrnLongleaveDtls trnLongleaveDtls1 = read(trnLongleaveDtls.getId());

        trnLongleaveDtls1.setFromDate(trnLongleaveDtls.getFromDate());
        trnLongleaveDtls1.setToDate(trnLongleaveDtls.getToDate());
        trnLongleaveDtls1.setLeaveTypeId(trnLongleaveDtls.getLeaveTypeId());
        trnLongleaveDtls1.setfId(trnLongleaveDtls.getfId());
        
        return trnLongleaveDtlsServiceRepository.save(trnLongleaveDtls1);   
    }

    @Override
    public void delete(Long iId) {
        logger.debug("delete");
        TrnLongleaveDtls trnLongleaveDtls = read(iId);
        trnLongleaveDtlsServiceRepository.delete(trnLongleaveDtls);
    }

    @Override
    public List<TrnLongleaveDtls> readbyfid(Long fId) {
        return trnLongleaveDtlsServiceRepository.findByFId(fId);
    }

    

}
