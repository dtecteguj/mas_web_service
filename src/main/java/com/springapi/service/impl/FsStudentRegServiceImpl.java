/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.springapi.service.impl;

import com.springapi.dto.response.AndroidVersionStatus;
import com.springapi.enums.FASMessageType;
import com.springapi.exception.FASException;
import com.springapi.model.FsStudentReg;
import com.springapi.repository.FsStudentRegRepository;
import com.springapi.service.FsStudentRegService;
import java.util.List;
import java.util.Optional;
import javax.persistence.EntityManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author shahb
 */

@Service
@Transactional
public class FsStudentRegServiceImpl implements FsStudentRegService{

     @Autowired
    static Logger logger = Logger.getLogger(InstituteTypeServiceImpl.class);

    @Autowired
    private FsStudentRegRepository fsStudentRegRepository;

    @Autowired
    private EntityManager entityManager;

    @Override
    public FsStudentReg create(FsStudentReg fsStudentReg) {
        logger.debug("create");
        return fsStudentRegRepository.save(fsStudentReg);
    }



    @Override
    public List<FsStudentReg> list() {
        logger.debug("list");
        return fsStudentRegRepository.findAll();
    }


    @Override
    public FsStudentReg read(Long aId) {
         logger.debug("read");
        Optional<FsStudentReg> optional = fsStudentRegRepository.findById(aId);
        AndroidVersionStatus androidVersionStatus = new AndroidVersionStatus();
        if (!optional.isPresent()) {
            throw new FASException(FASMessageType.ENTITY_NOT_FOUND);
        }

        return optional.get();
    }
    @Override
    public FsStudentReg update(FsStudentReg fsStudentReg) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void delete(Long aId) {
        logger.debug("delete");
        Optional<FsStudentReg> optional = fsStudentRegRepository.findById(aId);
        if (!optional.isPresent()) {
            throw new FASException(FASMessageType.ENTITY_NOT_FOUND);
        }

        fsStudentRegRepository.delete(optional.get());
    }
    
}
