/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.springapi.service.impl;

import com.springapi.dto.response.LeaveDetails;
import com.springapi.enums.FASMessageType;
import com.springapi.exception.FASException;
import com.springapi.model.FacultyAttendance;
import com.springapi.model.HolidayMaster;
import com.springapi.model.LeaveTypeMaster;
import com.springapi.model.TrnDtlIssueMemo;
import com.springapi.model.TrnDtlLeaveStatus;
import com.springapi.model.TrnLongleaveDtls;
import com.springapi.repository.LeaveTypeMasterRepository;
import com.springapi.repository.TrnDtlLeaveStatusRepository;
import com.springapi.service.FacultyAttendanceService;
import com.springapi.service.TrnDtlLeaveStatusService;
import com.springapi.service.TrnLongleaveDtlsService;
import static com.springapi.service.impl.TrnDtlIssueMemoServiceImpl.logger;
import com.springapi.util.CustDateFormat;
import java.util.List;
import java.util.Optional;
import javax.persistence.EntityManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author shahb
 */
@Service
@Transactional
public class TrnDtlLeaveStatusServiceImpl implements TrnDtlLeaveStatusService {

    static Logger logger = Logger.getLogger(TrnDtlLeaveStatusServiceImpl.class);

    @Autowired
    private TrnDtlLeaveStatusRepository trnDtlLeaveStatusRepository;

    @Autowired
    private LeaveTypeMasterRepository leavaLeaveTypeMasterRepository;

    @Autowired
    private EntityManager entityManager;

    @Autowired
    private FacultyAttendanceService facultyAttendanceService;
    
    @Autowired
    private HolidayMasterServiceImpl holidayMasterServiceImpl;

    @Autowired
    private TrnLongleaveDtlsService trnLongleaveDtlsService;

    @Override
    public TrnDtlLeaveStatus create(TrnDtlLeaveStatus trnDtlLeaveStatus) {
        logger.debug("create");
        return trnDtlLeaveStatusRepository.save(trnDtlLeaveStatus);
    }

    @Override
    public List<TrnDtlLeaveStatus> list() {
        logger.debug("list");
        return trnDtlLeaveStatusRepository.findAll();
    }

    @Override
    public TrnDtlLeaveStatus read(Long tId) {
        logger.debug("read");
        Optional<TrnDtlLeaveStatus> optional = trnDtlLeaveStatusRepository.findById(tId);
        if (!optional.isPresent()) {
            throw new FASException(FASMessageType.ENTITY_NOT_FOUND);
        }
        return optional.get();
    }

    @Override
    public TrnDtlLeaveStatus update(TrnDtlLeaveStatus trnDtlLeaveStatus) {
        TrnDtlLeaveStatus term = read(trnDtlLeaveStatus.getId());

        term.setAttdId(trnDtlLeaveStatus.getAttdId());
        term.setAttendance_Date(trnDtlLeaveStatus.getAttendance_Date());
        term.setIsDeleted(trnDtlLeaveStatus.getIsDeleted());
        term.setLeaveDesc(trnDtlLeaveStatus.getLeaveDesc());
        term.setLeaveTypeId(trnDtlLeaveStatus.getLeaveTypeId());
        term.setYear(trnDtlLeaveStatus.getYear());
        term.setfId(trnDtlLeaveStatus.getfId());
        return trnDtlLeaveStatusRepository.save(term);
    }

    @Override
    public void delete(Long tId) {
        logger.debug("delete");
        TrnDtlLeaveStatus trnDtlLeaveStatus = read(tId);
        trnDtlLeaveStatusRepository.delete(trnDtlLeaveStatus);
    }

    @Override
    public LeaveDetails readData(Long iId) {
        logger.debug("readData");
        String TypeName = null;
        String Duration = null;
        FacultyAttendance facultyAttendance = facultyAttendanceService.read(iId);
        if (facultyAttendance.getLeaveStatus() == 1L) {
            Optional<TrnDtlLeaveStatus> optional = trnDtlLeaveStatusRepository.findByAttdIdAndIsDeletedFalse(iId);
            if (!optional.isPresent()) {
                LeaveDetails leaveDetails = new LeaveDetails();
                leaveDetails.setIsError(Boolean.TRUE);

                return leaveDetails;
            }
            Optional<LeaveTypeMaster> optional1 = leavaLeaveTypeMasterRepository.findById(optional.get().getLeaveTypeId());
            //return optional.get().;
            if (!optional1.isPresent()) {

                LeaveDetails leaveDetails = new LeaveDetails();
                leaveDetails.setIsError(Boolean.TRUE);
                leaveDetails.setLeaveDesc(optional.get().getLeaveDesc());
                return leaveDetails;
            }
            TypeName = optional1.get().getName();

            if (facultyAttendance.getSchdlRefId() != 0L && facultyAttendance.getSchdlTbleId() == 1L) {
                TrnLongleaveDtls trnLongleaveDtls = trnLongleaveDtlsService.read(facultyAttendance.getSchdlRefId());
                Duration = CustDateFormat.dateToINDString(trnLongleaveDtls.getFromDate()) + " To " + CustDateFormat.dateToINDString(trnLongleaveDtls.getToDate());
                // System.err.println("Duration : "+Duration);
            }
        } else if (facultyAttendance.getLeaveStatus() == 2L && facultyAttendance.getPresenceStatus()) {

            TypeName = facultyAttendance.getReasonByPrincipal().replace("#", " : ");
        } else if (facultyAttendance.getLeaveStatus() == 2L) {
            TypeName = "Weekend";
        } else if (facultyAttendance.getLeaveStatus() == 3L && !facultyAttendance.getPresenceStatus()) {
            TypeName = "Holiday";

        }

        LeaveDetails leaveDetails = new LeaveDetails();

        leaveDetails.setLeaveDesc(Duration);

        leaveDetails.setLeaveType(TypeName);
        leaveDetails.setIsError(Boolean.FALSE);
        return leaveDetails;

        //  return optional1.get().getName();
    }

    @Override
    public LeaveDetails readTodayData(Long fId) {
        logger.debug("readData");
        String TypeName = null;
        String Duration = null;
        FacultyAttendance facultyAttendance = facultyAttendanceService.readByFacultyId(fId);
        if (facultyAttendance.getLeaveStatus() == 1L) {
            Optional<TrnDtlLeaveStatus> optional = trnDtlLeaveStatusRepository.findByAttdIdAndIsDeletedFalse(facultyAttendance.getId());
            if (!optional.isPresent()) {
                LeaveDetails leaveDetails = new LeaveDetails();
                leaveDetails.setIsError(Boolean.TRUE);

                return leaveDetails;
            }
            Optional<LeaveTypeMaster> optional1 = leavaLeaveTypeMasterRepository.findById(optional.get().getLeaveTypeId());
            //return optional.get().;
            if (!optional1.isPresent()) {

                LeaveDetails leaveDetails = new LeaveDetails();
                leaveDetails.setIsError(Boolean.TRUE);
                leaveDetails.setLeaveDesc(optional.get().getLeaveDesc());
                return leaveDetails;
            }
            TypeName = optional1.get().getName();

            if (facultyAttendance.getSchdlRefId() != 0L && facultyAttendance.getSchdlTbleId() == 1L) {
                TrnLongleaveDtls trnLongleaveDtls = trnLongleaveDtlsService.read(facultyAttendance.getSchdlRefId());
                Duration = CustDateFormat.dateToINDString(trnLongleaveDtls.getFromDate()) + " To " + CustDateFormat.dateToINDString(trnLongleaveDtls.getToDate());
            }
        } else if (facultyAttendance.getLeaveStatus() == 2L && facultyAttendance.getPresenceStatus()) {

            String arr[] = facultyAttendance.getReasonByPrincipal().split("#");

            System.out.println(arr[0]);
            if (arr.length > 1) {
                TypeName = facultyAttendance.getReasonByPrincipal().replace("#", " : ");
            } else {
                TypeName = arr[0];
            }

        } else if (facultyAttendance.getLeaveStatus() == 2L) {
            TypeName = "Weekend";
        } else if (facultyAttendance.getLeaveStatus() == 3L && !facultyAttendance.getPresenceStatus()) {
            TypeName = "Holiday";

            if (facultyAttendance.getSchdlTbleId()== 2L) {
                HolidayMaster holidayMaster =     holidayMasterServiceImpl.read(facultyAttendance.getSchdlRefId());
                //return optional.get().;
               if(holidayMaster!=null)
               {
                    TypeName = holidayMaster.getName();
               }
            }
        }
        LeaveDetails leaveDetails = new LeaveDetails();

        leaveDetails.setLeaveDesc(Duration);

        leaveDetails.setLeaveType(TypeName);
        leaveDetails.setIsError(Boolean.FALSE);
        return leaveDetails;
    }

}
