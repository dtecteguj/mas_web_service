/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.springapi.service.impl;

import com.springapi.enums.FASMessageType;
import com.springapi.exception.FASException;
import com.springapi.model.LeaveTypeMaster;
import com.springapi.repository.LeaveTypeMasterRepository;
import com.springapi.service.LeaveTypeMasterService;
import java.util.List;
import java.util.Optional;
import javax.persistence.EntityManager;
import org.jboss.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author shahb
 */

@Service
@Transactional
public class LeaveTypeMasterServiceImpl implements LeaveTypeMasterService {
    static Logger logger=Logger.getLogger(LeaveTypeMasterServiceImpl.class);
    
    @Autowired
    private LeaveTypeMasterRepository leaveTypeMasterRepository;
    
    @Autowired
    private EntityManager entityManager;
    
    @Override
    public LeaveTypeMaster create(LeaveTypeMaster leaveTypeMaster) {
        logger.debug("create");
        return leaveTypeMasterRepository.save(leaveTypeMaster);
    }

    @Override
    public List<LeaveTypeMaster> list() {
        logger.debug("list");
        return leaveTypeMasterRepository.findAll();
    }

    @Override
    public LeaveTypeMaster read(Long iId) {
         logger.debug("read");
        Optional<LeaveTypeMaster> optional = leaveTypeMasterRepository.findById(iId);
        if (!optional.isPresent()) {
            throw new FASException(FASMessageType.ENTITY_NOT_FOUND);
        }
        return optional.get();
       }

    @Override
    public LeaveTypeMaster update(LeaveTypeMaster leaveTypeMaster) {
       logger.debug("update");
        LeaveTypeMaster leaveTypeMaster1 = read(leaveTypeMaster.getId());

        leaveTypeMaster1.setName(leaveTypeMaster.getName());
       leaveTypeMaster1.setTypeId(leaveTypeMaster.getTypeId());
       leaveTypeMaster1.setIsDeleted(leaveTypeMaster.getIsDeleted());
        return leaveTypeMasterRepository.save(leaveTypeMaster1);   
    }

    @Override
    public void delete(Long lId) {
        logger.debug("delete");
        LeaveTypeMaster leaveTypeMaster = read(lId);
        leaveTypeMasterRepository.delete(leaveTypeMaster);
    }
}
