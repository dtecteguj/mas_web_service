package com.springapi.service.impl;

import com.springapi.dto.request.StudentTimeTableRequest;
import com.springapi.dto.response.StudentFeedbackAttendanceResponse;
import com.springapi.dto.response.StudentTimeTableResponse;
import com.springapi.model.Faculty;
import com.springapi.model.Semester;
import com.springapi.model.Student;
import com.springapi.model.StudentFeedback;
import com.springapi.model.Subject;
import com.springapi.model.TimeTable;
import com.springapi.repository.StudentFeedbackRepository;
import com.springapi.repository.TimeTableRepository;
import com.springapi.service.FacultyService;
import com.springapi.service.StudentFeedbackService;
import com.springapi.service.SubjectService;
import static com.springapi.service.impl.FacultyAttendanceServiceImpl.logger;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Order;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author shahb
 */
@Service
@Transactional
public class StudentFeedbackServiceImpl implements StudentFeedbackService {

    @Autowired
    private StudentFeedbackRepository studentFeedbackRepository;

    @Autowired
    private TimeTableRepository timeTableRepository;

    @Autowired
    private FacultyService facultyService;

    @Autowired
    private SubjectService subjectService;

    @Autowired
    private EntityManager entityManager;

    @Override
    public StudentFeedback create(StudentFeedback studentFeedback) {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
        String currentDate = formatter.format(new Date());

        studentFeedback.setCreatedAt(new Date());
        studentFeedback.setFeedbackDate(currentDate);
        return studentFeedbackRepository.save(studentFeedback);
    }

    @Override
    public StudentTimeTableResponse studentTimeTableInfo(StudentTimeTableRequest studentFeedbackRequest) {
        StudentTimeTableResponse studentTimeTableResponse = new StudentTimeTableResponse();
        TimeTable timeTable = timeTableRepository.findByDayIdAndSemIdAndSlotId(studentFeedbackRequest.getDayId(),
                studentFeedbackRequest.getSemId(), studentFeedbackRequest.getSlotId());

        if (timeTable == null) {
            return null;
        }
        Faculty faculty = facultyService.read(timeTable.getFacultyId());
        Subject subject = subjectService.readByCode(timeTable.getSubjectCode());
        studentTimeTableResponse.setFacultyId(faculty.getId());
        studentTimeTableResponse.setFacultyName(faculty.getName());
        studentTimeTableResponse.setSemId(timeTable.getSemId());
        studentTimeTableResponse.setSlotId(timeTable.getSlotId());
        studentTimeTableResponse.setSubjectId(subject.getId());
        studentTimeTableResponse.setSubjectName(subject.getsName());
        return studentTimeTableResponse;
    }

    @Override
    public StudentFeedbackAttendanceResponse studentAttendanceList(StudentTimeTableRequest studentFeedbackRequest) {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
        String currentDate = formatter.format(new Date());

        List<StudentFeedback> studentFeedbackList = studentFeedbackRepository.findAllByFeedbackDateAndFacultyIdAndSlotId(
                currentDate, studentFeedbackRequest.getFacultyId(), studentFeedbackRequest.getSlotId());

        if (studentFeedbackList.size() > 0) {
            StudentFeedbackAttendanceResponse studFbAttenResponse = new StudentFeedbackAttendanceResponse();
            List<Long> studentIds = new ArrayList<>();

            for (StudentFeedback studentFeedback : studentFeedbackList) {
                studentIds.add(studentFeedback.getStudentId());
            }
            studFbAttenResponse.setTotalCount(studentFeedbackList.size());
            studFbAttenResponse.setStudentIds(studentIds);

            System.out.println("student response::" + studFbAttenResponse);
            return studFbAttenResponse;
        }
        return null;
    }

    @Override
    public Page<StudentFeedback> studentFeedbackList(StudentTimeTableRequest listRequest) {
        logger.debug("studentFeedbackList");

        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery criteriaQuery = criteriaBuilder.createQuery(StudentFeedback.class);

        prepareListQuery(criteriaQuery, criteriaBuilder, false, listRequest);

        // Prepare Pagination
        Pageable pageable = new PageRequest(listRequest.getPage(), listRequest.getLimit());
        TypedQuery typedQuery = entityManager.createQuery(criteriaQuery);
        typedQuery.setFirstResult((int) pageable.getOffset());
        typedQuery.setMaxResults(pageable.getPageSize());

        List results = typedQuery.getResultList();
        System.out.println("result ::" + results);
//        return results;
        return new PageImpl(results, pageable, getListCount(listRequest));

    }

    private long getListCount(StudentTimeTableRequest listRequest) {
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Long> criteriaQuery = criteriaBuilder.createQuery(Long.class);

        // Prepare list query
        prepareListQuery(criteriaQuery, criteriaBuilder, true, listRequest);

        return entityManager.createQuery(criteriaQuery).getSingleResult();
    }

    private void prepareListQuery(CriteriaQuery criteriaQuery, CriteriaBuilder criteriaBuilder, boolean countQuery,
            StudentTimeTableRequest listRequest) {
        logger.debug("prepareListQuery");
        Root<StudentFeedback> studentFeedbackRoot = criteriaQuery.from(StudentFeedback.class);
        Root<Student> studentRoot = criteriaQuery.from(Student.class);
        Root<Faculty> facultyRoot = criteriaQuery.from(Faculty.class);
        Root<Semester> semRoot = criteriaQuery.from(Semester.class);
        Root<Subject> subRoot = criteriaQuery.from(Subject.class);

        if (countQuery) {
            criteriaQuery.select(criteriaBuilder.count(studentFeedbackRoot));
        } else {
            criteriaQuery.multiselect(
                    studentFeedbackRoot.get("studentId"), studentFeedbackRoot.get("rate"), studentFeedbackRoot.get("isLectureAttend"),
                    studentFeedbackRoot.get("isLectureConducted"), studentFeedbackRoot.get("comment"), studentFeedbackRoot.get("facultyId"),
                    studentFeedbackRoot.get("semId"), studentFeedbackRoot.get("slotId"), studentFeedbackRoot.get("feedbackDate")
                    ,facultyRoot.get("name"), studentRoot.get("name"), semRoot.get("name"), subRoot.get("sName")
            );

            List<Order> orders = new ArrayList<>();
            if (!orders.isEmpty()) {
                criteriaQuery.orderBy(orders);
            }
        }

        String pattern = "yyyy-MM-dd";
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
        String date = simpleDateFormat.format(new Date());
        
        System.out.println("today date ::"+date);
        ArrayList<Predicate> predicateList = new ArrayList<>();
        predicateList.add(criteriaBuilder.equal(studentFeedbackRoot.get("facultyId"), facultyRoot.get("id")));
        predicateList.add(criteriaBuilder.equal(studentFeedbackRoot.get("studentId"), studentRoot.get("id")));
        predicateList.add(criteriaBuilder.equal(studentFeedbackRoot.get("semId"), semRoot.get("id")));
        predicateList.add(criteriaBuilder.equal(studentFeedbackRoot.get("subjectId"), subRoot.get("id")));

        if (listRequest.getStartDate() == null) {
        predicateList.add(criteriaBuilder.equal(studentFeedbackRoot.get("feedbackDate"), date));
        }
    
        if (listRequest.getFacultyId() != null) {
        predicateList.add(criteriaBuilder.equal(studentFeedbackRoot.get("facultyId"), listRequest.getFacultyId()));
        }
        
        if (listRequest.getStartDate() != null) {
            predicateList.add(criteriaBuilder.greaterThanOrEqualTo(studentFeedbackRoot.<Date>get("feedbackDate"), listRequest.getStartDate()));
        }

        if (listRequest.getEndDate() != null) {
            predicateList.add(criteriaBuilder.lessThanOrEqualTo(studentFeedbackRoot.<Date>get("feedbackDate"), listRequest.getEndDate()));
        }
        

        Predicate[] predicates = new Predicate[predicateList.size()];
        predicateList.toArray(predicates);
        criteriaQuery.where(predicates);
    }

    @Override
    public List<StudentFeedback> studentFeedbackDailyList(StudentFeedback studentFeedbackRequest) {
      String pattern = "yyyy-MM-dd";
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
        String date = simpleDateFormat.format(new Date());

        List<StudentFeedback> studentFeedbacks = studentFeedbackRepository.findByStudentIdAndFeedbackDate(studentFeedbackRequest.getStudentId(), date);
        for(StudentFeedback sf:studentFeedbacks)
        {
             Faculty faculty = facultyService.read(sf.getFacultyId());
             sf.setFacultyName(faculty.getName());
        }
        return studentFeedbacks;   
    }

}
