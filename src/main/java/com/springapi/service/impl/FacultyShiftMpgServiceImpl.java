/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.springapi.service.impl;

import com.springapi.enums.FASMessageType;
import com.springapi.exception.FASException;
import com.springapi.model.Faculty;
import com.springapi.model.FacultyAttendance;
import com.springapi.model.FacultyShiftMpg;
import com.springapi.model.ShiftMaster;
import com.springapi.model.TermMaster;
import com.springapi.repository.FacultyAttendanceRepository;
import com.springapi.repository.FacultyRepository;
import com.springapi.repository.FacultyShiftMpgRepository;
import com.springapi.repository.ShiftRepository;
import com.springapi.repository.TermMasterRepository;
import com.springapi.service.FacultyShiftMpgService;
import com.springapi.util.CustDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import javax.persistence.EntityManager;
import org.jboss.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author shahb
 */
@Service
@Transactional
public class FacultyShiftMpgServiceImpl implements FacultyShiftMpgService {

    static Logger logger = Logger.getLogger(FacultyActivityServiceImpl.class);

    @Autowired
    private FacultyShiftMpgRepository facultyShiftMpgRepository;

    @Autowired
    private FacultyRepository facultyRepository;

    @Autowired
    private ShiftRepository shiftRepository;

    @Autowired
    private TermMasterRepository termMasterRepository;

    @Autowired
    private FacultyAttendanceRepository facultyAttendanceRepository;

    @Autowired
    private EntityManager entityManager;

    @Override
    public FacultyShiftMpg create(FacultyShiftMpg facultyShiftMpg) {
        logger.debug("create");
        Optional<FacultyShiftMpg> optional = facultyShiftMpgRepository.findByIsActiveAndFId(Boolean.TRUE, facultyShiftMpg.getfId());
        if (optional.isPresent()) {
            optional.get().setIsActive(Boolean.FALSE);
            facultyShiftMpgRepository.save(optional.get());
        }

        Optional<Faculty> fOptional = facultyRepository.findById(facultyShiftMpg.getfId());
        if (fOptional.isPresent()) {
            fOptional.get().setActiveFlag(Boolean.TRUE);
            facultyRepository.save(fOptional.get());

        }
        Optional<TermMaster> tOptional = termMasterRepository.findByIsActiveTrue();
        if (tOptional.isPresent()) {
            facultyShiftMpg.setTermId(tOptional.get().getId());
        }
        facultyShiftMpg.setIsActive(Boolean.TRUE);
        facultyShiftMpg.setIsDeleted(Boolean.FALSE);

        facultyShiftMpgRepository.save(facultyShiftMpg);
        FacultyAttendance facultyAttendance = facultyAttendanceRepository.findByAttendanceDateAndFId(CustDateFormat.todaySQLString(), facultyShiftMpg.getfId());

        readByFacultyId(facultyShiftMpg.getfId(), CustDateFormat.weekDay(new Date()), facultyAttendance);
        facultyAttendanceRepository.save(facultyAttendance);

        return facultyShiftMpg;
    }

    @Override
    public List<FacultyShiftMpg> list() {
        logger.debug("list");
        return facultyShiftMpgRepository.findAll();
    }

    @Override
    public FacultyShiftMpg read(Long fId) {
        logger.debug("read");
        Optional<FacultyShiftMpg> optional = facultyShiftMpgRepository.findById(fId);
        if (!optional.isPresent()) {
            throw new FASException(FASMessageType.ENTITY_NOT_FOUND);
        }
        return optional.get();
    }

    @Override
    public FacultyShiftMpg update(FacultyShiftMpg facultyShiftMpg) {
        logger.debug("update");
        FacultyShiftMpg facultySM = read(facultyShiftMpg.getId());

        facultySM.setfId(facultyShiftMpg.getfId());
        facultySM.setShiftId(facultyShiftMpg.getShiftId());
        facultySM.setTermId(facultyShiftMpg.getTermId());
        facultySM.setIsActive(facultyShiftMpg.getIsActive());
        facultySM.setInstituteId(facultyShiftMpg.getInstituteId());
        facultySM.setIsDeleted(facultyShiftMpg.getIsDeleted());
        return facultyShiftMpgRepository.save(facultySM);
    }

    @Override
    public void delete(Long fId) {
        logger.debug("delete");
        FacultyShiftMpg facultyShiftMpg = read(fId);
        facultyShiftMpgRepository.delete(facultyShiftMpg);
    }

    @Override
    public void readByFacultyId(Long fId, int dayN, FacultyAttendance facAtt) {
        Optional<FacultyShiftMpg> optional = facultyShiftMpgRepository.findByFIdAndIsDeletedFalseAndIsActiveTrue(fId);
        if (!optional.isPresent()) {
            System.out.println("Faculty ID Shift Not Properly Mapped : " + facAtt.getfId());
            facAtt.setActualInTime("00:02");
            facAtt.setActualOutTime("23:58");
            facAtt.setShiftRefId(0L);
            facAtt.setSchdlTbleId(0L);
            return;
        }
        Long shiftId = 0L;
        FacultyShiftMpg facultyShiftMpg = optional.get();
        switch (dayN) {
            case 1:
                shiftId = facultyShiftMpg.getDay1();
                break;
            case 2:
                shiftId = facultyShiftMpg.getDay2();
                break;
            case 3:
                shiftId = facultyShiftMpg.getDay3();
                break;
            case 4:
                shiftId = facultyShiftMpg.getDay4();
                break;
            case 5:
                shiftId = facultyShiftMpg.getDay5();
                break;
            case 6:
                shiftId = facultyShiftMpg.getDay6();
                break;

        }
        if (dayN < 0L && dayN > 6L) {
            System.out.println("WEEKEND DATA or unaccpected Error..: " + facAtt.getfId());
            facAtt.setActualInTime("00:01");
            facAtt.setActualOutTime("23:59");
            facAtt.setShiftRefId(0L);
            facAtt.setSchdlTbleId(0L);
        } else {

            ShiftMaster shiftMaster = shiftRepository.findById(optional.get().getShiftId());
            if (shiftMaster == null) {

                System.out.println("Shift Id Not Available in Shift Master: " + facAtt.getfId());
                facAtt.setActualInTime("00:03");
                facAtt.setActualOutTime("23:57");
                facAtt.setShiftRefId(0L);
                facAtt.setSchdlTbleId(0L);
            } else {

                facAtt.setActualInTime(shiftMaster.getInTime());
                facAtt.setActualOutTime(shiftMaster.getOutTime());
                facAtt.setShiftRefId(facultyShiftMpg.getId());
                facAtt.setShiftTbleId(1L);
            }
        }
    }

}
