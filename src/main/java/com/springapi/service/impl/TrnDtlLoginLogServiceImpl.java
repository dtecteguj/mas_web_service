/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.springapi.service.impl;

import com.springapi.dto.request.LoginLogRequest;
import com.springapi.model.TrnDtlLoginLog;
import com.springapi.repository.TrnDtlLoginLogRepository;
import com.springapi.service.TrnDtlLoginLogService;
import com.springapi.util.CustDateFormat;
import java.text.ParseException;
import java.util.List;
import java.util.logging.Level;
import javax.persistence.EntityManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author shahb
 */
@Service
@Transactional
public class TrnDtlLoginLogServiceImpl implements TrnDtlLoginLogService {

    static Logger logger = Logger.getLogger(TrnDtlLoginLogServiceImpl.class);

    @Autowired
    private TrnDtlLoginLogRepository trnDtlLoginLogRepository;

    @Autowired
    private EntityManager entityManager;

    @Override
    public TrnDtlLoginLog create(TrnDtlLoginLog trnDtlLoginLog) {
        logger.debug("create");
        return trnDtlLoginLogRepository.save(trnDtlLoginLog);
    }

    @Override
    public List<TrnDtlLoginLog> list() {
        logger.debug("list");
        return trnDtlLoginLogRepository.findAll();
    }

    @Override
    public TrnDtlLoginLog read(Long aId) {
        logger.debug("list");
        return trnDtlLoginLogRepository.findById(aId);
    }

    @Override
    public TrnDtlLoginLog update(TrnDtlLoginLog trnDtlLoginLog) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void delete(Long aId) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public TrnDtlLoginLog findByAndroidId(LoginLogRequest loginLogRequest) {
        try {
            List<TrnDtlLoginLog> dtlLoginLogs = trnDtlLoginLogRepository.findByLoginDateAndAndroidIdAndIsDeletedFalse(CustDateFormat.SQLTypeStringToDate(CustDateFormat.todaySQLString()), loginLogRequest.getAndroidId());
            if(dtlLoginLogs == null)
            {
                //Insert New Record
            }
            else if(dtlLoginLogs.get(0).getToken().equals(loginLogRequest.getToken()))
            {
                //Active User
            }
            else if(dtlLoginLogs.get(0).getFId() == loginLogRequest.getFId())
            {
                // Already Login
                //Insert ENtry and login
            }
            else if(dtlLoginLogs.get(0).getLoginCnt() < 3)
            {
                // New User in Login
                //
            }
             else if(dtlLoginLogs.get(0).getLoginCnt() > 3)
            {
                // LOCK USER 
                
            } 
             else{}
        } catch (ParseException ex) {
            
            java.util.logging.Logger.getLogger(TrnDtlLoginLogServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return null;
    }
}
 
