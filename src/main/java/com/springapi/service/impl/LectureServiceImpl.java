/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.springapi.service.impl;

import com.springapi.enums.FASMessageType;
import com.springapi.exception.FASException;
import com.springapi.model.Lecture;
import com.springapi.repository.LectureRepository;
import com.springapi.service.LectureService;
import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author shahb
 */
@Service
@Transactional
public class LectureServiceImpl implements LectureService {

    @Autowired
    private LectureRepository lectureRepository;

    @Override
    public Lecture create(Lecture lecture) {
        return lectureRepository.save(lecture);
    }

    @Override
    public List<Lecture> list() {
        return lectureRepository.findAll();
    }

    @Override
    public Lecture read(Long lId) {
        Optional<Lecture> optional = lectureRepository.findById(lId);
        if (!optional.isPresent()) {
             throw new FASException(FASMessageType.ENTITY_NOT_FOUND);
        }
        return optional.get();
    }

    @Override
    public Lecture update(Lecture lectureDetails) {
        Lecture lecture = read(lectureDetails.getId());

        lecture.setlTime(lectureDetails.getlTime());

        return lectureRepository.save(lecture);
    }

    @Override
    public void delete(Long lId) {
        Lecture lecture = read(lId);
        lectureRepository.delete(lecture);
    }
}
