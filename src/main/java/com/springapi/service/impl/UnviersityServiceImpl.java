package com.springapi.service.impl;

import com.springapi.enums.FASMessageType;
import com.springapi.exception.FASException;
import com.springapi.model.UniversityMaster;
import com.springapi.repository.UniversityRepository;
import com.springapi.service.UniversityService;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author Jyoti Patel
 */
@Service
@Transactional
public class UnviersityServiceImpl implements UniversityService {

    static Logger logger = Logger.getLogger(UnviersityServiceImpl.class);

    @Autowired
    private UniversityRepository universityRepository;

    @Override
    public UniversityMaster create(UniversityMaster university) {
        logger.debug("create");
        return universityRepository.save(university);
    }

    @Override
    public List<UniversityMaster> list() {
        logger.debug("list");
        return universityRepository.findAll();
    }

    @Override
    public UniversityMaster read(Long uId) {
        logger.debug("read");
        Optional<UniversityMaster> optional = universityRepository.findById(uId);
        if (!optional.isPresent()) {
            throw new FASException(FASMessageType.ENTITY_NOT_FOUND);
        }
        return optional.get();
    }

    @Override
    public UniversityMaster update(UniversityMaster universityDetails) {
        logger.debug("update");
        UniversityMaster university = read(universityDetails.getId());

        university.setCode(universityDetails.getCode());
        university.setName(universityDetails.getName());
        university.setLink(universityDetails.getLink());
        university.setUpdatedAt(new Date());

        return universityRepository.save(university);
    }

    @Override
    public void delete(Long uId) {
        logger.debug("delete");
        UniversityMaster university = read(uId);
        universityRepository.delete(university);
    }
}
