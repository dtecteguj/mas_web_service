package com.springapi.service.impl;

import java.util.List;
import java.util.Optional;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.springapi.model.TrnAttendanceLog;
import com.springapi.repository.TrnAttendanceLogRepository;
import com.springapi.service.TrnAttendanceLogService;

@Service
@Transactional
public class TrnAttendanceLogServiceImpl implements TrnAttendanceLogService {

	static Logger logger = Logger.getLogger(TrnAttendanceLogServiceImpl.class);

	@Autowired
	private TrnAttendanceLogRepository TrnAttendanceLogRepository1;

	@Override
	public TrnAttendanceLog create(TrnAttendanceLog tal) {
		logger.debug("create");
		return TrnAttendanceLogRepository1.save(tal);
	}

	@Override
	public List<TrnAttendanceLog> list() {
		logger.debug("list");
		return TrnAttendanceLogRepository1.findAll();
	}

	@Override
	public TrnAttendanceLog read(Long id) {
		logger.debug("read");
		Optional<TrnAttendanceLog> optional = TrnAttendanceLogRepository1.findById(id);
		if (!optional.isPresent()) {
			optional = null;
		}
		return optional.get();
	}

	@Override
	public TrnAttendanceLog update(TrnAttendanceLog tal) {
		logger.debug("update");
		TrnAttendanceLog tal1 = read(tal.getId());
		return TrnAttendanceLogRepository1.save(tal1);
	}

	@Override
	public void delete(Long id) {
		logger.debug("delete");
		TrnAttendanceLog tpl = read(id);
		TrnAttendanceLogRepository1.delete(tpl);
	}

}
