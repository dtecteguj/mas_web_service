package com.springapi.service.impl;

import java.util.List;
import java.util.Optional;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.springapi.model.TrnbyPriHodLog;
import com.springapi.repository.TrnbyPriHodLogRepository;
import com.springapi.service.TrnbyPriHodLogService;



@Service
@Transactional
public class TrnbyPriHodLogServiceImpl implements TrnbyPriHodLogService{
	
static Logger logger = Logger.getLogger(TrnbyPriHodLogServiceImpl.class);
	
	@Autowired
	private TrnbyPriHodLogRepository TrnbyPriHodLogRepository1;

	@Override
	public TrnbyPriHodLog create(TrnbyPriHodLog tpl) {
		logger.debug("create");
		return TrnbyPriHodLogRepository1.save(tpl);
	}

	@Override
	public List<TrnbyPriHodLog> list() {
		logger.debug("list");
		return TrnbyPriHodLogRepository1.findAll();
	}

	@Override
	public TrnbyPriHodLog read(Long id) {
		logger.debug("read");
		Optional<TrnbyPriHodLog> optional = TrnbyPriHodLogRepository1.findById(id);
		if (!optional.isPresent()) {
			optional = null;
		}
		return optional.get();
	}

	@Override
	public TrnbyPriHodLog update(TrnbyPriHodLog tpl) {
		logger.debug("update");
		TrnbyPriHodLog tpl1 = read(tpl.getId());
		return TrnbyPriHodLogRepository1.save(tpl1);
	}

	@Override
	public void delete(Long id) {
		logger.debug("delete");
		TrnbyPriHodLog tpl = read(id);
		TrnbyPriHodLogRepository1.delete(tpl);
	}

}
