package com.springapi.service.impl;

import com.springapi.model.TokensManage;
import com.springapi.repository.TokensManageRepository;
import com.springapi.service.TokenService;
import java.util.Date;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author Jyoti_patel
 */
@Service
@Transactional
public class TokenServiceImpl implements TokenService {

    @Autowired
    private TokensManageRepository tokensManageRepository;

    @Override
    public TokensManage createOrUpdate(TokensManage tokenManangeRequest) {
        TokensManage tokensManage = tokensManageRepository.findByUId(tokenManangeRequest.getuId());
        if (tokensManage == null) {
            tokensManage = new TokensManage();
            tokensManage.setuId(tokenManangeRequest.getuId());
            tokensManage.setCreatedAt(new Date());
            tokensManage.setToken(tokenManangeRequest.getToken());
            tokensManage.setType(tokenManangeRequest.getType());
            return tokensManageRepository.save(tokensManage);
        }
        tokensManage.setToken(tokenManangeRequest.getToken());
        return tokensManageRepository.save(tokensManage);
    }

}
