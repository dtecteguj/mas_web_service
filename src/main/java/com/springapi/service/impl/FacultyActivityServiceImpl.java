package com.springapi.service.impl;

import com.springapi.dto.request.FacultyActivityRequest;
import com.springapi.dto.response.FacultyActivityRecordResponse;
import com.springapi.dto.response.FacultyActivityResponse;
import com.springapi.model.Faculty;
import com.springapi.model.FacultyActivity;
import com.springapi.model.Subject;
import com.springapi.model.TimeTable;
import java.util.Date;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.springapi.repository.FacultyActivityRepository;
import com.springapi.repository.TimeTableRepository;
import com.springapi.service.FacultyActivityService;
import com.springapi.service.SubjectService;
import static com.springapi.service.impl.FacultyAttendanceServiceImpl.logger;
import com.springapi.util.CustDateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Order;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import static java.time.DayOfWeek.MONDAY;
import static java.time.DayOfWeek.SUNDAY;
import static java.time.temporal.TemporalAdjusters.nextOrSame;
import static java.time.temporal.TemporalAdjusters.previousOrSame;

/**
 * @author Jyoti Patel
 */
@Service
@Transactional
public class FacultyActivityServiceImpl implements FacultyActivityService {

    @Autowired
    private FacultyActivityRepository facultyFeedbackRepository;

    @Autowired
    private TimeTableRepository timeTableRepository;

    @Autowired
    private SubjectService subjectService;

    @Autowired
    private EntityManager entityManager;

    @Override
    public FacultyActivity create(FacultyActivityRequest facultyFeedbackRequest) {
        FacultyActivity facultyFeedback = new FacultyActivity();
        facultyFeedback.setCreatedAt(new Date());
        facultyFeedback.setFacultyId(facultyFeedbackRequest.getFacultyId());
        facultyFeedback.setMessage(facultyFeedbackRequest.getMessage());

        return facultyFeedbackRepository.save(facultyFeedback);
    }

    @Override
    public FacultyActivityResponse getFacultyInfo(FacultyActivityRequest facultyActivityRequest) {
        FacultyActivityResponse facultyActivityResponse = new FacultyActivityResponse();
        TimeTable timeTable = timeTableRepository.findByDayIdAndFacultyIdAndSlotId(facultyActivityRequest.getDayId(),
                facultyActivityRequest.getFacultyId(), facultyActivityRequest.getSlotId());

        if (timeTable == null) {
            return null;
        }
        Subject subject = subjectService.readByCode(timeTable.getSubjectCode());
        facultyActivityResponse.setSemId(timeTable.getSemId());
        facultyActivityResponse.setSubjectId(subject.getId());
        facultyActivityResponse.setSubjectName(subject.getsName());
        return facultyActivityResponse;
    }

    @Override
    public FacultyActivity updateFacultyInfo(FacultyActivity facultyActivity) {
        String pattern = "yyyy-MM-dd";
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
        String date = simpleDateFormat.format(new Date());

        FacultyActivity facultyActivityResponse = new FacultyActivity();
        facultyActivityResponse.setActivityDate(date);
        facultyActivityResponse.setCreatedAt(new Date());
        facultyActivityResponse.setDayId(facultyActivity.getDayId());
        facultyActivityResponse.setFacultyId(facultyActivity.getFacultyId());
        facultyActivityResponse.setMessage(facultyActivity.getMessage());
        facultyActivityResponse.setSemId(facultyActivity.getSemId());
        facultyActivityResponse.setSlotId(facultyActivity.getSlotId());
        facultyActivityResponse.setSubjectId(facultyActivity.getSubjectId());

        return facultyFeedbackRepository.save(facultyActivityResponse);
    }

    @Override
    public List<FacultyActivityRecordResponse> facultyActivityRecord(FacultyActivity listRequest) {
        logger.debug("facultyActivityRecord");
        System.out.println("Inside ");
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery criteriaQuery = criteriaBuilder.createQuery(FacultyActivityRecordResponse.class);

        try {
            prepareListQuery(criteriaQuery, criteriaBuilder, false, listRequest);
        } catch (ParseException ex) {
            Logger.getLogger(FacultyActivityServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
        }

        // Prepare Pagination
        TypedQuery typedQuery = entityManager.createQuery(criteriaQuery);

        List results = typedQuery.getResultList();
//        System.out.println("result of program ::" + results.get(0));
        return results;
    }

    private void prepareListQuery(CriteriaQuery criteriaQuery, CriteriaBuilder criteriaBuilder, boolean countQuery,
            FacultyActivity listRequest) throws ParseException {
        logger.debug("prepareListQuery");
        Root<FacultyActivity> facultyActivityRoot = criteriaQuery.from(FacultyActivity.class);
        Root<Faculty> facultyRoot = criteriaQuery.from(Faculty.class);

        if (countQuery) {
            criteriaQuery.select(criteriaBuilder.count(facultyActivityRoot));
        } else {
            criteriaQuery.multiselect(
                    facultyActivityRoot.get("message"), criteriaBuilder.count(facultyActivityRoot)
            ).groupBy(facultyActivityRoot.get("message")
            );

            List<Order> orders = new ArrayList<>();

            if (!orders.isEmpty()) {
                criteriaQuery.orderBy(orders);
            }
        }

//        LocalDate today = LocalDate.now();
//
//        LocalDate monday = today.with(previousOrSame(MONDAY));
//        LocalDate sunday = today.with(nextOrSame(SUNDAY));
//
//        String pattern = "yyyy-MM-dd";
//        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
//        System.out.println("monday ::"+monday +" sunday ::"+sunday);
//        Date startDate = simpleDateFormat.parse(monday.toString());
//        Date endDate = simpleDateFormat.parse(sunday.toString());
//          Date endDate = simpleDateFormat.parse("2019-02-10");
        LocalDate startD = LocalDate.now();

        String pattern = "yyyy-MM-dd";
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
        String date = simpleDateFormat.format(new Date());
//        Date startDate = simpleDateFormat.parse("2019-01-01");
//        Date endDate = simpleDateFormat.parse("2019-01-31");
        Date startDate = simpleDateFormat.parse(startD.withDayOfMonth(1).toString());
        Date endDate = simpleDateFormat.parse(startD.withDayOfMonth(startD.lengthOfMonth()).toString());
        System.out.println("start  date ::"+startDate + "end date ::"+endDate);

        ArrayList<Predicate> predicateList = new ArrayList<>();
        predicateList.add(criteriaBuilder.equal(facultyActivityRoot.get("facultyId"), listRequest.getFacultyId()));
        predicateList.add(criteriaBuilder.equal(facultyActivityRoot.get("facultyId"), facultyRoot.get("id")));

        predicateList.add(criteriaBuilder.greaterThanOrEqualTo(facultyActivityRoot.<Date>get("activityDate"), startDate));
        predicateList.add(criteriaBuilder.lessThanOrEqualTo(facultyActivityRoot.<Date>get("activityDate"), endDate));
        Predicate[] predicates = new Predicate[predicateList.size()];
        predicateList.toArray(predicates);
        criteriaQuery.where(predicates);
    }

    @Override
    public List<FacultyActivity> getFacultyActivities(FacultyActivity facultyActivity) {
       String date = facultyActivity.getDayDate();
        if(date == null)
       {
            date = CustDateFormat.todaySQLString();
       }
       
        

        List<FacultyActivity> facultyActivities = facultyFeedbackRepository.findByFacultyIdAndActivityDate(
                facultyActivity.getFacultyId(), date);

        return facultyActivities;
    }
}
