package com.springapi.service.impl;

import com.opencsv.CSVWriter;
import com.opencsv.bean.StatefulBeanToCsv;
import com.opencsv.bean.StatefulBeanToCsvBuilder;
import com.springapi.model.Faculty;
import com.springapi.model.view.FacultyMEMOView;
import com.springapi.service.FacultyService;
import com.springapi.service.SendEmailService;
import com.springapi.util.CustDateFormat;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import javax.mail.internet.MimeMessage;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVPrinter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.FileSystemResource;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;

/**
 * @author Jyoti_patel
 */
@Service
@Transactional
public class SendEmailServiceImpl implements SendEmailService {

    @Autowired
    private JavaMailSender sender;

    @Autowired
    private FacultyService facultyService;

    @Override
    public void sendEmail(String email) throws Exception {
        System.out.println("Inside send email method !!:  " + email);
        MimeMessage message = sender.createMimeMessage();
        MimeMessageHelper helper = new MimeMessageHelper(message, true);
        helper.setTo(email);

        helper.setText("http://www.dheemati.in/angular");
        helper.setSubject("Forget Password ?");
        String filename = "users.csv";

        FileWriter writers = new FileWriter(filename);

        //create a csv writer
        StatefulBeanToCsv<Faculty> writer = new StatefulBeanToCsvBuilder<Faculty>(writers)
                .withQuotechar(CSVWriter.NO_QUOTE_CHARACTER)
                .withSeparator(CSVWriter.DEFAULT_SEPARATOR)
                .withOrderedResults(false)
                .build();

        //write all users to csv file
        writer.write(facultyService.list());

        System.out.println("Befor send email ");
        FileSystemResource file = new FileSystemResource(filename);
        helper.addAttachment(filename, file);
        sender.send(message);
        System.out.println("Send Email ");
    }

    @Override
    @SuppressWarnings("empty-statement")
    public void sendMEMOEmail(String[] email, List<FacultyMEMOView> facultyMEMOViewsT, List<FacultyMEMOView> facultyMEMOViewsH) throws Exception {
        System.out.println("Inside send email method !!:  " + email);
        MimeMessage message = sender.createMimeMessage();
        MimeMessageHelper helper = new MimeMessageHelper(message, true);
        String filePath = "C:\\Users\\hemantjoshi979\\Desktop\\MAS_MEMO\\";
//        String filePath = "C:\\Users\\shahb\\OneDrive\\Desktop\\MAS\\";
        helper.setTo(email);
        String text = "PFA, Here \n"
                + "Technical Institute MEMO : " + facultyMEMOViewsT.size()
                + "\nHigher Institute MEMO : " + facultyMEMOViewsH.size();

        //helper.setSubject("LAN-DB MAS MEMO of (TEST)" + CustDateFormat.todayINDString());
        helper.setSubject("MAS MEMO of " + CustDateFormat.todayINDString());
        if (facultyMEMOViewsT.isEmpty() && facultyMEMOViewsH.isEmpty()) {
            text = "No MEMO Generated Today!";
        } else {

            try {

                String fileTechnicalName = filePath + "MEMO_Technical_" + CustDateFormat.todayFileDateTimeString() + ".csv";
                String fileHigherName = filePath + "MEMO_Higher_" + CustDateFormat.todayFileDateTimeString() + ".csv";

                try (Writer writerT = Files.newBufferedWriter(Paths.get(fileTechnicalName))) {
                    CSVPrinter printerT = CSVFormat.DEFAULT.withHeader("lnstitute_Id", "Institute_Name", " Institute_Type_Id", "Department_Name", "String Faculty_Name", " Faculty_Email", "Memo_Date", " MEMO_Number").print(writerT);

                    List<Object[]> data = new ArrayList<>();
                    for (FacultyMEMOView facultyMEMOView : facultyMEMOViewsT) {
                        data.add(new Object[]{facultyMEMOView.getInstitute_Id(), facultyMEMOView.getInstitute_Name().trim(), facultyMEMOView.getInstitute_Type_Id(), facultyMEMOView.getDepartment_Name().trim(), facultyMEMOView.getFaculty_Name().trim(), facultyMEMOView.getFaculty_Email().trim(), facultyMEMOView.getMemo_Date().trim(), facultyMEMOView.getMEMO_Number()});
                    }
                    printerT.printRecords(data);
                    // flush the stream
                    printerT.flush();
                    // close the writer
                }

                try (Writer writerH = Files.newBufferedWriter(Paths.get(fileHigherName))) {
                    CSVPrinter printerH = CSVFormat.DEFAULT.withHeader("lnstitute_Id", "Institute_Name", " Institute_Type_Id", "Department_Name", "String Faculty_Name", " Faculty_Email", "Memo_Date", " MEMO_Number").print(writerH);

                    List<Object[]> data = new ArrayList<>();
                    for (FacultyMEMOView facultyMEMOView : facultyMEMOViewsH) {
                        data.add(new Object[]{facultyMEMOView.getInstitute_Id(), facultyMEMOView.getInstitute_Name().trim(), facultyMEMOView.getInstitute_Type_Id(), facultyMEMOView.getDepartment_Name().trim(), facultyMEMOView.getFaculty_Name().trim(), facultyMEMOView.getFaculty_Email().trim(), facultyMEMOView.getMemo_Date().trim(), facultyMEMOView.getMEMO_Number()});
                    }
                    printerH.printRecords(data);
                    // flush the stream
                    printerH.flush();
                    // close the writer
                }

                System.out.println("Befor send email ");
                FileSystemResource fileDataT = new FileSystemResource(fileTechnicalName);
                helper.addAttachment(fileTechnicalName, fileDataT);

                FileSystemResource fileDataH = new FileSystemResource(fileHigherName);
                helper.addAttachment(fileHigherName, fileDataH);
            } catch (IOException ex) {
                ex.printStackTrace();
            }
//
//            FileWriter writers = new FileWriter(filePath + fileTechnicalName);
//
//            //create a csv writer
//            StatefulBeanToCsv<FacultyMEMOView> writer = new StatefulBeanToCsvBuilder<FacultyMEMOView>(writers)
//                    .withQuotechar(CSVWriter.NO_QUOTE_CHARACTER)
//                    .withSeparator(CSVWriter.DEFAULT_SEPARATOR)
//                    .withOrderedResults(false)
//                    .build();
//
//            //write all users to csv file
//            writer.write(facultyMEMOViewsT);
//
//            writers = new FileWriter(filePath + fileHigherName);
//
//            //create a csv writer
//            writer = new StatefulBeanToCsvBuilder<FacultyMEMOView>(writers)
//                    .withQuotechar(CSVWriter.NO_QUOTE_CHARACTER)
//                    .withSeparator(CSVWriter.DEFAULT_SEPARATOR)
//                    .withOrderedResults(false)
//                    .build();
//
//            //write all users to csv file
//            writer.write(facultyMEMOViewsH);

        }
        helper.setText(text);
        sender.send(message);
        System.out.println("Send Email ");
    }

    @Override
    public void sendSchedulEmail(String[] email, int trainerCount, int longleaveCount, int totalRecord) throws Exception {
        System.out.println("Inside send email method !!:  " + email);
        MimeMessage message = sender.createMimeMessage();
        MimeMessageHelper helper = new MimeMessageHelper(message, true);

        helper.setTo(email);
        String text = "Total FS Trainners : " + trainerCount
                + "\nTotal Long Leave of Faculty  : " + longleaveCount
                + "\nTotal Staff Records : " + totalRecord;

        if (totalRecord == 0) {
            text = "Issue in Faculty Record Generating Schedule!";
        }
        helper.setSubject("MAS Faculty Attendance Generation Schedule for " + CustDateFormat.todayINDString());
       // helper.setSubject("LAN-DB MAS Faculty Attendance Generation Schedule for (TEST)" + CustDateFormat.todayINDString());
        helper.setText(text);
        sender.send(message);
        System.out.println("Send Email ");
    }
}
