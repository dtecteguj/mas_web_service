package com.springapi.service.impl;

import com.springapi.enums.FASMessageType;
import static com.springapi.enums.MASConstant.DEPT;
import static com.springapi.enums.MASConstant.PROG;
import com.springapi.exception.FASException;
import com.springapi.model.DepartmentMaster;
import com.springapi.model.InstituteMaster;
import com.springapi.model.ProgramMst;
import com.springapi.repository.InstituteRepository;
import com.springapi.service.DepartmentService;
import com.springapi.service.InstituteService;
import com.springapi.service.ProgramInstMpgService;
import com.springapi.service.ProgramMstService;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Jyoti Patel
 */
@Service
@Transactional
public class InstituteServiceImpl implements InstituteService {

    static Logger logger = Logger.getLogger(UnviersityServiceImpl.class);

    @Autowired
    private InstituteRepository instituteRepository;

    @Autowired
    private DepartmentService departmentService;

    @Autowired
    private ProgramInstMpgService programMstInstMpgService;

    @Override
    public InstituteMaster create(InstituteMaster instituteMaster) {
        logger.debug("create");
        return instituteRepository.save(instituteMaster);
    }

    @Override
    public List<InstituteMaster> list() {
        logger.debug("list");
        return instituteRepository.findAll();
    }

    @Override
    public InstituteMaster read(Long uId) {
        logger.debug("read");
        Optional<InstituteMaster> optional = instituteRepository.findById(uId);
        if (!optional.isPresent()) {
            throw new FASException(FASMessageType.ENTITY_NOT_FOUND);
        }
        return optional.get();
    }

    @Override
    public InstituteMaster update(InstituteMaster instituteMaster) {
        logger.debug("update");
        InstituteMaster institute = read(instituteMaster.getId()); 

        institute.setCode(instituteMaster.getCode());
        institute.setName(instituteMaster.getName());
        institute.setLink(instituteMaster.getLink());
        institute.setUniversityId(instituteMaster.getUniversityId());
        institute.setUpdatedAt(new Date());

        return instituteRepository.save(institute);
    }

    @Override
    public void delete(Long uId) {
        logger.debug("delete");
        InstituteMaster university = read(uId);
        instituteRepository.delete(university);
    }

    @Override
    public List<InstituteMaster> getAllInstituteByType(Long instituteTypeId) {
        logger.debug("getAllInstitutesByType");
        return instituteRepository.findByInstituteTypeIdOrderByName(instituteTypeId);
    }

    @Override
    public HashMap<String, Object> readDepartmentAndProgarm(Long uId) {
        List<DepartmentMaster> departmentMasters= departmentService.readDepartmentByInstituteId(uId);
        List<ProgramMst> programMsts= programMstInstMpgService.readProgramByInstituteId(uId);
        HashMap<String, Object> hashMap = new HashMap<String, Object>();
        hashMap.put(PROG, programMsts);
        hashMap.put(DEPT, departmentMasters);
        
        return hashMap;
    }
}
