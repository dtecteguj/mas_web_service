/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.springapi.service.impl;

import com.springapi.model.ShiftMaster;
import com.springapi.repository.AndroidVersionRepository;
import com.springapi.repository.ShiftRepository;
import com.springapi.service.ShiftMasterService;
import java.util.List;
import javax.persistence.EntityManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author shahb
 */
@Service
@Transactional
public class ShiftMasterServiceImpl implements ShiftMasterService {
 static Logger logger = Logger.getLogger(InstituteTypeServiceImpl.class);

    @Autowired
    private ShiftRepository shiftRepository;

    @Autowired
    private EntityManager entityManager;
    @Override
    public List<ShiftMaster> readbyinst(Long iId) {
      //  throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
      return shiftRepository.findAllByInstituteIdAndIsDeletedFalse(iId);
    }
    
}
