/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.springapi.service.impl;

import com.springapi.enums.FASMessageType;
import com.springapi.exception.FASException;
import com.springapi.model.TrnFSPrgmSchd;
import com.springapi.repository.TrnFSPrgmSchdRepository;
import com.springapi.service.TrnFSPrgmSchdService;
import java.util.List;
import java.util.Optional;
import javax.persistence.EntityManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author shahb
 */
@Service
@Transactional
public class TrnFSPrgmSchdServiceImpl implements TrnFSPrgmSchdService {

    static Logger logger = Logger.getLogger(TrnLongleaveDtlsServiceImpl.class);

    @Autowired
    private TrnFSPrgmSchdRepository trnFSPrgmSchdRepository;

    @Autowired
    private EntityManager entityManager;

    @Override
    public TrnFSPrgmSchd create(TrnFSPrgmSchd trnFSPrgmSchd) {
        logger.debug("create");
        return trnFSPrgmSchdRepository.save(trnFSPrgmSchd);
    }

    @Override
    public List<TrnFSPrgmSchd> list() {
        logger.debug("list");
        return trnFSPrgmSchdRepository.findAll();
    }

    @Override
    public TrnFSPrgmSchd read(Long iId) {
        logger.debug("read");
        Optional<TrnFSPrgmSchd> optional = trnFSPrgmSchdRepository.findById(iId);
        if (!optional.isPresent()) {
            throw new FASException(FASMessageType.ENTITY_NOT_FOUND);
        }
        return optional.get();
    }

    @Override
    public TrnFSPrgmSchd update(TrnFSPrgmSchd trnFSPrgmSchd) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void delete(Long tId) {
         logger.debug("delete");
        TrnFSPrgmSchd trnFSPrgmSchd = read(tId);
        trnFSPrgmSchdRepository.delete(trnFSPrgmSchd);
    }

}
