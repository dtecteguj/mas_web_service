/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.springapi.service.impl;

import com.springapi.enums.FASMessageType;
import com.springapi.exception.FASException;
import com.springapi.model.TrnDtlDlytskVerify;
import com.springapi.repository.TrnDtlDlytskVerifyRepository;
import com.springapi.service.TrnDtlDlytskVerifyService;
import java.util.List;
import java.util.Optional;
import javax.persistence.EntityManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author shahb
 */
@Service
@Transactional
public class TrnDtlDlytskVerifyServiceImpl implements TrnDtlDlytskVerifyService {

    static Logger logger = Logger.getLogger(InstituteTypeServiceImpl.class);

    @Autowired
    private TrnDtlDlytskVerifyRepository trnDtlDlytskVerifyRepository;

    @Autowired
    private EntityManager entityManager;

    @Override
    public TrnDtlDlytskVerify create(TrnDtlDlytskVerify trnDtlDlytskVerify) {
        logger.debug("create");
        return trnDtlDlytskVerifyRepository.save(trnDtlDlytskVerify);
    }

    @Override
    public List<TrnDtlDlytskVerify> list() {
        logger.debug("list");
        return trnDtlDlytskVerifyRepository.findAll();
    }

    @Override
    public TrnDtlDlytskVerify read(Long tId) {
        logger.debug("read");
        Optional<TrnDtlDlytskVerify> optional = trnDtlDlytskVerifyRepository.findById(tId);
        if (!optional.isPresent()) {
            throw new FASException(FASMessageType.ENTITY_NOT_FOUND);
        }
        return optional.get();
    }

    @Override
    public TrnDtlDlytskVerify update(TrnDtlDlytskVerify trnDtlDlytskVerify) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void delete(Long tId) {
        logger.debug("delete");
        Optional<TrnDtlDlytskVerify> optional = trnDtlDlytskVerifyRepository.findById(tId);
        if (!optional.isPresent()) {
            throw new FASException(FASMessageType.ENTITY_NOT_FOUND);
        }
        trnDtlDlytskVerifyRepository.delete(optional.get());
    }
}
