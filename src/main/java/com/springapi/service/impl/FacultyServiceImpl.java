package com.springapi.service.impl;

import com.springapi.dto.request.ChangePasswordRequest;
import com.springapi.dto.request.FacultyLStatusRequest;
import com.springapi.dto.request.FacultyLoginRequest;
import com.springapi.dto.request.FacultyRegistrationRequest;
import com.springapi.dto.request.ForgetPasswordRequest;
import com.springapi.dto.request.MASLoginRequest;
import com.springapi.dto.request.ResetPasswordRequest;
import com.springapi.dto.response.ChangePasswordResponse;
import com.springapi.dto.response.FacultyData;
import com.springapi.dto.response.GeneralListResponse;
import com.springapi.dto.response.InstituteWiseFacultyWithDepartment;
import com.springapi.dto.response.LoginResponse;
import com.springapi.dto.response.MessageResponse;
import com.springapi.enums.CommonFunction;
import com.springapi.enums.FASMessageType;
import com.springapi.exception.FASException;
import com.springapi.model.AndroidVersion;
import com.springapi.model.DepartmentMaster;
import com.springapi.model.Faculty;
import com.springapi.model.FacultyAttendance;
import com.springapi.model.FacultyShiftMpg;
import com.springapi.model.InstituteMaster;
import com.springapi.model.InstituteTypeMaster;
import com.springapi.model.QRCodeData;
import com.springapi.model.RoleMaster;
import com.springapi.model.ScheduleMaster;
import com.springapi.model.ShiftMaster;
import com.springapi.model.TermMaster;
import com.springapi.model.TrnFSPrgmSchd;
import com.springapi.repository.AndroidVersionRepository;
import com.springapi.repository.DepartmentRepository;
import com.springapi.repository.FacultyAttendanceRepository;
import com.springapi.repository.FacultyRepository;
import com.springapi.repository.FacultyShiftMpgRepository;
import com.springapi.repository.InstituteRepository;
import com.springapi.repository.InstituteTypeRepository;
import com.springapi.repository.InstituteWifiRepository;
import com.springapi.repository.QRCodeDataRepository;
import com.springapi.repository.RoleRepository;
import com.springapi.repository.ShiftRepository;
import com.springapi.repository.TermMasterRepository;
import com.springapi.repository.TrnDtlIssueMemoRepository;
import com.springapi.service.FacRoleMpgService;
import com.springapi.service.FacultyService;
import com.springapi.service.SendEmailService;
import com.springapi.util.CustDateFormat;
import com.springapi.util.RoleTypes;
import static java.lang.Boolean.FALSE;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Order;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author shahb
 */
@Service
@Transactional
public class FacultyServiceImpl implements FacultyService {

    @Autowired
    private FacultyRepository facultyRepository;

    @Autowired
    private QRCodeDataRepository qRCodeDataRepository;

    @Autowired
    private InstituteWifiRepository instituteWifiRepository;

    @Autowired
    private InstituteRepository instituteRepository;

    @Autowired
    private FacultyAttendanceRepository facultyAttendanceRepository;

    @Autowired
    private ShiftRepository shiftRepository;

    @Autowired
    private InstituteTypeRepository instituteTypeRepository;

    @Autowired
    private RoleRepository roleRepository;

    @Autowired
    private FacultyShiftMpgRepository facultyShiftMpgRepository;

    @Autowired
    private DepartmentRepository departmentRepository;

    @Autowired
    private TermMasterRepository termMasterRepository;

    @Autowired
    private AndroidVersionRepository androidVersionRepository;

    @Autowired
    private EntityManager entityManager;

    @Autowired
    private TrnDtlIssueMemoRepository trnDtlIssueMemoRepository;

    @Autowired
    private Environment env;

    @Autowired
    private SendEmailService sendEmailService;

    @Autowired
    private FacRoleMpgService facRoleMpgService;

    @Autowired
    private FacultyAttendanceServiceImpl facultyAttendanceServiceImpl;

    @Override
    public Faculty create(Faculty faculty) {
        Faculty faculty1 = facultyRepository.findByEmail(faculty.getEmail());
        if (faculty1 != null) {
            try {

                faculty1.setInstituteId(faculty.getInstituteId());
                faculty1.setDepartmentId(faculty.getDepartmentId());
                faculty1.setInstituteTypeId(faculty.getInstituteTypeId());
                faculty1.setName(faculty.getName());
                faculty1.setRole(faculty.getRole());
                faculty1.setPhone(faculty.getPhone());

                return facultyRepository.save(faculty1);
            } catch (Exception ex) {
                Logger.getLogger(FacultyServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return facultyRepository.save(faculty);

    }

    @Override
    public List<Faculty> list() {
        return facultyRepository.findAll();
    }

    @Override
    public Faculty read(Long fId) {

        Optional<Faculty> optional = facultyRepository.findById(fId);
        if (!optional.isPresent()) {
            throw new FASException(FASMessageType.ENTITY_NOT_FOUND);
        }
        return optional.get();
    }

    @Override
    public Faculty update(Faculty facultyDetails) {
        Faculty faculty = read(facultyDetails.getId());

        faculty.setEmail(facultyDetails.getEmail());
        faculty.setPassword(facultyDetails.getPassword());
        faculty.setPasswordHint(facultyDetails.getPasswordHint());
        faculty.setPhone(facultyDetails.getPhone());
        faculty.setRole(facultyDetails.getRole());
        faculty.setName(facultyDetails.getName());

        return facultyRepository.save(faculty);
    }

    @Override
    public Faculty delete(Long fId) {
        Faculty faculty = read(fId);
        faculty.setInstituteId(200L);
        faculty.setInstituteTypeId(3L);
        facultyRepository.save(faculty);
        return faculty;
    }

    @Override
    public Faculty register(FacultyRegistrationRequest facultyRegistrationRequest) {
        Faculty faculty = facultyRepository.findByEmail(facultyRegistrationRequest.getFacultyEmail());
        if (faculty != null) {
            try {
                throw new Exception("User Already Exist");
            } catch (Exception ex) {
                Logger.getLogger(FacultyServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
            }
        } else {
            faculty = new Faculty();
            faculty.setCreatedAt(new Date());
            faculty.setCreatedBy(1L);
            faculty.setEmail(facultyRegistrationRequest.getFacultyEmail());
            faculty.setPassword(facultyRegistrationRequest.getPassword());
            faculty.setPasswordHint(facultyRegistrationRequest.getPasswordHint());
            faculty.setName(facultyRegistrationRequest.getFacultyName());
            faculty.setRole(1L);

            return facultyRepository.save(faculty);
        }

        return null;
    }

    @Override
    public LoginResponse login(FacultyLoginRequest facultyLoginRequest) {
        LoginResponse loginResponse = new LoginResponse();
        System.out.println("login Request bssid ::" + facultyLoginRequest.getBssid());

        Optional<AndroidVersion> optional = androidVersionRepository.findByVersionNumberAndIsActive(facultyLoginRequest.getVersionCode(), Boolean.TRUE);
        if (!optional.isPresent()) {
            loginResponse.setMessage("Your Application runing on Lower Version! Plz Update on Playstore for Login!");
            loginResponse.setUserType("Faculty");
            loginResponse.setId(-1L);
            return loginResponse;
        }

        /*
        InstituteWifiMaster instituteWifiMaster = instituteWifiRepository.findByBssid(facultyLoginRequest.getBssid());
        if (instituteWifiMaster == null) {
            System.out.println("inside wifi not register");
            loginResponse.setMessage("You are not in registered Wifi Range ");
            loginResponse.setUserType("Faculty");
            loginResponse.setId(-1L);
            return loginResponse;
        }
         */
        String pattern = "yyyy-MM-dd";
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
        String date = simpleDateFormat.format(new Date());
        Faculty faculty = facultyRepository.findByEmailAndPassword(facultyLoginRequest.getEmail(), facultyLoginRequest.getPassword());
        if (faculty != null) {
            if (faculty.getIsDeleted()) {
                loginResponse.setMessage("User account deactivated...");
                loginResponse.setUserType("Faculty");
                loginResponse.setId(-1L);
                return loginResponse;
            }
            if (RoleTypes.isSpecialRole(faculty.getRole())) {
                loginResponse.setMessage("Invalid User Role...");
                loginResponse.setUserType("Faculty");
                loginResponse.setId(-1L);
                return loginResponse;
            }
            Optional<InstituteMaster> instituteMaster = instituteRepository.findById(faculty.getInstituteId());

            FacultyAttendance facultyAttendance = facultyAttendanceRepository.findByAttendanceDateAndFId(date, faculty.getId());
            if (facultyAttendance != null) {
                loginResponse.setInTime(facultyAttendance.getInTime());
                loginResponse.setOutTime(facultyAttendance.getOutTime());
            } else {
                loginResponse.setInTime("#LOCK#");
                loginResponse.setOutTime("#LOCK#");
            }
            loginResponse.setFaId(facultyAttendance.getId());
            loginResponse.setFacultyName(faculty.getName());
            loginResponse.setInstituteId(instituteMaster.get().getId());
            loginResponse.setInstituteName(instituteMaster.get().getName());
            loginResponse.setMessage("Successfully logged in");
            loginResponse.setUserType("Faculty");
            loginResponse.setId(faculty.getId());
            loginResponse.setCurrentDate(date);
            loginResponse.setIsActive(faculty.getActiveFlag());
            loginResponse.setUserRole(faculty.getRole());
            loginResponse.setIsLate(facultyAttendance.getIsLate());
            loginResponse.setIsEarly(facultyAttendance.getIsEarly());
            loginResponse.setIsStatus(facultyAttendance.getStatus());
            loginResponse.setPresentStatus(facultyAttendance.getPresenceStatus());
            loginResponse.setLeaveStatus(facultyAttendance.getLeaveStatus());

            if (faculty.getRole() == 3L) {
                loginResponse.setIsMemo(trnDtlIssueMemoRepository.existsByInstituteIdAndIsDeletedFalseAndIsEndorseFalse(faculty.getInstituteId()));
            } else {
                loginResponse.setIsMemo(trnDtlIssueMemoRepository.existsByFIdAndIsDeletedFalseAndIsSubmittedFalse(faculty.getId()));
            }

            return loginResponse;
        }
        loginResponse.setIsMemo(Boolean.TRUE);
        loginResponse.setMessage("Invalid Email / Password");
        loginResponse.setUserType("Faculty");
        loginResponse.setId(-1L);
        return loginResponse;
    }

    @Override
    public LoginResponse loginByDevice(MASLoginRequest masLoginRequest) {
        LoginResponse loginResponse = new LoginResponse();

        String pattern = "yyyy-MM-dd";
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
        String date = simpleDateFormat.format(new Date());
        Faculty faculty = facultyRepository.findByEmailAndPassword(masLoginRequest.getEmail(), masLoginRequest.getPassword());
        if (faculty != null || loginResponse.getActLogId() != null) {
//            if (faculty.getIsDeleted()) {
//                loginResponse.setMessage("User account deactivated...");
//                loginResponse.setUserType("Faculty");
//                loginResponse.setId(-1L);
//                return loginResponse;
//            }
//            if (RoleTypes.isSpecialRole(faculty.getRole())) {
//                loginResponse.setMessage("Invalid User Role...");
//                loginResponse.setUserType("Faculty");
//                loginResponse.setId(-1L);
//                return loginResponse;
//            }
            Optional<InstituteMaster> instituteMaster = instituteRepository.findById(faculty.getInstituteId());

            FacultyAttendance facultyAttendance = facultyAttendanceRepository.findByAttendanceDateAndFId(date, faculty.getId());
//            if (facultyAttendance != null) {
//                loginResponse.setInTime(facultyAttendance.getInTime());
//                loginResponse.setOutTime(facultyAttendance.getOutTime());
//            } else {
//                loginResponse.setInTime("#LOCK#");
//                loginResponse.setOutTime("#LOCK#");
//            }
            long errorId = 100L;
            if (errorId == 201L) {
                loginResponse.setErrorStatus(201L, "Staff is Block");
                return loginResponse;
            }
            if (errorId == 202L) {
                loginResponse.setErrorStatus(202L, "Device is Block");
                return loginResponse;
            }
            if (errorId == 203L) {
                loginResponse.setErrorStatus(203L, "For Block");
                loginResponse.setIsReqForBlock(Boolean.TRUE);
                loginResponse.setBlockUserList(null);
                /*
                        Set by Query
                    
                 */
                return loginResponse;
            }

            loginResponse.setFaId(facultyAttendance.getId());
            loginResponse.setFacultyName(faculty.getName());
            loginResponse.setInstituteId(instituteMaster.get().getId());
            loginResponse.setInstituteName(instituteMaster.get().getName());
            loginResponse.setMessage("Successfully logged in");
            loginResponse.setUserType("Faculty");
            loginResponse.setId(faculty.getId());
            loginResponse.setCurrentDate(date);
            loginResponse.setIsActive(faculty.getActiveFlag());
            loginResponse.setUserRole(faculty.getRole());
            loginResponse.setIsLate(facultyAttendance.getIsLate());
            loginResponse.setIsEarly(facultyAttendance.getIsEarly());
            loginResponse.setIsStatus(facultyAttendance.getStatus());
            loginResponse.setPresentStatus(facultyAttendance.getPresenceStatus());
            loginResponse.setLeaveStatus(facultyAttendance.getLeaveStatus());

            loginResponse.setStaftDepartments(facRoleMpgService.activeRolelistByFId(faculty.getId()));
            loginResponse.setIsMemo(trnDtlIssueMemoRepository.existsByFIdAndIsDeletedFalseAndIsSubmittedFalse(faculty.getId()));

            return loginResponse;
        }

        loginResponse.setIsMemo(Boolean.TRUE);
        loginResponse.setMessage("Invalid Email / Password");
        loginResponse.setUserType("Faculty");
        loginResponse.setId(-1L);
        return loginResponse;
    }

    @Override
    public QRCodeData generateQRCode() {

        String qrCode = "GMCA@" + new Date();
        System.out.println("QR code ::" + qrCode);
        // Convert Today's Date into string
        String pattern = "yyyy-MM-dd";
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
        String date = simpleDateFormat.format(new Date());
        System.out.println(date);

        QRCodeData qrCodeDate = new QRCodeData();
        qrCodeDate.setCreatedAt(new Date());
        qrCodeDate.setQrCode(qrCode);

        return qRCodeDataRepository.save(qrCodeDate);
    }

    @Override
    public List<Faculty> getFacultyByInstitute(Long instituteId) {
        //logger.debug("getFacultyByInstitute");
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery criteriaQuery = criteriaBuilder.createQuery(Faculty.class);

        // Prepare list query
        prepareListFacultyQuery(criteriaQuery, criteriaBuilder, false, instituteId);

        TypedQuery typedQuery = entityManager.createQuery(criteriaQuery);
        List<Faculty> results = typedQuery.getResultList();
        for (Faculty f : results) {
            String Name = CommonFunction.setFormalName(f);
            f.setfName(Name);
            f.setName("[" + f.getDepartmentName().trim() + "] " + Name);

        }

        System.out.println("result::" + results);
        return results;
    }

    @Override
    public List<Faculty> getFacultyByHoD(Long instituteId, Long fId) {
        //logger.debug("getFacultyByInstitute");
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery criteriaQuery = criteriaBuilder.createQuery(Faculty.class);

        // Prepare list query
        prepareListFacultyQuery(criteriaQuery, criteriaBuilder, false, instituteId);

        TypedQuery typedQuery = entityManager.createQuery(criteriaQuery);
        List<Faculty> results = typedQuery.getResultList();

        //Other Business Logic
        /*
        
            @Hemant Department Code
        
         */
        for (Faculty f : results) {
            String Name = CommonFunction.setFormalName(f);
            f.setfName(Name);
            f.setName("[" + f.getDepartmentName().trim() + "] " + Name);

        }

        String reportInTime = CustDateFormat.todayTime();

        String times[] = reportInTime.split(":");
        int hours = Integer.parseInt(times[0]);
        int minute = Integer.parseInt(times[1]);

        if (hours > 11 || (hours == 11 && minute > 15)) {
            return null;
        }

        System.out.println("result::" + results);
        return results;
    }

    private void prepareListFacultyQuery(CriteriaQuery criteriaQuery, CriteriaBuilder criteriaBuilder, boolean countQuery, Long instituteId) {

        Root<Faculty> facultyRoot = criteriaQuery.from(Faculty.class);
        Root<FacultyAttendance> facAttenRoot = criteriaQuery.from(FacultyAttendance.class);
        Root<DepartmentMaster> dRoot = criteriaQuery.from(DepartmentMaster.class);
        if (countQuery) {
            criteriaQuery.select(criteriaBuilder.count(facultyRoot));
        } else {
            criteriaQuery.multiselect(
             facultyRoot.get("id"), facultyRoot.get("name"), dRoot.get("name"),
             facAttenRoot.get("status"), facultyRoot.get("fName"), facultyRoot.get("mName"), facultyRoot.get("lName"), facultyRoot.get("activeFlag")
            );
            List<Order> orders = new ArrayList<>();
            orders.add(criteriaBuilder.asc(dRoot.get("name")));
            orders.add(criteriaBuilder.asc(facultyRoot.get("name")));
            if (!orders.isEmpty()) {
                criteriaQuery.orderBy(orders);
            }
        }
        String pattern = "yyyy-MM-dd";
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
        String date = simpleDateFormat.format(new Date());

        ArrayList<Predicate> predicateList = new ArrayList<>();
        predicateList.add(criteriaBuilder.equal(facultyRoot.get("id"), facAttenRoot.get("fId")));
        predicateList.add(criteriaBuilder.equal(dRoot.get("id"), facultyRoot.get("departmentId")));
        predicateList.add(criteriaBuilder.equal(facAttenRoot.get("instituteId"), instituteId));
        predicateList.add(criteriaBuilder.equal(facAttenRoot.get("attendanceDate"), date));
        predicateList.add(criteriaBuilder.equal(facAttenRoot.get("status"), false));
        predicateList.add(criteriaBuilder.notEqual(facAttenRoot.get("isDeleted"), Boolean.TRUE));
        Predicate[] predicates = new Predicate[predicateList.size()];
        predicateList.toArray(predicates);
        criteriaQuery.where(predicates);
    }

    @Override
    public LoginResponse login(String email, String password, String bssid, String ssid) {

        //Doing Task
        LoginResponse loginResponse = new LoginResponse();
//        InstituteWifiMaster instituteWifiMaster = instituteWifiRepository.findByBssid(bssid);
//        if (instituteWifiMaster == null) {
//            loginResponse.setMessage("You are not in registered Wifi Range ");
//            loginResponse.setId(-1L);
//            return loginResponse;
//        }
        Faculty faculty = facultyRepository.findByEmailAndPassword(email, password);
        if (faculty != null) {
            loginResponse.setMessage("Successfully logged in");
            loginResponse.setId(faculty.getId());
            loginResponse.setUserRole(faculty.getRole());
            return loginResponse;
        }
        loginResponse.setMessage("Invalid Email or Password");
        loginResponse.setId(-1L);
        return loginResponse;
    }

    @Override
    public LoginResponse loginUser(FacultyLoginRequest facultyLoginRequest) {
        LoginResponse loginResponse = new LoginResponse();

        Faculty faculty = facultyRepository.findByEmailAndPassword(facultyLoginRequest.getEmail(), facultyLoginRequest.getPassword());
        if (faculty == null) {
            try {
                throw new Exception("Invalid username/password");
            } catch (Exception ex) {
                Logger.getLogger(FacultyServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        loginResponse.setMessage("Successfully logged in");
        loginResponse.setId(faculty.getId());
        loginResponse.setUserRole(faculty.getRole());
        loginResponse.setIsActive(faculty.getActiveFlag());
        loginResponse.setIsMemo(Boolean.TRUE);
        return loginResponse;
    }

    @Override
    public ChangePasswordResponse changePassword(ChangePasswordRequest changePasswordRequest) {
        ChangePasswordResponse changePasswordResponse = new ChangePasswordResponse();
        Faculty faculty = null;
        if (changePasswordRequest.getfId() == null) {
            faculty = facultyRepository.findByEmail(changePasswordRequest.getEmail());
            if (faculty == null) {
                changePasswordResponse.setMessage("Invalid Email Address");
                return changePasswordResponse;
            }
        } else {
            Optional<Faculty> optional = facultyRepository.findById(changePasswordRequest.getfId());
            faculty = optional.get();
            if (faculty == null) {
                changePasswordResponse.setMessage("Invalid faculty");
                return changePasswordResponse;
            }
        }
        if (!(faculty.getPassword().equals(changePasswordRequest.getOldPassword()))) {
            changePasswordResponse.setMessage("Invaild Old Password");
            return changePasswordResponse;
        }

        faculty.setPassword(changePasswordRequest.getNewPassword());
        facultyRepository.save(faculty);
        changePasswordResponse.setMessage("Password updated successfully");
        changePasswordResponse.setNewPassword(changePasswordRequest.getNewPassword());
        return changePasswordResponse;
    }

    @Override
    public List<Faculty> getFacultyByDepartmentId(Long departmentId) {
        return facultyRepository.findAllByDepartmentId(departmentId);
    }

    @Override
    public List<Faculty> getFacultyListByInstitute(Long instituteId) {
        List<Faculty> facultys = facultyRepository.findAllByInstituteIdAndIsDeletedFalse(instituteId);
        if (facultys.size() > 0) {
            for (Faculty f : facultys) {
                f.setName(CommonFunction.setFormalName(f));
            }

            Collections.sort(facultys, new Comparator<Faculty>() {
                @Override
                public int compare(final Faculty object1, final Faculty object2) {
                    return object1.getName().compareTo(object2.getName());
                }
            });
        }
        for (Faculty f : facultys) {
            if (departmentRepository.findById(f.getDepartmentId()).isPresent()) {

                f.setDepartmentName(departmentRepository.findById(f.getDepartmentId()).get().getName());
            }
        }
        return facultys;
    }

    @Override
    public String forgetPassword(ForgetPasswordRequest forgetPasswordRequest) {
        try {
            System.out.println("1 inside try !!");
            sendEmailService.sendEmail(forgetPasswordRequest.getEmail());
        } catch (Exception ex) {
            Logger.getLogger(FacultyServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
        }

        return "Please check your mail !!!";
    }

    @Override
    public FacultyData readAllData(Long fId) {
        // logger.debug("facultyAttendanceList");
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery criteriaQuery = criteriaBuilder.createQuery(FacultyData.class);
        prepareListQuery(criteriaQuery, criteriaBuilder, fId);
        TypedQuery typedQuery = entityManager.createQuery(criteriaQuery);
        FacultyData results = (FacultyData) typedQuery.getSingleResult();
        results.setInTime(facultyShiftTiming(results.getFId()));
        FacultyAttendance facultyAttendance = facultyAttendanceServiceImpl.readByFacultyId(results.getFId());
        if (facultyAttendance != null) {
            if (facultyAttendance.getActualInTime().equalsIgnoreCase("00:01") || facultyAttendance.getActualInTime().equalsIgnoreCase("00:02") || facultyAttendance.getActualInTime().equalsIgnoreCase("00:03")) {
                results.setActInTime("-");
            } else {
                results.setActInTime(facultyAttendance.getActualInTime() + " - " + facultyAttendance.getActualOutTime());
            }
            results.setOutTime("");
        } else {
            results.setActInTime("");
        }
        /*        Optional<Faculty> optional = facultyRepository.findById(fId);
        if (!optional.isPresent()) {
            throw new FASException(FASMessageType.ENTITY_NOT_FOUND);
        }
        Faculty faculty = optional.get();
        FacultyData facultyData = new FacultyData();
        facultyData.setEmail(faculty.getEmaiFacultyDatal());
        facultyData.setSalutationName(faculty.getSalutationName());
        facultyData.setName(faculty.getName());
        facultyData.setfId(faculty.getId());
        facultyData.setActiveFlag(faculty.getActiveFlag());
        facultyData.setiSDeleted(faculty.getiSDeleted());
        facultyData.setDepartmentId(faculty.getDepartmentId());
        facultyData.setInstituteId(faculty.getInstituteId());
        facultyData.setInstituteTypeId(faculty.getInstituteTypeId());
        Optional<InstituteMaster> instituteMaster = instituteRepository.findById(faculty.getInstituteId());
        if (instituteMaster.isPresent()) {
            facultyData.setInstituteName(instituteMaster.get().getName());

        }

        Optional<InstituteTypeMaster> instituteTypeMaster = instituteTypeRepository.findById(faculty.getInstituteTypeId());
        if (instituteTypeMaster.isPresent()) {
            facultyData.setInstituteTypeName(instituteTypeMaster.get().getName());
        }
        Optional<DepartmentMaster> departmentMaster = departmentRepository.findById(faculty.getDepartmentId());
        if (departmentMaster.isPresent()) {
            facultyData.setDepartmentName(departmentMaster.get().getName());
        }

        Optional<RoleMaster> rOptional = roleRepository.findById(faculty.getRole());
        if (departmentMaster.isPresent()) {
            facultyData.setRoleName(rOptional.get().getRole());
        }
        Optional<FacultyShiftMpg> fOptional = facultyShiftMpgRepository.findByFId(faculty.getId());
        if (fOptional.isPresent()) {

            ShiftMaster shiftMaster = shiftRepository.findById(fOptional.get().getShiftId());
            facultyData.setInTime(shiftMaster.getInTime());
            facultyData.setOutTime(shiftMaster.getOutTime());
            Optional<TermMaster> tOptional = termMasterRepository.findById(fOptional.get().getTermId());
            if (tOptional.isPresent()) {
                  facultyData.setTermStartDate(tOptional.get().getTermStartDate());
                  facultyData.setTermEndDate(tOptional.get().getTermEndDate());
                  facultyData.setTermYear(tOptional.get().getTermYear());
            }
        }
            facultyData.setRole(faculty.getRole());
            facultyData.setPhone(faculty.getPhone());

            return facultyData;
        }*/
        return results;
    }

    public FacultyData readAllDatafoNotice(Long fId) {
        // logger.debug("facultyAttendanceList");
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery criteriaQuery = criteriaBuilder.createQuery(FacultyData.class);
        prepareListQuery(criteriaQuery, criteriaBuilder, fId);
        TypedQuery typedQuery = entityManager.createQuery(criteriaQuery);
        FacultyData results = (FacultyData) typedQuery.getSingleResult();

        /*        
        
        Optional<Faculty> optional = facultyRepository.findById(fId);
        if (!optional.isPresent()) {
            throw new FASException(FASMessageType.ENTITY_NOT_FOUND);
        }
        Faculty faculty = optional.get();
        FacultyData facultyData = new FacultyData();
        facultyData.setEmail(faculty.getEmaiFacultyDatal());
        facultyData.setSalutationName(faculty.getSalutationName());
        facultyData.setName(faculty.getName());
        facultyData.setfId(faculty.getId());
        facultyData.setActiveFlag(faculty.getActiveFlag());
        facultyData.setiSDeleted(faculty.getiSDeleted());
        facultyData.setDepartmentId(faculty.getDepartmentId());
        facultyData.setInstituteId(faculty.getInstituteId());
        facultyData.setInstituteTypeId(faculty.getInstituteTypeId());
        Optional<InstituteMaster> instituteMaster = instituteRepository.findById(faculty.getInstituteId());
        if (instituteMaster.isPresent()) {
            facultyData.setInstituteName(instituteMaster.get().getName());

        }

        Optional<InstituteTypeMaster> instituteTypeMaster = instituteTypeRepository.findById(faculty.getInstituteTypeId());
        if (instituteTypeMaster.isPresent()) {
            facultyData.setInstituteTypeName(instituteTypeMaster.get().getName());
        }
        Optional<DepartmentMaster> departmentMaster = departmentRepository.findById(faculty.getDepartmentId());
        if (departmentMaster.isPresent()) {
            facultyData.setDepartmentName(departmentMaster.get().getName());
        }

        Optional<RoleMaster> rOptional = roleRepository.findById(faculty.getRole());
        if (departmentMaster.isPresent()) {
            facultyData.setRoleName(rOptional.get().getRole());
        }
        Optional<FacultyShiftMpg> fOptional = facultyShiftMpgRepository.findByFId(faculty.getId());
        if (fOptional.isPresent()) {

            ShiftMaster shiftMaster = shiftRepository.findById(fOptional.get().getShiftId());
            facultyData.setInTime(shiftMaster.getInTime());
            facultyData.setOutTime(shiftMaster.getOutTime());
            Optional<TermMaster> tOptional = termMasterRepository.findById(fOptional.get().getTermId());
            if (tOptional.isPresent()) {
                  facultyData.setTermStartDate(tOptional.get().getTermStartDate());
                  facultyData.setTermEndDate(tOptional.get().getTermEndDate());
                  facultyData.setTermYear(tOptional.get().getTermYear());
            }
        }
            facultyData.setRole(faculty.getRole());
            facultyData.setPhone(faculty.getPhone());

            return facultyData;
        }*/
        return results;
    }

    private void prepareListQuery(CriteriaQuery criteriaQuery, CriteriaBuilder criteriaBuilder, Long fId) {
        Root<Faculty> facultyAttenRoot = criteriaQuery.from(Faculty.class);

        Root<InstituteMaster> iRoot = criteriaQuery.from(InstituteMaster.class);
        Root<InstituteTypeMaster> itRoot = criteriaQuery.from(InstituteTypeMaster.class);
        Root<DepartmentMaster> dRoot = criteriaQuery.from(DepartmentMaster.class);
        Root<RoleMaster> roleRoot = criteriaQuery.from(RoleMaster.class);
        Root<ShiftMaster> sRoot = criteriaQuery.from(ShiftMaster.class);
        Root<TermMaster> tRoot = criteriaQuery.from(TermMaster.class);
        Root<FacultyShiftMpg> fsRoot = criteriaQuery.from(FacultyShiftMpg.class);

        criteriaQuery.multiselect(
         facultyAttenRoot.get("id"), facultyAttenRoot.get("name"), facultyAttenRoot.get("email"),
         facultyAttenRoot.get("phone"), facultyAttenRoot.get("role"), facultyAttenRoot.get("instituteId"),
         facultyAttenRoot.get("instituteTypeId"), facultyAttenRoot.get("departmentId"), facultyAttenRoot.get("isDeleted"),
         facultyAttenRoot.get("designationId"), facultyAttenRoot.get("salutationName"), facultyAttenRoot.get("activeFlag"),
         iRoot.get("name"), itRoot.get("name"), dRoot.get("name"),
         sRoot.get("inTime"), sRoot.get("outTime"), roleRoot.get("role"),
         tRoot.get("termStartDate"), tRoot.get("termEndDate"), tRoot.get("termYear"), facultyAttenRoot.get("fName"),
         facultyAttenRoot.get("mName"), facultyAttenRoot.get("lName"), facultyAttenRoot.get("classId"));

        ArrayList<Predicate> predicateList = new ArrayList<>();
        predicateList.add(criteriaBuilder.equal(facultyAttenRoot.get("instituteId"), iRoot.get("id")));
        predicateList.add(criteriaBuilder.equal(facultyAttenRoot.get("instituteTypeId"), itRoot.get("id")));
        predicateList.add(criteriaBuilder.equal(facultyAttenRoot.get("departmentId"), dRoot.get("id")));
        predicateList.add(criteriaBuilder.equal(facultyAttenRoot.get("role"), roleRoot.get("id")));
        predicateList.add(criteriaBuilder.equal(fsRoot.get("shiftId"), sRoot.get("id")));
        predicateList.add(criteriaBuilder.equal(tRoot.get("id"), fsRoot.get("termId")));
        predicateList.add(criteriaBuilder.equal(facultyAttenRoot.get("id"), fsRoot.get("fId")));
        predicateList.add(criteriaBuilder.equal(fsRoot.get("isActive"), Boolean.TRUE));

        if (fId != null) {
            predicateList.add(criteriaBuilder.equal(facultyAttenRoot.get("id"), fId));
        }

        Predicate[] predicates = new Predicate[predicateList.size()];
        predicateList.toArray(predicates);
        criteriaQuery.where(predicates);

// throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    private void prepareListQueryMEMO(CriteriaQuery criteriaQuery, CriteriaBuilder criteriaBuilder, Long fId) {
        Root<Faculty> facultyAttenRoot = criteriaQuery.from(Faculty.class);
        Root<InstituteMaster> iRoot = criteriaQuery.from(InstituteMaster.class);
        Root<DepartmentMaster> dRoot = criteriaQuery.from(DepartmentMaster.class);

        criteriaQuery.multiselect(
         facultyAttenRoot.get("id"), facultyAttenRoot.get("name"), facultyAttenRoot.get("email"),
         facultyAttenRoot.get("phone"), facultyAttenRoot.get("role"), facultyAttenRoot.get("instituteId"),
         facultyAttenRoot.get("instituteTypeId"), facultyAttenRoot.get("departmentId"), facultyAttenRoot.get("isDeleted"),
         facultyAttenRoot.get("designationId"), facultyAttenRoot.get("salutationName"), facultyAttenRoot.get("activeFlag"),
         iRoot.get("name"), null, dRoot.get("name"), null, null, null, null, null, null, facultyAttenRoot.get("fName"),
         facultyAttenRoot.get("mName"), facultyAttenRoot.get("lName"), facultyAttenRoot.get("classId"));

        ArrayList<Predicate> predicateList = new ArrayList<>();
        predicateList.add(criteriaBuilder.equal(facultyAttenRoot.get("instituteId"), iRoot.get("id")));
        predicateList.add(criteriaBuilder.equal(facultyAttenRoot.get("departmentId"), dRoot.get("id")));

        if (fId != null) {
            predicateList.add(criteriaBuilder.equal(facultyAttenRoot.get("id"), fId));
        }

        Predicate[] predicates = new Predicate[predicateList.size()];
        predicateList.toArray(predicates);
        criteriaQuery.where(predicates);

// throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    private void prepareListforNoticeQuery(CriteriaQuery criteriaQuery, CriteriaBuilder criteriaBuilder, Long fId) {

        Root<Faculty> facultyAttenRoot = criteriaQuery.from(Faculty.class);
        Root<InstituteMaster> iRoot = criteriaQuery.from(InstituteMaster.class);
        Root<InstituteTypeMaster> itRoot = criteriaQuery.from(InstituteTypeMaster.class);
        Root<DepartmentMaster> dRoot = criteriaQuery.from(DepartmentMaster.class);

        criteriaQuery.multiselect(
         facultyAttenRoot.get("id"), facultyAttenRoot.get("name"), facultyAttenRoot.get("email"),
         facultyAttenRoot.get("phone"), facultyAttenRoot.get("role"), facultyAttenRoot.get("instituteId"),
         facultyAttenRoot.get("instituteTypeId"), facultyAttenRoot.get("departmentId"), facultyAttenRoot.get("isDeleted"),
         iRoot.get("name"), itRoot.get("name"), dRoot.get("name"), facultyAttenRoot.get("fName"),
         facultyAttenRoot.get("mName"), facultyAttenRoot.get("lName"));

        ArrayList<Predicate> predicateList = new ArrayList<>();
        predicateList.add(criteriaBuilder.equal(facultyAttenRoot.get("instituteId"), iRoot.get("id")));
        predicateList.add(criteriaBuilder.equal(facultyAttenRoot.get("instituteTypeId"), itRoot.get("id")));
        predicateList.add(criteriaBuilder.equal(facultyAttenRoot.get("departmentId"), dRoot.get("id")));
        predicateList.add(criteriaBuilder.equal(facultyAttenRoot.get("isDeleted"), FALSE));

        if (fId != null) {
            predicateList.add(criteriaBuilder.equal(facultyAttenRoot.get("id"), fId));
        }

        Predicate[] predicates = new Predicate[predicateList.size()];
        predicateList.toArray(predicates);
        criteriaQuery.where(predicates);

// throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public ChangePasswordResponse resetPassword(ResetPasswordRequest resetPasswordRequest) {
        ChangePasswordResponse changePasswordResponse = new ChangePasswordResponse();
        Faculty faculty = null;

        faculty = facultyRepository.findByEmail(resetPasswordRequest.getEmail());
        if (faculty != null) {
            faculty.setPassword("12345");
            faculty.setPhone(resetPasswordRequest.getPhone());
            facultyRepository.save(faculty);
            changePasswordResponse.setMessage("Password updated successfully");
        } else {
            changePasswordResponse.setMessage("Invaild Email Address");
        }

        changePasswordResponse.setNewPassword("12345");
        return changePasswordResponse;

    }

    @Override
    public Faculty editdata(Faculty facultyDetails) {
        MessageResponse messageResponse = new MessageResponse();

        Faculty faculty = read(facultyDetails.getId());
        if (faculty == null) {
            messageResponse.setIsError(Boolean.TRUE);
            messageResponse.setMessage("Invalid User Information!");
            return null;
        }
        if (facultyDetails.getPhone() != null) {
            faculty.setPhone(facultyDetails.getPhone());
        }
        if (facultyDetails.getName() != null) {
            faculty.setName(facultyDetails.getName());
        }
        if (facultyDetails.getDepartmentId() != null) {
            faculty.setDepartmentId(facultyDetails.getDepartmentId());
        }
        if (facultyDetails.getDesignationId() != null) {
            faculty.setDesignationId(facultyDetails.getDesignationId());
        }
        if (facultyDetails.getClassId() != null) {
            faculty.setClassId(facultyDetails.getClassId());
        }
        if (facultyDetails.getfName() != null) {
            faculty.setfName(facultyDetails.getfName());
        }
        if (facultyDetails.getmName() != null) {
            faculty.setmName(facultyDetails.getmName());
        }
        if (facultyDetails.getlName() != null) {
            faculty.setlName(facultyDetails.getlName());
        }
        if (facultyDetails.getSalutationName() != null) {
            faculty.setSalutationName(facultyDetails.getSalutationName());
        }
        faculty.setActiveFlag(Boolean.TRUE);
        facultyRepository.save(faculty);
        messageResponse.setIsError(Boolean.FALSE);
        messageResponse.setMessage("User Data Updated Successfully!");
        return faculty;

        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Faculty> getFacultyListByInstituteDept(Long instituteId, Long departmentId) {

        Optional<InstituteMaster> iOptional = instituteRepository.findById(instituteId);
        if (!iOptional.isPresent()) {
            return null;
        }
        List<Faculty> facultys = new ArrayList<>();
        if (iOptional.get().getInstituteTypeId() == 1L) {

            facultys = facultyRepository.findAllByInstituteIdAndDepartmentId(instituteId, departmentId);
            facultys.addAll(facultyRepository.findAllByInstituteIdAndDepartmentId(instituteId, 48L));

        } else {
            facultys = facultyRepository.findAllByInstituteIdAndRoleNot(instituteId, 8L);
        }
        List<Faculty> facultys1 = finishingSchoolScheduler(instituteId);
        if (facultys1.size() > 0) {
            facultys.addAll(facultys1);
        }
        if (facultys.size() > 0) {
            for (Faculty f : facultys) {
                if (f.getfName() != null) {
                    if (f.getmName() == null) {
                        f.setName(f.getfName() + " " + f.getlName());
                    } else {
                        f.setName(f.getfName() + " " + f.getmName() + " " + f.getlName());
                    }
                }

            }

            Collections.sort(facultys, new Comparator<Faculty>() {
                @Override
                public int compare(final Faculty object1, final Faculty object2) {
                    return object1.getName().compareTo(object2.getName());
                }
            });
        }

        return facultys;
    }

    @Override
    public List<Faculty> getFacultyPresentByInstitute(Long instituteId) {

        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery criteriaQuery = criteriaBuilder.createQuery(Faculty.class);

        // Prepare list query
        preparePresentListFacultyQuery(criteriaQuery, criteriaBuilder, false, instituteId);

        TypedQuery typedQuery = entityManager.createQuery(criteriaQuery);
        List<Faculty> results = typedQuery.getResultList();
        for (Faculty f : results) {
            String Name = CommonFunction.setFormalName(f);
            f.setfName(Name);
            f.setName("[" + f.getDepartmentName().trim() + "] " + Name);
        }
        System.out.println("result::" + results);
        return results;
    }

    @Override
    public List<Faculty> getFacultyPresentByHoD(Long instituteId, Long fId) {

        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery criteriaQuery = criteriaBuilder.createQuery(Faculty.class);

        // Prepare list query
        preparePresentListFacultyQuery(criteriaQuery, criteriaBuilder, false, instituteId);

        TypedQuery typedQuery = entityManager.createQuery(criteriaQuery);
        List<Faculty> results = typedQuery.getResultList();
        /*
                @Hemant Department Code 
         */
        for (Faculty f : results) {
            String Name = CommonFunction.setFormalName(f);
            f.setfName(Name);
            f.setName("[" + f.getDepartmentName().trim() + "] " + Name);
        }
        System.out.println("result::" + results);
        return results;
    }

    @Override
    public List<Faculty> getFacultyStatusByInstitute(Long instituteId) {

        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery criteriaQuery = criteriaBuilder.createQuery(Faculty.class);

        // Prepare list query
        prepareStatusListFacultyQuery(criteriaQuery, criteriaBuilder, false, instituteId);

        TypedQuery typedQuery = entityManager.createQuery(criteriaQuery);
        List<Faculty> results = typedQuery.getResultList();
        for (Faculty f : results) {
            String Name = CommonFunction.setFormalName(f);
            f.setfName(Name);
            f.setName("[" + f.getDepartmentName().trim() + "] " + Name);
        }
        System.out.println("result::" + results);
        return results;
    }

    @Override
    public List<Faculty> getFacultyStatusByHoD(Long instituteId, Long fId) {

        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery criteriaQuery = criteriaBuilder.createQuery(Faculty.class);

        // Prepare list query
        prepareStatusListFacultyQuery(criteriaQuery, criteriaBuilder, false, instituteId);

        TypedQuery typedQuery = entityManager.createQuery(criteriaQuery);
        List<Faculty> results = typedQuery.getResultList();
        /*
                @Hemant Department Code 
         */
        for (Faculty f : results) {
            String Name = CommonFunction.setFormalName(f);
            f.setfName(Name);
            f.setName("[" + f.getDepartmentName().trim() + "] " + Name);
        }
        System.out.println("result::" + results);
        return results;
    }

    private void preparePresentListFacultyQuery(CriteriaQuery criteriaQuery, CriteriaBuilder criteriaBuilder, boolean countQuery, Long instituteId) {

        Root<Faculty> facultyRoot = criteriaQuery.from(Faculty.class);
        Root<FacultyAttendance> facAttenRoot = criteriaQuery.from(FacultyAttendance.class);
        Root<DepartmentMaster> dRoot = criteriaQuery.from(DepartmentMaster.class);
        if (countQuery) {
            criteriaQuery.select(criteriaBuilder.count(facultyRoot));
        } else {
            criteriaQuery.multiselect(
             facultyRoot.get("id"), facultyRoot.get("name"), dRoot.get("name"), facAttenRoot.get("presenceStatus"), facAttenRoot.get("leaveStatus"), facultyRoot.get("fName"), facultyRoot.get("mName"), facultyRoot.get("lName")
            );
            List<Order> orders = new ArrayList<>();
            orders.add(criteriaBuilder.asc(dRoot.get("name")));
            orders.add(criteriaBuilder.asc(facultyRoot.get("name")));
            if (!orders.isEmpty()) {
                criteriaQuery.orderBy(orders);
            }
        }

        String pattern = "yyyy-MM-dd";
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
        String date = simpleDateFormat.format(new Date());

        ArrayList<Predicate> predicateList = new ArrayList<>();
        predicateList.add(criteriaBuilder.equal(facultyRoot.get("id"), facAttenRoot.get("fId")));
        predicateList.add(criteriaBuilder.equal(dRoot.get("id"), facultyRoot.get("departmentId")));
        predicateList.add(criteriaBuilder.equal(facAttenRoot.get("instituteId"), instituteId));
        predicateList.add(criteriaBuilder.equal(facAttenRoot.get("attendanceDate"), date));
        predicateList.add(criteriaBuilder.equal(facAttenRoot.get("isPrincipalFilled"), true));
        predicateList.add(criteriaBuilder.notEqual(facAttenRoot.get("isDeleted"), Boolean.TRUE));
        Predicate[] predicates = new Predicate[predicateList.size()];
        predicateList.toArray(predicates);
        criteriaQuery.where(predicates);
    }

    private void prepareStatusListFacultyQuery(CriteriaQuery criteriaQuery, CriteriaBuilder criteriaBuilder, boolean countQuery, Long instituteId) {

        Root<Faculty> facultyRoot = criteriaQuery.from(Faculty.class);
        Root<FacultyAttendance> facAttenRoot = criteriaQuery.from(FacultyAttendance.class);
        Root<DepartmentMaster> dRoot = criteriaQuery.from(DepartmentMaster.class);
        if (countQuery) {
            criteriaQuery.select(criteriaBuilder.count(facultyRoot));
        } else {
            criteriaQuery.multiselect(
             facultyRoot.get("id"), facultyRoot.get("name"), dRoot.get("name"), facAttenRoot.get("presenceStatus"), facAttenRoot.get("leaveStatus"), facultyRoot.get("fName"), facultyRoot.get("mName"), facultyRoot.get("lName"), facAttenRoot.get("isLate"), facAttenRoot.get("isEarly")
            );
            List<Order> orders = new ArrayList<>();
            orders.add(criteriaBuilder.asc(dRoot.get("name")));
            orders.add(criteriaBuilder.asc(facultyRoot.get("name")));
            if (!orders.isEmpty()) {
                criteriaQuery.orderBy(orders);
            }
        }

        String pattern = "yyyy-MM-dd";
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
        String date = simpleDateFormat.format(new Date());

        ArrayList<Predicate> predicateList = new ArrayList<>();
        predicateList.add(criteriaBuilder.equal(facultyRoot.get("id"), facAttenRoot.get("fId")));
        predicateList.add(criteriaBuilder.equal(dRoot.get("id"), facultyRoot.get("departmentId")));
        predicateList.add(criteriaBuilder.equal(facAttenRoot.get("instituteId"), instituteId));
        predicateList.add(criteriaBuilder.equal(facAttenRoot.get("attendanceDate"), date));
        //predicateList.add(criteriaBuilder.equal(facAttenRoot.get("isPrincipalFilled"), true));
        predicateList.add(criteriaBuilder.notEqual(facAttenRoot.get("isDeleted"), Boolean.TRUE));
        Predicate[] predicates = new Predicate[predicateList.size()];
        predicateList.toArray(predicates);
        criteriaQuery.where(predicates);
    }

    public List<Faculty> finishingSchoolScheduler(Long institute_id) {
        List<Faculty> Trainers = new ArrayList<>();
        try {

            Class.forName("com.mysql.jdbc.Driver");
            String date = CustDateFormat.todaySQLString();
            String path = env.getProperty("spring.datasource.url");
            String userName = env.getProperty("spring.datasource.username");
            String password = env.getProperty("spring.datasource.password");

            Connection c = DriverManager.getConnection(path, userName, password);
            // gets a new connection
            Statement s = c.createStatement();
            //  PreparedStatement preparedStatement = c.prepareStatement("update dheemati_attendance.faculty_attendance set status = true,is_principal_filled = true,leave_status = 1 ,presence_status=false,in_time='#',out_time='#' where f_id in (SELECT f_id FROM dheemati_attendance.trn_longleave_dtls where leave_type_id  in (4,11,15,16,17,18,19) and to_date >= '" + startDate + "') and attendance_date = '" + startDate + "';");
//                PreparedStatement preparedStatement = c.prepareStatement("SELECT f_id,leave_type_id FROM dheemati_attendance.trn_longleave_dtls where leave_type_id  not in (2,4,11,15,16,17,18,19) and to_date >= '" + startDate + "';");
//
//                ResultSet rs = preparedStatement.executeQuery();
//                while (rs.next()) {
//                    LeaveFId.put(rs.getLong("f_id"), rs.getLong("leave_type_id"));
//                }
//                //  preparedStatement = c.prepareStatement("update dheemati_attendance.faculty_attendance set status = true,is_principal_filled = true,leave_status = 1 ,presence_status=true,in_time='#',out_time='#' where f_id not in (SELECT f_id FROM dheemati_attendance.trn_longleave_dtls where leave_type_id  in (4,11,15,16,17,18,19) and to_date >= '" + startDate + "') and attendance_date = '" + startDate + "';");
//                preparedStatement = c.prepareStatement("SELECT f_id,leave_type_id FROM dheemati_attendance.trn_longleave_dtls where leave_type_id  in (2,4,11,15,16,17,18,19) and to_date >= '" + startDate + "';");
//
//                rs = preparedStatement.executeQuery();
//                while (rs.next()) {
//                    DutyFId.put(rs.getLong("f_id"), rs.getLong("leave_type_id"));
//                }

            PreparedStatement preparedStatement = c.prepareStatement("select * from dheemati_attendance.trn_fs_prgm_schd where ? between from_date and to_date and institute_id = ? and is_deleted = false;");
            preparedStatement.setString(1, date);
            preparedStatement.setLong(2, institute_id);
            ResultSet rs = preparedStatement.executeQuery();
            while (rs.next()) {
                Optional<Faculty> f = facultyRepository.findById(rs.getLong("trainer_id"));
                if (f.isPresent()) {

                    Trainers.add(f.get());
                }
            }

        } catch (ClassNotFoundException | SQLException ex) {
            System.out.println("Error During Opeartion");
            java.util.logging.Logger.getLogger(FacultyAttendanceServiceImpl.class
             .getName()).log(Level.SEVERE, null, ex);
        }
        return Trainers;
    }

    public String facultyShiftTiming(Long f_id) {

        String time = "";
        List<Faculty> Trainers = new ArrayList<>();
        try {
            Class.forName("com.mysql.jdbc.Driver");
            String date = CustDateFormat.todaySQLString();
            String path = env.getProperty("spring.datasource.url");
            String userName = env.getProperty("spring.datasource.username");
            String password = env.getProperty("spring.datasource.password");

            Connection c = DriverManager.getConnection(path, userName, password);

            PreparedStatement preparedStatement = c.prepareStatement("SELECT * FROM dheemati_attendance.faculty_shift_timing_view where f_id = ?;");
            preparedStatement.setLong(1, f_id);
            ResultSet rs = preparedStatement.executeQuery();
            while (rs.next()) {
                long shiftTypeId = rs.getLong("shift_type_id");
                switch ((int) shiftTypeId) {
                    case 3:
                        time += "(M) " + rs.getString("day1_in") + " - " + rs.getString("day1_out") + "\n";
                        time += "(Tu) " + rs.getString("day2_in") + " - " + rs.getString("day2_out") + "\n";
                        time += "(W) " + rs.getString("day3_in") + " - " + rs.getString("day3_out") + "\n";
                        time += "(Th) " + rs.getString("day4_in") + " - " + rs.getString("day4_out") + "\n";
                        time += "(F) " + rs.getString("day5_in") + " - " + rs.getString("day5_out") + "\n";
                        time += "(S) " + rs.getString("day6_in") + " - " + rs.getString("day6_out");
                        break;
                    case 2:
                        time += "(M-F) " + rs.getString("day1_in") + " - " + rs.getString("day1_out") + "\n";
                        time += "(S) " + rs.getString("day6_in") + " - " + rs.getString("day6_out");
                        break;
                    default:
                        time += "(ALL) " + rs.getString("day1_in") + " - " + rs.getString("day1_out");
                        break;
                }

            }

        } catch (ClassNotFoundException | SQLException ex) {
            System.out.println("Error During Opeartion");
            java.util.logging.Logger.getLogger(FacultyAttendanceServiceImpl.class
             .getName()).log(Level.SEVERE, null, ex);
        }
        return time;
    }

    @Override
    public LoginResponse userStatus(Long fidt) {
        LoginResponse loginResponse = new LoginResponse();

        /*
        InstituteWifiMaster instituteWifiMaster = instituteWifiRepository.findByBssid(facultyLoginRequest.getBssid());
        if (instituteWifiMaster == null) {
            System.out.println("inside wifi not register");
            loginResponse.setMessage("You are not in registered Wifi Range ");
            loginResponse.setUserType("Faculty");
            loginResponse.setId(-1L);
            return loginResponse;
        }
         */
        String pattern = "yyyy-MM-dd";
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
        String date = simpleDateFormat.format(new Date());
        Optional<Faculty> fOptional = facultyRepository.findById(fidt);
        if (!fOptional.isPresent()) {
            loginResponse.setMessage("User Info. Invalid");
            loginResponse.setUserType("Faculty");
            loginResponse.setId(-1L);
            return loginResponse;
        } else {
            if (fOptional.get().getIsDeleted()) {
                loginResponse.setMessage("User account deactivated...");
                loginResponse.setUserType("Faculty");
                loginResponse.setId(-1L);
                return loginResponse;
            }
            if (RoleTypes.isSpecialRole(fOptional.get().getRole())) {
                loginResponse.setMessage("Invalid User Role...");
                loginResponse.setUserType("Faculty");
                loginResponse.setId(-1L);
                return loginResponse;
            }

            FacultyAttendance facultyAttendance = facultyAttendanceRepository.findByAttendanceDateAndFId(date, fOptional.get().getId());
            if (facultyAttendance != null) {
                loginResponse.setInTime(facultyAttendance.getInTime());
                loginResponse.setOutTime(facultyAttendance.getOutTime());
            } else {
                loginResponse.setInTime("#LOCK#");
                loginResponse.setOutTime("#LOCK#");
            }
            loginResponse.setFaId(facultyAttendance.getId());
            loginResponse.setMessage("Successfully logged in");
            loginResponse.setUserType("Faculty");

            loginResponse.setCurrentDate(date);
            loginResponse.setIsActive(fOptional.get().getActiveFlag());
            loginResponse.setUserRole(fOptional.get().getRole());

            if (fOptional.get().getRole() == 3L) {
                loginResponse.setIsMemo(trnDtlIssueMemoRepository.existsByInstituteIdAndIsDeletedFalseAndIsEndorseFalse(fOptional.get().getInstituteId()));
            } else {
                loginResponse.setIsMemo(trnDtlIssueMemoRepository.existsByFIdAndIsDeletedFalseAndIsSubmittedFalse(fOptional.get().getId()));
            }

            return loginResponse;
        }

    }

    @Override
    public LoginResponse userStatus(Long fidt, Long vCode) {
        LoginResponse loginResponse = new LoginResponse();

        Optional<AndroidVersion> androidVersion = androidVersionRepository.findByVersionNumberAndIsActive(vCode, Boolean.TRUE);
        if (!androidVersion.isPresent()) {
            loginResponse.setMessage("Lower Version!");
            loginResponse.setUserType("Faculty");
            loginResponse.setId(-1L);
            return loginResponse;
        }
        String pattern = "yyyy-MM-dd";
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
        String date = simpleDateFormat.format(new Date());
        Optional<Faculty> fOptional = facultyRepository.findById(fidt);
        if (!fOptional.isPresent()) {
            loginResponse.setMessage("User Info. Invalid");
            loginResponse.setUserType("Faculty");
            loginResponse.setId(-1L);
            return loginResponse;
        }
        if (fOptional.get().getIsDeleted()) {
            loginResponse.setMessage("User account deactivated...");
            loginResponse.setUserType("Faculty");
            loginResponse.setId(-1L);
            return loginResponse;
        }
        if (RoleTypes.isSpecialRole(fOptional.get().getRole())) {
            loginResponse.setMessage("Invalid User Role...");
            loginResponse.setUserType("Faculty");
            loginResponse.setId(-1L);
            return loginResponse;
        }

        FacultyAttendance facultyAttendance = facultyAttendanceRepository.findByAttendanceDateAndFId(date, fOptional.get().getId());
        if (facultyAttendance != null) {
            loginResponse.setInTime(facultyAttendance.getInTime());
            loginResponse.setOutTime(facultyAttendance.getOutTime());
        } else {
            loginResponse.setInTime("#LOCK#");
            loginResponse.setOutTime("#LOCK#");
        }
        loginResponse.setFaId(facultyAttendance.getId());
        loginResponse.setMessage("Successfully");
        loginResponse.setUserType("Faculty");

        loginResponse.setCurrentDate(date);
        loginResponse.setIsActive(fOptional.get().getActiveFlag());
        loginResponse.setUserRole(fOptional.get().getRole());

        if (fOptional.get().getRole() == 3L) {
            loginResponse.setIsMemo(trnDtlIssueMemoRepository.existsByInstituteIdAndIsDeletedFalseAndIsEndorseFalse(fOptional.get().getInstituteId()));
        } else {
            loginResponse.setIsMemo(trnDtlIssueMemoRepository.existsByFIdAndIsDeletedFalseAndIsSubmittedFalse(fOptional.get().getId()));
        }
        return loginResponse;
    }

    private void preparePresentListFacultyDepartmentQuery(CriteriaQuery criteriaQuery, CriteriaBuilder criteriaBuilder, boolean countQuery, Long instituteId) {

        Root<Faculty> facultyRoot = criteriaQuery.from(Faculty.class);
        Root<DepartmentMaster> dRoot = criteriaQuery.from(DepartmentMaster.class);
        if (countQuery) {
            criteriaQuery.select(criteriaBuilder.count(facultyRoot));
        } else {
            criteriaQuery.multiselect(
             facultyRoot.get("id"), dRoot.get("id"), facultyRoot.get("name"), dRoot.get("name")
            );
            List<Order> orders = new ArrayList<>();
            orders.add(criteriaBuilder.asc(facultyRoot.get("name")));
            if (!orders.isEmpty()) {
                criteriaQuery.orderBy(orders);
            }
        }

        String pattern = "yyyy-MM-dd";
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
        String date = simpleDateFormat.format(new Date());

        ArrayList<Predicate> predicateList = new ArrayList<>();
        predicateList.add(criteriaBuilder.equal(dRoot.get("id"), facultyRoot.get("departmentId")));
        predicateList.add(criteriaBuilder.notEqual(facultyRoot.get("isDeleted"), Boolean.TRUE));
        Predicate[] predicates = new Predicate[predicateList.size()];
        predicateList.toArray(predicates);
        criteriaQuery.where(predicates);
    }

    @Override
    public List<InstituteWiseFacultyWithDepartment> getFacultyDeptListByInstitute(Long instituteId) {
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery criteriaQuery = criteriaBuilder.createQuery(InstituteWiseFacultyWithDepartment.class);

        // Prepare list query
        preparePresentListFacultyQuery(criteriaQuery, criteriaBuilder, false, instituteId);

        TypedQuery typedQuery = entityManager.createQuery(criteriaQuery);
        List<InstituteWiseFacultyWithDepartment> results = typedQuery.getResultList();
        return results;

    }

    @Override
    public GeneralListResponse facultyStatusToday(FacultyLStatusRequest facultyLStatusRequest) {

        GeneralListResponse generalListResponse = new GeneralListResponse();
        if (facultyLStatusRequest.getDepartmentId() != 0 && facultyLStatusRequest.getRetriveType() == 1) {
            String reportInTime = CustDateFormat.todayTime();

            String times[] = reportInTime.split(":");
            int hours = Integer.parseInt(times[0]);
            int minute = Integer.parseInt(times[1]);

            if (hours > 11 || (hours == 11 && minute > 15)) {
                generalListResponse.setErrorStatus(501L, "HoD Attendacne Timeout!");
                return generalListResponse;
            }

        }
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery criteriaQuery = criteriaBuilder.createQuery(FacultyLStatusRequest.class);

        // Prepare list query
        prepareFacultyStatusListQuery(criteriaQuery, criteriaBuilder, false, facultyLStatusRequest);

        TypedQuery typedQuery = entityManager.createQuery(criteriaQuery);
        List<FacultyLStatusRequest> results = typedQuery.getResultList();
        
        generalListResponse.setListDataforCount((List<Object>)(FacultyLStatusRequest)results);

        return generalListResponse;
        // throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    private void prepareFacultyStatusListQuery(CriteriaQuery criteriaQuery, CriteriaBuilder criteriaBuilder, boolean countQuery, FacultyLStatusRequest facultyLStatusRequest) {
        Root<Faculty> facultyRoot = criteriaQuery.from(Faculty.class);
        Root<FacultyAttendance> facAttenRoot = criteriaQuery.from(FacultyAttendance.class);
        Root<DepartmentMaster> dRoot = criteriaQuery.from(DepartmentMaster.class);
        if (countQuery) {
            criteriaQuery.select(criteriaBuilder.count(facultyRoot));
        } else {
            criteriaQuery.multiselect(
             facultyRoot.get("id"), dRoot.get("id"),facultyRoot.get("name"), dRoot.get("name"),facAttenRoot.get("isLate"),facAttenRoot.get("isEarly"),
             facAttenRoot.get("presenceStatus"), facAttenRoot.get("leaveStatus"), facAttenRoot.get("inTime"), facAttenRoot.get("outTime"),facAttenRoot.get("actInTime"), facAttenRoot.get("actOutTime"),facultyRoot.get("fName"), facultyRoot.get("mName"), facultyRoot.get("lName"),facultyRoot.get("activeFlag"));
            List<Order> orders = new ArrayList<>();
            orders.add(criteriaBuilder.asc(dRoot.get("name")));
            orders.add(criteriaBuilder.asc(facultyRoot.get("name")));
            if (!orders.isEmpty()) {
                criteriaQuery.orderBy(orders);
            }
        }

        String pattern = "yyyy-MM-dd";
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
        String date = simpleDateFormat.format(new Date());

        ArrayList<Predicate> predicateList = new ArrayList<>();
        predicateList.add(criteriaBuilder.equal(facultyRoot.get("id"), facAttenRoot.get("fId")));
        predicateList.add(criteriaBuilder.equal(dRoot.get("id"), facultyRoot.get("departmentId")));
        predicateList.add(criteriaBuilder.equal(facAttenRoot.get("instituteId"), facultyLStatusRequest.getInstituteId()));
        if(facultyLStatusRequest.getDepartmentId() != 0)
        {
            predicateList.add(criteriaBuilder.equal(dRoot.get("id"), facultyLStatusRequest.getDepartmentId()));
        
        }
        
        if(facultyLStatusRequest.getRetriveType()== 1L)
        {
            predicateList.add(criteriaBuilder.notEqual(facAttenRoot.get("status"), Boolean.FALSE));
        
        }
        if(facultyLStatusRequest.getRetriveType()== 2L)
        {
            predicateList.add(criteriaBuilder.isNull(facAttenRoot.get("outTime")));
        
        }
        if(facultyLStatusRequest.getRetriveType()== 3L)
        {
            predicateList.add(criteriaBuilder.equal(facAttenRoot.get("isPrincipalFilled"), true));
       
        }
        
        predicateList.add(criteriaBuilder.equal(facAttenRoot.get("attendanceDate"), date));
        //predicateList.add(criteriaBuilder.equal(facAttenRoot.get("isPrincipalFilled"), true));
        predicateList.add(criteriaBuilder.notEqual(facAttenRoot.get("isDeleted"), Boolean.TRUE));
        Predicate[] predicates = new Predicate[predicateList.size()];
        predicateList.toArray(predicates);
        criteriaQuery.where(predicates);
    }

}
