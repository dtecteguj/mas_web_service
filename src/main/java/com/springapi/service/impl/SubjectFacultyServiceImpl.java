/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.springapi.service.impl;

import com.springapi.enums.FASMessageType;
import com.springapi.exception.FASException;
import com.springapi.model.SubjectFaculty;
import com.springapi.repository.SubjectFacultyRepository;
import com.springapi.service.SubjectFacultyService;
import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author shahb
 */
@Service
@Transactional
public class SubjectFacultyServiceImpl implements SubjectFacultyService {

    @Autowired
    private SubjectFacultyRepository subjectFacultyRepository;

    @Override
    public SubjectFaculty create(SubjectFaculty subjectFaculty) {
        return subjectFacultyRepository.save(subjectFaculty);
    }

    @Override
    public List<SubjectFaculty> list() {
        return subjectFacultyRepository.findAll();
    }

    @Override
    public SubjectFaculty read(Long sfId) {
        Optional<SubjectFaculty> optional = subjectFacultyRepository.findById(sfId);
        if (!optional.isPresent()) {
            throw new FASException(FASMessageType.ENTITY_NOT_FOUND);
        }
        return optional.get();
    }

    @Override
    public SubjectFaculty update(SubjectFaculty subjectFacultyDetails) {
        SubjectFaculty subjectFaculty = read(subjectFacultyDetails.getId());

        subjectFaculty.setfId(subjectFacultyDetails.getfId());
        subjectFaculty.setsId(subjectFacultyDetails.getsId());

        return subjectFacultyRepository.save(subjectFaculty);
    }

    @Override
    public void delete(Long sfId) {
        SubjectFaculty subjectFaculty = read(sfId);
        subjectFacultyRepository.delete(subjectFaculty);
    }
}
