package com.springapi.service.impl;

import com.springapi.dto.request.AttendanceByPrincipalRequest;
import com.springapi.dto.request.FacAttendVerifyRequest;
import com.springapi.dto.request.FacultyAttendanceListRequest;
import com.springapi.dto.request.FacultyQRVerificationRequest;
import com.springapi.dto.request.LateAttendanceReasonRequest;
import com.springapi.dto.response.BaseResponse;
import com.springapi.enums.FASMessageType;
import com.springapi.enums.FacultyAttendanceStatus;
import com.springapi.enums.MASConstant;
import com.springapi.exception.FASException;
import com.springapi.model.AndroidVersion;
import com.springapi.model.DepartmentMaster;
import com.springapi.model.Faculty;
import com.springapi.model.FacultyAttendance;
import com.springapi.model.FacultyShiftMpg;
import com.springapi.model.HolidayMaster;
import com.springapi.model.InstituteMaster;
import com.springapi.model.InstituteWifiMaster;
import com.springapi.model.LeaveTypeMaster;
import com.springapi.model.QRCodeData;
import com.springapi.model.ScheduleMaster;
import com.springapi.model.ShiftMaster;
import com.springapi.model.TrnAttendanceLog;
import com.springapi.model.TrnDtlIssueMemo;
import com.springapi.model.TrnDtlLeaveStatus;
import com.springapi.model.TrnFSPrgmSchd;
import com.springapi.model.TrnInstMisfnclty;
import com.springapi.model.TrnLongleaveDtls;
import com.springapi.model.TrnbyPriHodLog;
import com.springapi.model.view.FacultyMEMOView;
import com.springapi.model.view.FacultyShiftTiming;
import com.springapi.model.view.LongleaveView;
import com.springapi.repository.AndroidVersionRepository;
import com.springapi.repository.FacultyAttendanceRepository;
import com.springapi.repository.FacultyShiftMpgRepository;
import com.springapi.repository.InstituteWifiRepository;
import com.springapi.repository.LeaveTypeMasterRepository;
import com.springapi.repository.QRCodeDataRepository;
import com.springapi.repository.ScheduleMasterRepository;
import com.springapi.repository.ShiftRepository;
import com.springapi.repository.TrnDtlIssueMemoRepository;
import com.springapi.repository.TrnDtlLeaveStatusRepository;
import com.springapi.repository.TrnLongleaveDtlsRepository;
import com.springapi.repository.HolidayMasterRepository;
import com.springapi.repository.TrnActiveLoginLogRepository;
import com.springapi.service.FacultyAttendanceService;
import com.springapi.service.FacultyService;
import com.springapi.service.FacultyShiftMpgService;
import com.springapi.service.ScheduleMasterService;
import com.springapi.service.SendEmailService;
import com.springapi.service.TrnAttendanceLogService;
import com.springapi.service.TrnInstMisfncltyService;
import com.springapi.service.TrnbyPriHodLogService;
import com.springapi.util.CustDateFormat;
import com.springapi.util.RoleTypes;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaBuilder.In;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Order;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Jyoti Patel
 */
@Service
@Transactional
public class FacultyAttendanceServiceImpl implements FacultyAttendanceService {

    static Logger logger = Logger.getLogger(FacultyAttendanceServiceImpl.class);

    @Autowired
    private TrnActiveLoginLogRepository trnActiveLoginLogRepository;
    @Autowired
    private FacultyAttendanceRepository facultyAttendanceRepository;

    @Autowired
    private TrnAttendanceLogService trnAttendanceLogService;

    @Autowired
    private TrnbyPriHodLogService trnbyPriHodLogService;

    @Autowired
    private QRCodeDataRepository qRCodeDataRepository;

    @Autowired
    private HolidayMasterRepository holidayMasterRepository;

    @Autowired
    private EntityManager entityManager;

    @Autowired
    private FacultyService facultyService;

    @Autowired
    private Environment env;

    @Autowired
    private InstituteWifiRepository instituteWifiRepository;

    @Autowired
    private TrnDtlLeaveStatusRepository trnDtlLeaveStatusRepository;

    @Autowired
    private TrnLongleaveDtlsRepository trnLongleaveDtlsRepository;

    @Autowired
    private AndroidVersionRepository androidVersionRepository;

    @Autowired
    private ShiftRepository shiftRepository;

    @Autowired
    private ScheduleMasterRepository scheduleMasterRepository;

    @Autowired
    private ScheduleMasterService scheduleMasterService;

    @Autowired
    private LeaveTypeMasterRepository leaveTypeMasterRepository;

    @Autowired
    private TrnDtlIssueMemoRepository traDtlIssueMemoRepository;

    @Override
    public FacultyAttendance create(FacultyAttendance facultyAttendance) {
        return facultyAttendanceRepository.save(facultyAttendance);
    }

    @Override
    public List<FacultyAttendance> list() {
        return facultyAttendanceRepository.findAll();
    }

    @Autowired
    private FacultyShiftMpgRepository facultyShiftMpgRepository;

    @Autowired
    private TrnDtlIssueMemoRepository trnDtlIssueMemoRepository;

    @Autowired
    private SendEmailService sendEmailService;

    @Autowired
    private FacultyShiftMpgService facultyShiftMpgService;

    @Autowired
    private TrnInstMisfncltyService trnInstMisfncltyService;

    @Override
    public FacultyAttendance read(Long faId) {
        String date = CustDateFormat.todaySQLString();
        Optional<FacultyAttendance> optional = facultyAttendanceRepository.findById(faId);
        if (!optional.isPresent()) {
            throw new FASException(FASMessageType.ENTITY_NOT_FOUND);
        }
        return optional.get();
    }

    @Override
    public FacultyAttendance update(FacultyAttendance facultyAttendanceDetails) {
        FacultyAttendance facultyAttendance = read(facultyAttendanceDetails.getId());

        facultyAttendance.setAndroidID(facultyAttendanceDetails.getAndroidID());
        facultyAttendance.setBSSID(facultyAttendanceDetails.getBSSID());
        facultyAttendance.setIMEINo(facultyAttendanceDetails.getIMEINo());
        facultyAttendance.setMacID(facultyAttendanceDetails.getMacID());
        facultyAttendance.setfId(facultyAttendanceDetails.getfId());
        facultyAttendance.setSSID(facultyAttendanceDetails.getSSID());
        facultyAttendance.setInTime(facultyAttendanceDetails.getInTime());
        facultyAttendance.setOutTime(facultyAttendanceDetails.getOutTime());
        facultyAttendance.setIsEarly(facultyAttendanceDetails.getIsEarly());
        facultyAttendance.setIsLate(facultyAttendanceDetails.getIsLate());
        facultyAttendance.setIsPrincipalFilled(facultyAttendanceDetails.getIsPrincipalFilled());
        facultyAttendance.setPresenceStatus(facultyAttendanceDetails.getPresenceStatus());
        facultyAttendance.setLeaveStatus(facultyAttendanceDetails.getLeaveStatus());

        return facultyAttendanceRepository.save(facultyAttendance);
    }

    @Override
    public void delete(Long faId) {
        FacultyAttendance facultyAttendance = read(faId);
        facultyAttendanceRepository.delete(facultyAttendance);
    }

    @Override
    public BaseResponse verifyQRCode(FacultyQRVerificationRequest facultyQRVerificationRequest) {
        BaseResponse baseResponse = new BaseResponse();
        QRCodeData qRCodeData = qRCodeDataRepository.findByQrCode(facultyQRVerificationRequest.getQrCode());
        if (qRCodeData == null) {
            throw new FASException(FASMessageType.QR_CODE_INVALID);
        } else {
            String pattern = "yyyy-MM-dd";
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
            String date = simpleDateFormat.format(new Date());
            FacultyAttendance facultyAttendance = facultyAttendanceRepository.findByAttendanceDateAndFId(date, facultyQRVerificationRequest.getfId());
            if (facultyAttendance == null) {
                //In Entry
                facultyAttendance = new FacultyAttendance();
                // Set Current date
                date = simpleDateFormat.format(new Date());
                facultyAttendance.setAttendanceDate(date);
                facultyAttendance.setCreatedAt(new Date());
                facultyAttendance.setfId(facultyQRVerificationRequest.getfId());
                facultyAttendance.setAndroidID(facultyQRVerificationRequest.getAndroidID());
                facultyAttendance.setBSSID(facultyQRVerificationRequest.getBSSID());
                facultyAttendance.setIMEINo(facultyQRVerificationRequest.getIMEINo());
                facultyAttendance.setSSID(facultyQRVerificationRequest.getSSID());
                facultyAttendance.setMacID(facultyQRVerificationRequest.getMacID());

                //Report In time calculation
                DateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");
                Date todayDate = Calendar.getInstance().getTime();
                String reportInTime = dateFormat.format(todayDate);

                // Print what date is today!
                System.out.println("Report Date: " + reportInTime);
                facultyAttendance.setInTime(reportInTime);

                //Comparing two dates
                String result = getTimeDiff(new Date(), qRCodeData.getCreatedAt());
                System.out.println("result in time::" + result);
                facultyAttendance.setInStatus(FacultyAttendanceStatus.REGULAR_IN);
            } else {
                //Out Entry
                facultyAttendance.setUpdatedAt(new Date());
                DateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");
                Date todayDate = Calendar.getInstance().getTime();
                String reportOutTime = dateFormat.format(todayDate);
                facultyAttendance.setOutTime(reportOutTime);
                facultyAttendance.setOutStatus(FacultyAttendanceStatus.REGULAR_OUT);
                //Comparing two dates
                String result = getTimeDiff(new Date(), qRCodeData.getCreatedAt());
                System.out.println("result in time::" + result);
            }

            facultyAttendanceRepository.save(facultyAttendance);
            baseResponse.setMessage("Successfully Updated ");
            return baseResponse;
        }
    }

    public String getTimeDiff(Date dateOne, Date dateTwo) {
        String diff = "";
        long timeDiff = Math.abs(dateOne.getTime() - dateTwo.getTime());
        diff = String.format("%d hour(s) %d min(s)", TimeUnit.MILLISECONDS.toHours(timeDiff), TimeUnit.MILLISECONDS.toMinutes(timeDiff) - TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(timeDiff)));
        return diff;
    }

    @Override
    public BaseResponse verifyAttendance(FacAttendVerifyRequest facAttendVerifyRequest) {
        BaseResponse baseResponse = new BaseResponse();
        //QRCodeData qRCodeData = qRCodeDataRepository.findByQrCode(facultyQRVerificationRequest.getQrCode());
        if (!facAttendVerifyRequest.getIsVerified()) {
            throw new FASException(FASMessageType.QR_CODE_INVALID);
        } else {
            String pattern = "yyyy-MM-dd";
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
            String date = simpleDateFormat.format(new Date());
            FacultyAttendance facultyAttendance = facultyAttendanceRepository.findByAttendanceDateAndFId(date, facAttendVerifyRequest.getfId());
            if (facultyAttendance == null) {
                //In Entry
                facultyAttendance = new FacultyAttendance();
                // Set Current date
                date = simpleDateFormat.format(new Date());
                facultyAttendance.setAttendanceDate(date);
                facultyAttendance.setCreatedAt(new Date());
                facultyAttendance.setfId(facAttendVerifyRequest.getfId());
                facultyAttendance.setAndroidID(facAttendVerifyRequest.getAndroidID());
                facultyAttendance.setBSSID(facAttendVerifyRequest.getBssid());
                facultyAttendance.setIMEINo(facAttendVerifyRequest.getIMEINo());
                facultyAttendance.setSSID(facAttendVerifyRequest.getSsid());
                facultyAttendance.setMacID(facAttendVerifyRequest.getMacID());

                Faculty faculty = facultyService.read(facAttendVerifyRequest.getfId());
                facultyAttendance.setInstituteId(faculty.getInstituteId());
                facultyAttendance.setDepartmentId(faculty.getDepartmentId());
                //Report In time calculation
                DateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");
                Date todayDate = Calendar.getInstance().getTime();
                String reportInTime = dateFormat.format(todayDate);

                // Print what date is today!
                System.out.println("Report Date: " + reportInTime);
                facultyAttendance.setInTime(reportInTime);

                // Comparing two dates
                // String result = getTimeDiff(new Date(), qRCodeData.getCreatedAt());
                // System.out.println("result in time::" + result);
                facultyAttendance.setInStatus(FacultyAttendanceStatus.REGULAR_IN);
            } else {
                //Out Entry
                facultyAttendance.setUpdatedAt(new Date());
                DateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");
                Date todayDate = Calendar.getInstance().getTime();
                String reportOutTime = dateFormat.format(todayDate);
                facultyAttendance.setOutTime(reportOutTime);
                facultyAttendance.setOutStatus(FacultyAttendanceStatus.REGULAR_OUT);
                // Comparing two dates
                // String result = getTimeDiff(new Date(), qRCodeData.getCreatedAt());
                // System.out.println("result in time::" + result);
            }

            facultyAttendanceRepository.save(facultyAttendance);
            baseResponse.setMessage("Successfully Updated ");
            return baseResponse;
        }

    }

    @Override
    public Page<FacultyAttendance> facultyAttendanceList(FacultyAttendanceListRequest listRequest) {
        logger.debug("facultyAttendanceList");

        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery criteriaQuery = criteriaBuilder.createQuery(FacultyAttendance.class);

        prepareListQuery(criteriaQuery, criteriaBuilder, false, listRequest);

        // Prepare Pagination
        Pageable pageable = new PageRequest(listRequest.getPage(), listRequest.getLimit());
        TypedQuery typedQuery = entityManager.createQuery(criteriaQuery);
        typedQuery.setFirstResult((int) pageable.getOffset());
        typedQuery.setMaxResults(pageable.getPageSize());

        List results = typedQuery.getResultList();
        System.out.println("result ::" + results);
        return new PageImpl(results, pageable, getListCount(listRequest));

//        TypedQuery typedQuery = entityManager.createQuery(criteriaQuery);
//        List results = typedQuery.getResultList();
        // return results;
    }

    private long getListCount(FacultyAttendanceListRequest listRequest) {
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Long> criteriaQuery = criteriaBuilder.createQuery(Long.class);

        // Prepare list query
        prepareListQuery(criteriaQuery, criteriaBuilder, true, listRequest);

        return entityManager.createQuery(criteriaQuery).getSingleResult();
    }

    private void prepareListQuery(CriteriaQuery criteriaQuery, CriteriaBuilder criteriaBuilder, boolean countQuery,
     FacultyAttendanceListRequest listRequest) {
        logger.debug("prepareListQuery");
        Root<FacultyAttendance> facultyAttenRoot = criteriaQuery.from(FacultyAttendance.class);
        Root<InstituteMaster> instituteRoot = criteriaQuery.from(InstituteMaster.class);
        Root<Faculty> facultyRoot = criteriaQuery.from(Faculty.class);
        Root<DepartmentMaster> departmentRoot = criteriaQuery.from(DepartmentMaster.class);

        if (countQuery) {
            criteriaQuery.select(criteriaBuilder.count(facultyAttenRoot));
        } else {
            criteriaQuery.multiselect(facultyAttenRoot.get("id"),
             facultyAttenRoot.get("fId"), facultyAttenRoot.get("BSSID"), facultyAttenRoot.get("inTime"),
             facultyAttenRoot.get("outTime"), facultyAttenRoot.get("instituteId"),
             instituteRoot.get("name"), facultyRoot.get("name"), departmentRoot.get("name"),
             facultyAttenRoot.get("attendanceDate"),
             facultyAttenRoot.get("status"), facultyAttenRoot.get("lateComingReason"), facultyAttenRoot.get("isPrincipalFilled"), facultyAttenRoot.get("reasonByPrincipal"),
             facultyAttenRoot.get("earlyOutReason"), facultyAttenRoot.get("isLate"), facultyAttenRoot.get("isEarly"), facultyAttenRoot.get("presenceStatus"), facultyAttenRoot.get("leaveStatus")
            );

            List<Order> orders = new ArrayList<>();
//            if (!CollectionUtils.isEmpty(listRequest.getSort())) {
//                for (Map<String, ListSortOrder> properties : listRequest.getSort()) {
//                    properties.forEach((property, sortOrder) -> {
//                        orders.add(ListSortOrder.DESC.toString().equalsIgnoreCase(sortOrder.toString())
//                                ? criteriaBuilder.desc(cityRoot.get(property))
//                                : criteriaBuilder.asc(cityRoot.get(property)));
//
//                    });
//                }
//            }

            if (!orders.isEmpty()) {
                criteriaQuery.orderBy(orders);
            }
        }

        String pattern = "yyyy-MM-dd";
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
        String date = simpleDateFormat.format(new Date());

        ArrayList<Predicate> predicateList = new ArrayList<>();
        predicateList.add(criteriaBuilder.equal(facultyAttenRoot.get("departmentId"), departmentRoot.get("id")));
        predicateList.add(criteriaBuilder.equal(facultyAttenRoot.get("instituteId"), instituteRoot.get("id")));
        predicateList.add(criteriaBuilder.equal(facultyAttenRoot.get("fId"), facultyRoot.get("id")));

        if (listRequest.getStartDate() == null) {
            predicateList.add(criteriaBuilder.equal(facultyAttenRoot.get("attendanceDate"), date));
        }
        if (listRequest.getDepartmentId() != null) {
            predicateList.add(criteriaBuilder.equal(facultyAttenRoot.get("departmentId"), listRequest.getDepartmentId()));
        }

        if (listRequest.getInstituteId() != null) {
            predicateList.add(criteriaBuilder.equal(facultyAttenRoot.get("instituteId"), listRequest.getInstituteId()));
        }

        if (listRequest.getFacultyId() != null) {
            predicateList.add(criteriaBuilder.equal(facultyAttenRoot.get("fId"), listRequest.getFacultyId()));
        }

        if (listRequest.getInstituteTypeId() != null) {
            predicateList.add(criteriaBuilder.equal(facultyAttenRoot.get("instituteTypeId"), listRequest.getInstituteTypeId()));
        }

        if (listRequest.getStartDate() != null) {
            predicateList.add(criteriaBuilder.greaterThanOrEqualTo(facultyAttenRoot.<Date>get("attendanceDate"), listRequest.getStartDate()));
        }

        if (listRequest.getEndDate() != null) {
            predicateList.add(criteriaBuilder.lessThanOrEqualTo(facultyAttenRoot.<Date>get("attendanceDate"), listRequest.getEndDate()));
        }

        if (listRequest.getSearchText() != null && !listRequest.getSearchText().trim().isEmpty()) {
            predicateList.add(criteriaBuilder.equal(facultyAttenRoot.get("facultyName"), "%" + listRequest.getSearchText() + "%"));
        }

//        if (listRequest.getIsFaculty() != null && listRequest.getIsFaculty()) {
//            predicateList.add(criteriaBuilder.equal(facultyAttenRoot.get("fId"), 1));
//        }
        Predicate[] predicates = new Predicate[predicateList.size()];
        predicateList.toArray(predicates);
        criteriaQuery.where(predicates);
    }

    @Override
    public BaseResponse verifyAndAdd(FacAttendVerifyRequest facAttendVerifyRequest) {
        BaseResponse baseResponse = new BaseResponse();
        System.out.println("bsssid from verify and add ::" + facAttendVerifyRequest.getBssid());
        InstituteWifiMaster instituteWifiMaster = instituteWifiRepository.findByBssid(facAttendVerifyRequest.getBssid());
        if (instituteWifiMaster == null) {
            return null;
        }

        if (!facAttendVerifyRequest.getIsVerified()) {
            throw new FASException(FASMessageType.QR_CODE_INVALID);
        } else {
            String pattern = "yyyy-MM-dd";
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
            String date = simpleDateFormat.format(new Date());
            //FacultyAttendance facultyAttendance = facultyAttendanceRepository.findByAttendanceDate(date);
            List<FacultyAttendance> facultyAttendancelist = facultyAttendanceRepository.findAllByAttendanceDate(date);
            if (facultyAttendancelist.size() <= 0) {

                // enter all records
                // Faculty faculty = facultyService.read(facAttendVerifyRequest.getfId());
                List<Faculty> facultyList = facultyService.list();

                for (Faculty fac : facultyList) {
                    FacultyAttendance facultyAttendance = new FacultyAttendance();

                    if (Objects.equals(fac.getId(), facAttendVerifyRequest.getfId())) {
                        Optional<FacultyShiftMpg> fOptional = facultyShiftMpgRepository.findByIsActiveAndFId(Boolean.TRUE, facAttendVerifyRequest.getfId());
                        if (fOptional.isPresent()) {

                            ShiftMaster shiftMaster = shiftRepository.findById(fOptional.get().getShiftId());
                            baseResponse.setInstituteInTime(shiftMaster.getInTime());
                            baseResponse.setInstituteOutTime(shiftMaster.getOutTime());
                        }
                        facultyAttendance.setAndroidID(facAttendVerifyRequest.getAndroidID());
                        facultyAttendance.setBSSID(facAttendVerifyRequest.getBssid());
                        facultyAttendance.setIMEINo(facAttendVerifyRequest.getIMEINo());
                        facultyAttendance.setSSID(facAttendVerifyRequest.getSsid());
                        facultyAttendance.setMacID(facAttendVerifyRequest.getMacID());
                        facultyAttendance.setInstituteTypeId(fac.getInstituteTypeId());
                        DateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");
                        Date todayDate = Calendar.getInstance().getTime();
                        String reportInTime = dateFormat.format(todayDate);

                        // Print what date is today!
                        System.out.println("Report Date: " + reportInTime);
                        facultyAttendance.setInTime(reportInTime);
                        facultyAttendance.setStatus(true);
                        baseResponse.setInTime(reportInTime);
                        facultyAttendance.setAttendanceDate(date);
                        facultyAttendance.setCreatedAt(new Date());
                        facultyAttendance.setfId(fac.getId());
                        facultyAttendance.setInstituteId(fac.getInstituteId());
                        facultyAttendance.setDepartmentId(fac.getDepartmentId());
                        facultyAttendanceRepository.save(facultyAttendance);
                    } else {
                        facultyAttendance.setAttendanceDate(date);
                        facultyAttendance.setCreatedAt(new Date());
                        facultyAttendance.setfId(fac.getId());
                        facultyAttendance.setInstituteId(fac.getInstituteId());
                        facultyAttendance.setDepartmentId(fac.getDepartmentId());
                        facultyAttendance.setStatus(false);
                        facultyAttendance.setInstituteTypeId(fac.getInstituteTypeId());
                        facultyAttendanceRepository.save(facultyAttendance);
                    }
                }
            } else {
                FacultyAttendance facultyAttendance = facultyAttendanceRepository.findByAttendanceDateAndFId(date, facAttendVerifyRequest.getfId());
                Optional<FacultyShiftMpg> fOptional = facultyShiftMpgRepository.findByIsActiveAndFId(Boolean.TRUE, facultyAttendance.getfId());
                if (fOptional.isPresent()) {

                    ShiftMaster shiftMaster = shiftRepository.findById(fOptional.get().getShiftId());
                    baseResponse.setInstituteInTime(shiftMaster.getInTime());
                    baseResponse.setInstituteOutTime(shiftMaster.getOutTime());
                }

                facultyAttendance.setAndroidID(facAttendVerifyRequest.getAndroidID());
                facultyAttendance.setBSSID(facAttendVerifyRequest.getBssid());
                facultyAttendance.setIMEINo(facAttendVerifyRequest.getIMEINo());
                facultyAttendance.setSSID(facAttendVerifyRequest.getSsid());
                facultyAttendance.setMacID(facAttendVerifyRequest.getMacID());
                if (facultyAttendance.getInTime() == null) {
                    DateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");
                    Date todayDate = Calendar.getInstance().getTime();
                    String reportInTime = dateFormat.format(todayDate);
                    facultyAttendance.setInTime(reportInTime);
                    facultyAttendance.setStatus(true);
                    baseResponse.setInTime(reportInTime);
                } else {
                    DateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");
                    Date todayDate = Calendar.getInstance().getTime();
                    String reportOutTime = dateFormat.format(todayDate);
                    facultyAttendance.setOutTime(reportOutTime);
                    baseResponse.setOutTime(reportOutTime);
                }
                facultyAttendanceRepository.save(facultyAttendance);
            }
            baseResponse.setMessage("Successfully Updated ");
            return baseResponse;
        }
    }

//    @Override
//    public BaseResponse verifyInAttendance(FacAttendVerifyRequest facAttendVerifyRequest) {
//        BaseResponse baseResponse = new BaseResponse();
//        InstituteWifiMaster instituteWifiMaster = instituteWifiRepository.findByBssid(facAttendVerifyRequest.getBssid());
//        if (facAttendVerifyRequest.getRoleId() != null && facAttendVerifyRequest.getRoleId() != 3 && instituteWifiMaster == null) {
//            return null;
//        }
//        String pattern = "yyyy-MM-dd";
//        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
//        String date = simpleDateFormat.format(new Date());
//        List<FacultyAttendance> facultyAttendancelist = facultyAttendanceRepository.findAllByAttendanceDate(date);
//        if (facultyAttendancelist.size() <= 0) {
//            // enter all records
//            List<Faculty> facultyList = facultyService.list();
//            for (Faculty fac : facultyList) {
//                FacultyAttendance facultyAttendance = new FacultyAttendance();
//
//                if (Objects.equals(fac.getId(), facAttendVerifyRequest.getfId())) {
//                    ShiftMaster shiftMaster = shiftRepository.findByInstituteId(fac.getInstituteId());
//                    baseResponse.setInstituteInTime(shiftMaster.getInTime());
//                    baseResponse.setInstituteOutTime(shiftMaster.getOutTime());
//                    facultyAttendance.setAndroidID(facAttendVerifyRequest.getAndroidID());
//                    facultyAttendance.setBSSID(facAttendVerifyRequest.getBssid());
//                    facultyAttendance.setIMEINo(facAttendVerifyRequest.getIMEINo());
//                    facultyAttendance.setSSID(facAttendVerifyRequest.getSsid());
//                    facultyAttendance.setMacID(facAttendVerifyRequest.getMacID());
//                    facultyAttendance.setInstituteTypeId(fac.getInstituteTypeId());
//                    DateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");
//                    Date todayDate = Calendar.getInstance().getTime();
//                    String reportInTime = dateFormat.format(todayDate);
//
//                    // Print what date is today!
//                    System.out.println("Report Date: " + reportInTime);
//                    facultyAttendance.setInTime(reportInTime);
//                    facultyAttendance.setStatus(true);
//                    baseResponse.setInTime(reportInTime);
//                    facultyAttendance.setAttendanceDate(date);
//                    facultyAttendance.setCreatedAt(new Date());
//                    facultyAttendance.setfId(fac.getId());
//                    facultyAttendance.setInstituteId(fac.getInstituteId());
//                    facultyAttendance.setDepartmentId(fac.getDepartmentId());
//                    facultyAttendanceRepository.save(facultyAttendance);
//                } else {
//                    facultyAttendance.setAttendanceDate(date);
//                    facultyAttendance.setCreatedAt(new Date());
//                    facultyAttendance.setfId(fac.getId());
//                    facultyAttendance.setInstituteId(fac.getInstituteId());
//                    facultyAttendance.setDepartmentId(fac.getDepartmentId());
//                    facultyAttendance.setStatus(false);
//                    facultyAttendance.setInstituteTypeId(fac.getInstituteTypeId());
//                    facultyAttendanceRepository.save(facultyAttendance);
//                }
//            }
//        } else {
//            FacultyAttendance facultyAttendance = facultyAttendanceRepository.findByAttendanceDateAndFId(date, facAttendVerifyRequest.getfId());
//            ShiftMaster shiftMaster = shiftRepository.findByInstituteId(facultyAttendance.getInstituteId());
//            baseResponse.setInstituteInTime(shiftMaster.getInTime());
//            baseResponse.setInstituteOutTime(shiftMaster.getOutTime());
//
//            facultyAttendance.setAndroidID(facAttendVerifyRequest.getAndroidID());
//            facultyAttendance.setBSSID(facAttendVerifyRequest.getBssid());
//            facultyAttendance.setIMEINo(facAttendVerifyRequest.getIMEINo());
//            facultyAttendance.setSSID(facAttendVerifyRequest.getSsid());
//            facultyAttendance.setMacID(facAttendVerifyRequest.getMacID());
//            if (facultyAttendance.getInTime() == null) {
//                DateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");
//                Date todayDate = Calendar.getInstance().getTime();
//                String reportInTime = dateFormat.format(todayDate);
//                facultyAttendance.setInTime(reportInTime);
//                facultyAttendance.setStatus(true);
//                baseResponse.setInTime(reportInTime);
//            }
//            facultyAttendanceRepository.save(facultyAttendance);
//        }
//        baseResponse.setMessage("Successfully Updated ");
//        return baseResponse;
//
//    }
//
//    @Override
//    public BaseResponse verifyOutAttendance(FacAttendVerifyRequest facAttendVerifyRequest) {
//        BaseResponse baseResponse = new BaseResponse();
//        InstituteWifiMaster instituteWifiMaster = instituteWifiRepository.findByBssid(facAttendVerifyRequest.getBssid());
//        if (facAttendVerifyRequest.getRoleId() != null && facAttendVerifyRequest.getRoleId() != 3 && instituteWifiMaster == null) {
//            return null;
//        }
//        String pattern = "yyyy-MM-dd";
//        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
//        String date = simpleDateFormat.format(new Date());
//        FacultyAttendance facultyAttendance = facultyAttendanceRepository.findByAttendanceDateAndFId(date, facAttendVerifyRequest.getfId());
//        if (facultyAttendance != null) {
//            // enter all records
//            Faculty fac = facultyService.read(facultyAttendance.getfId());
//
//            ShiftMaster shiftMaster = shiftRepository.findByInstituteId(fac.getInstituteId());
//            baseResponse.setInstituteInTime(shiftMaster.getInTime());
//            baseResponse.setInstituteOutTime(shiftMaster.getOutTime());
//            facultyAttendance.setAndroidID(facAttendVerifyRequest.getAndroidID());
//            facultyAttendance.setBSSID(facAttendVerifyRequest.getBssid());
//            facultyAttendance.setIMEINo(facAttendVerifyRequest.getIMEINo());
//            facultyAttendance.setSSID(facAttendVerifyRequest.getSsid());
//            facultyAttendance.setMacID(facAttendVerifyRequest.getMacID());
//            facultyAttendance.setInstituteTypeId(fac.getInstituteTypeId());
//            DateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");
//            facultyAttendance.setInstituteId(fac.getInstituteId());
//            facultyAttendance.setDepartmentId(fac.getDepartmentId());
//
//            if (facultyAttendance.getInTime() == null) {
//                baseResponse.setMessage("Please fill in attendance first");
//
//            } else {
//                baseResponse.setMessage("Successfully Updated ");
//                Date todayDate = Calendar.getInstance().getTime();
//                String reportOutTime = dateFormat.format(todayDate);
//                facultyAttendance.setOutTime(reportOutTime);
//                baseResponse.setOutTime(reportOutTime);
//            }
//
//            facultyAttendanceRepository.save(facultyAttendance);
//        } else {
//            baseResponse.setMessage("Please fill in attendance first");
//        }
//
//        return baseResponse;
//
//    }
    @Override
    public FacultyAttendance lateAttendanceReason(LateAttendanceReasonRequest lateAttendanceReasonRequest) {
        String pattern = "yyyy-MM-dd";
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
        String date = simpleDateFormat.format(new Date());
        FacultyAttendance facultyAttendance = facultyAttendanceRepository.findByAttendanceDateAndFId(date,
         lateAttendanceReasonRequest.getFacultyId());
        DateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");
        Date todayDate = Calendar.getInstance().getTime();
        String reportTime = dateFormat.format(todayDate);

        if (facultyAttendance == null) {
            return null;
        }

        if (lateAttendanceReasonRequest.getEarlyOutReason() != null) {
            facultyAttendance.setEarlyOutReason(lateAttendanceReasonRequest.getEarlyOutReason());
            facultyAttendance.setOutTime(reportTime);
            facultyAttendance.setIsEarly(Boolean.TRUE);
        }
        if (lateAttendanceReasonRequest.getLateComingReason() != null) {
            facultyAttendance.setLateComingReason(lateAttendanceReasonRequest.getLateComingReason());
            facultyAttendance.setStatus(Boolean.TRUE);
            facultyAttendance.setPresenceStatus(Boolean.TRUE);
            facultyAttendance.setInTime(reportTime);
            facultyAttendance.setIsLate(Boolean.TRUE);
        }

        return facultyAttendanceRepository.save(facultyAttendance);
    }

    @Override
    public String facultyAttendanceByInstituteId(AttendanceByPrincipalRequest attendanceByPrincipalRequest) {

        LocalDate startD = LocalDate.now();

        String pattern = "yyyy-MM-dd";
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
        String date = simpleDateFormat.format(new Date());
        // ShiftMaster shiftMaster = shiftRepository.findByInstituteId(attendanceByPrincipalRequest.getInstituteId());
        for (Long facultyId : attendanceByPrincipalRequest.getFacultyIds()) {
            FacultyAttendance fa = facultyAttendanceRepository.findByAttendanceDateAndFId(date, facultyId);
            if (fa != null) {
                fa.setStatus(true);
//                DateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");
//                Date todayDate = Calendar.getInstance().getTime();
//                String reportInTime = dateFormat.format(todayDate);
                if (!attendanceByPrincipalRequest.getReasonByPrincipal().equalsIgnoreCase("Half Pay Leave (HPL)")) {
                    if (attendanceByPrincipalRequest.getTypeId() == 99) {
                        if (fa.getInTime() == null) {
                            fa.setInTime(CustDateFormat.todayTime());

                        }
                    } else if (attendanceByPrincipalRequest.getTypeId() == 100) {
                        if (fa.getOutTime() == null) {
                            fa.setOutTime(CustDateFormat.todayTime());

                        }
                    } else if (attendanceByPrincipalRequest.getTypeId() == 6) {
//                        if (fa.getInTime() == null) {
//                            fa.setInTime("#");
//
//                        }
//                       else if (fa.getOutTime() == null) {
//                            fa.setOutTime("#");
//
//                        }
                        fa.setShiftRefId(0L);
                    } else {
                        fa.setInTime("#");
                        fa.setOutTime("#");

                    }
                    fa.setIsEarly(Boolean.FALSE);
                    fa.setIsLate(Boolean.FALSE);
                    //Optional<FacultyShiftMpg> fOptional = facultyShiftMpgRepository.findByIsActiveAndFId(Boolean.TRUE, facultyId);
                    Optional<TrnDtlLeaveStatus> findByIdL = trnDtlLeaveStatusRepository.findByAttdIdAndIsDeletedFalse(fa.getId());

                    //  preparedStatement = c.prepareStatement("update dheemati_attendance.faculty_attendance set status = true,is_principal_filled = true,leave_status = 1 ,presence_status=true,in_time='#',out_time='#' where f_id not in (SELECT f_id FROM dheemati_attendance.trn_longleave_dtls where leave_type_id  in (4,11,15,16,17,18,19) and to_date >= '" + startDate + "') and attendance_date = '" + startDate + "';");
                    if (findByIdL.isPresent()) {
                        System.err.println("Edit Long Leave of Faculty!");
                        findByIdL.get().setIsDeleted(Boolean.TRUE);
                        trnDtlLeaveStatusRepository.save(findByIdL.get());
                    }

                    if (fa.getSchdlRefId() != 0L) {
                        Optional<TrnLongleaveDtls> tOptional = trnLongleaveDtlsRepository.findById(fa.getSchdlRefId());
                        if (tOptional.isPresent()) {
                            tOptional.get().setIsDeleted(Boolean.TRUE);
                            trnLongleaveDtlsRepository.save(tOptional.get());
                            fa.setSchdlRefId(0L);
                            fa.setSchdlTbleId(0L);
                        }
                    }
                    fa.setIsPrincipalFilled(Boolean.TRUE);
                    if (attendanceByPrincipalRequest.getTypeId() == 99 || attendanceByPrincipalRequest.getTypeId() == 100) {
                        fa.setIsPrincipalFilled(Boolean.FALSE);
                        fa.setUpdateBy(attendanceByPrincipalRequest.getByFacultyId());
                        fa.setPresenceStatus(Boolean.TRUE);
                        fa.setLeaveStatus(2L);
                        TrnbyPriHodLog trnbyPriHodLog = new TrnbyPriHodLog();
                        try {
                            trnbyPriHodLog.setAttendanceDate(CustDateFormat.SQLTypeStringToDate(date));
                        } catch (ParseException ex) {
                            trnbyPriHodLog.setAttendanceDate(new Date());

                        }
                        trnbyPriHodLog.setActlogId(attendanceByPrincipalRequest.getActLogId());
                        trnbyPriHodLog.setFId(fa.getfId());
                        trnbyPriHodLog.setById(attendanceByPrincipalRequest.getByFacultyId());
                        if (attendanceByPrincipalRequest.getTypeId() == 99) {
                            trnbyPriHodLog.setMarkType(Boolean.FALSE);
                        } else {
                            trnbyPriHodLog.setMarkType(Boolean.TRUE);
                        }

                        trnbyPriHodLogService.create(trnbyPriHodLog);

                    } else {
                        // Set Based on Status Type

                        Optional<LeaveTypeMaster> findById = leaveTypeMasterRepository.findById(attendanceByPrincipalRequest.getTypeId());
                        if (findById.isPresent()) {
                            System.out.println("Type : " + findById.get().getTypeId());
                            if (findById.get().getTypeId() == 0) {
                                fa.setPresenceStatus(Boolean.TRUE);
                                fa.setLeaveStatus(1L);
                            } else {
                                fa.setPresenceStatus(Boolean.FALSE);
                                fa.setLeaveStatus(1L);
                            }
                        } else {
                            System.out.println("Not receving data");
                            fa.setPresenceStatus(Boolean.FALSE);
                            fa.setLeaveStatus(1L);
                        }

                        //  PreparedStatement preparedStatement = c.prepareStatement("update dheemati_attendance.faculty_attendance set status = true,is_principal_filled = true,leave_status = 1 ,presence_status=false,in_time='#',out_time='#' where f_id in (SELECT f_id FROM dheemati_attendance.trn_longleave_dtls where leave_type_id  in (4,11,15,16,17,18,19) and to_date >= '" + startDate + "') and attendance_date = '" + startDate + "';");
//                        Optional<TrnDtlLeaveStatus> findByIdL = trnDtlLeaveStatusRepository.findByAttdIdAndIsDeletedFalse(fa.getId());
//
//                        //  preparedStatement = c.prepareStatement("update dheemati_attendance.faculty_attendance set status = true,is_principal_filled = true,leave_status = 1 ,presence_status=true,in_time='#',out_time='#' where f_id not in (SELECT f_id FROM dheemati_attendance.trn_longleave_dtls where leave_type_id  in (4,11,15,16,17,18,19) and to_date >= '" + startDate + "') and attendance_date = '" + startDate + "';");
//                        if (findByIdL.isPresent()) {
//                            System.err.println("Edit Long Leave of Faculty!");
//                            findByIdL.get().setIsDeleted(Boolean.TRUE);
//                            trnDtlLeaveStatusRepository.save(findByIdL.get());
//                        }
                        //
                        TrnDtlLeaveStatus trnDtlLeaveStatus = new TrnDtlLeaveStatus();
                        trnDtlLeaveStatus.setAttendance_Date(new Date());
                        trnDtlLeaveStatus.setfId(facultyId);
                        trnDtlLeaveStatus.setAttdId(fa.getId());
                        String yrF = "yyyy";
                        SimpleDateFormat simpleDateFormatyr = new SimpleDateFormat(yrF);
                        String yr = simpleDateFormatyr.format(new Date());
                        trnDtlLeaveStatus.setIsDeleted(Boolean.FALSE);
                        trnDtlLeaveStatus.setYear(new Long(yr));
                        trnDtlLeaveStatus.setLeaveTypeId(attendanceByPrincipalRequest.getTypeId());
                        trnDtlLeaveStatus.setLeaveDesc(attendanceByPrincipalRequest.getReasonByPrincipal());
                        trnDtlLeaveStatusRepository.save(trnDtlLeaveStatus);
                    }

                    System.out.println("Duration :: " + attendanceByPrincipalRequest.getDuration());
                    if (attendanceByPrincipalRequest.getDuration() > 1) {
                        System.out.println("Call Long Duration");

                        //
                        TrnLongleaveDtls trnLongleaveDtls = new TrnLongleaveDtls();
                        trnLongleaveDtls.setFromDate(attendanceByPrincipalRequest.getStartDate());
                        trnLongleaveDtls.setToDate(attendanceByPrincipalRequest.getEndDate());
                        trnLongleaveDtls.setfId(facultyId);
                        trnLongleaveDtls.setLeaveTypeId(attendanceByPrincipalRequest.getTypeId());
                        trnLongleaveDtlsRepository.save(trnLongleaveDtls);
                        fa.setSchdlRefId(trnLongleaveDtls.getId());
                        fa.setSchdlTbleId(1L);
                    }
                }

                fa.setReasonByPrincipal(attendanceByPrincipalRequest.getReasonByPrincipal());
                facultyAttendanceRepository.save(fa);
            }

        }

        return "updated successfully";
    }

    @Override
    public FacultyAttendance readByFacultyId(Long fId) {

        FacultyAttendance fa = facultyAttendanceRepository.findByAttendanceDateAndFId(CustDateFormat.todaySQLString(), fId);

        if (fa == null) {
            return null;
        }
        return fa;
    }

    @Override
    public BaseResponse checkInAttendance(FacAttendVerifyRequest facAttendVerifyRequest) {

        InstituteWifiMaster instituteWifiMaster1 = new InstituteWifiMaster(); //***
        BaseResponse baseResponse = new BaseResponse();
        String date = CustDateFormat.todaySQLString();

        /*
            Check Validation of Android Version
         */
        Optional<AndroidVersion> optional = androidVersionRepository.findByVersionNumberAndIsActive(facAttendVerifyRequest.getVersionCode(), Boolean.TRUE);
        if (!optional.isPresent()) {
            System.out.println("Version Code ::" + facAttendVerifyRequest.getVersionCode());
            baseResponse.setMessage("Lower Version");
            System.out.println("Message Code ::" + baseResponse.getMessage());
            return baseResponse;
        }

        /*
            Check Valid BSSID or NOT!
         */
        InstituteWifiMaster instituteWifiMaster = instituteWifiRepository.findByBssid(facAttendVerifyRequest.getBssid());
        if (instituteWifiMaster == null) {
            System.out.println("Invalid bsssid ::" + facAttendVerifyRequest.getBssid() + " :: " + facAttendVerifyRequest.getfId());
            baseResponse.setMessage("Invalid BSSID!");
            // System.out.println("Message Code ::" + baseResponse.getMessage());

            /*
                If BSSID is NAMO Wifi then add on System by using Faculty Id and Institute Id
             */
//            if (facAttendVerifyRequest.getSsid().trim().equalsIgnoreCase("\"NAMO WiFi\"")) {
//                FacultyAttendance facultyAttendance = facultyAttendanceRepository.findByAttendanceDateAndFId(date, facAttendVerifyRequest.getfId());
//                instituteWifiMaster1.setSsid(facAttendVerifyRequest.getSsid().trim());
//                instituteWifiMaster1.setBssid(facAttendVerifyRequest.getBssid());
//                instituteWifiMaster1.setInstituteId(facultyAttendance.getInstituteId());
//                instituteWifiMaster1.setCreatedBy(facultyAttendance.getId());
//                instituteWifiRepository.save(instituteWifiMaster1);
//            }
            return baseResponse;
        }

        /*
            Faculty Attendance Data based on Today and fId
        
        
         */
        FacultyAttendance facultyAttendance = null;
        if (facAttendVerifyRequest.getFacultyAttendanceId() != null) {
            Optional<FacultyAttendance> optional1 = facultyAttendanceRepository.findByIdAndAttendanceDateAndFId(facAttendVerifyRequest.getFacultyAttendanceId(), date, facAttendVerifyRequest.getfId());
            if (optional1.isPresent()) {
                System.out.println("Using FA_ID ::" + facAttendVerifyRequest.getFacultyAttendanceId());
                facultyAttendance = optional1.get();
            } else if (facAttendVerifyRequest.getfId() != null) {
                System.out.println("Using FA_ID but record Invalid ::" + facAttendVerifyRequest.getFacultyAttendanceId());
                facultyAttendance = facultyAttendanceRepository.findByAttendanceDateAndFId(date, facAttendVerifyRequest.getfId());
            } else {
                baseResponse.setMessage("Your Data are Invalid!");
                System.out.println("Message Code ::" + baseResponse.getMessage());
                return baseResponse;
            }
        } else {
            facultyAttendance = facultyAttendanceRepository.findByAttendanceDateAndFId(date, facAttendVerifyRequest.getfId());
        }

        baseResponse.setFacultyAttendanceId(facultyAttendance.getId());

        /*
            Check if IN Attendance Already Mark 
         */
        if (facultyAttendance.getInTime() != null) {
            baseResponse.setMessage("Already Marked!");
            baseResponse.setInTime(facultyAttendance.getInTime());
            return baseResponse;
        }

        /*
            Check Faculty Authenticated or Not!
         */
        if (!facAttendVerifyRequest.getIsVerified()) {
            throw new FASException(FASMessageType.QR_CODE_INVALID);
        } else {

            //FacultyAttendance facultyAttendance = facultyAttendanceRepository.findByAttendanceDate(date);
//            Optional<FacultyShiftMpg> fOptional = facultyShiftMpgRepository.findByIsActiveAndFId(Boolean.TRUE, facAttendVerifyRequest.getfId());
//            if (fOptional.isPresent()) {
//
//                ShiftMaster shiftMaster = shiftRepository.findById(fOptional.get().getShiftId());
//                baseResponse.setInstituteInTime(shiftMaster.getInTime());
//                baseResponse.setInstituteOutTime(shiftMaster.getOutTime());
//            }
            baseResponse.setInstituteInTime(facultyAttendance.getActualInTime());
            baseResponse.setInstituteOutTime(facultyAttendance.getActualOutTime());

            /*
                Fetch Current Time and Faculty IN Time and Split in Hours and Minute
             */
            String reportInTime = CustDateFormat.todayTime();
            baseResponse.setInTime(reportInTime);
            String times[] = reportInTime.split(":");
            int hours = Integer.parseInt(times[0]);
            int minute = Integer.parseInt(times[1]);
            String Fixtimes[] = facultyAttendance.getActualInTime().split(":");
            int Fixhours = Integer.parseInt(Fixtimes[0]);
            int Fixminute = Integer.parseInt(Fixtimes[1]);

            /*
                Add LATE_IN_ALLOW for Attendance
             */
            if (Fixminute > (59 - MASConstant.LATE_IN_ALLOW)) {
                Fixminute = Fixminute - (60 - MASConstant.LATE_IN_ALLOW);
                Fixhours++;
            } else {
                Fixminute += MASConstant.LATE_IN_ALLOW;
            }

            /*
                Check Faculty Late IN or NOT 
                And Ignore when Weekend or Holiday or Shift not Mapped!
             */
            if (facultyAttendance.getShiftRefId() != 0L && (hours > Fixhours || (hours == Fixhours && minute > Fixminute))) {
                baseResponse.setMessage("Late In Request!");
                baseResponse.setIsLate(Boolean.TRUE);
                return baseResponse;
            }

            /*
                Set Basic Data for IN Attendance
             */
            facultyAttendance.setAndroidID(facAttendVerifyRequest.getAndroidID());
            facultyAttendance.setBSSID(facAttendVerifyRequest.getBssid());
            facultyAttendance.setIMEINo(facAttendVerifyRequest.getIMEINo());
            facultyAttendance.setSSID(facAttendVerifyRequest.getSsid());
            facultyAttendance.setMacID(facAttendVerifyRequest.getMacID());
            facultyAttendance.setPresenceStatus(Boolean.TRUE);
            facultyAttendance.setLeaveStatus(0L);
            facultyAttendance.setStatus(Boolean.TRUE);
            facultyAttendance.setInTime(reportInTime);
            TrnAttendanceLog trnAttendanceLog = new TrnAttendanceLog();
            trnAttendanceLog.setActlogId(facAttendVerifyRequest.getActLogId());
            trnAttendanceLog.setBSSID(facAttendVerifyRequest.getBssid());
            trnAttendanceLog.setSSID(facAttendVerifyRequest.getSsid());
            trnAttendanceLog.setLocLat(facAttendVerifyRequest.getLocLat());

            trnAttendanceLog.setLocLong(facAttendVerifyRequest.getLocLng());

            trnAttendanceLog.setAttType(Boolean.FALSE);
            trnAttendanceLog.setMarkType(Boolean.FALSE);

            trnAttendanceLogService.create(trnAttendanceLog);
            facultyAttendanceRepository.save(facultyAttendance);
            baseResponse.setMessage("Successfully Marked!");

        }
        return baseResponse;
    }

    @Override
    public BaseResponse checkLocInAttendance(FacAttendVerifyRequest facAttendVerifyRequest) {

        InstituteWifiMaster instituteWifiMaster1 = new InstituteWifiMaster(); //***
        BaseResponse baseResponse = new BaseResponse();
        String date = CustDateFormat.todaySQLString();
        if (!trnActiveLoginLogRepository.existsByIdAndFIdAndDeviceIdAndIsActiveTrue(facAttendVerifyRequest.getActLogId(), facAttendVerifyRequest.getfId(), facAttendVerifyRequest.getDeviceTypeId())) {
            baseResponse.setErrorStatus(400L, "Login from Other Device");
            baseResponse.setIsLogin(Boolean.FALSE);
            baseResponse.setMessage("Login from Other Device");
            return baseResponse;
        }
        /*
            Check Validation of Android Version
         */
//        Optional<AndroidVersion> optional = androidVersionRepository.findByVersionNumberAndIsActive(facAttendVerifyRequest.getVersionCode(), Boolean.TRUE);
//        if (!optional.isPresent()) {
//            System.out.println("Version Code ::" + facAttendVerifyRequest.getVersionCode());
//            baseResponse.setMessage("Lower Version");
//            System.out.println("Message Code ::" + baseResponse.getMessage());
//            return baseResponse;
//        }

        /*
            Check Valid BSSID or NOT!
         */
        if (!facAttendVerifyRequest.getMarkType()) {
            InstituteWifiMaster instituteWifiMaster = instituteWifiRepository.findByBssid(facAttendVerifyRequest.getBssid());
            if (instituteWifiMaster == null) {
                System.out.println("Invalid bsssid ::" + facAttendVerifyRequest.getBssid() + " :: " + facAttendVerifyRequest.getfId());
                baseResponse.setMessage("Invalid BSSID!");
                // System.out.println("Message Code ::" + baseResponse.getMessage());

                /*
                If BSSID is NAMO Wifi then add on System by using Faculty Id and Institute Id
                 */
//            if (facAttendVerifyRequest.getSsid().trim().equalsIgnoreCase("\"NAMO WiFi\"")) {
//                FacultyAttendance facultyAttendance = facultyAttendanceRepository.findByAttendanceDateAndFId(date, facAttendVerifyRequest.getfId());
//                instituteWifiMaster1.setSsid(facAttendVerifyRequest.getSsid().trim());
//                instituteWifiMaster1.setBssid(facAttendVerifyRequest.getBssid());
//                instituteWifiMaster1.setInstituteId(facultyAttendance.getInstituteId());
//                instituteWifiMaster1.setCreatedBy(facultyAttendance.getId());
//                instituteWifiRepository.save(instituteWifiMaster1);
//            }
                baseResponse.setErrorStatus(401L, "Invalid BSSID of WiFi");
                return baseResponse;
            }
        }
        /*
            Faculty Attendance Data based on Today and fId
        
        
         */
        FacultyAttendance facultyAttendance = null;
        if (facAttendVerifyRequest.getFacultyAttendanceId() != null) {
            Optional<FacultyAttendance> optional1 = facultyAttendanceRepository.findByIdAndAttendanceDateAndFId(facAttendVerifyRequest.getFacultyAttendanceId(), date, facAttendVerifyRequest.getfId());
            if (optional1.isPresent()) {
                System.out.println("Using FA_ID ::" + facAttendVerifyRequest.getFacultyAttendanceId());
                facultyAttendance = optional1.get();
            } //            else if (facAttendVerifyRequest.getfId() != null) {
            //                System.out.println("Using FA_ID but record Invalid ::" + facAttendVerifyRequest.getFacultyAttendanceId());
            //                facultyAttendance = facultyAttendanceRepository.findByAttendanceDateAndFId(date, facAttendVerifyRequest.getfId());
            //            } else {
            //                baseResponse.setMessage("Your Data are Invalid!");
            //                System.out.println("Message Code ::" + baseResponse.getMessage());
            //                return baseResponse;
            //            }
            else {
                baseResponse.setErrorStatus(402L, "Invalid faculty Info ");
                baseResponse.setIsLogin(Boolean.FALSE);
                baseResponse.setMessage("Your Data are Invalid!");
                System.out.println("Message Code ::" + baseResponse.getMessage());
                return baseResponse;
            }
        } else {
            facultyAttendance = facultyAttendanceRepository.findByAttendanceDateAndFId(date, facAttendVerifyRequest.getfId());
        }

        baseResponse.setFacultyAttendanceId(facultyAttendance.getId());

        /*
            Check if IN Attendance Already Mark 
         */
        if (facultyAttendance.getInTime() != null) {
            baseResponse.setMessage("Already Marked!");
            baseResponse.setInTime(facultyAttendance.getInTime());
            return baseResponse;
        }

        /*
            Check Faculty Authenticated or Not!
         */
        if (!facAttendVerifyRequest.getIsVerified()) {
            throw new FASException(FASMessageType.QR_CODE_INVALID);
        } else {

            //FacultyAttendance facultyAttendance = facultyAttendanceRepository.findByAttendanceDate(date);
//            Optional<FacultyShiftMpg> fOptional = facultyShiftMpgRepository.findByIsActiveAndFId(Boolean.TRUE, facAttendVerifyRequest.getfId());
//            if (fOptional.isPresent()) {
//
//                ShiftMaster shiftMaster = shiftRepository.findById(fOptional.get().getShiftId());
//                baseResponse.setInstituteInTime(shiftMaster.getInTime());
//                baseResponse.setInstituteOutTime(shiftMaster.getOutTime());
//            }
            String reportInTime = CustDateFormat.todayTime();
            if (facAttendVerifyRequest.getIsLateIn()) {
                facultyAttendance.setIsLate(facAttendVerifyRequest.getIsLateIn());
                facultyAttendance.setLateComingReason(facAttendVerifyRequest.getLateInReason());
            } else {
                baseResponse.setInstituteInTime(facultyAttendance.getActualInTime());
                baseResponse.setInstituteOutTime(facultyAttendance.getActualOutTime());

                /*
                Fetch Current Time and Faculty IN Time and Split in Hours and Minute
                 */
                baseResponse.setInTime(reportInTime);
                String times[] = reportInTime.split(":");
                int hours = Integer.parseInt(times[0]);
                int minute = Integer.parseInt(times[1]);
                String Fixtimes[] = facultyAttendance.getActualInTime().split(":");
                int Fixhours = Integer.parseInt(Fixtimes[0]);
                int Fixminute = Integer.parseInt(Fixtimes[1]);

                /*
                Add LATE_IN_ALLOW for Attendance
                 */
                if (Fixminute > (59 - MASConstant.LATE_IN_ALLOW)) {
                    Fixminute = Fixminute - (60 - MASConstant.LATE_IN_ALLOW);
                    Fixhours++;
                } else {
                    Fixminute += MASConstant.LATE_IN_ALLOW;
                }

                /*
                Check Faculty Late IN or NOT 
                And Ignore when Weekend or Holiday or Shift not Mapped!
                 */
                if (facultyAttendance.getShiftRefId() != 0L && (hours > Fixhours || (hours == Fixhours && minute > Fixminute))) {
                    baseResponse.setMessage("Late In Request!");
                    baseResponse.setIsLate(Boolean.TRUE);
                    return baseResponse;
                }
            }
            /*
                Set Basic Data for IN Attendance
             */
            facultyAttendance.setAndroidID(facAttendVerifyRequest.getAndroidID());
            facultyAttendance.setBSSID(facAttendVerifyRequest.getBssid());
            facultyAttendance.setIMEINo(facAttendVerifyRequest.getIMEINo());
            facultyAttendance.setSSID(facAttendVerifyRequest.getSsid());
            facultyAttendance.setMacID(facAttendVerifyRequest.getMacID());
            facultyAttendance.setPresenceStatus(Boolean.TRUE);
            facultyAttendance.setLeaveStatus(0L);
            facultyAttendance.setStatus(Boolean.TRUE);
            facultyAttendance.setInTime(reportInTime);
            TrnAttendanceLog trnAttendanceLog = new TrnAttendanceLog();
            trnAttendanceLog.setActlogId(facAttendVerifyRequest.getActLogId());
            trnAttendanceLog.setBSSID(facAttendVerifyRequest.getBssid());
            trnAttendanceLog.setSSID(facAttendVerifyRequest.getSsid());
            trnAttendanceLog.setLocLat(facAttendVerifyRequest.getLocLat());

            trnAttendanceLog.setLocLong(facAttendVerifyRequest.getLocLng());

            trnAttendanceLog.setDeviceId(facAttendVerifyRequest.getAndroidID());
            trnAttendanceLog.setDeviceTypeId(facAttendVerifyRequest.getDeviceTypeId());

            trnAttendanceLog.setAttType(facAttendVerifyRequest.getAttType());

            trnAttendanceLog.setMarkType(facAttendVerifyRequest.getMarkType());

            trnAttendanceLogService.create(trnAttendanceLog);
            facultyAttendanceRepository.save(facultyAttendance);
            baseResponse.setMessage("Successfully Marked!");

        }
        return baseResponse;
    }

    @Override
    public BaseResponse verifyInAttendance(FacAttendVerifyRequest facAttendVerifyRequest) {
        return checkInAttendance(facAttendVerifyRequest);
    }

    @Override
    public BaseResponse verifyOutAttendance(FacAttendVerifyRequest facAttendVerifyRequest) {
        return checkOutAttendance(facAttendVerifyRequest);
    }

    @Override
    public BaseResponse checkOutAttendance(FacAttendVerifyRequest facAttendVerifyRequest) {

        InstituteWifiMaster instituteWifiMaster1 = new InstituteWifiMaster(); //***
        BaseResponse baseResponse = new BaseResponse();
        String date = CustDateFormat.todaySQLString();

        /*
            Check Validation of Android Version
         */
        Optional<AndroidVersion> optional = androidVersionRepository.findByVersionNumberAndIsActive(facAttendVerifyRequest.getVersionCode(), Boolean.TRUE);
        if (!optional.isPresent()) {
            System.out.println("Version Code ::" + facAttendVerifyRequest.getVersionCode());
            baseResponse.setMessage("Lower Version");
            System.out.println("Message Code ::" + baseResponse.getMessage());
            return baseResponse;
        }

        /*
            Check Valid BSSID or NOT!
         */
        InstituteWifiMaster instituteWifiMaster = instituteWifiRepository.findByBssid(facAttendVerifyRequest.getBssid());
        if (instituteWifiMaster == null) {
            System.out.println("Invalid bsssid ::" + facAttendVerifyRequest.getBssid() + " :: " + facAttendVerifyRequest.getfId());
            baseResponse.setMessage("Invalid BSSID!");
            // System.out.println("Message Code ::" + baseResponse.getMessage());

            /*
                If BSSID is NAMO Wifi then add on System by using Faculty Id and Institute Id
             */
            if (facAttendVerifyRequest.getSsid().trim().equalsIgnoreCase("\"NAMO WiFi\"")) {
                FacultyAttendance facultyAttendance = facultyAttendanceRepository.findByAttendanceDateAndFId(date, facAttendVerifyRequest.getfId());
                instituteWifiMaster1.setSsid(facAttendVerifyRequest.getSsid().trim());
                instituteWifiMaster1.setBssid(facAttendVerifyRequest.getBssid());
                instituteWifiMaster1.setInstituteId(facultyAttendance.getInstituteId());
                instituteWifiMaster1.setCreatedBy(facultyAttendance.getId());
                instituteWifiRepository.save(instituteWifiMaster1);
            }
            return baseResponse;
        }

        /*
            Check if Faculty Come with FacultyAttendanceId then get Data based on it => Check Validation for Unsespeias Activity
            Else getting Data based on Today and fId
         */
        FacultyAttendance facultyAttendance = null;
        if (facAttendVerifyRequest.getFacultyAttendanceId() != null) {
            Optional<FacultyAttendance> optional1 = facultyAttendanceRepository.findByIdAndAttendanceDateAndFId(facAttendVerifyRequest.getFacultyAttendanceId(), date, facAttendVerifyRequest.getfId());
            if (optional1.isPresent()) {
                facultyAttendance = optional1.get();
            } else {
                baseResponse.setMessage("Your Data are Invalid!");
                System.out.println("Message Code ::" + baseResponse.getMessage());
                return baseResponse;
            }
        } else {
            facultyAttendance = facultyAttendanceRepository.findByAttendanceDateAndFId(date, facAttendVerifyRequest.getfId());
        }

        if (!facAttendVerifyRequest.getIsVerified()) {
            throw new FASException(FASMessageType.QR_CODE_INVALID);
        } else {
            //FacultyAttendance facultyAttendance = facultyAttendanceRepository.findByAttendanceDate(date);

            /*
                Check IN Attendance Marked or Not
                If Not then give Msg for that
             */
            if (facultyAttendance.getInTime() == null) {
                baseResponse.setIsEarly(Boolean.FALSE);
                baseResponse.setMessage("Please, Mark IN attendance First!");
                return baseResponse;
            }
            String reportOutTime = CustDateFormat.todayTime();
            if (facAttendVerifyRequest.getIsEarlyOut()) {
                facultyAttendance.setIsEarly(facAttendVerifyRequest.getIsEarlyOut());
                facultyAttendance.setEarlyOutReason(facAttendVerifyRequest.getEarlyOutReason());
            } else {
                baseResponse.setInTime(facultyAttendance.getInTime());
//            Optional<FacultyShiftMpg> fOptional = facultyShiftMpgRepository.findByIsActiveAndFId(Boolean.TRUE, facAttendVerifyRequest.getfId());
//            if (fOptional.isPresent()) {
//                ShiftMaster shiftMaster = shiftRepository.findById(fOptional.get().getShiftId());
//                baseResponse.setInstituteInTime(shiftMaster.getInTime());
//                baseResponse.setInstituteOutTime(shiftMaster.getOutTime());
//            }

                baseResponse.setInstituteInTime(facultyAttendance.getActualInTime());
                baseResponse.setInstituteOutTime(facultyAttendance.getActualOutTime());

                /*
                Fetch Current Time and Faculty IN Time and Split in Hours and Minute
                 */
                // String reportOutTime = CustDateFormat.todayTime();
                baseResponse.setOutTime(reportOutTime);
                String times[] = reportOutTime.split(":");
                int hours = Integer.parseInt(times[0]);
                int minute = Integer.parseInt(times[1]);
                String Fixtimes[] = facultyAttendance.getActualOutTime().split(":");
                int Fixhours = Integer.parseInt(Fixtimes[0]);
                int Fixminute = Integer.parseInt(Fixtimes[1]);

                /*
               Set Earlty OUT Allow 
                 */
                if (Fixminute < MASConstant.EARLY_OUT_ALLOW) {
                    Fixminute = (60 - MASConstant.EARLY_OUT_ALLOW) + Fixminute;
                    Fixhours--;
                } else {
                    Fixminute -= MASConstant.EARLY_OUT_ALLOW;
                }

                /*
                Is Early Out condition and 
                Not check for Weekend or Holiday or Faculty Shift Not Mapping
                 */
                baseResponse.setIsEarly(Boolean.FALSE);
                if (facultyAttendance.getShiftRefId() != 0L && (hours < Fixhours || (hours == Fixhours && minute < Fixminute))) {
                    baseResponse.setIsEarly(Boolean.TRUE);
                    baseResponse.setMessage("Early Out Request!");
                    return baseResponse;
                }
            }
            /*
                Set Mark OUT Basic conditions  
             */
            baseResponse.setOutTime(reportOutTime);
            baseResponse.setMessage("Successfully Marked!");
            facultyAttendance.setOutTime(reportOutTime);
            facultyAttendance.setOutAndroidID(facAttendVerifyRequest.getAndroidID());
            facultyAttendance.setOutBSSID(facAttendVerifyRequest.getBssid());
            facultyAttendance.setOutIMEINo(facAttendVerifyRequest.getIMEINo());
            facultyAttendance.setOutSSID(facAttendVerifyRequest.getSsid());
            facultyAttendance.setOutMacID(facAttendVerifyRequest.getMacID());
            TrnAttendanceLog trnAttendanceLog = new TrnAttendanceLog();
            trnAttendanceLog.setActlogId(facAttendVerifyRequest.getActLogId());
            trnAttendanceLog.setBSSID(facAttendVerifyRequest.getBssid());
            trnAttendanceLog.setSSID(facAttendVerifyRequest.getSsid());
            trnAttendanceLog.setLocLat(facAttendVerifyRequest.getLocLat());

            trnAttendanceLog.setLocLong(facAttendVerifyRequest.getLocLng());

            trnAttendanceLog.setAttType(Boolean.FALSE);
            trnAttendanceLog.setMarkType(Boolean.TRUE);

            trnAttendanceLogService.create(trnAttendanceLog);
            facultyAttendanceRepository.save(facultyAttendance);
        }
        return baseResponse;
    }

    @Override
    public BaseResponse checkLocOutAttendance(FacAttendVerifyRequest facAttendVerifyRequest) {

        // InstituteWifiMaster instituteWifiMaster1 = new InstituteWifiMaster(); //***
        BaseResponse baseResponse = new BaseResponse();
        String date = CustDateFormat.todaySQLString();
        if (!trnActiveLoginLogRepository.existsByIdAndFIdAndDeviceIdAndIsActiveTrue(facAttendVerifyRequest.getActLogId(), facAttendVerifyRequest.getfId(), facAttendVerifyRequest.getDeviceTypeId())) {
            baseResponse.setErrorStatus(400L, "Login from Other Device");
            baseResponse.setIsLogin(Boolean.FALSE);
            baseResponse.setMessage("Login from Other Device");
            return baseResponse;
        }
        /*
            Check Validation of Android Version
         */
//        Optional<AndroidVersion> optional = androidVersionRepository.findByVersionNumberAndIsActive(facAttendVerifyRequest.getVersionCode(), Boolean.TRUE);
//        if (!optional.isPresent()) {
//            System.out.println("Version Code ::" + facAttendVerifyRequest.getVersionCode());
//            baseResponse.setMessage("Lower Version");
//            System.out.println("Message Code ::" + baseResponse.getMessage());
//            return baseResponse;
//        }

        /*
            Check Valid BSSID or NOT!
         */
        if (!facAttendVerifyRequest.getMarkType()) {
            InstituteWifiMaster instituteWifiMaster = instituteWifiRepository.findByBssid(facAttendVerifyRequest.getBssid());
            if (instituteWifiMaster == null) {
                System.out.println("Invalid bsssid ::" + facAttendVerifyRequest.getBssid() + " :: " + facAttendVerifyRequest.getfId());
                baseResponse.setMessage("Invalid BSSID!");
                // System.out.println("Message Code ::" + baseResponse.getMessage());

                /*
                If BSSID is NAMO Wifi then add on System by using Faculty Id and Institute Id
                 */
//            if (facAttendVerifyRequest.getSsid().trim().equalsIgnoreCase("\"NAMO WiFi\"")) {
//                FacultyAttendance facultyAttendance = facultyAttendanceRepository.findByAttendanceDateAndFId(date, facAttendVerifyRequest.getfId());
//                instituteWifiMaster1.setSsid(facAttendVerifyRequest.getSsid().trim());
//                instituteWifiMaster1.setBssid(facAttendVerifyRequest.getBssid());
//                instituteWifiMaster1.setInstituteId(facultyAttendance.getInstituteId());
//                instituteWifiMaster1.setCreatedBy(facultyAttendance.getId());
//                instituteWifiRepository.save(instituteWifiMaster1);
//            }
                baseResponse.setErrorStatus(401L, "Invalid BSSID of WiFi");
                return baseResponse;
            }
        }

        /*
            Check if Faculty Come with FacultyAttendanceId then get Data based on it => Check Validation for Unsespeias Activity
            Else getting Data based on Today and fId
         */
        FacultyAttendance facultyAttendance = null;
        if (facAttendVerifyRequest.getFacultyAttendanceId() != null) {
            Optional<FacultyAttendance> optional1 = facultyAttendanceRepository.findByIdAndAttendanceDateAndFId(facAttendVerifyRequest.getFacultyAttendanceId(), date, facAttendVerifyRequest.getfId());
            if (optional1.isPresent()) {
                facultyAttendance = optional1.get();
            } else {
                baseResponse.setErrorStatus(402L, "Invalid faculty Info ");
                baseResponse.setIsLogin(Boolean.FALSE);
                baseResponse.setMessage("Your Data are Invalid!");
                System.out.println("Message Code ::" + baseResponse.getMessage());
                return baseResponse;
            }
        } else {
            facultyAttendance = facultyAttendanceRepository.findByAttendanceDateAndFId(date, facAttendVerifyRequest.getfId());
        }

        if (!facAttendVerifyRequest.getIsVerified()) {
            throw new FASException(FASMessageType.QR_CODE_INVALID);
        } else {
            //FacultyAttendance facultyAttendance = facultyAttendanceRepository.findByAttendanceDate(date);

            /*
                Check IN Attendance Marked or Not
                If Not then give Msg for that
             */
            if (facultyAttendance.getInTime() == null) {
                baseResponse.setIsEarly(Boolean.FALSE);
                baseResponse.setMessage("Please, Mark IN attendance First!");
                return baseResponse;
            }
            baseResponse.setInTime(facultyAttendance.getInTime());
//            Optional<FacultyShiftMpg> fOptional = facultyShiftMpgRepository.findByIsActiveAndFId(Boolean.TRUE, facAttendVerifyRequest.getfId());
//            if (fOptional.isPresent()) {
//                ShiftMaster shiftMaster = shiftRepository.findById(fOptional.get().getShiftId());
//                baseResponse.setInstituteInTime(shiftMaster.getInTime());
//                baseResponse.setInstituteOutTime(shiftMaster.getOutTime());
//            }

            baseResponse.setInstituteInTime(facultyAttendance.getActualInTime());
            baseResponse.setInstituteOutTime(facultyAttendance.getActualOutTime());

            /*
                Fetch Current Time and Faculty IN Time and Split in Hours and Minute
             */
            String reportOutTime = CustDateFormat.todayTime();
            baseResponse.setOutTime(reportOutTime);
            String times[] = reportOutTime.split(":");
            int hours = Integer.parseInt(times[0]);
            int minute = Integer.parseInt(times[1]);
            String Fixtimes[] = facultyAttendance.getActualOutTime().split(":");
            int Fixhours = Integer.parseInt(Fixtimes[0]);
            int Fixminute = Integer.parseInt(Fixtimes[1]);

            /*
               Set Earlty OUT Allow 
             */
            if (Fixminute < MASConstant.EARLY_OUT_ALLOW) {
                Fixminute = (60 - MASConstant.EARLY_OUT_ALLOW) + Fixminute;
                Fixhours--;
            } else {
                Fixminute -= MASConstant.EARLY_OUT_ALLOW;
            }

            /*
                Is Early Out condition and 
                Not check for Weekend or Holiday or Faculty Shift Not Mapping
             */
            baseResponse.setIsEarly(Boolean.FALSE);
            if (facultyAttendance.getShiftRefId() != 0L && (hours < Fixhours || (hours == Fixhours && minute < Fixminute))) {
                baseResponse.setIsEarly(Boolean.TRUE);
                baseResponse.setMessage("Early Out Request!");
                return baseResponse;
            }

            /*
                Set Mark OUT Basic conditions  
             */
            baseResponse.setOutTime(reportOutTime);
            baseResponse.setMessage("Successfully Marked!");
            facultyAttendance.setOutTime(reportOutTime);
            facultyAttendance.setOutAndroidID(facAttendVerifyRequest.getAndroidID());
            facultyAttendance.setOutBSSID(facAttendVerifyRequest.getBssid());
            facultyAttendance.setOutIMEINo(facAttendVerifyRequest.getIMEINo());
            facultyAttendance.setOutSSID(facAttendVerifyRequest.getSsid());
            facultyAttendance.setOutMacID(facAttendVerifyRequest.getMacID());

            TrnAttendanceLog trnAttendanceLog = new TrnAttendanceLog();
            trnAttendanceLog.setActlogId(facAttendVerifyRequest.getActLogId());
            trnAttendanceLog.setBSSID(facAttendVerifyRequest.getBssid());
            trnAttendanceLog.setSSID(facAttendVerifyRequest.getSsid());
            trnAttendanceLog.setLocLat(facAttendVerifyRequest.getLocLat());
            trnAttendanceLog.setDeviceId(facAttendVerifyRequest.getAndroidID());
            trnAttendanceLog.setDeviceTypeId(facAttendVerifyRequest.getDeviceTypeId());
            trnAttendanceLog.setLocLong(facAttendVerifyRequest.getLocLng());

            trnAttendanceLog.setAttType(facAttendVerifyRequest.getAttType());

            trnAttendanceLog.setMarkType(facAttendVerifyRequest.getMarkType());

            trnAttendanceLogService.create(trnAttendanceLog);
            facultyAttendanceRepository.save(facultyAttendance);
        }
        return baseResponse;
    }

    @Override
    public void dailySchedule() {
        //  Map<Long, Long> LeaveFId = new HashMap<>();

        Date now = new Date(); // Use for Getting Current Local Date 
        String date = CustDateFormat.dateToSQLString(now); // Getting Today String

        List<FacultyAttendance> facultyAtt = facultyAttendanceRepository.findAllByAttendanceDate(date); // Geting Faculty Attendance Data based on Today Date

        if (facultyAtt.size() <= 0) { // Check Condition if Today Data not Available then 

            Long holidayId = 0L; //for Holiday Id if today Holiday
            Long LeaveStatus = 0L; // for Manage Leave status if today Holiday or Weekend
            String LeaveTypeName = null; // for Storing Leave type ***
            boolean flagHW = false; // Manage Holiday and Weeked Flag for All Faculty
            boolean flagT = false; // Manage Weekend for Technical Institute

            int weekDay = CustDateFormat.weekDay(now);// Use for getting Week Day Number 
            int weekNumber = CustDateFormat.weekNumberOfMonth(now); // Use for Getting week number of Month

            if (weekDay == 7) { // Weekend for Sunday
                flagHW = true;
                LeaveStatus = 2L;
            } else if (weekDay == 6 && (weekNumber % 2) == 0) { // Weekend for Technical Saturday
                flagT = true;
                LeaveStatus = 2L;
            } else {
                try {
                    Optional<HolidayMaster> hOptional = holidayMasterRepository.findByHolidayDateAndIsDeletedFalse(CustDateFormat.SQLTypeStringToDate(date)); // for finding Public Holiday  
                    if (hOptional.isPresent()) {
                        LeaveStatus = 3L;
                        flagHW = true;
                        holidayId = hOptional.get().getId(); // Store Holiday Id 
                    }
                } catch (Exception ex) {
                    java.util.logging.Logger.getLogger(FacultyAttendanceServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
                }
            }

            Map<Long, TrnFSPrgmSchd> TrainerFIdAndInst = finishingSchoolScheduler();
            Map<Long, LongleaveView> longMap = longleaveSchedule(); //***
            Map<Long, FacultyShiftTiming> facShiftMap = null;
            Map<Long, TrnInstMisfnclty> instMisFncMap = trnInstMisfncltyService.listByDate(now, 1L);
            Map<Long, TrnInstMisfnclty> instMisFncMap1 = trnInstMisfncltyService.listByDate(now, 2L);
            Map<Long, TrnInstMisfnclty> instMisFncMap2 = trnInstMisfncltyService.listByDate(now, 3L);

            //  System.out.println("Size of instMisFncMap  : " + instMisFncMap.size());
            if (weekDay > 0 && weekDay < 7) {
                facShiftMap = facultyShiftMpg(weekDay);
            }

            List<Faculty> facultyList = facultyService.list();

            for (Faculty fac : facultyList) {
                boolean notSpecShift = true;
                /*
                    Check is Faculty Delete or Faculty Role Special Like PS,CHE,CTE and Super Admin
                 */
                if (fac.getIsDeleted() == Boolean.TRUE || RoleTypes.isSpecialRole(fac.getRole())) {
                    continue;
                }

                /*
                    Set Basic Property of Faculty Attendance
                 */
                FacultyAttendance facultyAttendance = new FacultyAttendance();
                facultyAttendance.setAttendanceDate(date);
                facultyAttendance.setCreatedBy(0L);
                facultyAttendance.setfId(fac.getId());
                facultyAttendance.setInstituteTypeId(fac.getInstituteTypeId());
                facultyAttendance.setDepartmentId(fac.getDepartmentId());

                /*
                    Check if Faculty belong to Finishing School
                    And Faculty Schedule Active
                 */
                if (fac.getInstituteTypeId() == 4L) {

                    if (TrainerFIdAndInst.containsKey(fac.getId())) {
                        facultyAttendance.setInstituteId(TrainerFIdAndInst.get(fac.getId()).getInstituteId());
                        facultyAttendance.setSchdlRefId(TrainerFIdAndInst.get(fac.getId()).getId());
                        facultyAttendance.setSchdlTbleId(3L);
                        facultyAttendance.setShiftRefId(TrainerFIdAndInst.get(fac.getId()).getId());
                        facultyAttendance.setShiftTbleId(4L);
                        if (TrainerFIdAndInst.get(fac.getId()).getInTime() != null && !TrainerFIdAndInst.get(fac.getId()).getInTime().isEmpty()) {
                            facultyAttendance.setActualInTime(TrainerFIdAndInst.get(fac.getId()).getInTime());
                        } else {
                            facultyAttendance.setActualInTime("10:00");
                        }
                        if (TrainerFIdAndInst.get(fac.getId()).getOutTime() != null && !TrainerFIdAndInst.get(fac.getId()).getOutTime().isEmpty()) {
                            facultyAttendance.setActualOutTime(TrainerFIdAndInst.get(fac.getId()).getOutTime());
                        } else {
                            facultyAttendance.setActualOutTime("14:00");
                        }
                    } else {
                        continue;
                    }
                } else {
                    facultyAttendance.setInstituteId(fac.getInstituteId());
                }

                TrnInstMisfnclty trnInstMisfnclty = null;
                boolean flag = false; // Use for Type Effected or Not
                if (instMisFncMap != null && instMisFncMap.containsKey(facultyAttendance.getInstituteId())) {
                    trnInstMisfnclty = instMisFncMap.get(facultyAttendance.getInstituteId());
                    flag = true;
                } else if (instMisFncMap1 != null && instMisFncMap1.containsKey(facultyAttendance.getInstituteId()) && fac.getRole() != 8) {
                    trnInstMisfnclty = instMisFncMap1.get(facultyAttendance.getInstituteId());
                    if (fac.getIsTeaching()) {
                        flag = true;
                    }
                } else if (instMisFncMap2 != null && instMisFncMap2.containsKey(facultyAttendance.getInstituteId()) && fac.getRole() == 8) {
                    trnInstMisfnclty = instMisFncMap2.get(facultyAttendance.getInstituteId());
                    if (!fac.getIsTeaching()) {
                        flag = true;
                    }
                }
                boolean TechAvoid = false;
                if (fac.getInstituteTypeId() == 1L && flagT) {
                    TechAvoid = true;
                }

                if (trnInstMisfnclty != null && flag && !TechAvoid) {
                    Long funTypeId = trnInstMisfnclty.getFuncTypeId();
                    // Long applicableType = trnInstMisfnclty.getApplicableToId();
                    Long funId = trnInstMisfnclty.getId();


                    /*
                        Applicable Type 1 for ALL Staff
                        Applicable Type 2 for Teaching
                        Applicable Type 3 for Non-Teaching
                     */
//                    if (applicableType == 1L) {
//                        flag = true;
//                    } else if (applicableType == 2L && fac.getRole() != 8) {
//                        flag = true;
//                    } else if (applicableType == 3L && fac.getRole() == 8) {
//                        flag = true;
//                    }
                    /*
                        Function Type 1 for Special Holiday
                        Function Type 2 for Common Shift
                        Function Type 3 for Vacation 
                        Function Type 4 for Weekend
                     */
                    if (flag) {
                        if (funTypeId == 1L) {
                            facultyAttendance.setSchdlRefId(funId); // Set InstFuncTypeId to Schedule Reff
                            facultyAttendance.setSchdlTbleId(3L); // Institute Holiday
                            facultyAttendance.setActualInTime("00:01");
                            facultyAttendance.setActualOutTime("23:59");
                            facultyAttendance.setStatus(true); // Status true for MEMO Ignore
                            facultyAttendance.setLeaveStatus(3L); // Holiday
                            facultyAttendanceRepository.save(facultyAttendance);
                            continue;

                        } else if (funTypeId == 2L) {
                            if (trnInstMisfnclty.getStartTime() != null && !trnInstMisfnclty.getStartTime().isEmpty()) {
                                facultyAttendance.setActualInTime(trnInstMisfnclty.getStartTime());
                                facultyAttendance.setActualOutTime(trnInstMisfnclty.getEndTime());
                                notSpecShift = false; // Not Apply for shift Setting
                            }
                            facultyAttendance.setShiftRefId(funId); // Set InsFuncTypeId to Schedule Reff
                            facultyAttendance.setShiftTbleId(2L);
                        } else if (funTypeId == 3L) {

                            facultyAttendance.setSchdlRefId(funId); // Set InstFuncTypeId to Schedule Reff
                            facultyAttendance.setSchdlTbleId(4L); // Institute Holiday
                            facultyAttendance.setActualInTime("00:01");
                            facultyAttendance.setActualOutTime("23:59");
                            facultyAttendance.setStatus(true); // Status true for MEMO Ignore
                            facultyAttendance.setLeaveStatus(3L); // Holiday 
                            facultyAttendanceRepository.save(facultyAttendance);
                            continue;

                        } else if (funTypeId == 4L) {
                            if (flagHW || flagT) {

                                flagHW = false; // Ignore Holiday or Weekend
                                flagT = false; // Ignore Holiday or Weekend
                                System.out.println("Type 4 Option selected!");
                            }
                            notSpecShift = false;
                            facultyAttendance.setSchdlRefId(funId);
                            if (trnInstMisfnclty.getStartTime() != null && !trnInstMisfnclty.getStartTime().isEmpty()) { // Check if time not null
                                facultyAttendance.setActualInTime(trnInstMisfnclty.getStartTime());
                                facultyAttendance.setShiftRefId(funId); // Set Id
                            } else {
                                facultyAttendance.setActualInTime("00:01");
                                facultyAttendance.setShiftRefId(0L); // Set Id
                            }
                            if (trnInstMisfnclty.getEndTime() != null && !trnInstMisfnclty.getEndTime().isEmpty()) { // Check if time not null
                                facultyAttendance.setActualOutTime(trnInstMisfnclty.getEndTime());
                                facultyAttendance.setShiftRefId(funId); // Set Id
                            } else {
                                facultyAttendance.setActualInTime("23:59");
                                facultyAttendance.setShiftRefId(0L); // Set Id
                            }
                            facultyAttendance.setShiftTbleId(3L);

                        }
                    }

                }

                /*
                    Check Weekend and Holiday for Individual Faculty 
                    And Shift Mapping if not Holiday and Weekend
                 */
                if (flagHW) {
                    facultyAttendance.setStatus(true);
                    facultyAttendance.setLeaveStatus(LeaveStatus);

                    /*
                        Check for Holiday and set specific property of Holiday
                     */
                    if (LeaveStatus == 3L) {
                        facultyAttendance.setSchdlRefId(holidayId);
                        facultyAttendance.setSchdlTbleId(2L);
                    }

                    facultyAttendance.setActualInTime("00:01");
                    facultyAttendance.setActualOutTime("23:59");
                    facultyAttendance.setShiftRefId(0L);
                    facultyAttendance.setShiftTbleId(0L);
                    facultyAttendanceRepository.save(facultyAttendance);
                } else if (fac.getInstituteTypeId() == 1L && flagT) {
                    facultyAttendance.setStatus(true);
                    facultyAttendance.setLeaveStatus(LeaveStatus);
                    facultyAttendance.setActualInTime("00:01");
                    facultyAttendance.setActualOutTime("23:59");
                    facultyAttendanceRepository.save(facultyAttendance);
                } else if (facultyAttendance.getShiftTbleId() == 4L) {
                    facultyAttendanceRepository.save(facultyAttendance);

                } else {

                    /*
                        Set Individual Faculty Shift based on ShiftMapping and FacultyInstituteShiftMapping
                     */
                    // facultyShiftMpgService.readByFacultyId(fac.getId(), weekDay, facultyAttendance);
                    if (notSpecShift) {
                        if (facShiftMap.containsKey(fac.getId())) {

                            facultyAttendance.setShiftTimeData(facShiftMap.get(fac.getId()));
                        } else {
                            System.out.println("Faculty ID Shift Not Properly Mapped : " + facultyAttendance.getfId());
                            facultyAttendance.setActualInTime("00:02");
                            facultyAttendance.setActualOutTime("23:58");
                        }
                    }

//                    boolean longLeaveAvailable = false;
//                    TrnLongleaveDtls trnLongleaveDtls = null;
//                     System.out.println(fac.getId());
//                    for (TrnLongleaveDtls trnLongleaveDtls1 : trnLongleaveDtlses) {
//                        System.out.print(trnLongleaveDtls.getfId() + " : ");
//                        
//                        if (fac.getId() == trnLongleaveDtls1.getfId()) {
//                            longLeaveAvailable = true;
//                            trnLongleaveDtls = trnLongleaveDtls1;
//
//                            break;
//                        }
//                    }
//                    Optional<TrnLongleaveDtls> tOptional = null;
//                    Date dates = null;
//                    try {
//                        dates = CustDateFormat.SQLTypeStringToDate(date);
//                    } catch (Exception e) {
//                        dates = new Date();
//                    }
//                    tOptional = trnLongleaveDtlsRepository.findByFromDateLessThanEqualAndToDateGreaterThanEqualAndFIdAndIsDeletedFalse(dates,dates, fac.getId());
                    /*
                        If Faculty has Long Leave and set basic property based on it.
                     */
                    if (longMap.containsKey(fac.getId())) {
                        LongleaveView longleaveView = longMap.get(fac.getId());
//                        Optional<LeaveTypeMaster> tOptional1 = leaveTypeMasterRepository.findById(tOptional.get().getLeaveTypeId());
//                        if (tOptional1.isPresent()) {

                        /*
                            Check if Duty and Set statement and then 
                         */
                        if (longleaveView.getTypeId() == 0L) {
                            facultyAttendance.setPresenceStatus(Boolean.TRUE);
                        }

                        facultyAttendance.setStatus(true);
                        facultyAttendance.setLeaveStatus(1L);
                        facultyAttendance.setInTime("#");
                        facultyAttendance.setOutTime("#");
                        facultyAttendance.setSchdlRefId(longleaveView.getLongleaveId());
                        facultyAttendance.setSchdlTbleId(1L);
                        facultyAttendance.setIsPrincipalFilled(Boolean.TRUE);
                        facultyAttendanceRepository.save(facultyAttendance);

                        /*
                            Generate
                         */
//                        TrnDtlLeaveStatus trnDtlLeaveStatus = new TrnDtlLeaveStatus();
//                        try {
//                            trnDtlLeaveStatus.setAttendance_Date(CustDateFormat.SQLTypeStringToDate(CustDateFormat.todaySQLString()));
//                        } catch (Exception e) {
//                            trnDtlLeaveStatus.setAttendance_Date(new Date());
//                        }
//                        trnDtlLeaveStatus.setfId(fac.getId());
//                        String yrF = "yyyy";
//                        SimpleDateFormat simpleDateFormatyr = new SimpleDateFormat(yrF);
//                        String yr = simpleDateFormatyr.format(new Date());
//                        trnDtlLeaveStatus.setYear(new Long(yr));
//                        trnDtlLeaveStatus.setLeaveTypeId(longleaveView.getLeaveTypeId());
//                        trnDtlLeaveStatus.setAttdId(facultyAttendance.getId());
//                        trnDtlLeaveStatusRepository.save(trnDtlLeaveStatus);

                        /*
                            Set Today Leave Dtl Status
                         */
                        setLeaveStatus(fac.getId(), facultyAttendance.getId(), longleaveView.getLeaveTypeId());

                    } else {

                        /* 
                            In general Condititon
                         */
                        facultyAttendanceRepository.save(facultyAttendance);
                    }
                }
            }

            /*
                Send Mail About Daily Schedule Confirmation
             */
            String Mails[] = {"shahbhavya5800@gmail.com", "parthmodi.ic@gmail.com", "sathavaradhaval3094@gmail.com"};
            try {
                sendEmailService.sendSchedulEmail(Mails, TrainerFIdAndInst.size(), longMap.size(), facultyList.size());
            } catch (Exception ex) {
                java.util.logging.Logger.getLogger(FacultyAttendanceServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
            }
            scheduleMasterService.setScheduleData("Daily Attendance Scheduler", 1L, 0L);
        } else {
            scheduleMasterService.setScheduleData("Already Data Added on System!", 1L, 0L);
            System.out.println("Already Data Added on System!");
        }

        System.out.println(
         "Data Added Successfully!");
    }

    /*
        Set Today Leave Status
     */
    public void setLeaveStatus(Long fId, Long attId, Long leaveTypeId) {
        TrnDtlLeaveStatus trnDtlLeaveStatus = new TrnDtlLeaveStatus();
        try {
            trnDtlLeaveStatus.setAttendance_Date(CustDateFormat.SQLTypeStringToDate(CustDateFormat.todaySQLString()));
        } catch (Exception e) {
            trnDtlLeaveStatus.setAttendance_Date(new Date());
        }
        trnDtlLeaveStatus.setfId(fId);
        String yrF = "yyyy";
        SimpleDateFormat simpleDateFormatyr = new SimpleDateFormat(yrF);
        String yr = simpleDateFormatyr.format(new Date());
        trnDtlLeaveStatus.setYear(new Long(yr));
        trnDtlLeaveStatus.setLeaveTypeId(leaveTypeId);
        trnDtlLeaveStatus.setAttdId(attId);
        trnDtlLeaveStatusRepository.save(trnDtlLeaveStatus);
    }


    /*
        Finishing School Schedule Code
     */
    public Map<Long, TrnFSPrgmSchd> finishingSchoolScheduler() {

        System.out.println("Finishing School Scheduler Start : " + new Date());
        Map<Long, TrnFSPrgmSchd> TrainerFIdAndInst = new HashMap<>();
        try {
            String date = CustDateFormat.todaySQLString();
            Class.forName("com.mysql.jdbc.Driver");
            String path = env.getProperty("spring.datasource.url");
            String userName = env.getProperty("spring.datasource.username");
            String password = env.getProperty("spring.datasource.password");
//
            Connection c = DriverManager.getConnection(path, userName, password);
//            // gets a new connection
//            Statement s = c.createStatement();
            //  PreparedStatement preparedStatement = c.prepareStatement("update dheemati_attendance.faculty_attendance set status = true,is_principal_filled = true,leave_status = 1 ,presence_status=false,in_time='#',out_time='#' where f_id in (SELECT f_id FROM dheemati_attendance.trn_longleave_dtls where leave_type_id  in (4,11,15,16,17,18,19) and to_date >= '" + startDate + "') and attendance_date = '" + startDate + "';");
//                PreparedStatement preparedStatement = c.prepareStatement("SELECT f_id,leave_type_id FROM dheemati_attendance.trn_longleave_dtls where leave_type_id  not in (2,4,11,15,16,17,18,19) and to_date >= '" + startDate + "';");
//
//                ResultSet rs = preparedStatement.executeQuery();
//                while (rs.next()) {
//                    LeaveFId.put(rs.getLong("f_id"), rs.getLong("leave_type_id"));
//                }
//                //  preparedStatement = c.prepareStatement("update dheemati_attendance.faculty_attendance set status = true,is_principal_filled = true,leave_status = 1 ,presence_status=true,in_time='#',out_time='#' where f_id not in (SELECT f_id FROM dheemati_attendance.trn_longleave_dtls where leave_type_id  in (4,11,15,16,17,18,19) and to_date >= '" + startDate + "') and attendance_date = '" + startDate + "';");
//                preparedStatement = c.prepareStatement("SELECT f_id,leave_type_id FROM dheemati_attendance.trn_longleave_dtls where leave_type_id  in (2,4,11,15,16,17,18,19) and to_date >= '" + startDate + "';");
//
//                rs = preparedStatement.executeQuery();
//                while (rs.next()) {
//                    DutyFId.put(rs.getLong("f_id"), rs.getLong("leave_type_id"));
//                }

            // Statement s = DBConnection.dbConnection().createStatement();
            PreparedStatement preparedStatement = c.prepareStatement("select * from dheemati_attendance.trn_fs_prgm_schd where ? between from_date and to_date and  is_deleted = false;");
            preparedStatement.setString(1, date);
            ResultSet rs = preparedStatement.executeQuery();
            while (rs.next()) {
                TrnFSPrgmSchd trnFSPrgmSchd = new TrnFSPrgmSchd();
                trnFSPrgmSchd.setInTime(rs.getString("in_time"));
                trnFSPrgmSchd.setOutTime(rs.getString("out_time"));
                trnFSPrgmSchd.setId(rs.getLong("id"));
                trnFSPrgmSchd.setTrainerId(rs.getLong("trainer_id"));
                trnFSPrgmSchd.setInstituteId(rs.getLong("institute_id"));
                TrainerFIdAndInst.put(rs.getLong("trainer_id"), trnFSPrgmSchd);
            }
            rs.close();
            preparedStatement.close();
            c.close();
            scheduleMasterService.setScheduleData("Daily  Trainer Scheduler Verifier", 4L, 0L);
        } catch (Exception ex) {
            scheduleMasterService.setScheduleData("Error During Daily  Trainer Scheduler Verifier", 4L, 0L);
            java.util.logging.Logger.getLogger(FacultyAttendanceServiceImpl.class
             .getName()).log(Level.SEVERE, null, ex);
        }

        System.out.println("Finishing School Scheduler END : " + new Date());
        return TrainerFIdAndInst;
    }

    /*
        Long-Leave Schedule Code
     */
    public Map<Long, LongleaveView> longleaveSchedule() {

        System.out.println("Long Leave  Scheduler Start : " + new Date());
        Map<Long, LongleaveView> lMap = new HashMap<>();

        ScheduleMaster scheduleMaster = new ScheduleMaster();
        try {

            String date = CustDateFormat.todaySQLString();
            Class.forName("com.mysql.jdbc.Driver");
            String path = env.getProperty("spring.datasource.url");
            String userName = env.getProperty("spring.datasource.username");
            String password = env.getProperty("spring.datasource.password");
//
            Connection c = DriverManager.getConnection(path, userName, password);
            //  PreparedStatement preparedStatement = c.prepareStatement("update dheemati_attendance.faculty_attendance set status = true,is_principal_filled = true,leave_status = 1 ,presence_status=false,in_time='#',out_time='#' where f_id in (SELECT f_id FROM dheemati_attendance.trn_longleave_dtls where leave_type_id  in (4,11,15,16,17,18,19) and to_date >= '" + startDate + "') and attendance_date = '" + startDate + "';");
//                PreparedStatement preparedStatement = c.prepareStatement("SELECT f_id,leave_type_id FROM dheemati_attendance.trn_longleave_dtls where leave_type_id  not in (2,4,11,15,16,17,18,19) and to_date >= '" + startDate + "';");
//
//                ResultSet rs = preparedStatement.executeQuery();
//                while (rs.next()) {
//                    LeaveFId.put(rs.getLong("f_id"), rs.getLong("leave_type_id"));
//                }
//                //  preparedStatement = c.prepareStatement("update dheemati_attendance.faculty_attendance set status = true,is_principal_filled = true,leave_status = 1 ,presence_status=true,in_time='#',out_time='#' where f_id not in (SELECT f_id FROM dheemati_attendance.trn_longleave_dtls where leave_type_id  in (4,11,15,16,17,18,19) and to_date >= '" + startDate + "') and attendance_date = '" + startDate + "';");
//                preparedStatement = c.prepareStatement("SELECT f_id,leave_type_id FROM dheemati_attendance.trn_longleave_dtls where leave_type_id  in (2,4,11,15,16,17,18,19) and to_date >= '" + startDate + "';");
//
//                rs = preparedStatement.executeQuery();
//                while (rs.next()) {
//                    DutyFId.put(rs.getLong("f_id"), rs.getLong("leave_type_id"));
//                }
            PreparedStatement preparedStatement = c.prepareStatement("SELECT * FROM longleave_view where from_date <= ? and to_date >= ?;");
            preparedStatement.setString(1, date);
            preparedStatement.setString(2, date);

            ResultSet rs = preparedStatement.executeQuery();
            while (rs.next()) {
                LongleaveView longleaveView = new LongleaveView();
                longleaveView.setLongleaveId(rs.getLong("id"));
                longleaveView.setfId(rs.getLong("f_id"));
                longleaveView.setFromDate(rs.getString("from_date"));
                longleaveView.setToDate(rs.getString("to_date"));
                longleaveView.setLeaveTypeId(rs.getLong("leave_type_id"));
                longleaveView.setName(rs.getString("name"));
                longleaveView.setTypeId(rs.getLong("type_id"));
                lMap.put(longleaveView.getfId(), longleaveView);
            }
            rs.close();
            preparedStatement.close();
            c.close();
            scheduleMasterService.setScheduleData("Long Leave Scheduler Verifier", 5L, 0L);
        } catch (Exception ex) {
            scheduleMasterService.setScheduleData("Error During Long Leave Scheduler Verifier", 4L, 0L);
            System.out.println("Error During Schedule Opeartion");
            java.util.logging.Logger.getLogger(FacultyAttendanceServiceImpl.class
             .getName()).log(Level.SEVERE, null, ex);
        }
        System.out.println("Long Leave  Scheduler End : " + new Date());
        return lMap;
    }

    /*
        Faculty Shift Mapping Schedule Code
     */
    public Map<Long, FacultyShiftTiming> facultyShiftMpg(int weekDay) {

        System.out.println("Faculty Shift Mapping Scheduler Start : " + new Date());
        Map<Long, FacultyShiftTiming> lMap = new HashMap<>();

        ScheduleMaster scheduleMaster = new ScheduleMaster();
        try {

            String date = CustDateFormat.todaySQLString();
            Class.forName("com.mysql.jdbc.Driver");
            String path = env.getProperty("spring.datasource.url");
            String userName = env.getProperty("spring.datasource.username");
            String password = env.getProperty("spring.datasource.password");
//
            Connection c = DriverManager.getConnection(path, userName, password);
            //  PreparedStatement preparedStatement = c.prepareStatement("update dheemati_attendance.faculty_attendance set status = true,is_principal_filled = true,leave_status = 1 ,presence_status=false,in_time='#',out_time='#' where f_id in (SELECT f_id FROM dheemati_attendance.trn_longleave_dtls where leave_type_id  in (4,11,15,16,17,18,19) and to_date >= '" + startDate + "') and attendance_date = '" + startDate + "';");
//                PreparedStatement preparedStatement = c.prepareStatement("SELECT f_id,leave_type_id FROM dheemati_attendance.trn_longleave_dtls where leave_type_id  not in (2,4,11,15,16,17,18,19) and to_date >= '" + startDate + "';");
//
//                ResultSet rs = preparedStatement.executeQuery();
//                while (rs.next()) {
//                    LeaveFId.put(rs.getLong("f_id"), rs.getLong("leave_type_id"));
//                }
//                //  preparedStatement = c.prepareStatement("update dheemati_attendance.faculty_attendance set status = true,is_principal_filled = true,leave_status = 1 ,presence_status=true,in_time='#',out_time='#' where f_id not in (SELECT f_id FROM dheemati_attendance.trn_longleave_dtls where leave_type_id  in (4,11,15,16,17,18,19) and to_date >= '" + startDate + "') and attendance_date = '" + startDate + "';");
//                preparedStatement = c.prepareStatement("SELECT f_id,leave_type_id FROM dheemati_attendance.trn_longleave_dtls where leave_type_id  in (2,4,11,15,16,17,18,19) and to_date >= '" + startDate + "';");
//
//                rs = preparedStatement.executeQuery();
//                while (rs.next()) {
//                    DutyFId.put(rs.getLong("f_id"), rs.getLong("leave_type_id"));
//                }

            String sqlQuery = "select fsm.f_id 'f_id',fsm.id 'fsm_id',sm.id 'shift_id',sm.in_time,sm.out_time from shift_master sm,faculty_shift_mpg fsm where fsm.shift_id = sm.id and fsm.is_active=true and fsm.is_deleted = false;";
            switch (weekDay) {
                case 1:
                    sqlQuery = "select fsm.f_id 'f_id',fsm.id 'fsm_id',sm.id 'shift_id',sm.in_time,sm.out_time from shift_master sm,faculty_shift_mpg fsm where fsm.day1 = sm.id and fsm.is_active=true and fsm.is_deleted = false;";
                    break;
                case 2:
                    sqlQuery = "select fsm.f_id 'f_id',fsm.id 'fsm_id',sm.id 'shift_id',sm.in_time,sm.out_time from shift_master sm,faculty_shift_mpg fsm where fsm.day2 = sm.id and fsm.is_active=true and fsm.is_deleted = false;";
                    break;
                case 3:
                    sqlQuery = "select fsm.f_id 'f_id',fsm.id 'fsm_id',sm.id 'shift_id',sm.in_time,sm.out_time from shift_master sm,faculty_shift_mpg fsm where fsm.day3 = sm.id and fsm.is_active=true and fsm.is_deleted = false;";
                    break;
                case 4:
                    sqlQuery = "select fsm.f_id 'f_id',fsm.id 'fsm_id',sm.id 'shift_id',sm.in_time,sm.out_time from shift_master sm,faculty_shift_mpg fsm where fsm.day4 = sm.id and fsm.is_active=true and fsm.is_deleted = false;";
                    break;
                case 5:
                    sqlQuery = "select fsm.f_id 'f_id',fsm.id 'fsm_id',sm.id 'shift_id',sm.in_time,sm.out_time from shift_master sm,faculty_shift_mpg fsm where fsm.day5 = sm.id and fsm.is_active=true and fsm.is_deleted = false;";
                    break;
                case 6:
                    sqlQuery = "select fsm.f_id 'f_id',fsm.id 'fsm_id',sm.id 'shift_id',sm.in_time,sm.out_time from shift_master sm,faculty_shift_mpg fsm where fsm.day6 = sm.id and fsm.is_active=true and fsm.is_deleted = false;";
                    break;

            }
            PreparedStatement preparedStatement = c.prepareStatement(sqlQuery);
            ResultSet rs = preparedStatement.executeQuery();
            while (rs.next()) {
                FacultyShiftTiming facultyShiftTiming = new FacultyShiftTiming();
                facultyShiftTiming.setShiftRefId(rs.getLong("fsm_id"));
                facultyShiftTiming.setfId(rs.getLong("f_id"));
                facultyShiftTiming.setShiftTbleId(1L);
                facultyShiftTiming.setActualInTime(rs.getString("in_time"));
                facultyShiftTiming.setActualOutTime(rs.getString("out_time"));
                lMap.put(facultyShiftTiming.getfId(), facultyShiftTiming);
            }
            rs.close();
            preparedStatement.close();
            c.close();
            scheduleMasterService.setScheduleData("Faculty Shift Mapping Scheduler Verifier", 2L, 0L);
        } catch (ClassNotFoundException | SQLException ex) {
            scheduleMasterService.setScheduleData("Error During Faculty Shift Mapping Scheduler Verifier", 2L, 0L);
            System.out.println("Error During Schedule Opeartion");
            java.util.logging.Logger.getLogger(FacultyAttendanceServiceImpl.class
             .getName()).log(Level.SEVERE, null, ex);
        }
        System.out.println("Faculty Shift Mapping Scheduler End : " + new Date());
        return lMap;
    }

    /*
        Long-Leave Schedule Code
     */
//    public Map<Long, LongleaveView> longleaveSchedule() {
//
//        System.out.println("Long Leave  Scheduler Start : " + new Date());
//        Map<Long, LongleaveView> lMap = new HashMap<>();
//
//        ScheduleMaster scheduleMaster = new ScheduleMaster();
//        try {
//
//            String date = CustDateFormat.todaySQLString();
//            Class.forName("com.mysql.jdbc.Driver");
//            String path = env.getProperty("spring.datasource.url");
//            String userName = env.getProperty("spring.datasource.username");
//            String password = env.getProperty("spring.datasource.password");
////
//            Connection c = DriverManager.getConnection(path, userName, password);
//            //  PreparedStatement preparedStatement = c.prepareStatement("update dheemati_attendance.faculty_attendance set status = true,is_principal_filled = true,leave_status = 1 ,presence_status=false,in_time='#',out_time='#' where f_id in (SELECT f_id FROM dheemati_attendance.trn_longleave_dtls where leave_type_id  in (4,11,15,16,17,18,19) and to_date >= '" + startDate + "') and attendance_date = '" + startDate + "';");
////                PreparedStatement preparedStatement = c.prepareStatement("SELECT f_id,leave_type_id FROM dheemati_attendance.trn_longleave_dtls where leave_type_id  not in (2,4,11,15,16,17,18,19) and to_date >= '" + startDate + "';");
////
////                ResultSet rs = preparedStatement.executeQuery();
////                while (rs.next()) {
////                    LeaveFId.put(rs.getLong("f_id"), rs.getLong("leave_type_id"));
////                }
////                //  preparedStatement = c.prepareStatement("update dheemati_attendance.faculty_attendance set status = true,is_principal_filled = true,leave_status = 1 ,presence_status=true,in_time='#',out_time='#' where f_id not in (SELECT f_id FROM dheemati_attendance.trn_longleave_dtls where leave_type_id  in (4,11,15,16,17,18,19) and to_date >= '" + startDate + "') and attendance_date = '" + startDate + "';");
////                preparedStatement = c.prepareStatement("SELECT f_id,leave_type_id FROM dheemati_attendance.trn_longleave_dtls where leave_type_id  in (2,4,11,15,16,17,18,19) and to_date >= '" + startDate + "';");
////
////                rs = preparedStatement.executeQuery();
////                while (rs.next()) {
////                    DutyFId.put(rs.getLong("f_id"), rs.getLong("leave_type_id"));
////                }
//            PreparedStatement preparedStatement = c.prepareStatement("SELECT * FROM longleave_view where from_date <= ? and to_date >= ?;");
//            preparedStatement.setString(1, date);
//            preparedStatement.setString(2, date);
//
//            ResultSet rs = preparedStatement.executeQuery();
//            while (rs.next()) {
//                LongleaveView longleaveView = new LongleaveView();
//                longleaveView.setLongleaveId(rs.getLong("id"));
//                longleaveView.setfId(rs.getLong("f_id"));
//                longleaveView.setFromDate(rs.getString("from_date"));
//                longleaveView.setToDate(rs.getString("to_date"));
//                longleaveView.setLeaveTypeId(rs.getLong("leave_type_id"));
//                longleaveView.setName(rs.getString("name"));
//                longleaveView.setTypeId(rs.getLong("type_id"));
//                lMap.put(longleaveView.getfId(), longleaveView);
//            }
//            scheduleMasterService.setScheduleData("Long Leave Scheduler Verifier", 5L, 0L);
//        } catch (Exception ex) {
//            scheduleMasterService.setScheduleData("Error During Long Leave Scheduler Verifier", 4L, 0L);
//            System.out.println("Error During Schedule Opeartion");
//            java.util.logging.Logger.getLogger(FacultyAttendanceServiceImpl.class
//                    .getName()).log(Level.SEVERE, null, ex);
//        }
//        System.out.println("Long Leave  Scheduler End : " + new Date());
//        return lMap;
//    }
    @Override
    public void memoList() {
        System.out.println("MEMO MAIL SENDING Start : " + new Date());
        ScheduleMaster scheduleMaster = new ScheduleMaster();
        try {

            String date = CustDateFormat.todaySQLString();

            //  PreparedStatement preparedStatement = c.prepareStatement("update dheemati_attendance.faculty_attendance set status = true,is_principal_filled = true,leave_status = 1 ,presence_status=false,in_time='#',out_time='#' where f_id in (SELECT f_id FROM dheemati_attendance.trn_longleave_dtls where leave_type_id  in (4,11,15,16,17,18,19) and to_date >= '" + startDate + "') and attendance_date = '" + startDate + "';");
//                PreparedStatement preparedStatement = c.prepareStatement("SELECT f_id,leave_type_id FROM dheemati_attendance.trn_longleave_dtls where leave_type_id  not in (2,4,11,15,16,17,18,19) and to_date >= '" + startDate + "';");
//
//                ResultSet rs = preparedStatement.executeQuery();
//                while (rs.next()) {
//                    LeaveFId.put(rs.getLong("f_id"), rs.getLong("leave_type_id"));
//                }
//                //  preparedStatement = c.prepareStatement("update dheemati_attendance.faculty_attendance set status = true,is_principal_filled = true,leave_status = 1 ,presence_status=true,in_time='#',out_time='#' where f_id not in (SELECT f_id FROM dheemati_attendance.trn_longleave_dtls where leave_type_id  in (4,11,15,16,17,18,19) and to_date >= '" + startDate + "') and attendance_date = '" + startDate + "';");
//                preparedStatement = c.prepareStatement("SELECT f_id,leave_type_id FROM dheemati_attendance.trn_longleave_dtls where leave_type_id  in (2,4,11,15,16,17,18,19) and to_date >= '" + startDate + "';");
//
//                rs = preparedStatement.executeQuery();
//                while (rs.next()) {
//                    DutyFId.put(rs.getLong("f_id"), rs.getLong("leave_type_id"));
//                }
            List<FacultyMEMOView> facultyMEMOViewsT = new LinkedList<>();
            Class.forName("com.mysql.jdbc.Driver");
            String path = env.getProperty("spring.datasource.url");
            String userName = env.getProperty("spring.datasource.username");
            String password = env.getProperty("spring.datasource.password");
//
            Connection c = DriverManager.getConnection(path, userName, password);
            PreparedStatement preparedStatement = c.prepareStatement("select im.id as 'Institute_Id',im.name as 'Institute_Name',f.institute_type_id as 'Institute_Type_Id', dm.name as 'Department_Name',f.name as 'Faculty_Name', f.email as 'Faculty_Email',lot_creation_date as 'Memo_Date', memo_count as 'MEMO_Number' from dheemati_attendance.trn_dtl_issue_memo tdim, dheemati_attendance.faculty f, dheemati_attendance.institute_master im, dheemati_attendance.department_master as dm where tdim.lot_creation_date= ? and f.id = tdim.f_id and im.id = f.institute_id  and dm.id = f.department_id  and f.institute_type_id = ? order by im.id,dm.id;");
            preparedStatement.setString(1, date);
            preparedStatement.setLong(2, 1L);
            ResultSet rs = preparedStatement.executeQuery();
            while (rs.next()) {
                FacultyMEMOView technical = new FacultyMEMOView();
                technical.setInstitute_Id(rs.getLong("Institute_Id"));
                technical.setInstitute_Name(rs.getString("Institute_Name").trim());
                technical.setInstitute_Type_Id(rs.getLong("Institute_Type_Id"));
                technical.setDepartment_Name(rs.getString("Department_Name").trim());
                technical.setFaculty_Name(rs.getString("Faculty_Name").trim());
                technical.setFaculty_Email(rs.getString("Faculty_Email").trim());
                technical.setMemo_Date(rs.getString("Memo_Date").trim());
                technical.setMEMO_Number(rs.getLong("MEMO_Number"));
                facultyMEMOViewsT.add(technical);
            }

            List<FacultyMEMOView> facultyMEMOViewsH = new LinkedList<>();
            preparedStatement = c.prepareStatement("select im.id as 'Institute_Id',im.name as 'Institute_Name',f.institute_type_id as 'Institute_Type_Id', dm.name as 'Department_Name',f.name as 'Faculty_Name', f.email as 'Faculty_Email',lot_creation_date as 'Memo_Date', memo_count as 'MEMO_Number' from dheemati_attendance.trn_dtl_issue_memo tdim, dheemati_attendance.faculty f, dheemati_attendance.institute_master im, dheemati_attendance.department_master as dm where tdim.lot_creation_date= ? and f.id = tdim.f_id and im.id = f.institute_id  and dm.id = f.department_id  and f.institute_type_id = ? order by im.id,dm.id;");
            preparedStatement.setString(1, date);
            preparedStatement.setLong(2, 2L);
            rs = preparedStatement.executeQuery();
            while (rs.next()) {
                FacultyMEMOView higher = new FacultyMEMOView();
                higher.setInstitute_Id(rs.getLong("Institute_Id"));
                higher.setInstitute_Name(rs.getString("Institute_Name").trim());
                higher.setInstitute_Type_Id(rs.getLong("Institute_Type_Id"));
                higher.setDepartment_Name(rs.getString("Department_Name").trim());
                higher.setFaculty_Name(rs.getString("Faculty_Name").trim());
                higher.setFaculty_Email(rs.getString("Faculty_Email").trim());
                higher.setMemo_Date(rs.getString("Memo_Date").trim());
                higher.setMEMO_Number(rs.getLong("MEMO_Number"));
                facultyMEMOViewsH.add(higher);
            }
            rs.close();
            preparedStatement.close();
            c.close();
            String Mails[] = {"shahbhavya5800@gmail.com", "parthmodi.ic@gmail.com", "mas19@googlegroups.com", "hhrathod22@gmail.com"};
            sendEmailService.sendMEMOEmail(Mails, facultyMEMOViewsT, facultyMEMOViewsH);
            scheduleMasterService.setScheduleData("MEMO MAIL SENDING!", 6L, 0L);
        } catch (Exception ex) {
            scheduleMasterService.setScheduleData("Error During MEMO MAIL SENDING!", 6L, 0L);
            System.out.println("Error During Opeartion");
            java.util.logging.Logger.getLogger(FacultyAttendanceServiceImpl.class
             .getName()).log(Level.SEVERE, null, ex);
        }
        System.out.println("MEMO MAIL SENDING  Scheduler End : " + new Date());
    }

    @Override
    public void dailyMemoSchedule() {

        System.out.println("MEMO Generating  Scheduler Start : " + new Date());
        Long lotNumber = 1L;

        Date now = new Date();
        String date = CustDateFormat.todaySQLString();

        Optional<TrnDtlIssueMemo> fOptional = trnDtlIssueMemoRepository.findTopByIsDeletedFalseOrderByIdDesc();
        if (fOptional.isPresent()) {
            lotNumber = fOptional.get().getMemoLotNo();
            lotNumber = lotNumber + 1;
        }

        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery criteriaQuery = criteriaBuilder.createQuery(FacultyAttendance.class
        );
        prepareListQuery(criteriaQuery, criteriaBuilder, date);
        TypedQuery typedQuery = entityManager.createQuery(criteriaQuery);
        List<FacultyAttendance> facultyAttendances = (List<FacultyAttendance>) typedQuery.getResultList();
        try {
            now = CustDateFormat.SQLTypeStringToDate(date);
        } catch (Exception e) {
            now = new Date();
        }
        List<TrnDtlIssueMemo> trnDtlIssueMemos = traDtlIssueMemoRepository.findByLotCreationDate(now);
        int i = 0;
        // Prepare Pagination
        //   List<FacultyAttendance> facultyAtt = facultyAttendanceRepository.findAllByAttendanceDateAndStatus("2019-08-28", Boolean.FALSE);
        if (trnDtlIssueMemos.isEmpty() && facultyAttendances.size() >= 0) {
            for (FacultyAttendance fac : facultyAttendances) {
                if (fac.getIsDeleted()) {
                    continue;
                }
                fac.setStatus(Boolean.TRUE);  // Check Status after MEMO generate
                TrnDtlIssueMemo trnDtlIssueMemo = new TrnDtlIssueMemo();
                trnDtlIssueMemo.setfId(fac.getfId());
                trnDtlIssueMemo.setInstituteId(fac.getInstituteId());
                trnDtlIssueMemo.setIssueYear(2019L);
                trnDtlIssueMemo.setLotCreationDate(now);
                trnDtlIssueMemo.setIsDeleted(Boolean.FALSE);
                trnDtlIssueMemo.setTrnId(fac.getId());
                trnDtlIssueMemo.setTypeId(3L);
                // f_id,institute_id,issue_year,lot_creation_date,memo_count,memo_lot_no,trn_id,type_id
                if (lotNumber == 1) {
                    trnDtlIssueMemo.setMemoCount(1L);
                    trnDtlIssueMemo.setMemoLotNo(1L);
                } else {
                    trnDtlIssueMemo.setMemoLotNo(lotNumber);
                    if (trnDtlIssueMemoRepository.existsByFIdAndIsDeletedFalse(fac.getfId())) {
                        Optional<TrnDtlIssueMemo> tOptional = trnDtlIssueMemoRepository.findTopByFIdAndIsDeletedFalseOrderByIdDesc(fac.getfId());
                        if (tOptional.isPresent()) {
                            Long memoNumber = tOptional.get().getMemoCount() + 1;
                            trnDtlIssueMemo.setMemoCount(memoNumber);
                        }
                    } else {
                        trnDtlIssueMemo.setMemoCount(1L);
                    }
                    // System.out.println("Fac Id : " + fac.getfId());

                }
                trnDtlIssueMemoRepository.save(trnDtlIssueMemo);
                i++;
            }
            scheduleMasterService.setScheduleData("Daily Memo Generated Scheduler : ", 3L, 0L);
            //  memoList();
        } else if (facultyAttendances.isEmpty()) {
            scheduleMasterService.setScheduleData("Empty MEMO!", 3L, 0L);
            System.out.println("Data Empty!");
        } else if (trnDtlIssueMemos.isEmpty()) {
            scheduleMasterService.setScheduleData("Already Generated MEMO!", 3L, 0L);
            System.out.println("Data Empty!");
        }

        System.out.println("MEMO Generating  Scheduler END : " + new Date());
        System.out.println("MEMO Generated Successfully! Count = " + i);

    }

    private void prepareListQuery(CriteriaQuery criteriaQuery, CriteriaBuilder criteriaBuilder, String date) {
        Root<Faculty> fRoot = criteriaQuery.from(Faculty.class
        );
        Root<FacultyAttendance> facultyAttenRoot = criteriaQuery.from(FacultyAttendance.class
        );
        criteriaQuery.multiselect(facultyAttenRoot.get("id"), fRoot.get("id"), fRoot.get("instituteId"),
         facultyAttenRoot.get("status"), fRoot.get("instituteTypeId"), fRoot.get("role"));
        ArrayList<Predicate> predicateList = new ArrayList<>();
        predicateList.add(criteriaBuilder.equal(facultyAttenRoot.get("fId"), fRoot.get("id")));
        predicateList.add(criteriaBuilder.equal(facultyAttenRoot.get("status"), Boolean.FALSE));
        predicateList.add(criteriaBuilder.equal(facultyAttenRoot.get("attendanceDate"), date));
        In<Object> inClause = criteriaBuilder.in(fRoot.get("role"));
        inClause.value(1L);
        inClause.value(2L);
        inClause.value(8L);
        In<Object> inClauseType = criteriaBuilder.in(fRoot.get("instituteTypeId"));
        inClauseType.value(1L);
        inClauseType.value(2L);
        predicateList.add(inClause);
        predicateList.add(inClauseType);
        Predicate[] predicates = new Predicate[predicateList.size()];
        predicateList.toArray(predicates);
        criteriaQuery.where(predicates);
    }

    @Override
    public void dailyHolidaySchedule() {

        String pattern = "yyyy-MM-dd";
        Date now = new Date();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
        String date = simpleDateFormat.format(now);
        Long LeaveStatus = 0L;
        List<FacultyAttendance> facultyAtt = null;
        boolean flag = false;
        simpleDateFormat = new SimpleDateFormat("u");
        int weekDay = Integer.parseInt(simpleDateFormat.format(now));// the day of the week spelled out completely
        simpleDateFormat = new SimpleDateFormat("W");
        int weekNumber = Integer.parseInt(simpleDateFormat.format(now));
        simpleDateFormat = new SimpleDateFormat(pattern);
        ScheduleMaster scheduleMaster = new ScheduleMaster();
        if (weekDay == 7) {
            LeaveStatus = 2L;
            flag = true;
            facultyAtt = facultyAttendanceRepository.findAllByAttendanceDate(date);
            scheduleMaster.setScheduleName("Weeked for ALL Scheduler");

        } else if (weekDay == 6 && (weekNumber % 2) == 0) {
            System.out.println("Weekend for Technical!");
            LeaveStatus = 2L;
            flag = true;
            facultyAtt = facultyAttendanceRepository.findAllByAttendanceDateAndInstituteTypeId(date, 1L);
            scheduleMaster.setScheduleName("Weeked for Technical Scheduler");
        } else {
            try {
                if (holidayMasterRepository.existsByHolidayDateAndIsDeletedFalse(simpleDateFormat.parse(date))) {
                    LeaveStatus = 3L;
                    flag = true;
                    facultyAtt = facultyAttendanceRepository.findAllByAttendanceDate(date);

                }
            } catch (Exception ex) {
                java.util.logging.Logger.getLogger(FacultyAttendanceServiceImpl.class
                 .getName()).log(Level.SEVERE, null, ex);
            }
        }
        if (flag) {
            System.out.println("Data Process ...");

            if (facultyAtt.size() > 0) {
                scheduleMaster.setCreatedAt(new Date());
                for (FacultyAttendance facultyAttendance : facultyAtt) {
                    // System.out.println("Data Going...");
                    facultyAttendance.setLeaveStatus(LeaveStatus);
                    facultyAttendance.setPresenceStatus(Boolean.FALSE);
                    if (facultyAttendance.getStatus() == false) {
                        facultyAttendance.setInTime("#");
                        facultyAttendance.setOutTime("#");
                        facultyAttendance.setStatus(Boolean.TRUE);
                    }
                    facultyAttendanceRepository.save(facultyAttendance);

                }
                scheduleMaster.setUpdatedAt(new Date());

                scheduleMaster.setScheduleType(1L);
                scheduleMaster.setScheduleDate(date);
                scheduleMasterRepository.save(scheduleMaster);
            }

        } else {
            System.out.println("Already Data Added on System!");
        }

        System.out.println(
         "Data Added Successfully!");
    }

    @Override
    public void dailyFSSchedule() {

    }
}
