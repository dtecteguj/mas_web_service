package com.springapi.service.impl;

import com.springapi.enums.FASMessageType;
import com.springapi.exception.FASException;
import com.springapi.model.DepartmentMaster;
import com.springapi.model.Faculty;
import com.springapi.model.InstituteDepartmentMpg;
import com.springapi.model.ProgramMst;
import com.springapi.repository.DepartmentRepository;
import com.springapi.service.DepartmentService;
import com.springapi.service.ProgramMstService;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author Jyoti Patel
 */
@Service
@Transactional
public class DepartmentServiceImpl implements DepartmentService {

    static Logger logger = Logger.getLogger(DepartmentServiceImpl.class);

    @Autowired
    private DepartmentRepository departmentRepository;

    @Autowired
    private EntityManager entityManager;
    
   

    @Override
    public DepartmentMaster create(DepartmentMaster department) {
        logger.debug("create");
        return departmentRepository.save(department);
    }

    @Override
    public List<DepartmentMaster> list() {
        logger.debug("list");
        return departmentRepository.findAll();
    }

    @Override
    public DepartmentMaster read(Long uId) {
        logger.debug("read");
        Optional<DepartmentMaster> optional = departmentRepository.findById(uId);
        if (!optional.isPresent()) {
            throw new FASException(FASMessageType.ENTITY_NOT_FOUND);
        }
        return optional.get();
    }

    @Override
    public DepartmentMaster update(DepartmentMaster departmentDetails) {
        logger.debug("update");
        DepartmentMaster department = read(departmentDetails.getId());

        department.setCode(departmentDetails.getCode());
        department.setName(departmentDetails.getName());
        department.setDescription(departmentDetails.getDescription());
        department.setUpdatedAt(new Date());

        return departmentRepository.save(department);
    }

    @Override
    public void delete(Long uId) {
        logger.debug("delete");
        DepartmentMaster department = read(uId);
        departmentRepository.delete(department);
    }

    @Override
    public List<DepartmentMaster> readDepartmentByInstituteId(Long instituteId) {
        logger.debug("readDepartmentByInstituteId");
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery criteriaQuery = criteriaBuilder.createQuery(DepartmentMaster.class);
        prepareListQuery(criteriaQuery, criteriaBuilder, instituteId);
        TypedQuery typedQuery = entityManager.createQuery(criteriaQuery);
        List<DepartmentMaster> departmentList = typedQuery.getResultList();
        List<DepartmentMaster> departmentList1 = new ArrayList<>();
        for (DepartmentMaster departmentMaster : departmentList) {
            departmentMaster.setId(departmentMaster.getCode());
            departmentList1.add(departmentMaster);
        }

        if (departmentList1.size() > 0) {
          

            Collections.sort(departmentList1, new Comparator<DepartmentMaster>() {
                @Override
                public int compare(final DepartmentMaster object1, final DepartmentMaster object2) {
                    return object1.getName().compareTo(object2.getName());
                }
            });
        }
        //  List<DepartmentMaster> departmentList = departmentRepository.findAllByInstituteId(instituteId);
        return departmentList1;
    }

    private void prepareListQuery(CriteriaQuery criteriaQuery, CriteriaBuilder criteriaBuilder, Long instituteId) {
        Root<Faculty> facultyAttenRoot = criteriaQuery.from(InstituteDepartmentMpg.class);
        Root<DepartmentMaster> dRoot = criteriaQuery.from(DepartmentMaster.class);
        criteriaQuery.multiselect(dRoot.get("name"), dRoot.get("description"), dRoot.get("code"), facultyAttenRoot.get("instituteId"));
        ArrayList<Predicate> predicateList = new ArrayList<>();
        predicateList.add(criteriaBuilder.equal(facultyAttenRoot.get("departmentId"), dRoot.get("id")));
        if (instituteId != null) {
            predicateList.add(criteriaBuilder.equal(facultyAttenRoot.get("instituteId"), instituteId));
        }
         predicateList.add(criteriaBuilder.equal(facultyAttenRoot.get("isDeleted"), Boolean.FALSE));
        Predicate[] predicates = new Predicate[predicateList.size()];
        predicateList.toArray(predicates);
        criteriaQuery.where(predicates);

    }
    
     @Override
    public String namebyId(Long dId) {
        logger.debug("read");
        Optional<DepartmentMaster> optional = departmentRepository.findById(dId);
        if (!optional.isPresent()) {
            return null;
        }
        return optional.get().getName();
    }

}
