/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.springapi.service.impl;

import com.springapi.enums.FASMessageType;
import com.springapi.exception.FASException;
import com.springapi.model.ProgramMst;
import com.springapi.repository.ProgramMstRepository;
import com.springapi.service.ProgramMstService;
import java.util.List;
import java.util.Optional;
import javax.persistence.EntityManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author shahb
 */

@Service
@Transactional
public class ProgramMstServiceImpl implements ProgramMstService{
    static Logger logger = Logger.getLogger(InstituteTypeServiceImpl.class);

    @Autowired
    private ProgramMstRepository programMstRepository;

    @Autowired
    private EntityManager entityManager;

    @Override
    public ProgramMst create(ProgramMst programMst) {
        logger.debug("create");
        return programMstRepository.save(programMst);
    }

    @Override
    public List<ProgramMst> list() {
         logger.debug("list");
        return programMstRepository.findAll();
    }

    @Override
    public ProgramMst read(Long pId) {
        logger.debug("read");
        Optional<ProgramMst> optional = programMstRepository.findById(pId);
        if (!optional.isPresent()) {
            throw new FASException(FASMessageType.ENTITY_NOT_FOUND);
        }
        return optional.get();
    }

    @Override
    public ProgramMst update(ProgramMst programMst) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void delete(Long pId) {
         logger.debug("delete");
        Optional<ProgramMst> optional = programMstRepository.findById(pId);
        if (!optional.isPresent()) {
            throw new FASException(FASMessageType.ENTITY_NOT_FOUND);
        }
        programMstRepository.delete(optional.get());
    }

  
   
}
