/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.springapi.service.impl;

import com.springapi.enums.FASMessageType;
import com.springapi.exception.FASException;
import com.springapi.model.TestUsers;
import com.springapi.model.UniversityMaster;
import com.springapi.repository.TestUserRepository;
import com.springapi.service.TestUserService;
import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author shahb
 */
@Service
@Transactional
public class TestUserServiceImpl implements TestUserService {

    @Autowired
    private TestUserRepository testUserRepository;
    @Override
    public TestUsers create(TestUsers testUsers) {
      return testUserRepository.save(testUsers);
    }

    @Override
    public List<TestUsers> list() {
        return testUserRepository.findAll();
    }

    @Override
    public TestUsers read(Long uId) {
        Optional<TestUsers> optional = testUserRepository.findById(uId);
        if (!optional.isPresent()) {
            throw new FASException(FASMessageType.ENTITY_NOT_FOUND);
        }
        return optional.get();
    }

    @Override
    public TestUsers update(TestUsers testUsers) {
        return null;
    }

    @Override
    public String delete(Long uId) {
      testUserRepository.delete(read(uId));
      return null;
    }
    
}
