/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.springapi.service.impl;

import com.springapi.model.InstituteLocFancingInfo;
import com.springapi.model.InstituteMaster;
import com.springapi.repository.InstituteLocFancingInfoRepository;
import com.springapi.repository.InstituteRepository;
import com.springapi.service.InstituteLocFancingInfoService;
import static com.springapi.service.impl.InstituteTypeServiceImpl.logger;
import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author shahb
 */
@Service
@Transactional
public class InstituteLocFancingInfoServiceImpl implements InstituteLocFancingInfoService{
 
    
    
    @Autowired
    private InstituteLocFancingInfoRepository institutefancingInfoRepository;
    
    @Autowired
    private InstituteRepository instituteRepository;
    
    
    @Override
    public InstituteLocFancingInfo create(InstituteLocFancingInfo instituteLocFancingInfo) {
       logger.debug("create");
        institutefancingInfoRepository.save(instituteLocFancingInfo);
        
         Optional<InstituteMaster> instituteMaster = instituteRepository.findById(instituteLocFancingInfo.getInstituteId());
         if(instituteMaster.isPresent())
         {
             instituteMaster.get().setIsLocActive(Boolean.TRUE);
             instituteRepository.save(instituteMaster.get());
         }
        
         
        
        return instituteLocFancingInfo;
        
    }
 
    @Override
    public List<InstituteLocFancingInfo> list() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public InstituteLocFancingInfo read(Long aId) {
         return institutefancingInfoRepository.findByInstituteIdAndIsActiveTrue(aId); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public InstituteLocFancingInfo update(InstituteLocFancingInfo instituteLocFancingInfo) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void delete(Long aId) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

//    @Override
//    public InstituteLocFancingInfo insertFancingInfo(InstituteLocFancingInfo instituteLocFancingInfo) {
//      
//        InstituteLocFancingInfo instituteLocFancingInfoResponse = new InstituteLocFancingInfo();
////        instituteLocFancingInfoResponse.setAlogId(date);
////        instituteLocFancingInfoResponse.setCreatedAt(new Date());
////        instituteLocFancingInfoResponse.setDayId(facultyActivity.getDayId());
////        instituteLocFancingInfoResponse.setFacultyId(facultyActivity.getFacultyId());
////        instituteLocFancingInfoResponse.setMessage(facultyActivity.getMessage());
////        instituteLocFancingInfoResponse.setSemId(facultyActivity.getSemId());
////        instituteLocFancingInfoResponse.setSlotId(facultyActivity.getSlotId());
////        instituteLocFancingInfoResponse.setSubjectId(facultyActivity.getSubjectId());
//
//        return institutefancingInfoRepository.save(instituteLocFancingInfoResponse);
//    }

    @Override
    public List<InstituteLocFancingInfo> findByInstituteId(Long uId) {
        return null;
    }
    
}
