/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.springapi.service.impl;

import com.springapi.enums.FASMessageType;
import com.springapi.exception.FASException;
import com.springapi.model.DesignationMaster;
import com.springapi.repository.DepartmentRepository;
import com.springapi.repository.DesignationRepository;
import com.springapi.service.DesignationMasterService;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author shahb
 */

@Service
@Transactional
public class DesignationMasterServiceImpl implements DesignationMasterService {
    static Logger logger = Logger.getLogger(DepartmentServiceImpl.class);

    @Autowired
    private DesignationRepository designationRepository;

    @Override
    public DesignationMaster create(DesignationMaster designationMaster) {
        logger.debug("create");
        return designationRepository.save(designationMaster);
    }

    @Override
    public List<DesignationMaster> list() {
        logger.debug("list");
        return designationRepository.findAll();
    }

    @Override
    public DesignationMaster read(Long uId) {
        logger.debug("read");
        Optional<DesignationMaster> optional = designationRepository.findById(uId);
        if (!optional.isPresent()) {
            throw new FASException(FASMessageType.ENTITY_NOT_FOUND);
        }
        return optional.get();
    }

    @Override
    public DesignationMaster update(DesignationMaster designationMaster) {
        logger.debug("update");
        DesignationMaster department = read(designationMaster.getId());

        department.setName(designationMaster.getName());
        department.setDescription(designationMaster.getDescription());
        department.setDesgClass(designationMaster.getDesgClass());
        department.setIsDeleted(designationMaster.getIsDeleted());

        return designationRepository.save(department);
    }

    @Override
    public void delete(Long uId) {
        logger.debug("delete");
        DesignationMaster designationMaster = read(uId);
        designationRepository.delete(designationMaster);
    }
    
   
}
