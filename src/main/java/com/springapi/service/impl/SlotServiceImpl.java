package com.springapi.service.impl;

import com.springapi.dto.request.SlotRequest;
import com.springapi.model.SlotMaster;
import com.springapi.repository.SlotRepository;
import com.springapi.service.SlotService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Jyoti_patel
 */
@Service
@Transactional
public class SlotServiceImpl implements SlotService{
    
    @Autowired
    private SlotRepository slotRepository;
    
    @Override
    public List<SlotMaster> getSlotByInstitute(SlotRequest slotRequest){
        return slotRepository.findAllByInstituteId(slotRequest.getInstituteId());
    }
    
}
