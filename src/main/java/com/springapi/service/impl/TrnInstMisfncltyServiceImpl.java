/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.springapi.service.impl;

import com.springapi.model.TrnInstMisfnclty;
import com.springapi.repository.TrnInstMisfncltyRepository;
import com.springapi.service.TrnInstMisfncltyService;
import com.springapi.util.CustDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.persistence.EntityManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author shahb
 */
@Service
@Transactional
public class TrnInstMisfncltyServiceImpl implements TrnInstMisfncltyService {

    static Logger logger = Logger.getLogger(TrnInstMisfncltyServiceImpl.class);

    @Autowired
    private TrnInstMisfncltyRepository trnInstMisfncltyRepository;

    @Autowired
    private EntityManager entityManager;

    @Override
    public TrnInstMisfnclty create(TrnInstMisfnclty androidVersion) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<TrnInstMisfnclty> list() {
        logger.debug("list");
        return trnInstMisfncltyRepository.findAll();
    }

    @Override
    public TrnInstMisfnclty read(Long aId) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public TrnInstMisfnclty update(TrnInstMisfnclty trnInstMisfnclty) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void delete(Long aId) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Map<Long, TrnInstMisfnclty> listByDate(Date date) {
        Map<Long, TrnInstMisfnclty> map = new HashMap<>();
        logger.debug("list");
        Date now = null;
        try {
            now = CustDateFormat.SQLTypeStringToDate(CustDateFormat.todaySQLString());
        } catch (Exception e) {
            now = new Date();
        }
        for (TrnInstMisfnclty trnInstMisfnclty : trnInstMisfncltyRepository.findByFromDateLessThanEqualAndToDateGreaterThanEqualAndIsDeletedFalse(now, now)) {
            map.put(trnInstMisfnclty.getInstId(), trnInstMisfnclty);
        }
        return map;
    }

    @Override
    public Map<Long, TrnInstMisfnclty> listByDate(Date date, Long sfType) {
        Map<Long, TrnInstMisfnclty> map = new HashMap<>();
        logger.debug("list");
        Date now = null;
        try {
            now = CustDateFormat.SQLTypeStringToDate(CustDateFormat.todaySQLString());
        } catch (Exception e) {
            now = new Date();
        }
        for (TrnInstMisfnclty trnInstMisfnclty : trnInstMisfncltyRepository.findByFromDateLessThanEqualAndToDateGreaterThanEqualAndApplicableToIdAndIsDeletedFalse(now, now,sfType)) {
            map.put(trnInstMisfnclty.getInstId(), trnInstMisfnclty);
        }
        return map;
    }

}
