/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.springapi.service.impl;

import com.springapi.model.Subject;
import com.springapi.repository.SubjectRepository;
import com.springapi.service.SubjectService;
import java.util.List;
import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author shahb
 */
@Service
@Transactional
public class SubjectServiceImpl implements SubjectService {

    @Autowired
    private SubjectRepository subjectRepository;

    @Override
    public Subject create(Subject subject) {
        return subjectRepository.save(subject);
    }

    @Override
    public List<Subject> list() {
        return subjectRepository.findAll();
    }

    @Override
    public Subject read(Long sId) {
        Optional<Subject> optional = subjectRepository.findById(sId);
        if (!optional.isPresent()) {
            // throw Exception;
        }
        return optional.get();
    }

    @Override
    public Subject update(Subject subjectDetails) {
        Subject subject = read(subjectDetails.getId());

        subject.setsCode(subjectDetails.getsCode());
        subject.setsPictureLink(subjectDetails.getsPictureLink());
        subject.setsSem(subjectDetails.getsSem());
        subject.setsName(subjectDetails.getsName());

        return subjectRepository.save(subject);
    }

    @Override
    public void delete(Long sId) {
        Subject subject = read(sId);
        subjectRepository.delete(subject);
    }
    
    @Override
    public Subject readByCode(Long subjectCode){
        Subject subject = subjectRepository.findBySCode(subjectCode);
        if(subject == null){
            try {
                throw new Exception("Invalid Subject Code !!");
            } catch (Exception ex) {
                Logger.getLogger(SubjectServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        
        return subject;
    }
}
