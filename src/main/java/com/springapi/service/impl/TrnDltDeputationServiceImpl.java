/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.springapi.service.impl;

import com.springapi.enums.FASMessageType;
import com.springapi.exception.FASException;
import com.springapi.model.TrnDltDeputation;
import com.springapi.repository.TrnDltDeputationRepository;
import com.springapi.service.TrnDltDeputationService;
import java.util.List;
import java.util.Optional;
import javax.persistence.EntityManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author shahb
 */
@Service
@Transactional
public class TrnDltDeputationServiceImpl implements TrnDltDeputationService {

    static Logger logger = Logger.getLogger(TrnDltDeputationServiceImpl.class);

    @Autowired
    private TrnDltDeputationRepository trnDltDeputationRepository;

    @Autowired
    private EntityManager entityManager;

    @Override
    public TrnDltDeputation create(TrnDltDeputation trnDltDeputation) {
        logger.debug("create");
        return trnDltDeputationRepository.save(trnDltDeputation);
    }

    @Override
    public List<TrnDltDeputation> list() {
        logger.debug("list");
        return trnDltDeputationRepository.findAll();
    }

    @Override
    public TrnDltDeputation read(Long iId) {
        logger.debug("read");
        Optional<TrnDltDeputation> optional = trnDltDeputationRepository.findById(iId);
        if (!optional.isPresent()) {
            throw new FASException(FASMessageType.ENTITY_NOT_FOUND);
        }
        return optional.get();
    }

    @Override
    public TrnDltDeputation update(TrnDltDeputation trnDltDeputation) {
        logger.debug("update");
        TrnDltDeputation trnDltDeputation1 = read(trnDltDeputation.getId());

        trnDltDeputation1.setCancelledBy(trnDltDeputation.getCancelledBy());
        trnDltDeputation1.setDay1(trnDltDeputation.getDay1());
        trnDltDeputation1.setDay2(trnDltDeputation.getDay2());
        trnDltDeputation1.setDay3(trnDltDeputation.getDay3());
        trnDltDeputation1.setDay4(trnDltDeputation.getDay4());
        trnDltDeputation1.setDay5(trnDltDeputation.getDay5());
        trnDltDeputation1.setDay6(trnDltDeputation.getDay6());
        trnDltDeputation1.setDay7(trnDltDeputation.getDay7());
        trnDltDeputation1.setDepEndDate(trnDltDeputation.getDepEndDate());
        trnDltDeputation1.setDepStartDate(trnDltDeputation.getDepStartDate());
        trnDltDeputation1.setDeputationPlace(trnDltDeputation.getDeputationPlace());
        trnDltDeputation1.setDeputedInstId(trnDltDeputation.getDeputedInstId());
        trnDltDeputation1.setIsCancelled(trnDltDeputation.getIsCancelled());
        trnDltDeputation1.setTypeDeputation(trnDltDeputation.getTypeDeputation());
        trnDltDeputation1.setParentInstId(trnDltDeputation.getParentInstId());
        return trnDltDeputationRepository.save(trnDltDeputation1);
    }

    @Override
    public void delete(Long iId) {
        logger.debug("delete");
        TrnDltDeputation trnDltDeputation = read(iId);
        trnDltDeputationRepository.delete(trnDltDeputation);
    }

    @Override
    public List<TrnDltDeputation> readbyfid(Long fId) {
        return trnDltDeputationRepository.findByFId(fId);
    }

}
