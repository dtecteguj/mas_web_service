/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.springapi.service.impl;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.TimeUnit;

/**
 *
 * @author USER
 */
public class test {

    public static void main(String[] args) throws ParseException {
        String oldDate = "Thu Sep 20 16:34:07 IST 2018";
        Date dateOne = new SimpleDateFormat("dd/MM/yyyy").parse(oldDate);
        System.out.println("date one ::" + dateOne);
        Date dateTwo = new Date();
        System.out.println("dateTwo ::" + dateTwo);
        String diff = "";
        long timeDiff = Math.abs(dateOne.getTime() - dateTwo.getTime());
        diff = String.format("%d hour(s) %d min(s)", TimeUnit.MILLISECONDS.toHours(timeDiff), TimeUnit.MILLISECONDS.toMinutes(timeDiff) - TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(timeDiff)));
        System.out.println("difference is ::" + diff);
    }
}
