/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.springapi.service.impl;

import com.springapi.model.ScheduleMaster;
import com.springapi.repository.ScheduleMasterRepository;
import com.springapi.service.ScheduleMasterService;
import com.springapi.util.CustDateFormat;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author shahb
 */
@Service
@Transactional
public class ScheduleMasterServiceImpl implements ScheduleMasterService {

    @Autowired
    private ScheduleMasterRepository scheduleMasterRepository;

    @Override
    public void setScheduleData(String msg, Long sType, Long createdBy) {
        ScheduleMaster scheduleMaster = new ScheduleMaster();
        scheduleMaster.setCreatedBy(createdBy);
        scheduleMaster.setScheduleName(msg);
        scheduleMaster.setScheduleType(sType);
        scheduleMaster.setScheduleDate(CustDateFormat.todaySQLString());
        scheduleMasterRepository.save(scheduleMaster);
    }

}
