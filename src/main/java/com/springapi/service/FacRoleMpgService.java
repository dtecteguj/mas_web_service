package com.springapi.service;

import com.springapi.dto.StaftDepartment;
import com.springapi.model.FacRoleMpg;
import java.util.List;


public interface FacRoleMpgService {
	
	   public FacRoleMpg create(FacRoleMpg fm);

	    public List<FacRoleMpg> list();
            
            public List<StaftDepartment> activeRolelistByFId(Long fId);

	    public FacRoleMpg read(Long id);

	    public FacRoleMpg update(FacRoleMpg fm);

	    public void delete(Long id);

}
