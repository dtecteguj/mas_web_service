/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.springapi.service;

import com.springapi.model.TrnDtlDlytskVerify;
import java.util.List;

/**
 *
 * @author shahb
 */
public interface TrnDtlDlytskVerifyService {

    public TrnDtlDlytskVerify create(TrnDtlDlytskVerify trnDtlDlytskVerify);

    public List<TrnDtlDlytskVerify> list();

    public TrnDtlDlytskVerify read(Long tId);

    public TrnDtlDlytskVerify update(TrnDtlDlytskVerify trnDtlDlytskVerify);

    public void delete(Long tId);
}
