/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.springapi.service;

import com.springapi.dto.request.ChangePasswordRequest;
import com.springapi.dto.request.FacultyLStatusRequest;
import com.springapi.dto.request.FacultyLoginRequest;
import com.springapi.dto.request.FacultyQRVerificationRequest;
import com.springapi.dto.request.FacultyRegistrationRequest;
import com.springapi.dto.request.ForgetPasswordRequest;
import com.springapi.dto.request.MASLoginRequest;
import com.springapi.dto.request.ResetPasswordRequest;
import com.springapi.dto.response.ChangePasswordResponse;
import com.springapi.dto.response.FacultyData;
import com.springapi.dto.response.GeneralListResponse;
import com.springapi.dto.response.InstituteWiseFacultyWithDepartment;
import com.springapi.dto.response.LoginResponse;
import com.springapi.dto.response.MessageResponse;
import com.springapi.model.Faculty;
import com.springapi.model.QRCodeData;
import java.util.List;

/**
 *
 * @author shahb
 */
public interface FacultyService {

   Faculty create(Faculty faculty);

    List<Faculty> list();

    Faculty read(Long fId);

    Faculty update(Faculty facultyDetails);

    Faculty delete(Long fId);

    Faculty register(FacultyRegistrationRequest facultyRegistrationRequest);

    LoginResponse login(FacultyLoginRequest facultyLoginRequest);

    QRCodeData generateQRCode();

    List<Faculty> getFacultyByInstitute(Long instituteId);
    
    List<Faculty> getFacultyPresentByInstitute(Long instituteId);
    List<Faculty> getFacultyPresentByHoD(Long instituteId,Long fId);
    
    List<Faculty> getFacultyStatusByInstitute(Long instituteId);
    List<Faculty> getFacultyStatusByHoD(Long instituteId,Long fId);
     

    LoginResponse login(String email, String password, String bssid, String ssid);

    LoginResponse loginUser(FacultyLoginRequest facultyLoginRequest);

    ChangePasswordResponse changePassword(ChangePasswordRequest changePasswordRequest);

    List<Faculty> getFacultyByDepartmentId(Long departmentId);

    List<Faculty> getFacultyListByInstitute(Long instituteId);
    
     List<Faculty> getFacultyByHoD(Long instituteId,Long fId);
    
    
    List<Faculty> getFacultyListByInstituteDept(Long instituteId,Long departmentId);

    String forgetPassword(ForgetPasswordRequest forgetPasswordRequest);
    
    FacultyData readAllData(Long fId);
    
    ChangePasswordResponse resetPassword(ResetPasswordRequest resetPasswordRequest);
    
    Faculty editdata(Faculty facultyDetails);
    
    LoginResponse userStatus(Long fidt);
    
     LoginResponse userStatus(Long fidt,Long vCode);
     
      LoginResponse loginByDevice(MASLoginRequest masLoginRequest);
     
      GeneralListResponse facultyStatusToday(FacultyLStatusRequest facultyLStatusRequest);
    List<InstituteWiseFacultyWithDepartment> getFacultyDeptListByInstitute(Long instituteId);
}
