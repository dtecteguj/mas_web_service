package com.springapi.service;

import com.springapi.model.view.FacultyMEMOView;
import java.util.List;

/**
 * @author Jyoti_patel
 */
public interface SendEmailService {

    void sendEmail(String email) throws Exception;
    
    void sendMEMOEmail(String email[], List<FacultyMEMOView> facultyMEMOViewsT, List<FacultyMEMOView> facultyMEMOViewsH) throws Exception;
    
    void sendSchedulEmail(String email[], int trainerCount,int longleaveCount, int totalRecord) throws Exception;
    
}
