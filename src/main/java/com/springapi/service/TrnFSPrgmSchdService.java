/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.springapi.service;

import com.springapi.model.TrnFSPrgmSchd;
import java.util.List;

/**
 *
 * @author shahb
 */
public interface TrnFSPrgmSchdService {
    
    TrnFSPrgmSchd create(TrnFSPrgmSchd trnFSPrgmSchd);
    
    List<TrnFSPrgmSchd> list();
    
    TrnFSPrgmSchd read(Long iId);
    
    TrnFSPrgmSchd update(TrnFSPrgmSchd trnFSPrgmSchd);
    
    void delete(Long tId);
    
}
