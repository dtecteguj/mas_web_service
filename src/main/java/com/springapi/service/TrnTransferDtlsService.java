/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.springapi.service;

import com.springapi.model.TrnTransferDtls;
import java.util.List;
import java.util.Optional;

/**
 *
 * @author shahb
 */
public interface TrnTransferDtlsService {
    TrnTransferDtls create(TrnTransferDtls trnTransferDtls);

    List<TrnTransferDtls> list();

    TrnTransferDtls read(Long aId);

    TrnTransferDtls update(TrnTransferDtls trnTransferDtls);

    void delete(Long aId);
    
    List<TrnTransferDtls> facultyTransHist(Long fId);
    TrnTransferDtls facultyTransActiveHist(Long fId);
    List<TrnTransferDtls> instFacTransHist(Long iId);
    List<TrnTransferDtls> instFacTransActiveHist(Long iId);
    
  
}
