package com.springapi.service;

import com.springapi.dto.request.DailyEndrosRequest;
import com.springapi.dto.request.DailyEndrosStatusRequest;
import com.springapi.model.DailyEndrosment;
import java.util.List;

/**
 * @author Jyoti Patel
 */
public interface DailyEndrosmentService {

    DailyEndrosment endrosByHod(DailyEndrosRequest dailyEndrosRequest);

    DailyEndrosment endrosByprincipal(DailyEndrosRequest dailyEndrosRequest);

    Boolean endrosStatusForHod(DailyEndrosStatusRequest dailyEndrosStatusRequest);

    Boolean endrosStatusForPrincipal(DailyEndrosStatusRequest dailyEndrosStatusRequest);

}
