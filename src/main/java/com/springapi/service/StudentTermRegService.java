/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.springapi.service;

import com.springapi.model.StudentTermReg;
import java.util.List;

/**
 *
 * @author shahb
 */
public interface StudentTermRegService {
    StudentTermReg create(StudentTermReg studentTermReg);

    List<StudentTermReg> list();

    StudentTermReg read(Long sId);

    StudentTermReg update(StudentTermReg studentTermReg);

    void delete(Long sId);
}
