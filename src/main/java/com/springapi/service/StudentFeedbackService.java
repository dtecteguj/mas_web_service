package com.springapi.service;

import com.springapi.dto.request.StudentTimeTableRequest;
import com.springapi.dto.response.StudentFeedbackAttendanceResponse;
import com.springapi.dto.response.StudentTimeTableResponse;
import com.springapi.model.StudentFeedback;
import java.util.List;
import org.springframework.data.domain.Page;

/**
 * @author Jyoti Patel
 */
public interface StudentFeedbackService {
    StudentFeedback create(StudentFeedback studentFeedbackRequest);

    StudentTimeTableResponse studentTimeTableInfo(StudentTimeTableRequest studentFeedbackRequest);

    StudentFeedbackAttendanceResponse studentAttendanceList(StudentTimeTableRequest studentFeedbackRequest);

    Page<StudentFeedback> studentFeedbackList(StudentTimeTableRequest studentFeedbackRequest);
    List<StudentFeedback> studentFeedbackDailyList(StudentFeedback studentFeedbackRequest);

}
