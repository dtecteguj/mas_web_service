package com.springapi.service;

import com.springapi.dto.request.BlockByFacultyIdRequest;
import com.springapi.dto.response.BlockUserByDeviceResponse;
import java.util.List;

import com.springapi.model.FacultyLogin;

public interface FacultyLoginService {
	
	public FacultyLogin create(FacultyLogin baf);

    public List<FacultyLogin> list();

    public FacultyLogin read(Long id);

    public FacultyLogin update(FacultyLogin baf);

    public void delete(Long id);
    
    BlockUserByDeviceResponse blockUserById(BlockByFacultyIdRequest blockByFacultyIdRequest);

}
