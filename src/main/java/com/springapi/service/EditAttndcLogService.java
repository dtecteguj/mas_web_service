/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.springapi.service;

import com.springapi.model.EditAttndcLog;
import java.util.List;

/**
 *
 * @author shahb
 */
public interface EditAttndcLogService {
    EditAttndcLog create(EditAttndcLog editAttndcLog);

    List<EditAttndcLog> list();

    EditAttndcLog read(Long aId);

    EditAttndcLog update(EditAttndcLog editAttndcLog);

    void delete(Long aId);
}
