/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.springapi.service;

import com.springapi.model.InstituteLocFancingInfo;
import java.util.List;

/**
 *
 * @author shahb
 */
public interface InstituteLocFancingInfoService {
    
    InstituteLocFancingInfo create(InstituteLocFancingInfo instituteLocFancingInfo);

    List<InstituteLocFancingInfo> list();

    InstituteLocFancingInfo read(Long aId);

    InstituteLocFancingInfo update(InstituteLocFancingInfo instituteLocFancingInfo);
    
   List<InstituteLocFancingInfo> findByInstituteId(Long uId);
    
//    InstituteLocFancingInfo insertFancingInfo(InstituteLocFancingInfo instituteLocFancingInfo);

    void delete(Long aId);
}
