/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.springapi.service;

import com.springapi.dto.request.AttendanceByPrincipalRequest;
import com.springapi.dto.request.FacAttendVerifyRequest;
import com.springapi.dto.request.FacultyAttendanceListRequest;
import com.springapi.dto.request.FacultyQRVerificationRequest;
import com.springapi.dto.request.LateAttendanceReasonRequest;
import com.springapi.dto.response.BaseResponse;
import com.springapi.model.FacultyAttendance;
import java.util.List;
import org.springframework.data.domain.Page;

/**
 *
 * @author shahb
 */
public interface FacultyAttendanceService {

    FacultyAttendance create(FacultyAttendance facultyAttendance);

    List<FacultyAttendance> list();

    FacultyAttendance read(Long faId);

    FacultyAttendance update(FacultyAttendance facultyAttendanceDetails);

    void delete(Long faId);

    BaseResponse verifyQRCode(FacultyQRVerificationRequest facultyQRVerificationRequest);

    BaseResponse verifyAttendance(FacAttendVerifyRequest facAttendVerifyRequest);

    Page<FacultyAttendance> facultyAttendanceList(FacultyAttendanceListRequest facultyAttendanceListRequest);

    BaseResponse verifyAndAdd(FacAttendVerifyRequest facAttendVerifyRequest);

    FacultyAttendance lateAttendanceReason(LateAttendanceReasonRequest lateAttendanceReasonRequest);

    String facultyAttendanceByInstituteId(AttendanceByPrincipalRequest attendanceByPrincipalRequest);

    BaseResponse verifyInAttendance(FacAttendVerifyRequest facAttendVerifyRequest);

    BaseResponse verifyOutAttendance(FacAttendVerifyRequest facAttendVerifyRequest);

    BaseResponse checkInAttendance(FacAttendVerifyRequest facAttendVerifyRequest);

    BaseResponse checkOutAttendance(FacAttendVerifyRequest facAttendVerifyRequest);
    
    BaseResponse checkLocInAttendance(FacAttendVerifyRequest facAttendVerifyRequest);

    BaseResponse checkLocOutAttendance(FacAttendVerifyRequest facAttendVerifyRequest);

    FacultyAttendance readByFacultyId(Long fId);

    void dailySchedule();

    void dailyMemoSchedule();

    void dailyHolidaySchedule();

    void dailyFSSchedule();
    
    void memoList();

}
