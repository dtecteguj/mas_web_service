/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.springapi.service;

import com.springapi.model.ProgramInstMpg;
import com.springapi.model.ProgramMst;
import java.util.List;

/**
 *
 * @author shahb
 */
public interface ProgramInstMpgService {

    ProgramInstMpg create(ProgramInstMpg programInstMpg);

    List<ProgramInstMpg> list();

    ProgramInstMpg read(Long pId);

    ProgramInstMpg update(ProgramInstMpg programInstMpg);

    void delete(Long pId);

    List<ProgramMst> readProgramByInstituteId(Long instituteId);
    
    List<ProgramInstMpg> readProgramByProgramIdAndInstituteId(Long instituteId,Long programId);
}
