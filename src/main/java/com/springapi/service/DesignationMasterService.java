/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.springapi.service;

import com.springapi.model.DesignationMaster;
import java.util.List;

/**
 *
 * @author shahb
 */
public interface DesignationMasterService {
    
    public List<DesignationMaster> list();

    public DesignationMaster create(DesignationMaster designationMaster);

    public DesignationMaster read(Long uId);

    public DesignationMaster update(DesignationMaster designationMaster);

    public void delete(Long dId);

}
