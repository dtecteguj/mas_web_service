package com.springapi.service;

import java.util.List;

import com.springapi.model.TrnActiveLoginLog;

public interface TrnActiveLoginLogService {
	public TrnActiveLoginLog create(TrnActiveLoginLog fm);

    public List<TrnActiveLoginLog> list();

    public TrnActiveLoginLog read(Long id);
    
    public TrnActiveLoginLog readForVerification(Long id,Long fId,String deviceId);

    public TrnActiveLoginLog update(TrnActiveLoginLog fm);

    public void delete(Long id);

}
