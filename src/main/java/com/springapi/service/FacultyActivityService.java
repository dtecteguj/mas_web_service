/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.springapi.service;

import com.springapi.dto.request.FacultyActivityRequest;
import com.springapi.dto.response.FacultyActivityRecordResponse;
import com.springapi.dto.response.FacultyActivityResponse;
import com.springapi.model.FacultyActivity;
import java.util.List;

/**
 *
 * @author shahb
 */
public interface FacultyActivityService {

    FacultyActivity create(FacultyActivityRequest facultyFeedbackRequest);

    FacultyActivityResponse getFacultyInfo(FacultyActivityRequest facultyActivityRequest);

    FacultyActivity updateFacultyInfo(FacultyActivity facultyActivity);

    List<FacultyActivityRecordResponse> facultyActivityRecord(FacultyActivity facultyActivity);

    List<FacultyActivity> getFacultyActivities(FacultyActivity facultyActivity);
    
}
