/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.springapi.service;

import com.springapi.model.InstituteDepartmentMpg;
import com.springapi.model.LeaveTypeMaster;
import java.util.List;

/**
 *
 * @author shahb
 */
public interface InstituteDepartmentMpgService {
    InstituteDepartmentMpg create(InstituteDepartmentMpg instituteDepartmentMpg);
    
    List<InstituteDepartmentMpg> list();
    
    InstituteDepartmentMpg read(Long iId);
    
    InstituteDepartmentMpg update(InstituteDepartmentMpg instituteDepartmentMpg);
    
    void delete(Long iId);
}
