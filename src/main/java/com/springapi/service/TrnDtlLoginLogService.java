/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.springapi.service;

import com.springapi.dto.request.LoginLogRequest;
import com.springapi.model.TrnDtlLoginLog;
import java.util.List;

/**
 *
 * @author shahb
 */
public interface TrnDtlLoginLogService {
     TrnDtlLoginLog create(TrnDtlLoginLog trnDtlLoginLog);

    List<TrnDtlLoginLog> list();

    TrnDtlLoginLog read(Long aId);

    TrnDtlLoginLog update(TrnDtlLoginLog trnDtlLoginLog);

    void delete(Long aId);
    
    TrnDtlLoginLog findByAndroidId(LoginLogRequest loginLogRequest);
}
