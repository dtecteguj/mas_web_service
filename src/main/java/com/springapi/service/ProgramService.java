package com.springapi.service;

import com.springapi.model.ProgramMaster;
import java.util.List;

/**
 * @author Jyoti Patel
 */
public interface ProgramService {

    public ProgramMaster create(ProgramMaster program);

    public List<ProgramMaster> list();

    public ProgramMaster read(Long uId);

    public ProgramMaster update(ProgramMaster programDetails);

    public void delete(Long fId);
    
}
