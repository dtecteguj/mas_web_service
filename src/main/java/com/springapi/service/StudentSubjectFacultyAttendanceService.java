/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.springapi.service;

import com.springapi.dto.request.FacStudAttenRequest;
import com.springapi.dto.request.StudentAttendanceDetailRequest;
import com.springapi.dto.request.StudentAttendanceRequest;
import com.springapi.dto.request.StudentAttendanceTotalRequest;
import com.springapi.dto.response.StudentAttendanceCountResponse;
import com.springapi.model.StudentAttendaceCount;
import com.springapi.model.StudentSubjectFacultyAttendance;
import java.util.List;

/**
 *
 * @author shahb
 */
public interface StudentSubjectFacultyAttendanceService {

    StudentSubjectFacultyAttendance create(StudentSubjectFacultyAttendance studentSubjectFacultyAttendance);

    List<StudentSubjectFacultyAttendance> list();

    StudentSubjectFacultyAttendance read(Long ssfaId);

    void delete(Long ssfaId);

    List<StudentSubjectFacultyAttendance> defaultAttendance(FacStudAttenRequest facStudAttenRequest);

    String studentAttendance(StudentAttendanceRequest studentAttendanceRequest);

    List<StudentAttendanceCountResponse> studentAttendanceCount(StudentAttendanceRequest studentAttendanceRequest);

    String studentAttendanceTotalCount(StudentAttendanceTotalRequest studentAttendanceTotalRequest);

    List<StudentAttendaceCount> studentAttendanceCountList();

    StudentAttendaceCount getStudentAtendance(StudentAttendanceDetailRequest studentAttendanceDetailRequest);

}
