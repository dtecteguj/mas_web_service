/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.springapi.service;

import com.springapi.model.TrnInstMisfnclty;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 *
 * @author shahb
 */
public interface TrnInstMisfncltyService {

    TrnInstMisfnclty create(TrnInstMisfnclty androidVersion);

    List<TrnInstMisfnclty> list();

    TrnInstMisfnclty read(Long aId);

    TrnInstMisfnclty update(TrnInstMisfnclty trnInstMisfnclty);

    Map<Long, TrnInstMisfnclty> listByDate(Date date);
    
    Map<Long, TrnInstMisfnclty> listByDate(Date date, Long sfType);

    void delete(Long aId);
}
