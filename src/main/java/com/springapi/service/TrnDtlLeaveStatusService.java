/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.springapi.service;

import com.springapi.dto.response.LeaveDetails;
import com.springapi.model.TrnDtlLeaveStatus;
import java.util.List;

/**
 *
 * @author shahb
 */
public interface TrnDtlLeaveStatusService {

    TrnDtlLeaveStatus create(TrnDtlLeaveStatus trnDtlLeaveStatus);

    List<TrnDtlLeaveStatus> list();

    TrnDtlLeaveStatus read(Long iId);

    TrnDtlLeaveStatus update(TrnDtlLeaveStatus trnDtlLeaveStatus);

    void delete(Long iId);

    LeaveDetails readData(Long iId);
    
    LeaveDetails readTodayData(Long fId);
}
