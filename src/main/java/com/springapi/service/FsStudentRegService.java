/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.springapi.service;

import com.springapi.model.FsStudentReg;
import java.util.List;

/**
 *
 * @author shahb
 */
public interface FsStudentRegService {
     FsStudentReg create(FsStudentReg fsStudentReg);

    List<FsStudentReg> list();

    FsStudentReg read(Long aId);

    FsStudentReg update(FsStudentReg fsStudentReg);

    void delete(Long aId);
}
