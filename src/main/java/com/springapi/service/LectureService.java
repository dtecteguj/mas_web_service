/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.springapi.service;

import com.springapi.model.Lecture;
import java.util.List;

/**
 *
 * @author shahb
 */
public interface LectureService {

    Lecture create(Lecture lecture);

    List<Lecture> list();

    Lecture read(Long lId);

    Lecture update(Lecture lectureDetails);

    void delete(Long lId);
}
