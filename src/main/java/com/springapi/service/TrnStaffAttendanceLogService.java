/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.springapi.service;

import com.springapi.model.InstituteLocFancingInfo;
import com.springapi.model.TrnStaffAttendanceLog;
import java.util.List;

/**
 *
 * @author shahb
 */
public interface TrnStaffAttendanceLogService {

    TrnStaffAttendanceLog create(TrnStaffAttendanceLog trnStaffAttendanceLog);

    List<TrnStaffAttendanceLog> list();

    TrnStaffAttendanceLog read(Long aId);

    TrnStaffAttendanceLog update(TrnStaffAttendanceLog trnStaffAttendanceLog);

    void delete(Long aId);
}
