/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.springapi.service;

import com.springapi.model.TrnDltDeputation;
import java.util.List;

/**
 *
 * @author shahb
 */
public interface TrnDltDeputationService {
    
    TrnDltDeputation create(TrnDltDeputation trnDltDeputation);

    List<TrnDltDeputation> list();

    TrnDltDeputation read(Long iId);

    TrnDltDeputation update(TrnDltDeputation trnDltDeputation);

    void delete(Long iId);

    List<TrnDltDeputation> readbyfid(Long fId);
}
