package com.springapi.service;

import java.util.List;

import com.springapi.model.TrnAttendanceLog;

public interface TrnAttendanceLogService {

	public TrnAttendanceLog create(TrnAttendanceLog tal);

    public List<TrnAttendanceLog> list();

    public TrnAttendanceLog read(Long id);

    public TrnAttendanceLog update(TrnAttendanceLog tal);

    public void delete(Long id);
}
