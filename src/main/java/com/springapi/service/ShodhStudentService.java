/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.springapi.service;

import com.springapi.dto.request.CMDashSHODHRequest;
import com.springapi.dto.response.SHODHStudentAttendanceRespNew;
import com.springapi.dto.response.SHODHStudentAttendanceResponse;
import com.springapi.model.ShodhStudent;
import java.util.List;

/**
 *
 * @author shahb
 */
public interface ShodhStudentService {
    ShodhStudent create(ShodhStudent shodhStudent);

    List<ShodhStudent> list();

    ShodhStudent read(Long aId);
    
    ShodhStudent update(ShodhStudent shodhStudent);

    void delete(Long aId);
    
    SHODHStudentAttendanceRespNew countAttendanceForCMDash(CMDashSHODHRequest cMDashSHODHRequest);
}
