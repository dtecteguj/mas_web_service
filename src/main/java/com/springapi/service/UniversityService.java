package com.springapi.service;

import com.springapi.model.UniversityMaster;
import java.util.List;

/**
 * @author Jyoti Patel
 */
public interface UniversityService {

    public UniversityMaster create(UniversityMaster university);

    public List<UniversityMaster> list();

    public UniversityMaster read(Long uId);

    public UniversityMaster update(UniversityMaster universityDetails);

    public void delete(Long fId);
    
}
