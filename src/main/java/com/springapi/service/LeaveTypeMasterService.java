/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.springapi.service;

import com.springapi.controller.HolidayMasterController;
import com.springapi.model.HolidayMaster;
import com.springapi.model.LeaveTypeMaster;
import java.util.List;
import org.apache.log4j.Logger;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author shahb
 */

public interface LeaveTypeMasterService {
    LeaveTypeMaster create(LeaveTypeMaster leaveTypeMaster);
    
    List<LeaveTypeMaster> list();
    
    LeaveTypeMaster read(Long lId);
    
    LeaveTypeMaster update(LeaveTypeMaster leaveTypeMaster);
    
    void delete(Long lId);
}
