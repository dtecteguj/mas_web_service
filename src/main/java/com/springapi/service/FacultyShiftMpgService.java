/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.springapi.service;

import com.springapi.dto.response.FacultyWiseShiftData;
import com.springapi.model.FacultyAttendance;
import com.springapi.model.FacultyShiftMpg;
import com.springapi.model.ShiftMaster;
import com.springapi.model.view.FacultyShiftTiming;
import java.util.List;

/**
 *
 * @author shahb
 */
public interface FacultyShiftMpgService {

    FacultyShiftMpg create(FacultyShiftMpg facultyShiftMpg);

    List<FacultyShiftMpg> list();

    FacultyShiftMpg read(Long fId);

    FacultyShiftMpg update(FacultyShiftMpg facultyShiftMpg);

    void delete(Long fId);

    void readByFacultyId(Long fId, int dayN, FacultyAttendance facAtt);

}
