package com.springapi.service;

import com.springapi.model.InstituteTypeMaster;
import java.util.List;

/**
 * @author Jyoti_patel
 */
public interface InstituteTypeService {

    public InstituteTypeMaster create(InstituteTypeMaster institute);

    public List<InstituteTypeMaster> list();

    public InstituteTypeMaster read(Long uId);

    public void delete(Long fId);
    
}
