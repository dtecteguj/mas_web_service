package com.springapi.service;

import com.springapi.dto.request.SlotRequest;
import com.springapi.model.SlotMaster;
import java.util.List;

/**
 * @author Jyoti_patel
 */
public interface SlotService {

    List<SlotMaster> getSlotByInstitute(SlotRequest slotRequest);
    
}
