package com.springapi.service;

import com.springapi.model.TokensManage;

/*
 * @author Jyoti_patel
 */
public interface TokenService {

    TokensManage createOrUpdate(TokensManage tokenManange);
    
}
