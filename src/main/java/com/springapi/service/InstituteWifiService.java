package com.springapi.service;

import com.springapi.model.InstituteWifiMaster;
import java.util.List;

/**
 *
 * @author Jyoti Patel
 */
public interface InstituteWifiService {

    public InstituteWifiMaster create(InstituteWifiMaster instituteWifi);

    public List<InstituteWifiMaster> list();

    public InstituteWifiMaster read(Long uId);

    public InstituteWifiMaster update(InstituteWifiMaster instituteWifiDetails);

    public void delete(Long fId);
    
}
