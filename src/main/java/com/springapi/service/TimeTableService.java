/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.springapi.service;

import com.springapi.dto.request.TimeTableRequest;
import com.springapi.model.TimeTable;
import java.util.List;

/**
 *
 * @author USER
 */
public interface TimeTableService {

    List<TimeTable> timeTableList(TimeTableRequest timeTableRequest);
    
}
