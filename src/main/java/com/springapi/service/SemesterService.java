/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.springapi.service;

import com.springapi.model.Semester;
import java.util.List;

/**
 *
 * @author shahb
 */
public interface SemesterService {

    Semester create(Semester semester);

    List<Semester> list();

    Semester read(Long sId);

    Semester update(Semester semesterDetails);

    void delete(Long sId);

}
