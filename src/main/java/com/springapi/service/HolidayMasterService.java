/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.springapi.service;

import com.springapi.model.FacultyShiftMpg;
import com.springapi.model.HolidayMaster;
import java.util.List;

/**
 *
 * @author shahb
 */
public interface HolidayMasterService {
     HolidayMaster create(HolidayMaster holidayMaster);
    
    List<HolidayMaster> list();
    
    HolidayMaster read(Long hId);
    
    HolidayMaster update(HolidayMaster holidayMaster);
    
    void delete(Long hId);
}
