/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.springapi.service;

import com.springapi.model.TermMaster;
import java.util.List;

/**
 *
 * @author shahb
 */
public interface TermMasterService {
    
    TermMaster create(TermMaster termMaster);
    
    List<TermMaster> list();
    
    TermMaster read(Long tId);
    
    TermMaster update(TermMaster termMaster);
    
    void delete(Long tId);
    
    
}
