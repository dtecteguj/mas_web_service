/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.springapi.service;

import com.springapi.dto.request.VrfVrsRequest;
import com.springapi.dto.response.AndroidVersionStatus;
import com.springapi.dto.response.VrfVrsResponse;
import com.springapi.model.AndroidVersion;
import java.util.List;

/**
 *
 * @author shahb
 */

public interface AndroidVersionService {
    AndroidVersion create(AndroidVersion androidVersion);

    List<AndroidVersion> list();

    AndroidVersionStatus read(Long aId);
    
    AndroidVersionStatus read(Long aId,String type);

    AndroidVersion update(AndroidVersion androidVersion);

    void delete(Long aId);
    
    VrfVrsResponse verifyVersion(VrfVrsRequest vrfVrsRequest);
}
