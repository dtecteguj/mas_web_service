package com.springapi.service;

import com.springapi.dto.request.FacultyLoginRequest;
import com.springapi.dto.request.StudentRegistrationRequest;
import com.springapi.dto.request.StudentRequest;
import com.springapi.dto.response.LoginResponse;
import com.springapi.dto.response.RegistrationResponse;
import com.springapi.model.Student;
import java.util.List;

/**
 *
 * @author shahb
 */
public interface StudentService {

//    List<Student> list();
    List<Student> list(StudentRequest studentRequest);

    Student read(Long sId);

    Student update(Student studentDetails);

    void delete(Long sId);

    Student create(Student student);

    LoginResponse login(FacultyLoginRequest facultyLoginRequest);

    LoginResponse loginStudent(String email, String password, String bssid, String ssid);

    List<Student> findAllStudentBySem(Long semId);
    
    RegistrationResponse studentRegist(StudentRegistrationRequest studentRegistrationRequest);
}
