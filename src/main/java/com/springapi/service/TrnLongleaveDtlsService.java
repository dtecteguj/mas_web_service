/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.springapi.service;

import com.springapi.model.TrnDtlIssueMemo;
import com.springapi.model.TrnLongleaveDtls;
import java.util.List;

/**
 *
 * @author shahb
 */
public interface TrnLongleaveDtlsService {

    TrnLongleaveDtls create(TrnLongleaveDtls trnLongleaveDtls);

    List<TrnLongleaveDtls> list();

    TrnLongleaveDtls read(Long iId);

    TrnLongleaveDtls update(TrnLongleaveDtls trnLongleaveDtls);

    void delete(Long iId);

    List<TrnLongleaveDtls> readbyfid(Long fId);
    
}
