package com.springapi.service;

import java.util.List;

import com.springapi.model.TrnbyPriHodLog;

public interface TrnbyPriHodLogService {

    public TrnbyPriHodLog create(TrnbyPriHodLog tpl);

    public List<TrnbyPriHodLog> list();

    public TrnbyPriHodLog read(Long id);

    public TrnbyPriHodLog update(TrnbyPriHodLog tpl);

    public void delete(Long id);
}
