/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.springapi.service;

import com.springapi.model.BlockStaffByAndroidId;
import java.util.List;

/**
 *
 * @author shahb
 */
public interface BlockStaffByAndroidIdService {

    BlockStaffByAndroidId create(BlockStaffByAndroidId blockStaffByAndroidId);

    List<BlockStaffByAndroidId> list();

    BlockStaffByAndroidId read(Long aId);

    BlockStaffByAndroidId update(BlockStaffByAndroidId blockStaffByAndroidId);

    void delete(Long aId);
}
