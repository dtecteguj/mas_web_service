/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.springapi.service;

import com.springapi.model.InstituteMaster;
import java.util.HashMap;
import java.util.List;

/**
 *
 * @author USER
 */
public interface InstituteService {

    InstituteMaster create(InstituteMaster institute);

    List<InstituteMaster> list();

    InstituteMaster read(Long uId);

    InstituteMaster update(InstituteMaster instituteDetails);

    void delete(Long fId);

    List<InstituteMaster> getAllInstituteByType(Long instituteTypeId);

    HashMap<String,Object> readDepartmentAndProgarm(Long uId);

}
