package com.springapi.service;

import java.util.List;
import java.util.concurrent.CompletableFuture;
import org.springframework.http.HttpEntity;

/**
 * @author shahb
 */
public interface FcmDailyPushNotificationService {

    List<String> getTokensForNotification();

    CompletableFuture<String> send(HttpEntity<String> request);

    List<String> getAllAttendanceTokenView();

    List<String> getTokensForStudent();
    
    List<String> getTokensForFaculty();
    
    Long getStudAndFacAttnVerificationSlotId();
    
    Long getSlotId();
    
    Long getTodayDay();
    
}
