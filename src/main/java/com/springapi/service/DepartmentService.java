/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.springapi.service;

import com.springapi.model.DepartmentMaster;
import java.util.List;

/**
 *
 * @author USER
 */
public interface DepartmentService {

    public List<DepartmentMaster> list();

    public DepartmentMaster create(DepartmentMaster department);

    public DepartmentMaster read(Long uId);

    public DepartmentMaster update(DepartmentMaster departmentDetails);

    public String namebyId(Long dId);

    public void delete(Long fId);

    List<DepartmentMaster> readDepartmentByInstituteId(Long instituteId);

}
