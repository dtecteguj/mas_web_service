/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.springapi.dto;

import lombok.Data;

/**
 *
 * @author shahb
 */
@Data
public class FacultyDepartment {
    private String departmentName;
    private Long departmentId;
    private Long fId;
    private String fullName;
    private Long instituteId;
    private String instituteName;
}
