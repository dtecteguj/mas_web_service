package com.springapi.dto.response;

/**
 * @author Jyoti_patel
 */
public class ChangePasswordResponse {
    private String message ;
    
    private String newPassword;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getNewPassword() {
        return newPassword;
    }

    public void setNewPassword(String newPassword) {
        this.newPassword = newPassword;
    }
    
    
}
