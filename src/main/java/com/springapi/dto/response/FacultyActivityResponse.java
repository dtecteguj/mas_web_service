package com.springapi.dto.response;

/**
 * @author Jyoti_patel
 */
public class FacultyActivityResponse {
    private Long semId;
    
    private Long subjectId;
    
    private String subjectName;
    

    public Long getSemId() {
        return semId;
    }

    public void setSemId(Long semId) {
        this.semId = semId;
    }

    public Long getSubjectId() {
        return subjectId;
    }

    public void setSubjectId(Long subjectId) {
        this.subjectId = subjectId;
    }

    public String getSubjectName() {
        return subjectName;
    }

    public void setSubjectName(String subjectName) {
        this.subjectName = subjectName;
    }
    
    
    
    
}
