package com.springapi.dto.response;

import com.springapi.util.CustDateFormat;
import lombok.Data;

@Data
public class VrfVrsResponse extends ErrorStatus {

    private String inTime;
    private String outTime;
    private String todayDate = CustDateFormat.todaySQLString();
    private Long masId;
    private Boolean isLogin = false;
    private Long faId;
    private Boolean isMemo = false;
    private Boolean isLate = false;
    private Boolean isEarly = false;

    private Long leaveStatus = 0L;
    private Boolean presentStatus = false;
    private Boolean isStatus = false;

    public String setTodayDateWithReturn(String todayDate) {
        return this.todayDate = todayDate;
    }

}
