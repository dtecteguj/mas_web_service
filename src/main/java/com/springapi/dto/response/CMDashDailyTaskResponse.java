/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.springapi.dto.response;

/**
 *
 * @author shahb
 */
public class CMDashDailyTaskResponse {
     private String dates;
    private Long distId;
    private String distName;
    private Long instId;
    private Long instTypeId;
    private Long universityId;
    private String departmentName;
    private String facultyName;
    private Long semester;
    private Long slotNumber;
    private String divison;
    private Long totalStudent;
    private Long presentStudent;
    private String instituteName;
    private String instituteTypeName;
    private String universityName;

    public String getDates() {
        return dates;
    }

    public void setDates(String dates) {
        this.dates = dates;
    }

    public Long getDistId() {
        return distId;
    }

    public void setDistId(Long distId) {
        this.distId = distId;
    }

    public String getDistName() {
        return distName;
    }

    public void setDistName(String distName) {
        this.distName = distName;
    }

    public Long getInstId() {
        return instId;
    }

    public void setInstId(Long instId) {
        this.instId = instId;
    }

    public Long getInstTypeId() {
        return instTypeId;
    }

    public void setInstTypeId(Long instTypeId) {
        this.instTypeId = instTypeId;
    }

    public Long getUniversityId() {
        return universityId;
    }

    public void setUniversityId(Long universityId) {
        this.universityId = universityId;
    }

    public String getDepartmentName() {
        return departmentName;
    }

    public void setDepartmentName(String departmentName) {
        this.departmentName = departmentName;
    }

    public String getFacultyName() {
        return facultyName;
    }

    public void setFacultyName(String facultyName) {
        this.facultyName = facultyName;
    }

    public Long getSemester() {
        return semester;
    }

    public void setSemester(Long semester) {
        this.semester = semester;
    }

    public Long getSlotNumber() {
        return slotNumber;
    }

    public void setSlotNumber(Long slotNumber) {
        this.slotNumber = slotNumber;
    }


    public String getDivison() {
        return divison;
    }

    public void setDivison(String divison) {
        this.divison = divison;
    }

    public Long getTotalStudent() {
        return totalStudent;
    }

    public void setTotalStudent(Long totalStudent) {
        this.totalStudent = totalStudent;
    }

    public Long getPresentStudent() {
        return presentStudent;
    }

    public void setPresentStudent(Long presentStudent) {
        this.presentStudent = presentStudent;
    }

    public String getInstituteName() {
        return instituteName;
    }

    public void setInstituteName(String instituteName) {
        this.instituteName = instituteName;
    }

    public String getInstituteTypeName() {
        return instituteTypeName;
    }

    public void setInstituteTypeName(String instituteTypeName) {
        this.instituteTypeName = instituteTypeName;
    }

    public String getUniversityName() {
        return universityName;
    }

    public void setUniversityName(String universityName) {
        this.universityName = universityName;
    }

}
