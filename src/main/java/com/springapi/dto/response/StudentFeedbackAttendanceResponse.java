package com.springapi.dto.response;

import java.util.List;

/**
 * @author Jyoti_patel
 */
public class StudentFeedbackAttendanceResponse {
    private int totalCount;
    
    private List<Long> studentIds;

    public int getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(int totalCount) {
        this.totalCount = totalCount;
    }

    public List<Long> getStudentIds() {
        return studentIds;
    }

    public void setStudentIds(List<Long> studentIds) {
        this.studentIds = studentIds;
    }
    
    
}
