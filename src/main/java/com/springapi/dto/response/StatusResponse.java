package com.springapi.dto.response;

/**
 * @author Jyoti_patel
 */
public class StatusResponse {
    private Long presentCount;
    private Long absentCount;

    public Long getPresentCount() {
        return presentCount;
    }

    public void setPresentCount(Long presentCount) {
        this.presentCount = presentCount;
    }

    public Long getAbsentCount() {
        return absentCount;
    }

    public void setAbsentCount(Long absentCount) {
        this.absentCount = absentCount;
    }
    
    
}
