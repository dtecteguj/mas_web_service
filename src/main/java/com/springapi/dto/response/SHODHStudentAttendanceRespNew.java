/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.springapi.dto.response;

import java.util.List;
import lombok.Data;

/**
 *
 * @author shahb
 */
@Data
public class SHODHStudentAttendanceRespNew {
    private Boolean isError=false;
    private Integer errorNo=0;
    private String message="Success";
    private List<SHODHStudentAttendanceResponse> attendance;
   
}
