/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.springapi.dto.response;

import lombok.Data;

/**
 *
 * @author shahb
 */
@Data
public class FacultyStatusResponse {
    private Long fId;
    private Long depertmentId;
    private String facultyName;
    private String departmentName;
    private Boolean isLate=false;
    private Boolean isEarly=false;
    private Boolean presentStatus = false;
    private Long leaveStatus = 0L;
    private String inTime ;
    private String outTime;
    private String actInTime;
    private String actOutTime;
     private String fName;
    private String mName;
    private String lName;
    private Boolean isShiftMap = true;
    
}
