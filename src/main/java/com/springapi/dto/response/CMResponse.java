package com.springapi.dto.response;

/**
 * @author Jyoti_patel
 */
public class CMResponse {
    private String instituteName;
    
    private StatusResponse statusResponse;

    public String getInstituteName() {
        return instituteName;
    }

    public void setInstituteName(String instituteName) {
        this.instituteName = instituteName;
    }

    public StatusResponse getStatusResponse() {
        return statusResponse;
    }

    public void setStatusResponse(StatusResponse statusResponse) {
        this.statusResponse = statusResponse;
    }
    
    
}
