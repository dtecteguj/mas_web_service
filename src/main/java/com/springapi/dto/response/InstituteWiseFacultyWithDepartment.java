/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.springapi.dto.response;

import lombok.Data;

/**
 *
 * @author shahb
 */

@Data
public class InstituteWiseFacultyWithDepartment {
    private Long fId;
    private Long dId;
    private String fName;
    private String dName;
    
    
    InstituteWiseFacultyWithDepartment(Long fId, Long dId, String fName, String dName)
    {
        this.fId = fId;
        this.dId = dId;
        this.fName = fName;
        this.dName = dName;
    }
    InstituteWiseFacultyWithDepartment(){}
}
