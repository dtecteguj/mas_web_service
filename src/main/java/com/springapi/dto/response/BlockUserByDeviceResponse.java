/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.springapi.dto.response;

import com.springapi.dto.FacultyDepartment;
import java.util.List;
import lombok.Data;

/**
 *
 * @author shahb
 */

@Data
public class BlockUserByDeviceResponse extends ErrorStatus {

    private Boolean isBlock = true;
   
}
