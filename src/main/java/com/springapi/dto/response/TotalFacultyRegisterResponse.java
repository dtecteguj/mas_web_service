package com.springapi.dto.response;

/**
 * @author Jyoti_patel
 */
public class TotalFacultyRegisterResponse {
    
    private Long count ;
    
    private String instituteType;

    public TotalFacultyRegisterResponse() {
    }

    public TotalFacultyRegisterResponse(Long count, String instituteType) {
        this.count = count;
        this.instituteType = instituteType;
    }
    
    public Long getCount() {
        return count;
    }

    public void setCount(Long count) {
        this.count = count;
    }

    public String getInstituteType() {
        return instituteType;
    }

    public void setInstituteType(String instituteType) {
        this.instituteType = instituteType;
    }
    
    
}
