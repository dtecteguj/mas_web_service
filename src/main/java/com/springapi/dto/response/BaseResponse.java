/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.springapi.dto.response;

import lombok.Data;
/**
 *
 * @author shahb
 */

@Data
public class BaseResponse extends ErrorStatus {
    private String message;
    
    private String inTime;
    
    private String outTime;
    
    private String instituteInTime;
    
    private String instituteOutTime;
    
    private Boolean isLate = false;
    
    private Boolean isEarly = false;
    
    private Boolean isLogin = true;
    
    private Long facultyAttendanceId;
    
    
    
    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getInTime() {
        return inTime;
    }

    public void setInTime(String inTime) {
        this.inTime = inTime;
    }

    public String getOutTime() {
        return outTime;
    }

    public void setOutTime(String outTime) {
        this.outTime = outTime;
    }

    public String getInstituteInTime() {
        return instituteInTime;
    }

    public void setInstituteInTime(String instituteInTime) {
        this.instituteInTime = instituteInTime;
    }

    public String getInstituteOutTime() {
        return instituteOutTime;
    }

    public void setInstituteOutTime(String instituteOutTime) {
        this.instituteOutTime = instituteOutTime;
    }

    public Boolean getIsLate() {
        return isLate;
    }

    public void setIsLate(Boolean isLate) {
        this.isLate = isLate;
    }

    public Boolean getIsEarly() {
        return isEarly;
    }

    public void setIsEarly(Boolean isEarly) {
        this.isEarly = isEarly;
    }

    public Long getFacultyAttendanceId() {
        return facultyAttendanceId;
    }

    public void setFacultyAttendanceId(Long facultyAttendanceId) {
        this.facultyAttendanceId = facultyAttendanceId;
    }
    
    
}
