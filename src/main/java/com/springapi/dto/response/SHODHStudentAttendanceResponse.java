/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.springapi.dto.response;

import lombok.Data;

/**
 *
 * @author shahb
 */
@Data
public class SHODHStudentAttendanceResponse {
    private Integer stuUniqueId;
    private String attDate;
    private Boolean isPrecence;
    private String inTime;
    private String outTime;
    private Boolean onDuty;
    private String actInTime;
    private String actOutTime;
    
}
