/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.springapi.dto.response;

/**
 *
 * @author shahb
 */
public class DashboardResponse {
    private Long count;
    private boolean status;
    private String date;
    private String instituteName;
    private String instituteTypeName;
    
    private Long absentCount;

    public DashboardResponse(Long count, Boolean status, String instituteName) {
        this.count = count;
        this.status = status;
        this.instituteName = instituteName;
    }
    
    public DashboardResponse(String instituteTypeName,Long count, Boolean status) {
        this.count = count;
        this.status = status;
        this.instituteTypeName = instituteTypeName;
    }

    public DashboardResponse() {
    }

    public DashboardResponse(Long count, boolean status) {
        this.count = count;
        this.status = status;
    }
    
    public DashboardResponse(Boolean status, String date, Long count) {
        this.count = count;
        this.status = status;
        this.date = date;
    }

    public Long getCount() {
        return count;
    }

    public void setCount(Long count) {
        this.count = count;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getInstituteName() {
        return instituteName;
    }

    public void setInstituteName(String instituteName) {
        this.instituteName = instituteName;
    }

    public String getInstituteTypeName() {
        return instituteTypeName;
    }

    public void setInstituteTypeName(String instituteTypeName) {
        this.instituteTypeName = instituteTypeName;
    }

    public Long getAbsentCount() {
        return absentCount;
    }

    public void setAbsentCount(Long absentCount) {
        this.absentCount = absentCount;
    }
    
}
