/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.springapi.dto.response;

import java.util.Date;

/**
 *
 * @author shahb
 */
public class CMDashResponse {
    private String dates;
    private Long distId;
    private String distName;
    private Long instId;
    private Long instTypeId;
    private Long universityId;
    private String departmentName;
    private String facultyName;
    private Boolean present;
    private Boolean onDuty;
    private Boolean absent;
    private Boolean halfCL;
    private Boolean late;
    private Boolean early;
    private Boolean leave;
    private String facInTime;
    private String actInTime;
    private String facOutTimre;
    private String actOutTime;
    private Long absLast30;
    private String instituteName;
    private String instituteTypeName;
    private String universityName;

    public String getDates() {
        return dates;
    }

    public void setDates(String dates) {
        this.dates = dates;
    }

    public Long getDistId() {
        return distId;
    }

    public void setDistId(Long distId) {
        this.distId = distId;
    }

    public Long getInstId() {
        return instId;
    }

    public void setInstId(Long instId) {
        this.instId = instId;
    }

    public Long getInstTypeId() {
        return instTypeId;
    }

    public void setInstTypeId(Long instTypeId) {
        this.instTypeId = instTypeId;
    }

    public Long getUniversityId() {
        return universityId;
    }

    public void setUniversityId(Long universityId) {
        this.universityId = universityId;
    }

  
    public Long getAbsLast30() {
        return absLast30;
    }

    public void setAbsLast30(Long absLast30) {
        this.absLast30 = absLast30;
    }

    public String getInstituteName() {
        return instituteName;
    }

    public void setInstituteName(String instituteName) {
        this.instituteName = instituteName;
    }

    public String getInstituteTypeName() {
        return instituteTypeName;
    }

    public void setInstituteTypeName(String instituteTypeName) {
        this.instituteTypeName = instituteTypeName;
    }

    public String getUniversityName() {
        return universityName;
    }

    public void setUniversityName(String universityName) {
        this.universityName = universityName;
    }

    public String getDistName() {
        return distName;
    }

    public void setDistName(String distName) {
        this.distName = distName;
    }

    public String getDepartmentName() {
        return departmentName;
    }

    public void setDepartmentName(String departmentName) {
        this.departmentName = departmentName;
    }

    public String getFacultyName() {
        return facultyName;
    }

    public void setFacultyName(String facultyName) {
        this.facultyName = facultyName;
    }

    public Boolean getPresent() {
        return present;
    }

    public void setPresent(Boolean present) {
        this.present = present;
    }

    public Boolean getOnDuty() {
        return onDuty;
    }

    public void setOnDuty(Boolean onDuty) {
        this.onDuty = onDuty;
    }

    public Boolean getAbsent() {
        return absent;
    }

    public void setAbsent(Boolean absent) {
        this.absent = absent;
    }

    public Boolean getHalfCL() {
        return halfCL;
    }

    public void setHalfCL(Boolean halfCL) {
        this.halfCL = halfCL;
    }

    public Boolean getLate() {
        return late;
    }

    public void setLate(Boolean late) {
        this.late = late;
    }

    public String getFacInTime() {
        return facInTime;
    }

    public void setFacInTime(String facInTime) {
        this.facInTime = facInTime;
    }

    public String getActInTime() {
        return actInTime;
    }

    public void setActInTime(String actInTime) {
        this.actInTime = actInTime;
    }

    public String getFacOutTimre() {
        return facOutTimre;
    }

    public void setFacOutTimre(String facOutTimre) {
        this.facOutTimre = facOutTimre;
    }

    public String getActOutTime() {
        return actOutTime;
    }

    public void setActOutTime(String actOutTime) {
        this.actOutTime = actOutTime;
    }

    public Boolean getEarly() {
        return early;
    }

    public void setEarly(Boolean early) {
        this.early = early;
    }

    public Boolean getLeave() {
        return leave;
    }

    public void setLeave(Boolean leave) {
        this.leave = leave;
    }
    
    
}
