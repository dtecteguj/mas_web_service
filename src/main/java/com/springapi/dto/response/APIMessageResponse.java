/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.springapi.dto.response;

import lombok.Data;

/**
 *
 * @author shahb
 */
@Data
public class APIMessageResponse {
    private Long statusCode = 0L;
    private String statusMessage;
    private Boolean success;
       
}
