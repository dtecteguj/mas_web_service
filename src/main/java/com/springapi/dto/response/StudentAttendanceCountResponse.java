package com.springapi.dto.response;

/**
 * @author Jyoti_patel
 */
public class StudentAttendanceCountResponse {
    private Long facultyId;
    
    private Long semId;
    
    private Long slotId;
    
    private Long totalPresentCount;

    public StudentAttendanceCountResponse() {
    }

    public StudentAttendanceCountResponse(Long facultyId, Long semId, Long totalPresentCount) {
        this.facultyId = facultyId;
        this.semId = semId;
        this.totalPresentCount = totalPresentCount;
    }
    
    
    public Long getFacultyId() {
        return facultyId;
    }

    public void setFacultyId(Long facultyId) {
        this.facultyId = facultyId;
    }

    public Long getSemId() {
        return semId;
    }

    public void setSemId(Long semId) {
        this.semId = semId;
    }

    public Long getTotalPresentCount() {
        return totalPresentCount;
    }

    public void setTotalPresentCount(Long totalPresentCount) {
        this.totalPresentCount = totalPresentCount;
    }

    public Long getSlotId() {
        return slotId;
    }

    public void setSlotId(Long slotId) {
        this.slotId = slotId;
    }
    
    
}
