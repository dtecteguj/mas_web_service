/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.springapi.dto.response;

import com.springapi.dto.request.FacultyLStatusRequest;
import java.util.List;

/**
 *
 * @author shahb
 */
public class GeneralListResponse extends ErrorStatus {
    
    private List<Object> listData;
    private Integer listCount = 0;
    
    public void setListDataforCount(List<Object> listData)
    {
        this.listData = listData;
        if(listData != null)
        {
            listCount =  listData.size();
        }
    }

   
}
