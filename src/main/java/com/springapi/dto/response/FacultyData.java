/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.springapi.dto.response;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Temporal;
import lombok.Data;

/**
 *
 * @author shahb
 */
@Data
public class FacultyData {

    private Long fId;
    private String name;
    private String email;
    private String phone;
    private Long role;
    private Long instituteId;
    private Long instituteTypeId;
    private Long departmentId;
    private Boolean isDeleted;
    private Long designationId;
    private String salutationName;
    private Boolean activeFlag;
    private String instituteName;
    private String instituteTypeName;
    private String departmentName;
    private String inTime;
    private String outTime;
    private String roleName;
    private Date termStartDate;
    private Date termEndDate;
    private Long termYear;
    private String fName;
    private String mName;
    private String lName;
    private String designationName;
    private Long desgClass;
    private String actInTime;
    private String actOutTime;

    public FacultyData(Long fId, String name, String email, String phone, Long role, Long instituteId, Long instituteTypeId, Long departmentId, Boolean isDeleted, Long designationId, String salutationName, Boolean activeFlag, String instituteName, String instituteTypeName, String departmentName, String inTime, String outTime, String roleName, Date termStartDate, Date termEndDate, Long termYear) {
        this.fId = fId;
        this.name = name;
        this.email = email;
        this.phone = phone;
        this.role = role;
        this.instituteId = instituteId;
        this.instituteTypeId = instituteTypeId;
        this.departmentId = departmentId;
        this.isDeleted = isDeleted;
        this.designationId = designationId;
        this.salutationName = salutationName;
        this.activeFlag = activeFlag;
        this.instituteName = instituteName;
        this.instituteTypeName = instituteTypeName;
        this.departmentName = departmentName;
        this.inTime = inTime;
        this.outTime = outTime;
        this.roleName = roleName;
        this.termStartDate = termStartDate;
        this.termEndDate = termEndDate;
        this.termYear = termYear;
    }

    public FacultyData(Long fId, String name, String email, String phone, Long role, Long instituteId, Long instituteTypeId, Long departmentId, Boolean isDeleted, Long designationId, String salutationName, Boolean activeFlag, String instituteName, String instituteTypeName, String departmentName, String inTime, String outTime, String roleName, Date termStartDate, Date termEndDate, Long termYear, String fName, String mName, String lName, Long desgClass) {
        this.fId = fId;
        this.name = name;
        this.email = email;
        this.phone = phone;
        this.role = role;
        this.instituteId = instituteId;
        this.instituteTypeId = instituteTypeId;
        this.departmentId = departmentId;
        this.isDeleted = isDeleted;
        this.designationId = designationId;
        this.salutationName = salutationName;
        this.activeFlag = activeFlag;
        this.instituteName = instituteName;
        this.instituteTypeName = instituteTypeName;
        this.departmentName = departmentName;
        this.inTime = inTime;
        this.outTime = outTime;
        this.roleName = roleName;
        this.termStartDate = termStartDate;
        this.termEndDate = termEndDate;
        this.termYear = termYear;
        this.fName = fName;
        this.mName = mName;
        this.lName = lName;
        this.desgClass = desgClass;
    }

    public FacultyData(Long fId, String name, String email, String phone, Long role, Long instituteId, Long instituteTypeId, Long departmentId, Boolean isDeleted, String instituteName, String instituteTypeName, String departmentName, Long termYear, String fName, String mName, String lName) {
        this.fId = fId;
        this.name = name;
        this.email = email;
        this.phone = phone;
        this.role = role;
        this.instituteId = instituteId;
        this.instituteTypeId = instituteTypeId;
        this.departmentId = departmentId;
        this.isDeleted = isDeleted;
        this.instituteName = instituteName;
        this.instituteTypeName = instituteTypeName;
        this.departmentName = departmentName;
        this.termYear = termYear;
        this.fName = fName;
        this.mName = mName;
        this.lName = lName;
    }

}
