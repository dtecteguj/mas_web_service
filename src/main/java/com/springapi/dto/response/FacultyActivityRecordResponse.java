package com.springapi.dto.response;

/**
 * @author Jyoti_patel
 */
public class FacultyActivityRecordResponse {
    private String message;
    
    private Long count;

    public FacultyActivityRecordResponse() {
    }

    public FacultyActivityRecordResponse(String message, Long count) {
        this.count = count;
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
    
    public Long getCount() {
        return count;
    }

    public void setCount(Long count) {
        this.count = count;
    }
    
}
