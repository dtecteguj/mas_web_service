/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.springapi.dto.response;

import com.fasterxml.jackson.annotation.JsonFormat;

/**
 *
 * @author USER
 */
@JsonFormat
public class QRCodeResponse {
    
    private String qrCode;

    public String getQrCode() {
        return qrCode;
    }

    public void setQrCode(String qrCode) {
        this.qrCode = qrCode;
    }
    
    
    
}
