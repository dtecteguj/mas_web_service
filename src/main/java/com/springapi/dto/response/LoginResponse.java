package com.springapi.dto.response;

import com.springapi.dto.FacultyDepartment;
import com.springapi.dto.StaftDepartment;
import java.util.List;
import lombok.Data;

/**
 * @author Jyoti Patel
 */
@Data
public class LoginResponse extends ErrorStatus {
    private Long id;
    
    private String message ;
    
    private String userType;
    
    private Long studentSemId;
    
    private String facultyName;
    
    private String instituteName;
    
    private Long instituteId;
    
    private String studentName;
    
    private String inTime;
    
    private String outTime;
    
    private Boolean isLate=false;
    private Boolean isEarly=false;
    
    private Long leaveStatus = 0L;
    private Boolean presentStatus = false;
    private Boolean isStatus = false;

    private Boolean isPrincipal;
    
    private String currentDate;
    
    private Long userRole;
    
    private Long actLogId;
    
    private Boolean isHodPri = false;
   
    private List<StaftDepartment> staftDepartments; 
    
    private List<FacultyDepartment> blockUserList;
    
    private Boolean isActive;
    
    private Boolean isLocActive = false;
    
    private Boolean isReqForBlock = false;
    
    private Long faId;
    

    public Boolean getIsLocActive() {
        return isLocActive;
    }

    public void setIsLocActive(Boolean isLocActive) {
        this.isLocActive = isLocActive;
    }
    
    private Boolean isMemo =false;
    
    private Long departmentId;
    
    
    

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getUserType() {
        return userType;
    }

    public void setUserType(String userType) {
        this.userType = userType;
    }

    public Long getStudentSemId() {
        return studentSemId;
    }

    public void setStudentSemId(Long studentSemId) {
        this.studentSemId = studentSemId;
    }

    public String getFacultyName() {
        return facultyName;
    }

    public void setFacultyName(String facultyName) {
        this.facultyName = facultyName;
    }

    public String getInstituteName() {
        return instituteName;
    }

    public void setInstituteName(String instituteName) {
        this.instituteName = instituteName;
    }

    public String getStudentName() {
        return studentName;
    }

    public void setStudentName(String studentName) {
        this.studentName = studentName;
    }

    public String getInTime() {
        return inTime;
    }

    public void setInTime(String inTime) {
        this.inTime = inTime;
    }

    public String getOutTime() {
        return outTime;
    }

    public void setOutTime(String outTime) {
        this.outTime = outTime;
    }

    public String getCurrentDate() {
        return currentDate;
    }

    public void setCurrentDate(String currentDate) {
        this.currentDate = currentDate;
    }

    public Long getUserRole() {
        return userRole;
    }

    public void setUserRole(Long userRole) {
        this.userRole = userRole;
    }

    public Long getInstituteId() {
        return instituteId;
    }

    public void setInstituteId(Long instituteId) {
        this.instituteId = instituteId;
    }

    public Boolean getIsActive() {
        return isActive;
    }

    public void setIsActive(Boolean isActive) {
        this.isActive = isActive;
    }

    public Long getDepartmentId() {
        return departmentId;
    }

    public void setDepartmentId(Long departmentId) {
        this.departmentId = departmentId;
    }

    public Long getFaId() {
        return faId;
    }

    public void setFaId(Long faId) {
        this.faId = faId;
    }

    public Boolean getIsMemo() {
        return isMemo;
    }

    public void setIsMemo(Boolean isMemo) {
        this.isMemo = isMemo;
    }
    
    
    
}
