/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.springapi.dto.request;

import lombok.Data;

/**
 *
 * @author shahb
 */

@Data
public class LoginLogRequest {
    private Long fId;
    private String token;
    private String androidId;
    private Long masId;
               
}
