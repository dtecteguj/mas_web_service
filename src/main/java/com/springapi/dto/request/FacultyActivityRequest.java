package com.springapi.dto.request;

/**
 *
 * @author JYoti
 */
public class FacultyActivityRequest {
    
    private Long facultyId;
    
    private String message;
    
    private Long slotId;
    
    private Long dayId;
    
    

    public Long getFacultyId() {
        return facultyId;
    }

    public void setFacultyId(Long facultyId) {
        this.facultyId = facultyId;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Long getSlotId() {
        return slotId;
    }

    public void setSlotId(Long slotId) {
        this.slotId = slotId;
    }

    public Long getDayId() {
        return dayId;
    }

    public void setDayId(Long dayId) {
        this.dayId = dayId;
    }
    
}
