package com.springapi.dto.request;

/**
 * @author Jyoti Patel
 */
public class DailyEndrosRequest {
    
    private Boolean endrosStatus;
    
    private Long instituteId;
    
    private Long departmentId;

    public Boolean getEndrosStatus() {
        return endrosStatus;
    }

    public void setEndrosStatus(Boolean endrosStatus) {
        this.endrosStatus = endrosStatus;
    }

    public Long getInstituteId() {
        return instituteId;
    }

    public void setInstituteId(Long instituteId) {
        this.instituteId = instituteId;
    }

    public Long getDepartmentId() {
        return departmentId;
    }

    public void setDepartmentId(Long departmentId) {
        this.departmentId = departmentId;
    }
    
    
}
