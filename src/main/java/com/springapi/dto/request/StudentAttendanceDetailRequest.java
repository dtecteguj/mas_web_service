package com.springapi.dto.request;

import javax.persistence.Transient;

/**
 * @author Jyoti_patel
 */
public class StudentAttendanceDetailRequest {
    private Long facultyId;
    
    private Long slotId;
    
    @Transient
    private String dayDate;

    public Long getFacultyId() {
        return facultyId;
    }

    public void setFacultyId(Long facultyId) {
        this.facultyId = facultyId;
    }

    public Long getSlotId() {
        return slotId;
    }

    public void setSlotId(Long slotId) {
        this.slotId = slotId;
    }

    public String getDayDate() {
        return dayDate;
    }

    public void setDayDate(String dayDate) {
        this.dayDate = dayDate;
    }
    
}
