package com.springapi.dto.request;

/**
 * @author Jyoti_patel
 */
public class LateAttendanceReasonRequest {
    private Long facultyId;
    
    private String earlyOutReason;
    
    private String lateComingReason;
    
    
   

    public Long getFacultyId() {
        return facultyId;
    }

    public void setFacultyId(Long facultyId) {
        this.facultyId = facultyId;
    }

    public String getEarlyOutReason() {
        return earlyOutReason;
    }

    public void setEarlyOutReason(String earlyOutReason) {
        this.earlyOutReason = earlyOutReason;
    }

    public String getLateComingReason() {
        return lateComingReason;
    }

    public void setLateComingReason(String lateComingReason) {
        this.lateComingReason = lateComingReason;
    }

    
    
    
}
