package com.springapi.dto.request;

/**
 * @author Jyoti_patel
 */
public class StudentList {
    private Long studentId;
    
    private Boolean attendance;

    public Long getStudentId() {
        return studentId;
    }

    public void setStudentId(Long studentId) {
        this.studentId = studentId;
    }

    public Boolean getAttendance() {
        return attendance;
    }

    public void setAttendance(Boolean attendance) {
        this.attendance = attendance;
    }
    
    
}
