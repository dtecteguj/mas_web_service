package com.springapi.dto.request;

import java.util.Date;

/**
 *
 * @author USER
 */
 public class FacultyAttendanceListRequest extends PaginationRequest{
    private Long departmentId;
    
    private Long instituteId;
    
    private Date startDate;
    
    private Date endDate;
    
    private String searchText;
    
    private Boolean isFaculty;
    
    private Long facultyId;
    
    private Long instituteTypeId;
    
    public Long getDepartmentId() {
        return departmentId;
    }

    public void setDepartmentId(Long departmentId) {
        this.departmentId = departmentId;
    }

    public Long getInstituteId() {
        return instituteId;
    }

    public void setInstituteId(Long instituteId) {
        this.instituteId = instituteId;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public String getSearchText() {
        return searchText;
    }

    public void setSearchText(String searchText) {
        this.searchText = searchText;
    }

    public Boolean getIsFaculty() {
        return isFaculty;
    }

    public void setIsFaculty(Boolean isFaculty) {
        this.isFaculty = isFaculty;
    }

    public Long getFacultyId() {
        return facultyId;
    }

    public void setFacultyId(Long facultyId) {
        this.facultyId = facultyId;
    }

    public Long getInstituteTypeId() {
        return instituteTypeId;
    }

    public void setInstituteTypeId(Long instituteTypeId) {
        this.instituteTypeId = instituteTypeId;
    }
    
}
