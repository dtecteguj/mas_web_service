package com.springapi.dto.request;

/**
 * @author Jyoti Patel
 */
public class StudentRequest {
    
  private Long semId;
  

    public Long getSemId() {
        return semId;
    }

    public void setSemId(Long semId) {
        this.semId = semId;
    }
  
}
