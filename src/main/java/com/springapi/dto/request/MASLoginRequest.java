package com.springapi.dto.request;

import javax.validation.constraints.NotNull;
import lombok.Data;

@Data
public class MASLoginRequest {

    @NotNull
    private String email;

    @NotNull
    private String password;
    private Long deviceId;
    private Long masid;

}
