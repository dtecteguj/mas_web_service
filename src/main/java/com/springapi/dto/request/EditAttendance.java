/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.springapi.dto.request;

/**
 *
 * @author shahb
 */
public class EditAttendance {
    private Long attId;
    private Long fId;
    private Long attType;
    private Long attOldType;
    private String attOldDesc;
    private String remark;

    public Long getAttId() {
        return attId;
    }

    public void setAttId(Long attId) {
        this.attId = attId;
    }

    public Long getfId() {
        return fId;
    }

    public void setfId(Long fId) {
        this.fId = fId;
    }

    public Long getAttType() {
        return attType;
    }

    public void setAttType(Long attType) {
        this.attType = attType;
    }

    public Long getAttOldType() {
        return attOldType;
    }

    public void setAttOldType(Long attOldType) {
        this.attOldType = attOldType;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getAttOldDesc() {
        return attOldDesc;
    }

    public void setAttOldDesc(String attOldDesc) {
        this.attOldDesc = attOldDesc;
    }
    
    
}
