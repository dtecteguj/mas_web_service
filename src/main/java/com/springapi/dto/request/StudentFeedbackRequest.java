package com.springapi.dto.request;

/**
 * @author shahb
 */
public class StudentFeedbackRequest {

    private Long studentId;

    private Long rate;

    private Boolean isLectureAttend;

    private Boolean isLectureConducted;

    private String comment;

    public Long getStudentId() {
        return studentId;
    }

    public void setStudentId(Long studentId) {
        this.studentId = studentId;
    }

    public Long getRate() {
        return rate;
    }

    public void setRate(Long rate) {
        this.rate = rate;
    }

    public Boolean getIsLectureAttend() {
        return isLectureAttend;
    }

    public void setIsLectureAttend(Boolean isLectureAttend) {
        this.isLectureAttend = isLectureAttend;
    }

    public Boolean getIsLectureConducted() {
        return isLectureConducted;
    }

    public void setIsLectureConducted(Boolean isLectureConducted) {
        this.isLectureConducted = isLectureConducted;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    
}
