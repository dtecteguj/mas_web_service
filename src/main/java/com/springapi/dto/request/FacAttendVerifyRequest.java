package com.springapi.dto.request;

import javax.validation.constraints.NotNull;
import lombok.Data;

/**
 *
 * @author Jyoti Patel
 */
@Data
public class FacAttendVerifyRequest {

    @NotNull
    private Long fId;

    private Double locLat;
    private Double locLng;
    private Boolean attType = false;
    private Boolean markType = false;
    private Boolean isLateIn = false;
    private String lateInReason;
    private Boolean isEarlyOut = false;
    private String earlyOutReason;
    private Long actLogId;
    private String ssid;
    private String bssid;
    private String androidID;
    private String macID;
    private String IMEINo;

    private Long roleId;

    private Long versionCode;

    private String deviceTypeId;

    @NotNull
    private Boolean isVerified;

    private Long facultyAttendanceId;

    public Long getfId() {
        return fId;
    }

    public void setfId(Long fId) {
        this.fId = fId;
    }

    public String getSsid() {
        return ssid;
    }

    public void setSsid(String ssid) {
        this.ssid = ssid;
    }

    public String getBssid() {
        return bssid;
    }

    public void setBssid(String bssid) {
        this.bssid = bssid;
    }

    public String getAndroidID() {
        return androidID;
    }

    public void setAndroidID(String androidID) {
        this.androidID = androidID;
    }

    public String getMacID() {
        return macID;
    }

    public void setMacID(String macID) {
        this.macID = macID;
    }

    public String getIMEINo() {
        return IMEINo;
    }

    public void setIMEINo(String IMEINo) {
        this.IMEINo = IMEINo;
    }

    public Boolean getIsVerified() {
        return isVerified;
    }

    public void setIsVerified(Boolean isVerified) {
        this.isVerified = isVerified;
    }

    public Long getRoleId() {
        return roleId;
    }

    public void setRoleId(Long roleId) {
        this.roleId = roleId;
    }

    public Long getVersionCode() {
        return versionCode;
    }

    public void setVersionCode(Long versionCode) {
        this.versionCode = versionCode;
    }

    public Long getFacultyAttendanceId() {
        return facultyAttendanceId;
    }

    public void setFacultyAttendanceId(Long facultyAttendanceId) {
        this.facultyAttendanceId = facultyAttendanceId;
    }

    public Double getLocLat() {
        return locLat;
    }

    public void setLocLat(Double locLat) {
        this.locLat = locLat;
    }

    public Double getLocLng() {
        return locLng;
    }

    public void setLocLng(Double locLng) {
        this.locLng = locLng;
    }

   
    public Long getActLogId() {
        return actLogId;
    }

    public void setActLogId(Long actLogId) {
        this.actLogId = actLogId;
    }

}
