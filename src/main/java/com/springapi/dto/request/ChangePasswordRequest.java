package com.springapi.dto.request;

import javax.validation.constraints.NotNull;

/**
 * @author Jyoti_patel
 */
public class ChangePasswordRequest {
    
    private Long fId;
   
    private String email;
    
    @NotNull
    private String oldPassword;
    
    @NotNull
    private String newPassword;
    
//    @NotNull
    private String retypePassword;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getOldPassword() {
        return oldPassword;
    }

    public void setOldPassword(String oldPassword) {
        this.oldPassword = oldPassword;
    }

    public String getNewPassword() {
        return newPassword;
    }

    public void setNewPassword(String newPassword) {
        this.newPassword = newPassword;
    }

    public String getRetypePassword() {
        return retypePassword;
    }

    public void setRetypePassword(String retypePassword) {
        this.retypePassword = retypePassword;
    }

    public Long getfId() {
        return fId;
    }

    public void setfId(Long fId) {
        this.fId = fId;
    }
}
