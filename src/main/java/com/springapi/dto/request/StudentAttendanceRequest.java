package com.springapi.dto.request;

import java.util.List;

/**
 * @author Jyoti Patel
 */
//@JsonFormat(shape=JsonFormat.Shape.ARRAY)
public class StudentAttendanceRequest {
    private Long facultyId;
    
    private Long semId;
    
    private Long slotId;
    
    private Long subjectId;
    
    private List<StudentList> studentInfo;

    public Long getFacultyId() {
        return facultyId;
    }

    public void setFacultyId(Long facultyId) {
        this.facultyId = facultyId;
    }

    public Long getSemId() {
        return semId;
    }

    public void setSemId(Long semId) {
        this.semId = semId;
    }

    public Long getSubjectId() {
        return subjectId;
    }

    public void setSubjectId(Long subjectId) {
        this.subjectId = subjectId;
    }

    public Long getSlotId() {
        return slotId;
    }

    public void setSlotId(Long slotId) {
        this.slotId = slotId;
    }

    public List<StudentList> getStudentInfo() {
        return studentInfo;
    }

    public void setStudentInfo(List<StudentList> studentInfo) {
        this.studentInfo = studentInfo;
    }

}
