package com.springapi.dto.request;

/**
 * @author Jyoti Patel
 */
public class DailyEndrosStatusRequest {
    private Long hodId;
    
    //TODO : Use this in future for hod user too
    private Long userId;
    
    private Long instituteId;
    
    private Long departmentId;

    public Long getHodId() {
        return hodId;
    }

    public void setHodId(Long hodId) {
        this.hodId = hodId;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Long getInstituteId() {
        return instituteId;
    }

    public void setInstituteId(Long instituteId) {
        this.instituteId = instituteId;
    }

    public Long getDepartmentId() {
        return departmentId;
    }

    public void setDepartmentId(Long departmentId) {
        this.departmentId = departmentId;
    }
    
    
    
}
