package com.springapi.dto.request;

/**
 * @author Administrator
 */
public class DashboardRequest {
    
    private boolean isMonthly;
    
    private boolean isLastSevenDay;
    
    private Long instituteId;
    
    private Long instituteTypeId;
    
    private Long departmentId;
    
    public boolean isIsMonthly() {
        return isMonthly;
    }

    public void setIsMonthly(boolean isMonthly) {
        this.isMonthly = isMonthly;
    }

    public boolean isIsLastSevenDay() {
        return isLastSevenDay;
    }

    public void setIsLastSevenDay(boolean isLastSevenDay) {
        this.isLastSevenDay = isLastSevenDay;
    }

    public Long getInstituteId() {
        return instituteId;
    }

    public void setInstituteId(Long instituteId) {
        this.instituteId = instituteId;
    }

    public Long getInstituteTypeId() {
        return instituteTypeId;
    }

    public void setInstituteTypeId(Long instituteTypeId) {
        this.instituteTypeId = instituteTypeId;
    }

    public Long getDepartmentId() {
        return departmentId;
    }

    public void setDepartmentId(Long departmentId) {
        this.departmentId = departmentId;
    }
}
