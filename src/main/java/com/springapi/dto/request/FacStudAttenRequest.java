package com.springapi.dto.request;

/**
 * @author Jyoti Patel
 */
public class FacStudAttenRequest {
    
    private Long fId;
    
    private Long subjectId;
    
    private Long dayId;
    
    private Long semId;

    public Long getfId() {
        return fId;
    }

    public void setfId(Long fId) {
        this.fId = fId;
    }

    public Long getSubjectId() {
        return subjectId;
    }

    public void setSubjectId(Long subjectId) {
        this.subjectId = subjectId;
    }

    public Long getDayId() {
        return dayId;
    }

    public void setDayId(Long dayId) {
        this.dayId = dayId;
    }

    public Long getSemId() {
        return semId;
    }

    public void setSemId(Long semId) {
        this.semId = semId;
    }
    
    
}
