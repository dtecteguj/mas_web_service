package com.springapi.dto.request;

import lombok.Data;

@Data
public class VrfVrsRequest {

	private Long versionId;
	private String versionType;
	private String deviceId;
	private Long fId;
	private Long actLogId;
	private Boolean isLogin=false;
        
}
