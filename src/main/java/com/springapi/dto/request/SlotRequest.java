package com.springapi.dto.request;

/**
 * @author Jyoti_patel
 */
public class SlotRequest {
    private Long instituteId;

    public Long getInstituteId() {
        return instituteId;
    }

    public void setInstituteId(Long instituteId) {
        this.instituteId = instituteId;
    }
    
}
