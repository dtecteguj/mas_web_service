package com.springapi.dto.request;

/**
 * @author Jyoti_patel
 */
public class StudentAttendanceTotalRequest {
    private Long facultyId;
    
    private Long slotId;
    
    private Long dayId;
    
    private Long semId;
    
    private Long totPresent;
    
    private Long totCount;
    
    private String subjectName;
    
    private String batchName;
    
    private String lecOrLab;

    public Long getFacultyId() {
        return facultyId;
    }

    public void setFacultyId(Long facultyId) {
        this.facultyId = facultyId;
    }

    public Long getSlotId() {
        return slotId;
    }

    public void setSlotId(Long slotId) {
        this.slotId = slotId;
    }

    public Long getDayId() {
        return dayId;
    }

    public void setDayId(Long dayId) {
        this.dayId = dayId;
    }

    public Long getSemId() {
        return semId;
    }

    public void setSemId(Long semId) {
        this.semId = semId;
    }

    public Long getTotPresent() {
        return totPresent;
    }

    public void setTotPresent(Long totPresent) {
        this.totPresent = totPresent;
    }

    public Long getTotCount() {
        return totCount;
    }

    public void setTotCount(Long totCount) {
        this.totCount = totCount;
    }

    public String getSubjectName() {
        return subjectName;
    }

    public void setSubjectName(String subjectName) {
        this.subjectName = subjectName;
    }

    public String getBatchName() {
        return batchName;
    }

    public void setBatchName(String batchName) {
        this.batchName = batchName;
    }

    public String getLecOrLab() {
        return lecOrLab;
    }

    public void setLecOrLab(String lecOrLab) {
        this.lecOrLab = lecOrLab;
    }
}
