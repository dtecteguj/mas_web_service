package com.springapi.dto.request;

import java.util.Date;
import java.util.List;
import lombok.Data;

/**
 * @author Jyoti_patel
 */

@Data
public class AttendanceByPrincipalRequest {
    private Long instituteId;
    
    private List<Long> facultyIds;
    
    private String reasonByPrincipal;
    
    private Long typeId;
    
    private Date startDate;
    private Date endDate;
    private Long duration;
    
    private Long longLeaveId;

    private Long byFacultyId;
    
    private Long actLogId;
    
}
