/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.springapi.dto.request;

/**
 *
 * @author shahb
 */
public class StudentRegistrationRequest {

    private String name;
    private String phone;
    private String email;
    private String enrollment;
    private String fName;
    private String mName;
    private String lName;
    private Long departmentId;
    private Long instituteId;
    private Long instituteTypeId;
    private Long programId;
    private Long programTypeId;
    private Long divId;
    private Long semId;
    private String admityearName;
    private String dob;
    private String password;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getEnrollment() {
        return enrollment;
    }

    public void setEnrollment(String enrollment) {
        this.enrollment = enrollment;
    }

    public String getfName() {
        return fName;
    }

    public void setfName(String fName) {
        this.fName = fName;
    }

    public String getmName() {
        return mName;
    }

    public void setmName(String mName) {
        this.mName = mName;
    }

    public String getlName() {
        return lName;
    }

    public void setlName(String lName) {
        this.lName = lName;
    }

    public Long getDepartmentId() {
        return departmentId;
    }

    public void setDepartmentId(Long departmentId) {
        this.departmentId = departmentId;
    }

    public Long getInstituteId() {
        return instituteId;
    }

    public void setInstituteId(Long instituteId) {
        this.instituteId = instituteId;
    }

    public Long getInstituteTypeId() {
        return instituteTypeId;
    }

    public void setInstituteTypeId(Long instituteTypeId) {
        this.instituteTypeId = instituteTypeId;
    }

    public Long getProgramId() {
        return programId;
    }

    public void setProgramId(Long programId) {
        this.programId = programId;
    }

    public Long getProgramTypeId() {
        return programTypeId;
    }

    public void setProgramTypeId(Long programTypeId) {
        this.programTypeId = programTypeId;
    }

    public Long getDivId() {
        return divId;
    }

    public void setDivId(Long divId) {
        this.divId = divId;
    }

    public Long getSemId() {
        return semId;
    }

    public void setSemId(Long semId) {
        this.semId = semId;
    }

    public String getAdmityearName() {
        return admityearName;
    }

    public void setAdmityearName(String admityearName) {
        this.admityearName = admityearName;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    @Override
    public String toString() {
        return "StudentRegistrationRequest{" + "name=" + name + ", phone=" + phone + ", email=" + email + ", enrollment=" + enrollment + ", fName=" + fName + ", mName=" + mName + ", lName=" + lName + ", departmentId=" + departmentId + ", instituteId=" + instituteId + ", instituteTypeId=" + instituteTypeId + ", programId=" + programId + ", programTypeId=" + programTypeId + ", divId=" + divId + ", semId=" + semId + ", admityearName=" + admityearName + ", dob=" + dob + '}';
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
    
    
}
