/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.springapi.dto.request;

import lombok.Data;

/**
 *
 * @author shahb
 */

@Data
public class FacultyLStatusRequest {
    
    private Long fId;
    private Long instituteId;
    private Long departmentId;
    private Long actLogId;
    private Long retriveType = 4L;
}
