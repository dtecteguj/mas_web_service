package com.springapi.dto.request;

/**
 * @author Jyoti_patel
 */
public class ForgetPasswordRequest {
    private String email;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
