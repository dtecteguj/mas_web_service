/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.springapi.dto;

import com.springapi.enums.FASMessageType;

/**
 *
 * @author USER
 */
public class FASMessage {
    
    private FASMessageType messageType;
    
    private Object[] args;

    public FASMessage(FASMessageType messageType) {
        this.messageType = messageType;
       // throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public FASMessage(FASMessageType messageType, Object[] args) {
        this.messageType = messageType;
        this.args= args;
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public FASMessageType getMessageType() {
        return messageType;
    }

    public Object[] getArgs() {
        return args;
    }
}
