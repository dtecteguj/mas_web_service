/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.springapi.util;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author shahb
 */
public class RoleTypes {

    public static boolean isSpecialRole(long role) {
        int type = (int) role;
        switch (type) {
            case 5:
            case 6:
            case 7:
            case 10:
                return true;
        }
        return false;

    }
}
