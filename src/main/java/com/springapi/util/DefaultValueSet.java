/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.springapi.util;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

/**
 *
 * @author shahb
 */
public class DefaultValueSet {
    public static void fillNullObjects(Object object) {
        java.lang.reflect.Field[] fields = object.getClass().getDeclaredFields();
        for (java.lang.reflect.Field field : fields) {
            try {
                field.setAccessible(true);
                if (field.get(object) != null) {
                    continue;
                } else if (field.getType().equals(Integer.class)) {
                    field.set(object, 0);
                } else if (field.getType().equals(String.class)) {
                    field.set(object, "");
                } else if (field.getType().equals(Boolean.class)) {
                    field.set(object, false);
                } else if (field.getType().equals(Character.class)) {
                    field.set(object, '\u0000');
                } else if (field.getType().equals(Byte.class)) {
                    field.set(object, (byte) 0);
                } else if (field.getType().equals(Float.class)) {
                    field.set(object, 0.0f);
                } else if (field.getType().equals(Double.class)) {
                    field.set(object, 0.0d);
                } else if (field.getType().equals(Short.class)) {
                    field.set(object, (short) 0);
                } else if (field.getType().equals(Long.class)) {
                    field.set(object, 0L);
                } else if (field.getType().getDeclaredFields().length > 0) {
                    for (Constructor<?> constructor : field.getClass().getConstructors()) {
                        if (constructor.getParameterTypes().length == 0) {
                            field.set(object, constructor.newInstance());
                            fillNullObjects(field.get(object));
                        }
                    }
                }
            } catch (IllegalAccessException | InstantiationException | InvocationTargetException e) {
                e.printStackTrace();
            }
        }
    }

}
