/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.springapi.util;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;

/**
 *
 * @author shahb
 */
public class DBConnection {

    @Autowired
    private Environment env;

    public Connection dbConnection() {
        String path = env.getProperty("spring.datasource.url");
        String userName = env.getProperty("spring.datasource.username");
        String password = env.getProperty("spring.datasource.password");
        Connection c = null;

        try {
            Class.forName("com.mysql.jdbc.Driver");

            c = DriverManager.getConnection(path, userName, password);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return c;
    }

}
