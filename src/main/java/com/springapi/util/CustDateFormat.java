/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.springapi.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 *
 * @author shahb
 */
public class CustDateFormat {

    public static final String SQL_DATE_PATTEN = "yyyy-MM-dd";
    public static final String SQL_DATETIME_PATTEN = "yyyy-MM-dd hh:mm:ss";
    public static final String IND_DATE_PATTEN = "dd/MM/yyyy";
    public static final String FILE_DATE_PATTEN = "dd_MM_yyyy";
    public static final String IND_DATETIME_PATTEN = "dd/MM/yyyy hh:mm:ss";
    public static final String TODAY_TIME_PATTEN = "HH:mm:ss";
    public static final SimpleDateFormat SQL_DATE_FORMAT = new SimpleDateFormat(SQL_DATE_PATTEN);
    public static final SimpleDateFormat SQL_DATETIME_FORMAT = new SimpleDateFormat(SQL_DATETIME_PATTEN);
    public static final SimpleDateFormat IND_DATE_FORMAT = new SimpleDateFormat(IND_DATE_PATTEN);
    public static final SimpleDateFormat FILE_DATE_FORMAT = new SimpleDateFormat(FILE_DATE_PATTEN);
    public static final SimpleDateFormat IND_DATETIME_FORMAT = new SimpleDateFormat(IND_DATETIME_PATTEN);
    public static final SimpleDateFormat TODAY_TIME_FORMAT = new SimpleDateFormat(TODAY_TIME_PATTEN);

    public static String todaySQLString() {
        return SQL_DATE_FORMAT.format(new Date());
    }

    public static String todayTime() {
        return TODAY_TIME_FORMAT.format(new Date());
    }

    public static String dateToSQLString(Date date) {
        return SQL_DATE_FORMAT.format(date);
    }

    public static String todaySQLDateTimeString() {
        return SQL_DATETIME_FORMAT.format(new Date());
    }

    public static String todayFileDateTimeString() {
        return FILE_DATE_FORMAT.format(new Date());
    }

    public static String dateDateTimeSQLToString(Date date) {
        return SQL_DATETIME_FORMAT.format(date);
    }

    public static String todayINDString() {
        return IND_DATE_FORMAT.format(new Date());
    }

    public static String dateToINDString(Date date) {
        return IND_DATE_FORMAT.format(date);
    }

    public static String todayDateTimeINDString() {
        return IND_DATETIME_FORMAT.format(new Date());
    }

    public static String dateDateTimeINDToString(Date date) {
        return IND_DATETIME_FORMAT.format(date);
    }

    public static Date SQLTypeStringToDate(String date) throws ParseException {
        return SQL_DATE_FORMAT.parse(date);

    }

    public static Date SQLTypeStringToDateTime(String date) throws ParseException {
        return SQL_DATETIME_FORMAT.parse(date);
    }

    public static Date INDTypeStringToDate(String date) throws ParseException {
        return IND_DATE_FORMAT.parse(date);
    }

    public static Date INDTypeStringToDateTime(String date) throws ParseException {
        return IND_DATETIME_FORMAT.parse(date);
    }

    public static Date addDayInDate(Date date, int day) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.add(Calendar.DAY_OF_YEAR, day);
        return calendar.getTime();
    }

    public static Date previousDateFromDate(Date date) {
        return addDayInDate(date, -1);
    }

    public static Date nextDateFromDate(Date date) {
        return addDayInDate(date, +1);
    }

    public static String previousDateStringFromStringDate(String dateString, String pattern) throws ParseException {
        return new SimpleDateFormat(pattern).format(addDayInDate(new SimpleDateFormat(pattern).parse(dateString), -1));
    }

    public static String nextDateStringFromStringDate(String dateString, String pattern) throws ParseException {
        return new SimpleDateFormat(pattern).format(addDayInDate(new SimpleDateFormat(pattern).parse(dateString), 1));

    }

    public static String previousDateStringFromDate(Date date, String pattern) throws ParseException {
        return new SimpleDateFormat(pattern).format(addDayInDate(date, -1));
    }

    public static String nextDateStringFromDate(Date date, String pattern) throws ParseException {

        return new SimpleDateFormat(pattern).format(addDayInDate(date, 1));
    }

    public static Date previousDate() {
        return addDayInDate(new Date(), -1);
    }

    public static Date nextDate() {
        return addDayInDate(new Date(), 1);
    }

    public static String previousDateString(String pattern) {
        return new SimpleDateFormat(pattern).format(addDayInDate(new Date(), -1));
    }

    public static String nextDateString(String pattern) {
        return new SimpleDateFormat(pattern).format(addDayInDate(new Date(), 1));
    }

    public static Date addDayDateStringFromDate(Date date, int day) {
        return addDayInDate(date, day);
    }

    public static String addDayDateStringFromStringDate(String dateString, String pattern, int day) throws ParseException {
        return new SimpleDateFormat(pattern).format(addDayInDate(new SimpleDateFormat(pattern).parse(dateString), day));
    }

    public static String addDayDateStringFromDate(Date date, String pattern, int day) throws ParseException {
        return new SimpleDateFormat(pattern).format(addDayInDate(date, day));
    }

    public static Integer weekDay(Date now) {
        return Integer.parseInt(new SimpleDateFormat("u").format(now));
    }

    public static Integer weekNumberOfMonth(Date now) {
        return Integer.parseInt(new SimpleDateFormat("W").format(now));
    }
}
